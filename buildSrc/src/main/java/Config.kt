object Config {
    const val compileSdkVersion = 31
    const val minSdkVersion = 26
    const val targetSdkVersion = 31
}