object Versions {
    const val KTX = "1.7.0"

    const val APP_COMPAT = "1.4.0"
    const val MATERIAL = "1.5.0"
    const val CONSTRAINT_LAYOUT = "2.1.2"
    const val ACTIVITY = "1.4.0"
    const val FRAGMENT = "1.4.0"
    const val RECYCLERVIEW = "1.2.0-alpha06"
    const val SWIPE_REFRESH = "1.1.0"
    const val PAGING = "3.1.0"

    const val LIFECYCLE = "2.4.0"
    const val DATASTORE = "1.0.0"
    const val SPLASH = "1.0.0-alpha01"

    const val MULTIDEX = "1.0.3"
    const val BILLING = "4.0.0"

    const val PLAY_CORE = "1.10.3"
    const val PLAY_CORE_KTX = "1.8.1"

    const val NAVIGATION = "2.3.5"

    const val JAVA_INJECT = "1"

    const val ROOM = "2.4.1"

    const val RETROFIT2 = "2.9.0"
    const val OK_HTTP3 = "4.9.3"

    const val COIL = "1.4.0"
    const val GLIDE = "4.12.0"


    const val COROUTINES = "1.6.0"
    const val COROUTINE_ANDROID = "1.3.9"

    const val HILT = "2.40.5"
    const val HILT_VIEWMODEL = "1.0.0-alpha03"

    const val FIREBASE_AD = "20.5.0"
    const val FIREBASE_BOM = "29.0.4"

    const val KAKAO = "2.5.2"
}