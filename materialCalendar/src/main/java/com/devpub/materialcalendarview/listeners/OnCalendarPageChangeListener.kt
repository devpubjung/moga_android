package com.devpub.materialcalendarview.listeners

/**
 * This interface is used to handle calendar changed page (month)
 *
 * Created by Applandeo Team.
 */

interface OnCalendarPageChangeListener {
    fun onChange()
}
