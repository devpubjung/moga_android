package com.devpub.materialcalendarview.listeners

import com.devpub.materialcalendarview.EventDay

/**
 * This interface is used to handle clicks on calendar cells
 *
 * Created by Applandeo Team.
 */

interface OnDayClickListener {
    fun onDayClick(eventDay: EventDay)
}
