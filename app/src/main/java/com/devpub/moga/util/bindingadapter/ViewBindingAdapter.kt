package com.devpub.moga.util.bindingadapter

import androidx.databinding.BindingAdapter
import android.widget.*
import android.animation.ValueAnimator
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.paging.PagingData
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.devpub.common.model.ListItem
import com.devpub.moga.R
import com.devpub.moga.domain.model.KosisItem
import com.devpub.moga.domain.model.BudgetReport
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.model.state.UiState
import com.devpub.moga.ui.list.adapter.PageListAdapter
import com.devpub.moga.ui.widget.AdListView
import com.devpub.moga.ui.widget.HomeHeaderView
import com.devpub.moga.ui.widget.PagingListView
import com.devpub.moga.util.MoneyUtil.commaWon


@BindingAdapter(value = ["moneyText", "animate"], requireAll = false)
fun TextView.setMoneyText(amount: Long?, animate: Boolean?) {
    if (animate != true) {
        text = (amount ?: 0).commaWon()
    } else {
        val animator = ValueAnimator.ofInt(0, amount?.toInt() ?: 0)
        animator.duration = 500
        animator.addUpdateListener { animation ->
            text = (animation.animatedValue as Int).commaWon()
        }
        animator.start()
    }
}

@BindingAdapter("data_weekly")
fun HomeHeaderView.setData(budgetReport: BudgetReport?) {
    if (budgetReport != null) {
        setReportData(budgetReport)
    }
}

@BindingAdapter("adapter", "list_pagingItems")
fun PagingListView.bindPagingListItems(adapter: PageListAdapter, uiState: UiState) {
    setAdapter(adapter)
    if (uiState is UiState.Success<*>) {
        findViewTreeLifecycleOwner()?.let {
            adapter.submitData(
                it.lifecycle,
                uiState.data as PagingData<ListItem>
            )
        }
    }
}

@BindingAdapter("list_items")
fun AdListView.bindListItems(uiState: UiState) {
    if (uiState is UiState.Success<*>) {
        setListItem(uiState.data as List<ListItem>)
    }
}

@BindingAdapter("my_expend")
fun setMyExpendForKosisItem(textView: TextView, item: KosisItem) {
    val context = textView.context
    val icon: Drawable
    val textColor: Int
    if (item.myExpend > item.data) {
        icon = ContextCompat.getDrawable(context, R.drawable.ic_arrow_up)!!
        textColor = ContextCompat.getColor(context, R.color.expendLightColor)
    } else {
        icon = ContextCompat.getDrawable(context, R.drawable.ic_arrow_down)!!
        textColor = ContextCompat.getColor(context, R.color.incomeLightColor)
    }
    val h = icon.intrinsicHeight
    val w = icon.intrinsicWidth
    icon.setBounds(0, 0, w, h)
    textView.setCompoundDrawables(null, null, icon, null)
    textView.setTextColor(textColor)
    textView.text = item.myExpend.commaWon()
}

@BindingAdapter(value = ["profile_users", "profile_index"], requireAll = true)
fun ImageView.setUserProfiles(users: List<User>, index: Int) {
    if (users.size <= index) {
        return
    }
    if (users[index].thumbNailImgUrl.isBlank()) {
        Glide.with(context)
            .load(users[index].defaultProfile)
            .circleCrop()
            .into(this)
    } else {
        val options = RequestOptions()
            .error(users[index].defaultProfile)
        Glide.with(context)
            .load(users[index].thumbNailImgUrl)
            .placeholder(R.drawable.ic_groups)
            .circleCrop()
            .apply(options)
            .into(this)
    }
}

@BindingAdapter("budget_user_text")
fun TextView.setUsersText(users: List<User>) {
    text = "+${users.size - 3}"
    isVisible = users.size > 3
}

