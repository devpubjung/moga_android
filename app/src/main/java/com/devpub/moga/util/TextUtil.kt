package com.devpub.moga.util

import android.content.Context
import android.util.TypedValue

object TextUtil {
    fun dpToPx(context: Context, dp: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics)
    }

    fun isEmpty(cs: CharSequence?): Boolean {
        return cs.isNullOrEmpty()
    }
}