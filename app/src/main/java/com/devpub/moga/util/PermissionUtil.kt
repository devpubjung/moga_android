package com.devpub.moga.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.ui.widget.NotiPopupDialog

object PermissionUtil {

    fun isSmsPermissionAllowed(context: Context): Boolean {
        val permissionCheck =
            ContextCompat.checkSelfPermission(context, Manifest.permission.RECEIVE_SMS)
        return permissionCheck != PackageManager.PERMISSION_DENIED
    }

    fun isNotiPermissionAllowed(context: Context): Boolean {
        val notiListenerSet = NotificationManagerCompat.getEnabledListenerPackages(context)
        val myPackageName = context.packageName
        for (packageName in notiListenerSet) {
            if (packageName == null) {
                continue
            }
            if (packageName == myPackageName) {
                return true
            }
        }
        return false
    }

    fun requireSmsPermission(activity: Activity, requestCode : Int) {
//        if (!isSmsPermissionAllowed(activity)) {
//            SmsPopupDialog(activity).setOnClickListener {
//                val permissions = arrayOf(Manifest.permission.RECEIVE_SMS)
//                ActivityCompat.requestPermissions(activity,
//                    permissions,
//                    requestCode)
//            }.show()
//        }
    }

    fun requireNotiPermission(context: Context, appPreference: AppPreference) {
        NotiPopupDialog(context).setOnClickListener {
            appPreference.setNotiAutoInput(true)
            context.startActivity(Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))
        }.show()
    }
}