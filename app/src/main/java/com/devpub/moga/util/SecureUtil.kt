package com.devpub.moga.util

import java.util.*

object SecureUtil {

    fun Long.toByteArray() = toString().toByteArray()

    fun encode(byteArray: ByteArray): String = Base64.getEncoder().encodeToString(byteArray)
}