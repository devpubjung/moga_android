package com.devpub.moga.util

import android.app.Activity
import android.widget.EditText
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager


object ActivityUtil {
    fun addFragment(
        manager: FragmentManager,
        containerId: Int,
        fragment: Fragment,
        hasBackStack: Boolean
    ) {
        if (hasBackStack) {
            manager.beginTransaction()
                .add(containerId, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commitAllowingStateLoss()
        } else {
            manager.beginTransaction()
                .add(containerId, fragment, fragment.javaClass.simpleName)
                .commitAllowingStateLoss()
        }
    }

    fun replaceFragment(
        manager: FragmentManager,
        containerId: Int,
        fragment: Fragment,
        hasBackStack: Boolean
    ) {
        if (hasBackStack) {
            manager.beginTransaction()
                .replace(containerId, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commitAllowingStateLoss()
        } else {
            manager.beginTransaction()
                .replace(containerId, fragment, fragment.javaClass.simpleName)
                .commitAllowingStateLoss()
        }
    }

    @Nullable
    fun <T : Fragment?> findFragment(manager: FragmentManager, target: Class<T>): T? {
        return manager.findFragmentByTag(target.simpleName) as? T
    }

    fun <T : Fragment?> hasFragment(manager: FragmentManager, target: Class<T>): Boolean {
        return findFragment(manager, target) != null
    }

    fun isFocusOnEditText(activity: Activity): Boolean {
        return activity.currentFocus is EditText
    }

    /**
     * https://stackoverflow.com/questions/6186433/clear-back-stack-using-fragments
     */
    fun clearBackStack(activity: FragmentActivity) {
        val fm: FragmentManager = activity.supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
    }
}