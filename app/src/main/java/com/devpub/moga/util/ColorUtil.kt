package com.devpub.moga.util

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import java.util.*
import kotlin.math.min

object ColorUtil {

    const val COLOR_NONE = 0x00112233
    const val COLOR_SKIP = 0x00112234
    private const val DARKEN_SATURATION = 1.2f
    private const val DARKEN_INTENSITY = 0.93f
    private const val LIGHT_SATURATION = 0.4f
    private const val LIGHT_INTENSITY = 1.1f

    val LIBERTY_COLORS = intArrayOf(
        Color.rgb(207, 248, 246), Color.rgb(148, 212, 212), Color.rgb(136, 180, 187),
        Color.rgb(118, 174, 175), Color.rgb(42, 109, 130)
    )
    val JOYFUL_COLORS = intArrayOf(
        Color.rgb(217, 80, 138), Color.rgb(254, 149, 7), Color.rgb(254, 247, 120),
        Color.rgb(106, 167, 134), Color.rgb(53, 194, 209)
    )
    val PASTEL_COLORS = intArrayOf(
        Color.rgb(64, 89, 128), Color.rgb(149, 165, 124), Color.rgb(217, 184, 162),
        Color.rgb(191, 134, 134), Color.rgb(179, 48, 80)
    )
    val COLORFUL_COLORS = intArrayOf(
        Color.rgb(193, 37, 82), Color.rgb(255, 102, 0), Color.rgb(245, 199, 0),
        Color.rgb(106, 150, 31), Color.rgb(179, 100, 53)
    )
    val VORDIPLOM_COLORS = intArrayOf(
        Color.rgb(255, 245, 102), Color.rgb(192, 255, 140),  Color.rgb(255, 208, 140),
        Color.rgb(140, 234, 255), Color.rgb(255, 140, 157)
    )
    val MATERIAL_COLORS = intArrayOf(
        rgb("#2ecc71"), rgb("#f1c40f"), rgb("#e74c3c"), rgb("#3498db")
    )

    fun rgb(hex: String): Int {
        val color = hex.replace("#", "").toLong(16).toInt()
        val r = color shr 16 and 0xFF
        val g = color shr 8 and 0xFF
        val b = color shr 0 and 0xFF
        return Color.rgb(r, g, b)
    }

    fun getHoloBlue(): Int {
        return Color.rgb(51, 181, 229)
    }

    fun colorWithAlpha(color: Int, alpha: Int): Int {
        return color and 0xffffff or (alpha and 0xff shl 24)
    }

    fun createColors(context: Context, colors: IntArray): List<Int> {
        val result: MutableList<Int> = ArrayList()
        for (i in colors) {
            result.add(ContextCompat.getColor(context, i))
        }
        return result
    }

    fun createColors(colors: IntArray): List<Int> {
        val result: MutableList<Int> = ArrayList()
        for (i in colors) {
            result.add(i)
        }
        return result
    }

    fun darkenColor(color: Int): Int {
        val hsv = FloatArray(3)
        val alpha = Color.alpha(color)
        Color.colorToHSV(color, hsv)
        hsv[1] = min(hsv[1] * DARKEN_SATURATION, 1.0f)
        hsv[2] = hsv[2] * DARKEN_INTENSITY
        val tempColor = Color.HSVToColor(hsv)
        return Color.argb(alpha,
            Color.red(tempColor),
            Color.green(tempColor),
            Color.blue(tempColor))
    }

    fun lightColor(color: Int): Int {
        val hsv = FloatArray(3)
        val alpha = Color.alpha(color)
        Color.colorToHSV(color, hsv)
        hsv[1] = min(hsv[1] * LIGHT_SATURATION, 1.0f)
        hsv[2] = hsv[2] * LIGHT_INTENSITY
        val tempColor = Color.HSVToColor(hsv)
        return Color.argb(alpha,
            Color.red(tempColor),
            Color.green(tempColor),
            Color.blue(tempColor))
    }

    fun isDarkColor(color: Int): Boolean {
        val darkness =
            1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255
        return darkness >= 0.4
    }

    fun generatorColors(): List<Int> {
        val result: MutableList<Int> = ArrayList()
        for (c in VORDIPLOM_COLORS) result.add(c)
        for (c in JOYFUL_COLORS) result.add(c)
        for (c in COLORFUL_COLORS) result.add(c)
        for (c in LIBERTY_COLORS) result.add(c)
        for (c in PASTEL_COLORS) result.add(c)
        result.add(getHoloBlue())
        return result
    }
}