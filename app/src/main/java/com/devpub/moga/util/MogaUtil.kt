package com.devpub.moga.util

import com.devpub.moga.domain.model.Target

object MogaUtil {
    fun isSameTarget(target1: Target, target2: Target?): Boolean {
        if (target2 == null) {
            return false
        }

        if (target1 == target2) {
            return true
        }

        if (target1 != Target.INCOME) {
            return target2 != Target.INCOME
        }

        if (target2 != Target.INCOME) {
            return target1 != Target.INCOME
        }

        return false
    }
}