package com.devpub.moga.util

import android.content.Context
import android.util.TypedValue

object ViewUtil {

    fun dp2px(context: Context, dp: Int) =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), context.resources.displayMetrics).toInt()

}