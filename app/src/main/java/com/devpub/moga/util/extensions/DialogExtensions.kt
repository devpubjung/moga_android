package com.devpub.moga.util.extensions

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.devpub.common.util.ViewUtil.dpToPx
import com.devpub.moga.ui.model.SimpleTextItem
import com.devpub.moga.ui.widget.CommonAlertDialog
import com.devpub.moga.ui.widget.SelectItemBottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

fun Fragment.showAlertDialog(
    title: String? = null,
    content: String? = null,
    desc: String? = null,
    negative: String? = null,
    positive: String? = null,
    negativeClickListener: DialogInterface.OnClickListener? = null,
    positiveClickListener: DialogInterface.OnClickListener? = null,
) {
    context?.showAlertDialog(title,
        content,
        desc,
        negative,
        positive,
        negativeClickListener,
        positiveClickListener)
}

fun Context.showAlertDialog(
    title: String? = null,
    content: String? = null,
    desc: String? = null,
    negative: String? = null,
    positive: String? = null,
    negativeClickListener: DialogInterface.OnClickListener? = null,
    positiveClickListener: DialogInterface.OnClickListener? = null,
) {
    CommonAlertDialog.Builder(this)
        .setTitle(title)
        .setContent(content)
        .setDescription(desc)
        .setNegative(negative)
        .setPositive(positive)
        .setNegativeListener(negativeClickListener)
        .setPositiveListener(positiveClickListener)
        .build()
        .show()
}

fun BottomSheetDialogFragment.bottomSheetBehavior(block: BottomSheetBehavior<View>.() -> Unit) {
    dialog?.let {
        val bottomSheet =
            it.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
        BottomSheetBehavior.from(bottomSheet).apply {
            block(this)
        }
    }
}

fun BottomSheetDialogFragment.peekHeight(maxDp: Float) {
    bottomSheetBehavior {
        context?.let {

            peekHeight = it.dpToPx(maxDp)
        }
    }
}

fun BottomSheetDialogFragment.stateExpanded() {
    bottomSheetBehavior {
        state = BottomSheetBehavior.STATE_EXPANDED
        skipCollapsed = true
    }
}

fun BottomSheetDialogFragment.dragFixed() {
    bottomSheetBehavior {
        state = BottomSheetBehavior.STATE_EXPANDED
        skipCollapsed = true
        addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    state = BottomSheetBehavior.STATE_EXPANDED
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }
}

fun AppCompatActivity.setBottomSheetResultListener(
    requestKey: String,
    listener: ((requestKey: String, bundle: Bundle) -> Unit),
) {
    supportFragmentManager.setFragmentResultListener(requestKey, this, listener)
}

inline fun AppCompatActivity.showSelectSimpleItemBottomSheet(
    title: String? = "선택해 주세요.",
    list: List<SimpleTextItem>,
    crossinline listener: (selectedItem: SimpleTextItem) -> Unit,
) {
    SelectItemBottomSheetDialog.newInstance(title!!, list)
        .show(supportFragmentManager, SelectItemBottomSheetDialog.TAG)

    setBottomSheetResultListener(SelectItemBottomSheetDialog.TAG) { _, bundle ->
        listener(bundle.getSerializable(SelectItemBottomSheetDialog.itemKey) as SimpleTextItem)
    }
}