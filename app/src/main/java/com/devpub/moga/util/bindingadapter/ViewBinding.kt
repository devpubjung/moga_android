package com.devpub.moga.util.bindingadapter

import android.animation.ValueAnimator
import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.devpub.moga.R
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.model.User
import com.devpub.moga.ui.model.CheckTextItem
import com.devpub.moga.ui.model.SimpleTextItem
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MoneyUtil.commaWon
import com.devpub.moga.util.extensions.clicks
import com.devpub.moga.util.extensions.throttleFirst
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.text.DateFormatSymbols
import java.util.*

@BindingAdapter("visible")
fun setVisible(view: View, visible: Boolean) {
    if (visible) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("bold")
fun setBold(textView: TextView, bold: Boolean) {
    textView.apply {
        setTypeface(null, if (bold) {
            Typeface.BOLD
        } else {
            Typeface.NORMAL
        })
    }
}

@BindingAdapter("circle_user_image")
fun bindImage(view: ImageView, user: User?) {
    if (user == null) {
        return
    }
    if (user.thumbNailImgUrl.isBlank()) {
        Glide.with(view.context)
            .load(user.defaultProfile)
            .circleCrop()
            .into(view)
    } else {
        val options = RequestOptions()
            .error(user.defaultProfile)
        Glide.with(view.context)
            .load(user.thumbNailImgUrl)
            .placeholder(R.drawable.ic_groups)
            .circleCrop()
            .apply(options)
            .into(view)
    }
}

@BindingAdapter("profile")
fun setProfileImage(view: ImageView, user: User?) {
    if (user == null) {
        return
    }
    if (user.thumbNailImgUrl.isBlank()) {
        Glide.with(view.context)
            .load(user.defaultProfile)
            .circleCrop()
            .into(view)
    } else {
        val options = RequestOptions()
            .error(user.defaultProfile)
        Glide.with(view.context)
            .load(user.profileImgUrl)
            .placeholder(R.drawable.ic_groups)
            .circleCrop()
            .apply(options)
            .into(view)
    }
}

@BindingAdapter("birth_text")
fun setBirthText(textView: TextView, birthYear: Int) {
    textView.text = (if (birthYear > 0) birthYear else 1987).toString()
}

@BindingAdapter("money_text")
fun setMoneyText(textView: TextView, amount: Long = 0) {
    textView.text = amount.commaWon()
}

@BindingAdapter("money_color")
fun setMoneyColor(textView: TextView, target: Target) {
    val colorRes = when (target) {
        Target.INCOME -> R.color.incomeColor
        Target.EXPEND -> R.color.expendColor
        Target.TRANSFER -> R.color.transferColor
    }

    textView.setTextColor(ContextCompat.getColor(textView.context, colorRes))
}

@BindingAdapter("time")
fun setTimeText(textView: TextView, date: Date? = DateUtil.todayDate) {
    textView.text = DateUtil.getTimeString(date!!)
}

@BindingAdapter("year_month")
fun setYearMonthText(textView: TextView, date: Date? = DateUtil.todayDate) {
    textView.text = DateUtil.yyyy_MM.format(date!!)
}

@BindingAdapter("day")
fun setDayText(textView: TextView, date: Date? = DateUtil.todayDate) {
    textView.text = DateUtil.dd.format(date!!)
}

@BindingAdapter("week")
fun setWeekText(textView: TextView, date: Date? = DateUtil.todayDate) {
    val cal = Calendar.getInstance(Locale.KOREA)
    cal.time = date
    textView.text = DateFormatSymbols(Locale.KOREA).weekdays[cal[Calendar.DAY_OF_WEEK]]
}

@BindingAdapter("comma_text")
fun setCommaWonText(textView: TextView, money: Long) {
    textView.text = money.commaWon()
}

@BindingAdapter(value = ["sum_money_text", "income"], requireAll = false)
fun setSumCommaWonText(textView: TextView, sumMoney: Long, income: Boolean) {
    var signString = ""
    if (sumMoney > 0) {
        signString = "+"
        val color = if (income) {
            ContextCompat.getColor(textView.context, R.color.incomeLightColor)
        } else {
            ContextCompat.getColor(textView.context, R.color.expendLightColor)
        }
        textView.setTextColor(color)
    } else if (sumMoney < 0) {
        val color = if (income) {
            ContextCompat.getColor(textView.context, R.color.expendLightColor)
        } else {
            ContextCompat.getColor(textView.context, R.color.incomeLightColor)
        }
        textView.setTextColor(color)
    }
    startAnimationText(textView, 0, sumMoney.toInt(), signString)
}

private fun startAnimationText(
    textView: TextView,
    preData: Int,
    data: Int,
    signString: String = "",
) {
    val animator = ValueAnimator.ofInt(preData, data)
    animator.duration = 500
    animator.addUpdateListener { animation ->
        textView.text = signString + (animation.animatedValue as Int).commaWon()
    }
    animator.start()
}

@BindingAdapter("target_text")
fun setTargetText(textView: TextView, target: Target) {
    textView.text = when (target) {
        Target.INCOME -> "수입"
        Target.TRANSFER -> "이체"
        else -> "지출"
    }
}

@ExperimentalCoroutinesApi
@FlowPreview
@BindingAdapter("onceClick")
fun onOnceClick(view: View, listener: View.OnClickListener) {
    view.clicks()
        .throttleFirst(2000)
        .onEach { listener.onClick(view) }
        .launchIn(GlobalScope)
}

@BindingAdapter("item_simpleText")
fun TextView.setSimpleText(item: SimpleTextItem) {
    text = item.text ?: "${item.value}${item.unit}"
}

@BindingAdapter("item_chekText")
fun TextView.setCheckText(item: CheckTextItem) {
    text = item.text ?: "${item.value}${item.unit}"
}
