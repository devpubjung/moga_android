package com.devpub.moga.util.extensions

import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

inline fun BaseViewModel.launchLoading(
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    crossinline block: suspend CoroutineScope.() -> Unit
): Job {
    return viewModelScope.launch(context, start) {
        setLoading(true)
        block()
        setLoading(false)
    }
}

suspend inline fun <T> BaseViewModel.loadingWithContext(
    context: CoroutineContext,
    noinline block: suspend CoroutineScope.() -> T
): T {
    setLoading(true)
    val result = withContext(context, block)
    setLoading(false)
    return result
}