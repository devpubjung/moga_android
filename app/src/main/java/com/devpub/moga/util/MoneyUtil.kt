package com.devpub.moga.util

import com.devpub.moga.DEFAULT_MONTHLY_BUDGET
import com.devpub.moga.MONEY_SLICE
import java.lang.Exception
import java.text.NumberFormat
import java.util.*

object MoneyUtil {

    private const val UNIT = "원"
    private const val MAX_LENGTH = 12

    fun String.isMaxLength(): Boolean {
        return this.length >= MAX_LENGTH
    }

    fun Long.comma(): String {
        return NumberFormat.getInstance(Locale.KOREA).format(this)
    }

    fun Long.commaWon(): String {
        return this.comma() + UNIT
    }

    fun Int.comma(): String {
        return NumberFormat.getInstance(Locale.KOREA).format(this)
    }

    fun Int.commaWon(): String {
        return this.comma() + UNIT
    }

    fun Int.commaManWon(): String {
        return "${this.comma()}만$UNIT"
    }

    fun String.withoutCommaWon(): Long {
        return try {
            this.replace(",", "").replace(UNIT, "").toLong()
        } catch (e: Exception) {
            0
        }
    }

    fun getWeeklyBudget(budget: Int = DEFAULT_MONTHLY_BUDGET, calendar: Calendar = DateUtil.today): Int {
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
        return budget / calendar.get(Calendar.WEEK_OF_MONTH)
    }

    fun convertFullMoney(money: Int): Long = (money * MONEY_SLICE).toLong()
}