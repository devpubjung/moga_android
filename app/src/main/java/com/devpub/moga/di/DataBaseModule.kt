package com.devpub.moga.di

import com.devpub.moga.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataBaseModule {
    @Provides
    @Singleton
    fun provideUserDao(appDatabase: AppDatabase) = appDatabase.userDao()

    @Provides
    @Singleton
    fun provideMoimDao(appDatabase: AppDatabase) = appDatabase.moimDao()

    @Provides
    @Singleton
    fun provideAssetsDao(appDatabase: AppDatabase) = appDatabase.assetsDao()

    @Provides
    @Singleton
    fun provideAccountItemDao(appDatabase: AppDatabase) = appDatabase.accountItemDao()

    @Provides
    @Singleton
    fun provideReportDao(appDatabase: AppDatabase) = appDatabase.reportDao()

    @Provides
    @Singleton
    fun provideInsightDao(appDatabase: AppDatabase) = appDatabase.insightDao()
}