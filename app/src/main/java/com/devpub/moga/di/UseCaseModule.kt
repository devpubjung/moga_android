package com.devpub.moga.di

import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.domain.repository.*
import com.devpub.moga.domain.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {

    @Provides
    @ViewModelScoped
    fun providesStartUseCase(
        appPreference: AppPreference,
        userRepository: UserRepository,
        moimRepository: MoimRepository,
    ) = StartUseCase(appPreference, userRepository, moimRepository)

    @Provides
    @ViewModelScoped
    fun providesUserInfoSettingUseCase(
        userRepository: UserRepository,
        moimRepository: MoimRepository,
        budgetRepository: BudgetRepository,
        accountItemRepository: AccountItemRepository,
    ) = UserInfoSettingUseCase(
        userRepository,
        moimRepository,
        budgetRepository,
        accountItemRepository)

    @Provides
    @ViewModelScoped
    fun providesHomeUseCase(
        reportRepository: ReportRepository,
    ) = HomeUseCase(reportRepository)

    @Provides
    @ViewModelScoped
    fun providesNoteUseCase(
        commonRepository: CommonRepository,
        accountItemRepository: AccountItemRepository,
    ) = NoteUseCase(
        commonRepository,
        accountItemRepository)

    @Provides
    @ViewModelScoped
    fun providesReportUseCase(
        reportRepository: ReportRepository,
    ) = ReportUseCase(
        reportRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesCompareMonthlyUseCase(
        reportRepository: ReportRepository,
    ) = CompareMonthlyUseCase(
        reportRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesKosisUseCase(
        kosisRepository: KosisRepository,
    ) = KosisUseCase(
        kosisRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesSettingUseCase(
        commonRepository: CommonRepository,
        userRepository: UserRepository,
        moimRepository: MoimRepository,
    ) = EtcUseCase(
        commonRepository,
        userRepository,
        moimRepository
    )

    @Provides
    @ViewModelScoped
    fun providesBudgetUseCase(
        budgetRepository: BudgetRepository,
    ) = BudgetUseCase(
        budgetRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesCategoryUseCase(
        categoryRepository: CategoryRepository,
    ) = CategoryUseCase(
        categoryRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesSuggestUseCase(
        commonRepository: CommonRepository,
    ) = SuggestUseCase(
        commonRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesInviteUseCase(
        moimRepository: MoimRepository,
    ) = InviteUseCase(
        moimRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesUserUseCase(
        moimRepository: MoimRepository,
    ) = UserUseCase(
        moimRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesAssetsUseCase(
        assetsRepository: AssetsRepository,
    ) = AssetsUseCase(
        assetsRepository,
    )

    @Provides
    @ViewModelScoped
    fun providesInputUseCase(
        accountItemRepository: AccountItemRepository,
    ) = InputUseCase(
        accountItemRepository
    )
}