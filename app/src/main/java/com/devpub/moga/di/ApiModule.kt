package com.devpub.moga.di

import com.devpub.moga.data.api.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    @Singleton
    fun providesCommonApi(@DevPupRetrofit retrofit: Retrofit) : CommonApi{
        return retrofit.create(CommonApi::class.java)
    }

    @Provides
    @Singleton
    fun providesUserApi(@DevPupRetrofit retrofit: Retrofit) : UserApi{
        return retrofit.create(UserApi::class.java)
    }

    @Provides
    @Singleton
    fun providesMoimApi(@DevPupRetrofit retrofit: Retrofit) : MoimApi {
        return retrofit.create(MoimApi::class.java)
    }

    @Provides
    @Singleton
    fun providesCategoryApi(@DevPupRetrofit retrofit: Retrofit) : CategoryApi {
        return retrofit.create(CategoryApi::class.java)
    }

    @Provides
    @Singleton
    fun providesBudgetApi(@DevPupRetrofit retrofit: Retrofit) : BudgetApi {
        return retrofit.create(BudgetApi::class.java)
    }

    @Provides
    @Singleton
    fun providesAccountApi(@DevPupRetrofit retrofit: Retrofit) : AccountItemApi{
        return retrofit.create(AccountItemApi::class.java)
    }

    @Provides
    @Singleton
    fun providesKosisApi(@KosisRetrofit retrofit: Retrofit) : KosisApi {
        return retrofit.create(KosisApi::class.java)
    }

    @Provides
    @Singleton
    fun providesKakaoApi(@KakaoRetrofit retrofit: Retrofit) : KakaoApi {
        return retrofit.create(KakaoApi::class.java)
    }
}