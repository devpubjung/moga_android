package com.devpub.moga.di

import com.devpub.moga.data.database.AppDatabase
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.repository.*
import com.devpub.moga.data.source.local.*
import com.devpub.moga.data.source.remote.*
import com.devpub.moga.domain.repository.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    @ViewModelScoped
    fun providesCommonRepositoryNew(
        appPreference: AppPreference,
        commonService: CommonService,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): CommonRepository = CommonRepositoryImpl(
        appPreference,
        commonService,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesUserRepositoryNew(
        appDatabase: AppDatabase,
        appPreference: AppPreference,
        userLocalDataSource: UserLocalDataSource,
        userService: UserService,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): UserRepository = UserRepositoryImpl(
        appDatabase,
        appPreference,
        userLocalDataSource,
        userService,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesMoimRepositoryNew(
        appPreference: AppPreference,
        moimService: MoimService,
        moimLocalDataSource: MoimLocalDataSource,
        accountItemLocalDataSource: AccountItemLocalDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): MoimRepository = MoimRepositoryImpl(
        appPreference,
        moimService,
        moimLocalDataSource,
        accountItemLocalDataSource,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesBudgetRepositoryNew(
        appPreference: AppPreference,
        moimLocalDataSource: MoimLocalDataSource,
        budgetService: BudgetService,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): BudgetRepository = BudgetRepositoryImpl(
        appPreference,
        moimLocalDataSource,
        budgetService,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesAccountItemRepositoryNew(
        appPreference: AppPreference,
        accountItemService: AccountItemService,
        accountItemLocalDataSource: AccountItemLocalDataSource,
        moimLocalDataSource: MoimLocalDataSource,
        userLocalDataSource: UserLocalDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): AccountItemRepository = AccountItemRepositoryImpl(
        appPreference,
        accountItemService,
        accountItemLocalDataSource,
        moimLocalDataSource,
        userLocalDataSource,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesReportRepositoryNew(
        appPreference: AppPreference,
        moimLocalDataSource: MoimLocalDataSource,
        accountItemLocalDataSource: AccountItemLocalDataSource,
        reportLocalDataSource: ReportLocalDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): ReportRepository = ReportRepositoryImpl(
        appPreference,
        moimLocalDataSource,
        accountItemLocalDataSource,
        reportLocalDataSource,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesKosisRepositoryNew(
        appPreference: AppPreference,
        kosisService: KosisService,
        reportLocalDataSource: ReportLocalDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): KosisRepository = KosisRepositoryImpl(
        appPreference,
        kosisService,
        reportLocalDataSource,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesCategoryRepositoryNew(
        appPreference: AppPreference,
        moimLocalDataSource: MoimLocalDataSource,
        categoryService: CategoryService,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): CategoryRepository = CategoryRepositoryImpl(
        appPreference,
        moimLocalDataSource,
        categoryService,
        ioDispatcher)

    @Provides
    @ViewModelScoped
    fun providesAssetsRepositoryNew(
        appPreference: AppPreference,
        assetsLocalDataSource: AssetsLocalDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): AssetsRepository = AssetsRepositoryImpl(
        appPreference,
        assetsLocalDataSource,
        ioDispatcher)

    @ViewModelScoped
    @Provides
    fun providesKakaoRepository(
        kakaoRepository: KakaoRepositoryImpl,
    ): KakaoRepository {
        return kakaoRepository
    }
}