package com.devpub.moga.di

import android.content.Context
import androidx.room.Room
import com.devpub.moga.data.database.AppDatabase
import com.devpub.moga.data.model.entity.ACCOUNT_ITEM_MIGRATION_1_2
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.domain.repository.*
import com.devpub.moga.domain.usecase.MessageUseCase
import com.devpub.moga.helper.KakaoHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object SingletonModule {
    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "moga.db"
        ).addMigrations(ACCOUNT_ITEM_MIGRATION_1_2)
            .build()
    }

    @Provides
    @Singleton
    fun provideAppPreference(@ApplicationContext appContext: Context): AppPreference {
        return AppPreferenceImpl(appContext)
    }

    @Provides
    @Singleton
    fun providesKakaoHelper(): KakaoHelper {
        return KakaoHelper()
    }

    @Provides
    @Singleton
    fun providesMessageUseCase(
        @ApplicationContext context: Context,
        appPreference: AppPreference,
        commonRepository: CommonRepository,
        moimRepository: MoimRepository,
        userRepository: UserRepository,
        assetsRepository: AssetsRepository,
        accountItemRepository: AccountItemRepository,
        kakaoRepository: KakaoRepository,
    ) = MessageUseCase(
        context,
        appPreference,
        commonRepository,
        moimRepository,
        userRepository,
        assetsRepository,
        accountItemRepository,
        kakaoRepository)
}