package com.devpub.moga.di

import com.devpub.moga.API
import com.devpub.moga.BuildConfig
import com.devpub.moga.DEBUG_URL
import com.devpub.moga.VERSION_1
import com.devpub.moga.data.network.HeaderInterceptor
import com.devpub.moga.data.network.KakaoHeaderInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object RetrofitModule {
    private const val CONNECT_TIMEOUT = 2L
    private const val READ_TIMEOUT = 5L
    private const val WRITE_TIMEOUT = 5L
    private const val BASE_URL = "https://picsum.photos/"

    @Provides
    @Singleton
    fun providesOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder().apply {
            connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            })
        }
    }

    @DevPupRetrofit
    @Provides
    @Singleton
    fun providesDevPupRetrofit(client: OkHttpClient.Builder): Retrofit {
        client.addInterceptor(HeaderInterceptor())
        return Retrofit.Builder()
            .baseUrl("${if (BuildConfig.DEBUG) DEBUG_URL else BASE_URL}$API$VERSION_1/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    @KakaoRetrofit
    @Provides
    @Singleton
    fun providesKakaoRetrofit(client: OkHttpClient.Builder): Retrofit {
        client.addInterceptor(KakaoHeaderInterceptor())
        return Retrofit.Builder()
            .baseUrl("https://dapi.kakao.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    @KosisRetrofit
    @Provides
    @Singleton
    fun providesKosisRetrofit(client: OkHttpClient.Builder): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://kosis.kr/openapi/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

}