package com.devpub.moga.di

import com.devpub.moga.data.database.dao.*
import com.devpub.moga.data.source.local.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {
    @Provides
    @Singleton
    fun provideUserLocalDataSource(
        userDao: UserDao,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ) = UserLocalDataSource(userDao, ioDispatcher)

    @Provides
    @Singleton
    fun provideMoimLocalDataSource(
        moimDao: MoimDao,
        assetsDao: AssetsDao,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ) = MoimLocalDataSource(moimDao, assetsDao)

    @Provides
    @Singleton
    fun provideAccountItemLocalDataSource(
        accountItemDao: AccountItemDao,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ) = AccountItemLocalDataSource(accountItemDao)

    @Provides
    @Singleton
    fun provideAssetsLocalDataSource(
        assetsDao: AssetsDao,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ) = AssetsLocalDataSource(assetsDao)

    @Provides
    @Singleton
    fun provideReportLocalDataSource(
        reportDao: ReportDao,
        insightDao: InsightDao,
    ) = ReportLocalDataSource(reportDao, insightDao)
}