package com.devpub.moga.di

import android.content.Context
import com.devpub.moga.data.interactor.KakaoInteractor
import com.devpub.moga.helper.KakaoHelper
import com.devpub.moga.manager.BillingManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {

    @Provides
    @ActivityScoped
    fun providesKakaoInteractor(
        @ActivityContext context: Context,
        kakaoHelper: KakaoHelper,
    ) = KakaoInteractor(context, kakaoHelper)


    @Provides
    @ActivityScoped
    fun providesBillingManager(
        @ActivityContext context: Context,
    ) = BillingManager(context)
}