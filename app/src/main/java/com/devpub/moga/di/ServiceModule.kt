package com.devpub.moga.di

import com.devpub.moga.data.api.*
import com.devpub.moga.data.source.remote.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun provideCommonService(commonApi: CommonApi) = CommonService(commonApi)

    @Provides
    @Singleton
    fun provideUserService(userApi: UserApi) = UserService(userApi)

    @Provides
    @Singleton
    fun provideMoimService(moimApi: MoimApi) = MoimService(moimApi)

    @Provides
    @Singleton
    fun provideBudgetService(budgetApi: BudgetApi) = BudgetService(budgetApi)

    @Provides
    @Singleton
    fun provideAccountItemService(accountItemApi: AccountItemApi) =
        AccountItemService(accountItemApi)

    @Provides
    @Singleton
    fun provideKosisService(
        kosisApi: KosisApi,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ) = KosisService(kosisApi, ioDispatcher)

    @Provides
    @Singleton
    fun provideCategoryService(
        categoryApi: CategoryApi,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ) = CategoryService(categoryApi, ioDispatcher)

}