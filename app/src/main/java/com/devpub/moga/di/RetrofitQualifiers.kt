package com.devpub.moga.di

import javax.inject.Qualifier

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class DevPupRetrofit

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class KakaoRetrofit

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class KosisRetrofit