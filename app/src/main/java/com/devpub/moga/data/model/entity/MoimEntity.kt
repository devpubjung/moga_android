package com.devpub.moga.data.model.entity

import androidx.room.*
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.DEFAULT_MONTHLY_BUDGET
import com.devpub.moga.DEFAULT_NUMBER
import com.devpub.moga.data.database.converter.MoimConverters
import com.devpub.moga.data.model.common.TimeEntity
import com.devpub.moga.data.model.mapper.mapToEntity
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.model.Target
import java.io.Serializable

@Entity(tableName = "moim")
@TypeConverters(MoimConverters::class)
class MoimEntity(
    @PrimaryKey
    val id: Long = 1, //userId+No,
    val ownerId: Long = DEFAULT_ID, //userId
) : TimeEntity(), Serializable {
    var userList: MutableList<UserEntity> = mutableListOf()

    var categoryList: MutableList<CategoryEntity> = mutableListOf()

    var budgetList: MutableList<BudgetEntity> = mutableListOf()

    companion object {
        fun createMoim(me: User, index: Int = DEFAULT_NUMBER) =
            MoimEntity("${me.id}$index".toLong(), me.id).apply {
                val totalCategory = createCategory(0, "전체").apply { id = 0 }

                categoryList.add(createCategory(1, "식비/외식", tag = "식비,외식,음식,중식,일식,한식"))
                categoryList.add(createCategory(1, "마트/편의점"))
                categoryList.add(createCategory(2, "유흥/술"))
                categoryList.add(createCategory(2, "담배"))
                categoryList.add(createCategory(3, "패션/미용"))
                categoryList.add(createCategory(4, "주거/관리비"))
                categoryList.add(createCategory(5, "생활용품"))
                categoryList.add(createCategory(6, "건강/의료"))
                categoryList.add(createCategory(7, "대중교통/차량"))
                categoryList.add(createCategory(7, "택시"))
                categoryList.add(createCategory(8, "통신"))
                categoryList.add(createCategory(9, "문화생활"))
                categoryList.add(createCategory(10, "교육"))
                categoryList.add(createCategory(11, "숙박/여행"))
                categoryList.add(createCategory(12, "부모님"))
                categoryList.add(createCategory(12, "경조사/회비"))
                categoryList.add(createCategory(13, "기부"))
                categoryList.add(createCategory(13, "세금/이자"))
                categoryList.add(createCategory(12, "기타"))

                categoryList.add(createCategory(1, "급여/월급", Target.INCOME))
                categoryList.add(createCategory(2, "용돈", Target.INCOME))
                categoryList.add(createCategory(3, "상여/성과금", Target.INCOME))
                categoryList.add(createCategory(4, "투자수익", Target.INCOME))
                categoryList.add(createCategory(5, "더치페이", Target.INCOME))
                categoryList.add(createCategory(6, "기타", Target.INCOME))

                userList.add(me.mapToEntity())
                budgetList.add(createBudget(totalCategory))
            }

        private fun MoimEntity.createCategory(
            kosisId: Long,
            name: String,
            target: Target = Target.EXPEND,
            tag: String = name.replace("/", ","),
        ) = CategoryEntity(id, target).apply {
            this.id = (categoryList.size + 1).toLong()
            this.parentId = id
            this.kosisId = kosisId
            this.name = name
            this.tag = tag
            this.index = categoryList.size + 1
        }

        fun MoimEntity.createBudget(
            category: CategoryEntity,
            budget: Int = DEFAULT_MONTHLY_BUDGET,
        ) = BudgetEntity(id).apply {
            this.category = category
            this.budget = budget
            this.index = budgetList.size
        }
    }
}
