package com.devpub.moga.data.database.dao

import androidx.room.*
import com.devpub.moga.data.model.entity.AssetsEntity

@Dao
interface AssetsDao: BaseDao<AssetsEntity> {

    @Query("SELECT * FROM assets ORDER BY id")
    suspend fun getAssets() : List<AssetsEntity>

//    @Transaction
//    suspend fun createAll(objects: List<AssetsEntity>) = objects.forEach { insert(it)}

}