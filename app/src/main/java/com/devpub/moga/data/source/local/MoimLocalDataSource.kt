package com.devpub.moga.data.source.local

import com.devpub.moga.data.database.dao.AssetsDao
import com.devpub.moga.data.database.dao.MoimDao
import com.devpub.moga.data.model.entity.AssetsEntity
import com.devpub.moga.data.model.entity.MoimEntity
import com.devpub.moga.data.model.mapper.mapToEntity
import com.devpub.moga.data.model.mapper.mapToMoim
import com.devpub.moga.domain.model.Moim
import com.devpub.moga.domain.model.User
import javax.inject.Inject

class MoimLocalDataSource @Inject constructor(
    private val moimDao: MoimDao,
    private val assetsDao: AssetsDao,
) {

    suspend fun saveMoim(moim: Moim) {
        saveMoim(moim.mapToEntity())
    }

    suspend fun updateMoim(moim: Moim) {
        moimDao.update(moim.mapToEntity())
    }

    suspend fun getMoim(moimId: Long): Moim? {
        return moimDao.getMoim(moimId)?.mapToMoim()
    }

    suspend fun createMoim(user: User): Moim {
        return MoimEntity.createMoim(user).also {
            saveMoim(it)
        }.mapToMoim()
    }

    private suspend fun saveMoim(moimEntity: MoimEntity) {
        moimDao.insert(moimEntity)
        assetsDao.insertAll(AssetsEntity.generatorAssetsList(moimEntity.id))
    }
}