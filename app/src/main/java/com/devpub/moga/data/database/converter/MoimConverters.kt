package com.devpub.moga.data.database.converter

import androidx.room.TypeConverter
import com.devpub.moga.data.model.entity.BudgetEntity
import com.devpub.moga.data.model.entity.CategoryEntity
import com.devpub.moga.data.model.entity.UserEntity
import com.google.gson.Gson
import java.util.*

class MoimConverters {

    @TypeConverter
    fun userListToJson(value: List<UserEntity>): String = Gson().toJson(value)

    @TypeConverter
    fun userJsonToList(value: String) = Gson().fromJson(value, Array<UserEntity>::class.java).toList()

    @TypeConverter
    fun categoryListToJson(value: List<CategoryEntity>): String = Gson().toJson(value)

    @TypeConverter
    fun categoryJsonToList(value: String) = Gson().fromJson(value, Array<CategoryEntity>::class.java).toList()

    @TypeConverter
    fun budgetListToJson(value: List<BudgetEntity>): String = Gson().toJson(value)

    @TypeConverter
    fun budgetJsonToList(value: String) = Gson().fromJson(value, Array<BudgetEntity>::class.java).toList()

    @TypeConverter
    fun categoryToJson(value: CategoryEntity?): String? = value?.let { Gson().toJson(value) }

    @TypeConverter
    fun jsonToCategory(value: String?) = value?.let { Gson().fromJson(value, CategoryEntity::class.java) }

    @TypeConverter
    fun userToJson(value: UserEntity?): String? = value?.let { Gson().toJson(value) }

    @TypeConverter
    fun jsonToUSer(value: String?) = value?.let { Gson().fromJson(value, UserEntity::class.java) }

}
