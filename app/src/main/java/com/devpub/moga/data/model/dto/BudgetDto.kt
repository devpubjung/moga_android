package com.devpub.moga.data.model.dto

import java.io.Serializable


data class BudgetRequest(
    val moimId: Long,
    val categoryId: Long,
    val budget: Int,
    val weeklyBudget: Int,
    val maxBudget: Int,
): Serializable

data class BudgetSettingRequest(
    val budgetList: List<BudgetRequest> = listOf(),
    val removeBudgetList: List<BudgetRequest> = listOf(),
): Serializable
