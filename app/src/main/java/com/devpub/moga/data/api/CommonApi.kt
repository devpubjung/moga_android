package com.devpub.moga.data.api

import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.data.model.SuggestRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import retrofit2.http.*

interface CommonApi {

    @GET("ping")
    suspend fun ping(): ApiDataResponse<*>

    @POST("etc/suggest")
    suspend fun sendSuggest(@Body suggestRequest: SuggestRequest): ApiDataResponse<*>

    @POST("etc/parsing/sms")
    suspend fun parseSms(@Body data: SmsRequest): ApiDataResponse<PayMessage>

    @POST("etc/parsing/noti")
    suspend fun paseNoti(@Body data: NotiRequest): ApiDataResponse<PayMessage>

    @GET("etc/store/category")
    suspend fun categoryInfo(@Query("name") name: String): ApiDataResponse<String>
}