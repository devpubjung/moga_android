package com.devpub.moga.data.source.local

import com.devpub.moga.data.database.dao.UserDao
import com.devpub.moga.data.model.mapper.mapToEntity
import com.devpub.moga.data.model.mapper.mapToUser
import com.devpub.moga.domain.model.Provider
import com.devpub.moga.domain.model.User
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserLocalDataSource @Inject constructor(
    private val userDao: UserDao,
    private val ioDispatcher: CoroutineDispatcher,
) {

    suspend fun getUser(id: Long): User? {
        return withContext(ioDispatcher) { userDao.getUser(id)?.mapToUser() }
    }

    suspend fun getUserUid(uid: Long, provider: Provider): User? {
        return withContext(ioDispatcher) { userDao.getUserUid(uid, provider)?.mapToUser() }
    }

    suspend fun saveUser(user: User): User? {
        return withContext(ioDispatcher) {
            userDao.insert(user.mapToEntity())
            getUserUid(user.uid, user.provider)
        }
    }

    suspend fun updateUser(user: User) {
        return withContext(ioDispatcher) {
            userDao.update(user.mapToEntity())
        }
    }
}