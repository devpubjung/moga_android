package com.devpub.moga.data.model.external

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class KosisResponse(
    @SerializedName("ORG_ID")
    val orgId: String, // 기관코드
    @SerializedName("TBL_ID")
    val tableId: String, // 통계표ID
    @SerializedName("TBL_NM")
    val tableNm: String, // 통계표명
    @SerializedName("C1")
    val columnId1: String = "", // 분류값 ID1
    @SerializedName("C2")
    val columnId2: String = "", // 분류값 ID2
    @SerializedName("C3")
    val columnId3: String = "", // 분류값 ID3
    @SerializedName("C1_OBJ_NM")
    val columnObjectName1: String = "", // 분류명1
    @SerializedName("C2_OBJ_NM")
    val columnObjectName2: String = "", // 분류명2
    @SerializedName("C3_OBJ_NM")
    val columnObjectName3: String = "", // 분류명3
    @SerializedName("C1_NM")
    val columnName1: String = "", // 분류값1
    @SerializedName("C2_NM")
    val columnName2: String = "", // 분류값2
    @SerializedName("C3_NM")
    val columnName3: String = "", // 분류값3
    @SerializedName("ITM_ID")
    val itemId: String = "", // 항목 ID
    @SerializedName("ITM_NM")
    val itemName: String = "", // 항목명
    @SerializedName("UNIT_NM")
    val unitName: String = "", // 단위명
    @SerializedName("DT")
    val data: Float, // 수치값
) : Serializable

enum class Income(val code:String): Serializable {
    I99("M1"),
    I199("M2"),
    I299("M3"),
    I399("M4"),
    I499("M5"),
    I599("M6"),
    I699("M7"),
    I700("M8"),
}

enum class Age(val code:String) : Serializable{
    A39("G1"),
    A49("G2"),
    A59("G3"),
    A60("G4"),
}