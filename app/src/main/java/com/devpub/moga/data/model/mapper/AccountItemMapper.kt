package com.devpub.moga.data.model.mapper

import com.devpub.moga.data.model.dto.AccountItemRequest
import com.devpub.moga.data.model.entity.AccountItemEntity
import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.util.DateUtil

fun AccountItemEntity.mapToAccountItem() =
    AccountItem(
        moimId = this.moimId,
        id = this.id,
        target = this.target,
        date = this.date,
        user = this.user?.mapToUser(),
        amount = this.amount,
        assets = this.assets?.mapToAssets(),
        category = this.category?.mapToCategory(),
        content = this.content,
        memo = this.memo,
        expend = this.expend,
        share =this.share,
        report = this.report,
        ownerId = this.ownerId,
        type = this.type,
        assetType = this.assetType
    )

fun AccountItem.mapToEntity() =
    AccountItemEntity(
        moimId = this.moimId,
        id = this.id
    ).also { entity ->
        entity.target = target
        entity.date = date
        entity.user = user?.mapToEntity()
        entity.amount = amount
        entity.assets = assets?.mapToEntity()
        entity.category = category?.mapToEntity()
        entity.content = content
        entity.memo = memo
        entity.expend = expend
        entity.share =share
        entity.report = report
        entity.ownerId = ownerId
        entity.type = type
        entity.assetType = assetType
    }


fun AccountItem.mapToRequest(): AccountItemRequest {
    return AccountItemRequest(moimId,
        id,
        target,
        DateUtil.getServerDateString(date),
        user!!.id,
        amount,
        assets!!.name,
        category!!.id,
        content,
        memo,
        expend,
        share,
        report,
        ownerId,
        type,
        assetType)
}

fun AccountItemEntity.mapToRequest() =
    AccountItemRequest(
        moimId = this.moimId,
        id = this.id,
        target = this.target,
        date = DateUtil.getServerDateString(this.date),
        userId = this.user!!.id,
        amount = this.amount,
        assets = this.assets!!.name,
        categoryId = this.category!!.id,
        content = this.content,
        memo = this.memo,
        expend = this.expend,
        share =this.share,
        report = this.report,
        ownerId = this.ownerId,
        type = this.type,
        assetType = this.assetType
    )