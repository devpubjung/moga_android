package com.devpub.moga.data.model.mapper

import com.devpub.moga.data.model.dto.BudgetRequest
import com.devpub.moga.data.model.entity.BudgetEntity
import com.devpub.moga.domain.model.Budget

fun BudgetEntity.mapToBudget() =
    Budget(
        moimId = this.moimId,
        category = this.category.mapToCategory(),
        categoryId = this.categoryId,
        budget = this.budget,
        weeklyBudget = this.weeklyBudget,
        maxBudget = this.maxBudget,
        color = this.color,
        index = this.index
    )

fun Budget.mapToEntity() =
    BudgetEntity(
        moimId = this.moimId,
    ).also { entity ->
        entity.category = category.mapToEntity()
        entity.budget = budget
        entity.maxBudget = maxBudget
        entity.color = color
        entity.index = index
    }

fun Budget.mapToRequest() =
    BudgetRequest(
        moimId = this.moimId,
        categoryId = this.categoryId,
        budget = this.budget,
        weeklyBudget = this.weeklyBudget,
        maxBudget = this.maxBudget,
    )