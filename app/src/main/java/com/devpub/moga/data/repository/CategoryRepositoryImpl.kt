package com.devpub.moga.data.repository

import com.devpub.moga.data.extension.launchBackground
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.MoimLocalDataSource
import com.devpub.moga.data.source.remote.CategoryService
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.repository.CategoryRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CategoryRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val moimLocalDataSource: MoimLocalDataSource,
    private val categoryService: CategoryService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : CategoryRepository {

    override suspend fun loadCategoryList(target: Target): List<Category> {
        val mergedTarget = if (target != Target.INCOME) Target.EXPEND else target
        return moimLocalDataSource.getMoim(appPreference.moimId)?.categoryList?.filterTarget(
            mergedTarget)
            ?: emptyList()
    }

    override suspend fun loadParentCategoryList(target: Target): List<Category> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            moim.categoryList.filterTarget(target).filter { it.id == it.parentId }
        } ?: emptyList()
    }

    override suspend fun addCategory(
        target: Target,
        name: String,
        parentItem: Category?,
    ): List<Category> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            val category = Category(
                moimId = moim.id,
                target = target,
                ownerId = appPreference.userId,
                id = "${moim.ownerId}${moim.userList.size}${moim.categoryList.size}".toLong(),
                name = name).run {
                convertParentId(target, parentItem)
            }

            moim.categoryList.toMutableList()
                .apply {
                    add(category)
                }.also {
                    moimLocalDataSource.updateMoim(moim.copy(categoryList = it))
                }

            ioDispatcher.launchBackground { categoryService.save(category) }
            loadCategoryList(target)
        } ?: emptyList()
    }

    override suspend fun updateCategory(
        target: Target,
        name: String,
        parentItem: Category?,
        item: Category,
    ): List<Category> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            val categoryIndex =
                moim.categoryList.indexOfFirst { it.id == item.id }
            if (categoryIndex > 0) {
                val updateItem = item.copy(name = name).run {
                    convertParentId(target, parentItem)
                }

                moim.categoryList.toMutableList().apply {
                    this[categoryIndex] = updateItem
                }.also {
                    moimLocalDataSource.updateMoim(moim.copy(categoryList = it))
                }
                ioDispatcher.launchBackground { categoryService.save(updateItem) }
            }
            loadCategoryList(target)
        } ?: emptyList()
    }

    override suspend fun deleteCategory(target: Target, item: Category): List<Category> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            moim.categoryList.find { category ->
                category.target == target
                        && category.name == item.name
            }?.let { removeItem ->
                moim.categoryList.toMutableList().apply {
                    remove(removeItem)
                }.also {
                    moimLocalDataSource.updateMoim(moim.copy(categoryList = it))
                }
                ioDispatcher.launchBackground { categoryService.remove(removeItem) }
                loadCategoryList(target)
            }
        } ?: emptyList()
    }

    private fun List<Category>.filterTarget(target: Target) =
        filter { category -> category.target == target }
            .filterNot { category -> category.id == 0L }
            .sortedBy { category -> category.parentId }

    private fun Category.convertParentId(target: Target, selectedParentItem: Category?): Category {
        return if (selectedParentItem != null) {
            copy(parentId = selectedParentItem.id, kosisId = selectedParentItem.kosisId)
        } else {
            val id = if (target != Target.INCOME) 6L else 12L
            copy(parentId = id, kosisId = id)
        }
    }
}