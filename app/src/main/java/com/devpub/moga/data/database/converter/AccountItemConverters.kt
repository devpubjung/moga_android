package com.devpub.moga.data.database.converter

import androidx.room.TypeConverter
import com.devpub.moga.domain.model.Target
import com.devpub.moga.data.model.entity.AssetsEntity
import com.devpub.moga.data.model.entity.CategoryEntity
import com.devpub.moga.data.model.entity.UserEntity
import com.devpub.moga.domain.model.AssetType
import com.devpub.moga.domain.model.NoteType
import com.google.gson.Gson
import java.lang.Exception
import java.util.*

class AccountItemConverters {

    @TypeConverter
    fun toTarget(value: String) = try {
        enumValueOf(value.uppercase(Locale.getDefault()))
    } catch (e: Exception) {
        Target.EXPEND
    }

    @TypeConverter
    fun fromProvider(value: Target) = value.name

    @TypeConverter
    fun toNoteType(value: String) = try {
        enumValueOf(value.uppercase(Locale.getDefault()))
    } catch (e: Exception) {
        NoteType.NONE
    }

    @TypeConverter
    fun fromNoteType(value: NoteType) = value.name

    @TypeConverter
    fun toAssetType(value: String) = try {
        enumValueOf(value.uppercase(Locale.getDefault()))
    } catch (e: Exception) {
        AssetType.CASH
    }

    @TypeConverter
    fun fromAssetType(value: AssetType) = value.name

    @TypeConverter
    fun userToJson(value: UserEntity): String = Gson().toJson(value)

    @TypeConverter
    fun jsonToUser(value: String): UserEntity = Gson().fromJson(value, UserEntity::class.java)

    @TypeConverter
    fun categoryToJson(value: CategoryEntity?): String? = value?.let { Gson().toJson(value) }

    @TypeConverter
    fun jsonToCategory(value: String?) = value?.let { Gson().fromJson(value, CategoryEntity::class.java) }

    @TypeConverter
    fun assetsToJson(value: AssetsEntity?): String? = value?.let { Gson().toJson(value) }

    @TypeConverter
    fun jsonToAssets(value: String?) = value?.let { Gson().fromJson(value, AssetsEntity::class.java) }
}
