package com.devpub.moga.data.api

import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.dto.JoinedMoimInfoRequest
import com.devpub.moga.data.model.dto.UpdatedMoimData
import com.devpub.moga.domain.model.Moim
import retrofit2.http.*

interface MoimApi {

    @POST("moim/save")
    suspend fun saveMoim(@Body moim: Moim): ApiDataResponse<Moim>

    @POST("moim/join")
    suspend fun joinMoim(@Body joinedMoimInfoRequest: JoinedMoimInfoRequest): ApiDataResponse<Moim>

    @GET("moim/update/{id}")
    suspend fun getMoimData(
        @Path("id") id: Long,
        @Query("userId") userId: Long,
        @Query("date") date: String?,
        @Query("type") type: Int?,
    ): ApiDataResponse<UpdatedMoimData>
}