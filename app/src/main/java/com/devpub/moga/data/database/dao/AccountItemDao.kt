package com.devpub.moga.data.database.dao

import androidx.room.*
import com.devpub.moga.data.model.entity.AccountItemEntity

@Dao
interface AccountItemDao : BaseDao<AccountItemEntity> {

    @Query("SELECT * FROM accountItem WHERE id = :id LIMIT 1")
    suspend fun getAccountItem(id: Long): AccountItemEntity

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId ORDER BY date DESC")
    suspend fun getAllAccountItemList(moimId: Long): List<AccountItemEntity>

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime(\"%Y\", date) = :year ORDER BY date DESC")
    suspend  fun getYearAccountItemList(moimId: Long, year: String): List<AccountItemEntity>

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime(\"%Y%m\", date) = :yearMonth ORDER BY date DESC")
    suspend fun getMonthAccountItemList(moimId: Long, yearMonth: String): List<AccountItemEntity>

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime(\"%Y%m%d\", date) = :yearMonthDay ORDER BY date DESC")
    suspend fun getDayAccountItemList(moimId: Long, yearMonthDay: String): List<AccountItemEntity>

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime(\"%Y%m%d\", date) BETWEEN :startYMD AND :endYMD ORDER BY date DESC")
    suspend fun getRangeAccountItemList(moimId: Long, startYMD: String, endYMD: String): List<AccountItemEntity>

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime(\"%Y%m\", date) = :yearMonth AND expend = 1 ORDER BY date DESC")
    suspend fun getMonthExpendAccountItemList(moimId: Long, yearMonth: String): List<AccountItemEntity>

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime(\"%Y%m%d\", date) BETWEEN :startYMD AND :endYMD AND expend = 1 ORDER BY date DESC")
    suspend fun getRangeExpendAccountItemList(moimId: Long, startYMD: String, endYMD: String): List<AccountItemEntity>

}