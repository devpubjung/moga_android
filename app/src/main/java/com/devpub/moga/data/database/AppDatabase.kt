package com.devpub.moga.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.devpub.moga.data.database.converter.AccountItemConverters
import com.devpub.moga.data.database.converter.DateConverter
import com.devpub.moga.data.database.dao.*
import com.devpub.moga.data.model.*
import com.devpub.moga.data.model.entity.*

@Database(entities = [UserEntity::class, MoimEntity::class, AssetsEntity::class, AccountItemEntity::class], version = 2)
@TypeConverters(DateConverter::class, AccountItemConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun moimDao(): MoimDao
    abstract fun assetsDao(): AssetsDao
    abstract fun accountItemDao(): AccountItemDao
    abstract fun reportDao(): ReportDao
    abstract fun insightDao(): InsightDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "moga.db"
                    )
                        .addMigrations(ACCOUNT_ITEM_MIGRATION_1_2)
                        .build()
                }
            }
            return INSTANCE
        }
    }
}