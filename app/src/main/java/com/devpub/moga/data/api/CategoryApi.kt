package com.devpub.moga.data.api

import com.devpub.moga.data.model.dto.CategoryRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.domain.model.Category
import retrofit2.http.*

interface CategoryApi {

    @POST("category/save")
    suspend fun saveCategory(@Body category: CategoryRequest): ApiDataResponse<Category>

    @POST("category/saveList")
    suspend fun saveCategoryList(@Body categoryList: List<CategoryRequest>): ApiDataResponse<List<Category>>

    @DELETE("category/{categoryId}")
    suspend fun deleteCategory(@Path("categoryId") categoryId:Long, @Query("moimId") moimId: Long): ApiDataResponse<*>
}