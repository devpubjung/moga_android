package com.devpub.moga.data.model.mapper

import com.devpub.moga.data.model.entity.CompareItemEntity
import com.devpub.moga.domain.model.CompareItem

fun CompareItemEntity.mapToCompareItem() =
    CompareItem(
        category = this.category.mapToCategory(),
        target = this.target,
        prevAmount = this.prevAmount,
        nextAmount = this.nextAmount,
        sum = this.sum
    )

fun CompareItem.mapToEntity() =
    CompareItemEntity(
        category = this.category.mapToEntity(),
        target = this.target,
        prevAmount = this.prevAmount,
        nextAmount = this.nextAmount,
        sum = this.sum
    )
