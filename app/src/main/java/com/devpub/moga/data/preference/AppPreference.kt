package com.devpub.moga.data.preference

interface AppPreference {

    fun setWatchNotiPermission(flag: Boolean)

    val watchNotiPermission: Boolean

    fun setWatchSmsPermission(flag: Boolean)

    val watchSmsPermission: Boolean

    fun setNotiAutoInput(flag: Boolean)

    val notiAutoInput: Boolean

    fun setShowNotiIcon(flag: Boolean)

    val notiIcon: Boolean

    fun setPurchasedRemoveAds(flag: Boolean)

    val purchasedRemoveAds: Boolean

    fun saveUserId(id: Long)

    val userId: Long

    fun saveMoimId(id: Long)

    val moimId: Long

    fun saveMoimUserIndex(index: Int)

    val moimUserIndex: Int

    fun saveAccountItemIndex(index: Long)

    val accountItemIndex: Long

    fun saveAchieveDate(dateString: String)

    val achieveDate: String

    fun saveAchieveCalDate(dateString: String)

    val achieveCalDate: String

    fun saveSyncMoimDataTime(dateString: String)

    val syncMoimDataTime: String

    fun saveUserDate(dateString: String)

    val userSaveDate: String

    fun saveJoinMoim(join: Boolean)

    val joinMoim: Boolean

    fun setNotiShare(flag: Boolean)

    val notiShare: Boolean

    fun saveMoimSyncDate(dateString: String)

    val moimSyncDate: String

    fun saveSyncDate(dateString: String)

    val syncDate: String

    fun setStartWeek(startWeek: Int)

    val startWeek: Int

    fun setStartDayOfMonth(startDayOfMonth: Int)

    val startDayOfMonth: Int

    fun clear()
}