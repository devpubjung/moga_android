package com.devpub.moga.data.model.mapper

import com.devpub.moga.data.model.entity.MoimEntity
import com.devpub.moga.domain.model.Moim

fun MoimEntity.mapToMoim() =
    Moim(
        id = this.id,
        ownerId = this.ownerId,
        userList = this.userList.map { it.mapToUser() },
        categoryList = this.categoryList.map { it.mapToCategory() },
        budgetList = this.budgetList.map { it.mapToBudget() }
    )

fun Moim.mapToEntity() =
    MoimEntity(
        id = this.id,
        ownerId = this.ownerId,
    ).also { entity ->
        entity.userList = userList.map { it.mapToEntity() }.toMutableList()
        entity.categoryList = categoryList.map { it.mapToEntity() }.toMutableList()
        entity.budgetList = budgetList.map { it.mapToEntity() }.toMutableList()
    }