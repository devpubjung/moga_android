package com.devpub.moga.data.source

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.AccountItemLocalDataSource
import com.devpub.moga.data.source.local.MoimLocalDataSource
import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.model.BudgetDetailReport
import com.devpub.moga.domain.model.Moim
import com.devpub.moga.domain.model.User
import com.devpub.moga.util.DateUtil
import java.lang.Exception

class BudgetDetailPagingSource constructor(
    private val appPreference: AppPreference,
    private val moimLocalDataSource: MoimLocalDataSource,
    private val accountItemLocalDataSource: AccountItemLocalDataSource,
    private val isMonthly: Boolean
) : PagingSource<Int, BudgetDetailReport>() {

    override fun getRefreshKey(state: PagingState<Int, BudgetDetailReport>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, BudgetDetailReport> {
        return try {
            val page = params.key ?: 1
            val list = loadBudgetDetailReport(isMonthly, appPreference.startWeek)
            LoadResult.Page(
                data = list,
                prevKey = if (page == 1) null else page - 1,
                nextKey = null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    private suspend fun loadBudgetDetailReport(isMonthly: Boolean, startWeek: Int): List<BudgetDetailReport> {
        fun getBudgetDetailList(
            moim: Moim,
            accountList: List<AccountItem>,
            isMonthly: Boolean = true,
        ): List<BudgetDetailReport> {
            val budgetReportList = mutableListOf<BudgetDetailReport>()
            moim.budgetList.forEachIndexed { index, budget ->
                if (index > 0) {
                    val accountItems =
                        accountList.filter { item -> budget.category == item.category }
                    val accountSum = accountItems.sumOf { accountItem -> accountItem.amount }
                    val userList = mutableSetOf<User>()
                    accountItems.forEach {
                        it.user?.let { user ->
                            userList.add(user)
                        }
                    }
                    budgetReportList.add(
                        BudgetDetailReport(budget,
                            accountSum,
                            accountItems,
                            userList.toList(),
                            isMonthly)
                    )
                }
            }
            return budgetReportList
        }

        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            val thisMonthAccountList =
                accountItemLocalDataSource.getMonthlyList(moim.id, onlyExpend = true)

            if (!isMonthly) {
                val thisWeekAccountList = thisMonthAccountList
                    .filter { accountItem ->
                        DateUtil.containsThisWeek(accountItem.date, startWeek)
                    }
                getBudgetDetailList(moim, thisWeekAccountList, false)
            } else {
                getBudgetDetailList(moim, thisMonthAccountList)
            }
        } ?: emptyList()
    }
}