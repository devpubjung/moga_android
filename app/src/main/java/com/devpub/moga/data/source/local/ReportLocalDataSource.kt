package com.devpub.moga.data.source.local

import com.devpub.moga.data.database.dao.InsightDao
import com.devpub.moga.data.database.dao.ReportDao
import com.devpub.moga.data.model.dto.*
import com.devpub.moga.data.model.mapper.mapToAccountItem
import com.devpub.moga.data.model.mapper.mapToAssets
import com.devpub.moga.data.model.mapper.mapToCompareItem
import com.devpub.moga.domain.model.*
import com.devpub.moga.util.DateUtil
import java.util.*
import javax.inject.Inject

class ReportLocalDataSource @Inject constructor(
    private val reportDao: ReportDao,
    private val insightDao: InsightDao,
) {

    suspend fun getYearReportList(moimId: Long, yearString: String): List<MonthExpendReport> {
        return reportDao.getYearReport(moimId, yearString)
    }

    suspend fun getYearReportMap(moimId: Long, yearString: String): Map<String, Long> {
        return getYearReportList(moimId, yearString).associate { it.year + it.month to it.sum }
    }

    suspend fun getDayOfWeekSumList(moimId: Long, yearMonth: String): List<DayOfWeekSum> {
        return reportDao.getDayOfWeekSumReport(moimId, yearMonth)
    }

    suspend fun getDayOfWeekSumMap(moimId: Long, yearMonth: String): Map<Int, Long> {
        return getDayOfWeekSumList(moimId, yearMonth).associate { it.dayOfWeek to it.sum }
    }

    suspend fun getReportByUser(moim: Moim, yearMonth: String): List<UserSum> {
        val sumList = mutableListOf<UserSum>()
        reportDao.getUserSumReport(moim.id, yearMonth)
            .groupBy { it.user.id }.forEach { (id, list) ->
                moim.userList.find { it.id == id }?.also { user ->
                    sumList.add(UserSum(user, list.sumOf { it.sum }))
                }
            }
        return sumList
    }

    suspend fun getReportByCategory(moim: Moim, yearMonth: String): List<CategorySum> {
        val sumList = mutableListOf<CategorySum>()
        reportDao.getCategorySumReport(moim.id, yearMonth)
            .groupBy { it.category.id }.forEach { (id, list) ->
                moim.categoryList.find { it.id == id }?.also { category ->
                    sumList.add(CategorySum(category, list.sumOf { it.sum }))
                }
            }
        return sumList
    }

    suspend fun getReportByAssets(moim: Moim, yearMonth: String): List<AssetsSum> {
        return reportDao.getAssetsSumReport(moim.id, yearMonth)
            .map { AssetsSum(it.assets.mapToAssets(), it.sum) }
    }

    suspend fun getCompareList(moimId: Long, prevDate: Date, nextDate: Date): List<CompareItem> {
        val prevMonth = DateUtil.getQueryDateStringForDate(prevDate, DateUtil.QueryDate.MONTH)
        val nextMonth = DateUtil.getQueryDateStringForDate(nextDate, DateUtil.QueryDate.MONTH)
        return insightDao.getCompareItem(moimId, prevMonth, nextMonth).map { it.mapToCompareItem() }
    }

    suspend fun getOthersStandard(userId: Long, moimId: Long, date: Date): Standard {
        val monthQuery = DateUtil.getQueryDateStringForDate(date, DateUtil.QueryDate.MONTH)
        return insightDao.getOthersStandard(userId, moimId, monthQuery)
    }

    suspend fun getOthersData(moimId: Long, date: Date): List<AccountItem> {
        val monthQuery = DateUtil.getQueryDateStringForDate(date, DateUtil.QueryDate.MONTH)
        return insightDao.getOthersData(moimId, monthQuery).map { it.mapToAccountItem() }
    }
}