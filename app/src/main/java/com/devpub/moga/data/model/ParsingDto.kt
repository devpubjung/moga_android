package com.devpub.moga.data.model

import java.io.Serializable

data class SmsRequest(
    val sender: String,
    val content: String,
    val date: String,
    val moimId: Long,
    val accountItemIndex: Long,
) : Serializable

data class NotiRequest(
    val packagerName: String,
    val title: String,
    val text: String,
    val subText: String?,
    val date: String,
    val moimId: Long,
    val accountItemIndex: Long,
) : Serializable