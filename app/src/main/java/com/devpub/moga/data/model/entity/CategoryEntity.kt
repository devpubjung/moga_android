package com.devpub.moga.data.model.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.TypeConverters
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.data.database.converter.MoimConverters
import com.devpub.moga.domain.model.Target
import java.io.Serializable

@Entity(tableName = "category", primaryKeys = ["id"],
    indices = [Index(value = ["id"], unique = true)])
@TypeConverters(MoimConverters::class)
class CategoryEntity(
    val moimId: Long,
    val target: Target = Target.EXPEND,
) : Serializable {
    var id: Long = 0
    var parentId: Long = DEFAULT_ID
    var kosisId: Long = DEFAULT_ID
    var name: String = "전체"
        set(value) {
            tag = value.replace("/", ",")
            field = value
        }
    var tag: String = name.replace("/", ",")
    var ownerId: Long? = null

    @Transient
    var index: Int = 0

    val canDelete get() = id != parentId

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val that = o as CategoryEntity
        if (moimId != that.moimId) return false
        if (id != that.id) return false
        if (parentId != that.parentId) return false
        if (kosisId != that.kosisId) return false
        if (ownerId != that.ownerId) return false
        if (name != that.name) return false
        return true
    }

    override fun hashCode(): Int {
        var result = if (moimId != 0L) moimId else 0
        result = 31 * result + if (id != 0L) id else 0
        result = 31 * result + if (parentId != 0L) parentId else 0
        result = 31 * result + if (kosisId != 0L) kosisId else 0
        result = 31 * result + (ownerId ?: 0)
        result = 31 * result + name.toCharArray().contentHashCode()
        result = 31 * result + index
        return result.toInt()
    }

    override fun toString() = name
}
