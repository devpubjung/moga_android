package com.devpub.moga.data.model.dto

import com.devpub.common.model.ListItem
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.domain.model.*
import java.io.Serializable


class MoimResponse(
    val id: Long = 1, //userId+No,
    val ownerId: Long = DEFAULT_ID, //userId
    val userList: List<User> = emptyList(),
    val categoryList: List<Category> = emptyList(),
    val budgetList: List<Budget> = emptyList(),
) : ListItem

data class JoinedMoimInfoRequest(
    val encodedUserId: String,
    val encodedMoimCode: String,
    val encodedMoimId: String? = null,
) : Serializable



data class JoinedMoim(
    val encodedUserId: String,
    val encodedMoimCode: String,
    val encodedMoimId: String? = null,
) : Serializable

data class MoimInfo(
    val accountItemIndex: Long = 1,
) : Serializable


data class UpdatedMoimData(
    val moimInfo: MoimInfo? = null,
    val moim: Moim,
    val accountItemList: List<AccountItem>,
    val removedAccountItemList: List<AccountItem>,
) : Serializable