package com.devpub.moga.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.devpub.moga.data.model.entity.UserEntity
import com.devpub.moga.domain.model.Provider

@Dao
interface UserDao: BaseDao<UserEntity> {
    @Query("SELECT * FROM user WHERE id = :id")
    suspend fun getUser(id: Long) : UserEntity?

    @Query("SELECT * FROM user WHERE uid = :uid AND provider = :provider")
    suspend fun getUserUid(uid: Long, provider: Provider) : UserEntity?

    @Query("SELECT * FROM user")
    suspend fun getAll(): List<UserEntity>

}