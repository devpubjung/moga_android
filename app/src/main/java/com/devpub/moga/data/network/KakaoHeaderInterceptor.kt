package com.devpub.moga.data.network

import okhttp3.Interceptor
import okhttp3.Response


class KakaoHeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val origin = chain.request()
        val request = origin.newBuilder()
            .header("Content-Type", "application/json")
            .header("accept"," application/json")
            .header("Authorization","KakaoAK 391ac63a09365a534a2de4e6eaf8bbbe")
            .method(origin.method, origin.body)
            .build()

        return chain.proceed(request)
    }
}