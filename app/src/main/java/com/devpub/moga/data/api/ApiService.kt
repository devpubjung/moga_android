package com.devpub.moga.data.api

import com.devpub.moga.data.network.RetrofitBuilder

object ApiService {
   val commonApi: CommonApi
      get() = RetrofitBuilder.getRetrofit().create(CommonApi::class.java)

   val userApi: UserApi
      get() = RetrofitBuilder.getRetrofit().create(UserApi::class.java)

   val moimApi: MoimApi
      get() = RetrofitBuilder.getRetrofit().create(MoimApi::class.java)

   val categoryApi: CategoryApi
      get() = RetrofitBuilder.getRetrofit().create(CategoryApi::class.java)

   val budgetApi: BudgetApi
      get() = RetrofitBuilder.getRetrofit().create(BudgetApi::class.java)

   val accountItemApi: AccountItemApi
      get() = RetrofitBuilder.getRetrofit().create(AccountItemApi::class.java)

   val kosisApi: KosisApi
      get() = RetrofitBuilder.getExternalRetrofit("https://kosis.kr/openapi").create(KosisApi::class.java)

   val kakaoApi: KakaoApi
      get() = RetrofitBuilder.getKakaoApiRetrofit("https://dapi.kakao.com").create(KakaoApi::class.java)
}