package com.devpub.moga.data.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.devpub.moga.data.model.common.TimeEntity
import java.io.Serializable

@Entity(tableName = "assets", primaryKeys = ["moimId", "name"])
class AssetsEntity(
    val moimId: Long = 0,
    @ColumnInfo(index = true)
    val id: Long = 0,
    val name: String = "추가"
) : TimeEntity(), Serializable {
    @ColumnInfo(index = true)
    var index: Int = 1

    companion object {
        fun generatorAssetsList(moimId: Long): List<AssetsEntity> {
            return listOf(
                AssetsEntity(moimId = moimId, name = "현금"),
                AssetsEntity(moimId = moimId, name = "신용카드"),
                AssetsEntity(moimId = moimId, name = "체크카드")
            )
        }
    }
}