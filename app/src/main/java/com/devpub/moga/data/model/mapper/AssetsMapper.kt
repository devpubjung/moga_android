package com.devpub.moga.data.model.mapper

import com.devpub.moga.data.model.entity.AssetsEntity
import com.devpub.moga.domain.model.Assets

fun AssetsEntity.mapToAssets() =
    Assets(
        moimId = this.moimId,
        id = this.id,
        name = this.name,
        index = this.index
    )

fun Assets.mapToEntity() =
    AssetsEntity(
        moimId = this.moimId,
        id = this.id,
        name = this.name,
    ).also { entity ->
        entity.index = index
    }
