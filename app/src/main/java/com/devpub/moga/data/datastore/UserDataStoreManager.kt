package com.devpub.moga.data.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore

class UserDataStoreManager(context: Context) : DataStoreManager(context.dataStore) {

    suspend fun getConnectAssets() = getBooleanValue(CONNECT_ASSETS)

    suspend fun setConnectAssets(isConnect: Boolean) {
        saveValue(CONNECT_ASSETS, isConnect)
    }

    suspend fun getAppLock() = getBooleanValue(APP_LOCK, true)

    suspend fun setAppLock(lock: Boolean) {
        saveValue(APP_LOCK, lock)
    }

    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings_pref")

        private const val CONNECT_ASSETS = "connect_assets"
        private const val APP_LOCK = "app_lock"
    }
}