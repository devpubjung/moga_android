package com.devpub.moga.data.source.local

import com.devpub.moga.data.database.dao.AccountItemDao
import com.devpub.moga.data.model.mapper.mapToAccountItem
import com.devpub.moga.data.model.mapper.mapToEntity
import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.model.User
import com.devpub.moga.util.DateUtil
import java.util.*
import javax.inject.Inject

class AccountItemLocalDataSource @Inject constructor(
    private val accountItemDao: AccountItemDao,
) {

    suspend fun getItem(itemId: Long): AccountItem {
        return accountItemDao.getAccountItem(itemId).mapToAccountItem()
    }

    suspend fun getListAll(moimId: Long): List<AccountItem> {
        return accountItemDao.getAllAccountItemList(moimId).map { it.mapToAccountItem() }
    }

    suspend fun updateUserInList(moimId: Long, user: User) {
        accountItemDao.insertAll(accountItemDao.getAllAccountItemList(moimId)
            .filter { it.user?.id == user.id }
            .map {
                it.apply {
                    this.user = user.mapToEntity()
                }
            })
    }

    suspend fun save(item: AccountItem) {
        accountItemDao.insert(item.mapToEntity())
    }

    suspend fun saveAll(accountItemList: List<AccountItem>) {
        accountItemDao.insertAll(accountItemList.map { it.mapToEntity() })
    }

    suspend fun removeAll(accountItemList: List<AccountItem>) {
        accountItemDao.deleteAll(accountItemList.map { it.mapToEntity() })
    }

    suspend fun getMonthlyList(
        moimId: Long,
        date: Date = DateUtil.todayDate,
        onlyExpend: Boolean = false,
    ): List<AccountItem> {
        val monthQueryString = DateUtil.getQueryDateStringForDate(date, DateUtil.QueryDate.MONTH)
        return if (onlyExpend) {
            accountItemDao.getMonthExpendAccountItemList(moimId, monthQueryString)
                .map { it.mapToAccountItem() }
        } else {
            accountItemDao.getMonthAccountItemList(moimId, monthQueryString)
                .map { it.mapToAccountItem() }
        }
    }

    suspend fun getWeeklyList(
        moimId: Long,
        date: Date = DateUtil.todayDate,
        onlyExpend: Boolean = false,
        startWeek: Int,
    ): List<AccountItem> {
        val startDateQueryString = DateUtil.getFirstDayOfWeek(date, startWeek).let {
            DateUtil.getQueryDateStringForDate(it, DateUtil.QueryDate.DAY)
        }
        val endDateQueryString = DateUtil.getLastDayOfWeek(date, startWeek).let {
            DateUtil.getQueryDateStringForDate(it, DateUtil.QueryDate.DAY)
        }
        return if (onlyExpend) {
            accountItemDao.getRangeExpendAccountItemList(moimId,
                startDateQueryString,
                endDateQueryString).map { it.mapToAccountItem() }
        } else {
            accountItemDao.getRangeAccountItemList(moimId, startDateQueryString, endDateQueryString)
                .map { it.mapToAccountItem() }
        }
    }

    suspend fun getDayList(moimId: Long, date: Date = DateUtil.todayDate): List<AccountItem> {
        val dayString = DateUtil.getQueryDateStringForDate(date)
        return accountItemDao.getDayAccountItemList(moimId, dayString).map { it.mapToAccountItem() }
    }

    suspend fun remove(accountItem: AccountItem) {
        accountItemDao.delete(accountItem.mapToEntity())
    }
}