package com.devpub.moga.data.model.mapper

import android.annotation.SuppressLint
import com.devpub.moga.data.database.converter.UserConverters
import com.devpub.moga.data.model.entity.UserEntity
import com.devpub.moga.domain.model.Provider
import com.devpub.moga.util.DateUtil
import com.kakao.sdk.user.model.User

@SuppressLint("DefaultLocale")
fun User.convertForUser() =
    com.devpub.moga.domain.model.User(uid = id, provider = Provider.KAKAO).apply {
        kakaoAccount?.let { account ->
            email = account.email ?: ""
            ageRange = account.ageRange?.let { ageRange ->
                ageRange.name.split("_")[1].first().toString().toInt()
            } ?: 1
            birthYear = DateUtil.thisYear() - (ageRange * 10)
            gender = UserConverters().toGender(account.gender?.name?.toUpperCase()
                ?: "MALE")
            account.profile?.let { profile ->
                profileImgUrl = profile.profileImageUrl!!
                thumbNailImgUrl = profile.thumbnailImageUrl!!
                nickName = profile.nickname!!
            }
        }
    }

fun UserEntity.mapToUser() =
    com.devpub.moga.domain.model.User(
        id = this.id,
        uid = this.uid,
        provider = this.provider,
        nickName = this.nickName,
        email = this.email,
        profileImgUrl = this.profileImgUrl,
        thumbNailImgUrl = this.thumbNailImgUrl,
        ageRange = this.ageRange,
        birthYear = this.birthYear,
        gender = this.gender
    )

fun com.devpub.moga.domain.model.User.mapToEntity(): UserEntity {
    var userId = id
    if (userId == -1L) {
        userId = "${provider.ordinal}$uid".toLong()
    }
    return UserEntity(
        id = userId,
        uid = this.uid,
        provider = this.provider,
        nickName = this.nickName,
        email = this.email,
        profileImgUrl = this.profileImgUrl,
        thumbNailImgUrl = this.thumbNailImgUrl,
        ageRange = this.ageRange,
        birthYear = this.birthYear,
        gender = this.gender
    )
}
