package com.devpub.moga.data.repository

import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.AssetsLocalDataSource
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.Assets
import com.devpub.moga.domain.repository.AssetsRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class AssetsRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val assetsLocalDataSource: AssetsLocalDataSource,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : AssetsRepository {

    override suspend fun loadAssetsList() = assetsLocalDataSource.load()

    override suspend fun addAssets(name: String) =
        assetsLocalDataSource.add(appPreference.moimId, name)

    override suspend fun removeAssets(item: Assets) = assetsLocalDataSource.remove(item)

    override suspend fun findAssets(assetsName: String) : Assets {
        var assets = loadAssetsList().find { assets -> assets.name == assetsName }
        if (assets == null) {
            assets = Assets(moimId = appPreference.moimId, name = assetsName)
            assetsLocalDataSource.add(assets)
        }
        return assets
    }
}