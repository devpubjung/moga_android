package com.devpub.moga.data.api

import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.domain.model.Provider
import com.devpub.moga.domain.model.User
import retrofit2.http.*

interface UserApi {

    @GET("user/{id}")
    suspend fun getUser(@Path("id") id: Long) : ApiDataResponse<User>

    @GET("user/{provider}/{uid}")
    suspend fun getUserUid(@Path("uid") uid: Long, @Path("provider") provider: Provider, ) : ApiDataResponse<User>

    @POST("user/save")
    suspend fun saveUser(@Body user: User) : ApiDataResponse<User>

    @PUT("user/update")
    suspend fun updateUser(@Body user: User) : ApiDataResponse<User>

    @POST("user/delete")
    suspend fun deleteUser(@Body user: User) : ApiDataResponse<*>
}