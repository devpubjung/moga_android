package com.devpub.moga.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.devpub.moga.data.database.converter.UserConverters
import com.devpub.moga.data.model.common.TimeEntity
import com.devpub.moga.domain.model.Gender
import com.devpub.moga.domain.model.Provider
import java.io.Serializable

@Entity(tableName = "user")
@TypeConverters(UserConverters::class)
class UserEntity(
    val uid: Long = 0,
    val provider: Provider = Provider.KAKAO,
    @PrimaryKey
    var id: Long = "${provider.ordinal}$uid".toLong(),
    var nickName: String = "",
    var email: String = "",
    var profileImgUrl: String = "",
    var thumbNailImgUrl: String = "",
    var ageRange: Int = 1,
    var birthYear: Int = 1987,
    var gender: Gender = Gender.MALE
) : TimeEntity(), Serializable