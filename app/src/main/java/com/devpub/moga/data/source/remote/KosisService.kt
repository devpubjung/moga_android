package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.KosisApi
import com.devpub.moga.data.model.external.KosisResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class KosisService @Inject constructor(
    private val kosisApi: KosisApi,
     private val apiDispatcher: CoroutineDispatcher,
)  {

    suspend fun getByIncomeData(): List<KosisResponse> {
        return withContext(apiDispatcher) { kosisApi.loadByIncome(getPrams("overcome26/101/DT_1L9U021/2/1/20210305145532")) }
    }

    suspend fun getByAgeData(): List<KosisResponse> {
        return withContext(apiDispatcher) { kosisApi.loadByUser(getPrams("overcome26/101/DT_1L9U008/2/1/20210305112142")) }
    }

    private fun getPrams(userStatsId: String): Map<String, String> {
        return mapOf(
            "method" to "getList",
            "apiKey" to "NDU3OWQwNGI5OTQ2NmZkZTNiOTkwYjIyZDhmZDkyMTM=",
            "format" to "json",
            "jsonVD" to "Y",
            "prdSe" to "Y",
            "newEstPrdCnt" to "1",
            "userStatsId" to userStatsId
        )
    }
}