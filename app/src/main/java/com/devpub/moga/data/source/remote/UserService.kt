package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.UserApi
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.domain.model.User
import javax.inject.Inject

class UserService @Inject constructor(
    private val userApi: UserApi
) {
    suspend fun getUser(id: Long): ApiDataResponse<User> {
        return userApi.getUser(id)
    }

    suspend fun saveUser(user: User): ApiDataResponse<User> {
        return userApi.saveUser(user)
    }

    suspend fun updateUser(user: User): ApiDataResponse<User> {
        return userApi.updateUser(user)
    }

    suspend fun deleteUser(user: User): ApiDataResponse<*> {
        return  userApi.deleteUser(user)
    }
}