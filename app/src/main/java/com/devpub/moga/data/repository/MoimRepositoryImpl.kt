package com.devpub.moga.data.repository

import com.devpub.moga.DEFAULT_NUMBER
import com.devpub.moga.data.extension.launchBackground
import com.devpub.moga.data.model.dto.JoinedMoimInfoRequest
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.AccountItemLocalDataSource
import com.devpub.moga.data.source.local.MoimLocalDataSource
import com.devpub.moga.data.source.remote.MoimService
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.Moim
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.repository.MoimRepository
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.SecureUtil
import com.devpub.moga.util.SecureUtil.toByteArray
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.math.max

class MoimRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val moimService: MoimService,
    private val moimLocalDataSource: MoimLocalDataSource,
    private val accountItemLocalDataSource: AccountItemLocalDataSource,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : MoimRepository {

    override suspend fun getMoim() = moimLocalDataSource.getMoim(appPreference.moimId)

    override suspend fun saveMoim(moim: Moim) {
        val userIndex = moim.userList.indexOfLast { it.id == appPreference.userId }.run {
            if (this == -1) {
                moim.userList.size + 1
            } else {
                this + 1
            }
        }
        appPreference.saveMoimId(moim.id)
        appPreference.saveMoimUserIndex(userIndex)
        appPreference.saveAccountItemIndex(DEFAULT_NUMBER.toLong())

        moimLocalDataSource.saveMoim(moim)

        ioDispatcher.launchBackground { moimService.saveMoim(moim) }
    }

    override suspend fun updateMoim(moim: Moim) {
        moimLocalDataSource.updateMoim(moim)
    }

    override suspend fun joinMoim(joinCode: String?): Boolean {
        if (joinCode == null) {
            return false
        }
        try {
            moimService.joinMoim(JoinedMoimInfoRequest(
                SecureUtil.encode(appPreference.userId.toByteArray()),
                joinCode,
                SecureUtil.encode(appPreference.moimId.toByteArray()))
            ).data?.let { moim ->
                saveMoim(moim)
                appPreference.saveJoinMoim(true)
                return true
            }
        } catch (e:Exception) {}
        return false
    }

    override suspend fun createMoim(user: User) {
        moimLocalDataSource.createMoim(user).also {
            appPreference.saveMoimId(it.id)
            ioDispatcher.launchBackground { moimService.saveMoim(it) }
        }
    }

    override suspend fun syncMoimData(type: Int): Boolean {
        moimService.syncMoimData(
            appPreference.moimId,
            appPreference.userId,
            appPreference.syncMoimDataTime,
            type
        ).run {
            if (success) {
                data?.run {
                    moimLocalDataSource.updateMoim(moim)
                    appPreference.saveJoinMoim(moim.userList.distinctBy { it.id }.size > 1)
                    if (moimInfo != null) {
                        appPreference.saveAccountItemIndex(max(appPreference.accountItemIndex,
                            moimInfo.accountItemIndex))
                    }
                    if (accountItemList.isNotEmpty() || removedAccountItemList.isNotEmpty()) {
                        appPreference.saveSyncMoimDataTime(DateUtil.getPreferenceDate(sec = true))
                        accountItemLocalDataSource.saveAll(accountItemList)
                        accountItemLocalDataSource.removeAll(removedAccountItemList)
                    }
                    return true
                }
            }
            return false
        }
    }

    override fun syncMoimDataForBackground(type: Int) {
        ioDispatcher.launchBackground {
            if (!syncMoimData(type)) {
                moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
                    ioDispatcher.launchBackground { moimService.saveMoim(moim) }
                }
            }
        }
    }

    override suspend fun loadJoinCode(): String = SecureUtil.encode(appPreference.moimId.toByteArray())

    override suspend fun loadUsers() = getMoim()?.userList ?: emptyList()
}