package com.devpub.moga.data.model.dto

import java.io.Serializable

data class MonthExpendReport(
    val year: String,
    val month: String,
    val sum: Long,
): Serializable

data class DayOfWeekSum(
    val dayOfWeek: Int,
    val sum: Long,
) : Serializable

data class Standard(
    val income: Long,
    val ageRange: Int,
    val userNumber: Int,
) : Serializable