package com.devpub.moga.data.model.entity

import androidx.room.*
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.DEFAULT_NUMBER
import com.devpub.moga.data.database.converter.AccountItemConverters
import com.devpub.moga.data.model.common.TimeEntity
import com.devpub.moga.domain.model.AssetType
import com.devpub.moga.domain.model.NoteType
import com.devpub.moga.util.DateUtil
import com.devpub.moga.domain.model.Target
import java.io.Serializable
import java.util.*

@Entity(tableName = "accountItem",
    primaryKeys = ["moimId", "id"],
    indices = [Index(value = ["id"], unique = true)])
@TypeConverters(AccountItemConverters::class)
data class AccountItemEntity(
    val moimId: Long,
    val id: Long = "${moimId}$DEFAULT_NUMBER".toLong(),
) : TimeEntity(), Serializable {
    var target: Target = Target.EXPEND
    var date: Date = DateUtil.today.time
    var user: UserEntity? = null
    var amount: Long = 0
    var assets: AssetsEntity? = null
    var category: CategoryEntity? = null
    var content: String? = null
    var memo: String? = null
    var expend: Boolean = true
    var share: Boolean = true
    var report: Boolean = true
    var ownerId: Long = DEFAULT_ID
    var type: NoteType = NoteType.NONE
    var assetType: AssetType = AssetType.CASH
}

val ACCOUNT_ITEM_MIGRATION_1_2 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL(
            "ALTER TABLE accountItem ADD COLUMN expend INTEGER NOT NULL DEFAULT '1'")
        database.execSQL(
            "ALTER TABLE accountItem ADD COLUMN share INTEGER NOT NULL DEFAULT '1'")
        database.execSQL(
            "ALTER TABLE accountItem ADD COLUMN report INTEGER NOT NULL DEFAULT '1'")
        database.execSQL(
            "ALTER TABLE accountItem ADD COLUMN ownerId INTEGER NOT NULL DEFAULT '-1'")
        database.execSQL(
            "ALTER TABLE accountItem ADD COLUMN type TEXT NOT NULL DEFAULT 'NONE'")
        database.execSQL(
            "ALTER TABLE accountItem ADD COLUMN assetType TEXT NOT NULL DEFAULT 'CASH'")
    }
}