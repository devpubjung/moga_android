package com.devpub.moga.data.model.common

import java.io.Serializable

open class CommonResponse : Serializable {

    val success: Boolean = false

    val code: Int = -1

    val message: String = ""
}