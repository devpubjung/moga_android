package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.BudgetApi
import com.devpub.moga.data.model.dto.BudgetSettingRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.mapper.mapToRequest
import com.devpub.moga.domain.model.Budget
import javax.inject.Inject

class BudgetService @Inject constructor(
    private val budgetApi: BudgetApi,
) {
    suspend fun saveBudget(budget: Budget): ApiDataResponse<Budget> {
        return budgetApi.saveBudget(budget.mapToRequest())
    }

    suspend fun saveAllBudget(
        addBudgetList: List<Budget>,
        removeBudgetList: List<Budget>,
    ): ApiDataResponse<List<Budget>> {
        return budgetApi.saveBudgetList(
            BudgetSettingRequest(
                addBudgetList.map { it.mapToRequest() },
                removeBudgetList.map { it.mapToRequest() }
            )
        )
    }
}