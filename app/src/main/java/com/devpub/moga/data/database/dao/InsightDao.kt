package com.devpub.moga.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.devpub.moga.data.model.dto.Standard
import com.devpub.moga.data.model.entity.AccountItemEntity
import com.devpub.moga.data.model.entity.CompareItemEntity

@Dao
interface InsightDao {

    @Query("SELECT *, (nextAmount - prevAmount) as sum FROM (SELECT prev.category as category, prev.target as target, COALESCE(prev.sum, 0) as prevAmount, COALESCE(next.sum, 0) as nextAmount FROM (SELECT category, target, sum(amount) as sum FROM accountItem WHERE moimId = :moimId AND strftime('%Y%m', date) = :prevYearMonth GROUP BY category, target) prev LEFT JOIN (SELECT category, target, sum(amount) sum FROM accountItem WHERE moimId = :moimId AND strftime('%Y%m', date) = :yearMonth GROUP BY category, target) as next USING(category, target) UNION ALL SELECT next.category as category, next.target as target, COALESCE(prev.sum, 0) as prevAmount, COALESCE(next.sum, 0) as nextAmount FROM (SELECT category, target, sum(amount) as sum FROM accountItem WHERE moimId = :moimId AND strftime('%Y%m', date) = :yearMonth GROUP BY category, target) next LEFT JOIN (SELECT category, target, sum(amount) sum FROM accountItem WHERE moimId = :moimId AND strftime('%Y%m', date) = :prevYearMonth GROUP BY category, target) prev USING(category, target) WHERE prev.category IS NULL ORDER BY target DESC)")
    suspend fun getCompareItem(
        moimId: Long,
        prevYearMonth: String,
        yearMonth: String,
    ): List<CompareItemEntity>


    @Query("SELECT * FROM (SELECT sum(amount) income FROM accountItem WHERE moimId = :moimId AND strftime('%Y%m', date) = :yearMonth AND target = 'INCOME') a, (SELECT ageRange FROM user WHERE id = :userId) b, (SELECT count(userList) userNumber FROM moim WHERE id = :moimId) c")
    suspend fun getOthersStandard(
        userId: Long,
        moimId: Long,
        yearMonth: String,
    ): Standard

    @Query("SELECT * FROM accountItem WHERE moimId = :moimId AND strftime('%Y%m', date) = :yearMonth AND target != 'INCOME' AND expend = 1")
    suspend fun getOthersData(
        moimId: Long,
        yearMonth: String,
    ): List<AccountItemEntity>
}