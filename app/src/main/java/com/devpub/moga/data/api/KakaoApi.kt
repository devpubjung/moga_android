package com.devpub.moga.data.api

import com.devpub.moga.data.model.external.KakaoAddress
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface KakaoApi {

    @GET("/v2/local/search/keyword.json")
    suspend fun searchKeyword(@QueryMap params: Map<String, String>): KakaoAddress

}
