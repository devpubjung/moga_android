package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.CommonApi
import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.data.model.SuggestRequest
import javax.inject.Inject

class CommonService @Inject constructor(
    private val commonApi: CommonApi,
) {
    suspend fun ping() = commonApi.ping()

    suspend fun sendSuggest(userId: Long, content: String) =
        commonApi.sendSuggest(SuggestRequest(userId, content))

    suspend fun parseSms(smsRequest: SmsRequest) = commonApi.parseSms(smsRequest)

    suspend fun parseNoti(notiRequest: NotiRequest) = commonApi.paseNoti(notiRequest)

    suspend fun getCategoryInfo(name: String) = commonApi.categoryInfo(name)
}