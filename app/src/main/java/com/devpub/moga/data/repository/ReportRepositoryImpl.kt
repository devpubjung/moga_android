package com.devpub.moga.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.BudgetDetailPagingSource
import com.devpub.moga.data.source.local.AccountItemLocalDataSource
import com.devpub.moga.data.source.local.MoimLocalDataSource
import com.devpub.moga.data.source.local.ReportLocalDataSource
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.*
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.repository.ReportRepository
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MoneyUtil
import kotlinx.coroutines.CoroutineDispatcher
import java.util.*
import javax.inject.Inject

class ReportRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val moimLocalDataSource: MoimLocalDataSource,
    private val accountItemLocalDataSource: AccountItemLocalDataSource,
    private val reportLocalDataSource: ReportLocalDataSource,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ReportRepository {

    override suspend fun loadBudgetReport() =
        moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            val totalBudget = moim.budgetList.first()
            val thisMonthAccountList =
                accountItemLocalDataSource.getMonthlyList(moim.id, onlyExpend = true)
            val thisWeekAccountList = thisMonthAccountList
                .filter { accountItem ->
                    DateUtil.containsThisWeek(accountItem.date, appPreference.startWeek)
                }
            val monthSum =
                thisMonthAccountList.sumOf { accountItem -> if (accountItem.target != Target.INCOME) accountItem.amount else 0 }
            val weekSum =
                thisWeekAccountList.sumOf { accountItem -> if (accountItem.target != Target.INCOME) accountItem.amount else 0 }

            BudgetReport(totalBudget, monthSum, weekSum, isWeeklyAchievedGoal(totalBudget))
        }

    override suspend fun loadBudgetDetailReport(isMonthly: Boolean): List<BudgetDetailReport> {
        fun getBudgetDetailList(
            moim: Moim,
            accountList: List<AccountItem>,
            isMonthly: Boolean = true,
        ): List<BudgetDetailReport> {
            val budgetReportList = mutableListOf<BudgetDetailReport>()
            moim.budgetList.forEachIndexed { index, budget ->
                if (index > 0) {
                    val accountItems =
                        accountList.filter { item -> budget.category == item.category }
                    val accountSum = accountItems.sumOf { accountItem -> accountItem.amount }
                    val userList = mutableSetOf<User>()
                    accountItems.forEach {
                        it.user?.let { user ->
                            userList.add(user)
                        }
                    }
                    budgetReportList.add(
                        BudgetDetailReport(budget,
                            accountSum,
                            accountItems,
                            userList.toList(),
                            isMonthly)
                    )
                }
            }
            return budgetReportList
        }

        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            val thisMonthAccountList =
                accountItemLocalDataSource.getMonthlyList(moim.id, onlyExpend = true)

            if (!isMonthly) {
                val thisWeekAccountList = thisMonthAccountList
                    .filter { accountItem ->
                        DateUtil.containsThisWeek(accountItem.date, appPreference.startWeek)
                    }
                getBudgetDetailList(moim, thisWeekAccountList, false)
            } else {
                getBudgetDetailList(moim, thisMonthAccountList)
            }
        } ?: emptyList()
    }

    override suspend fun loadBudgetDetailReportPaging(isMonthly: Boolean) = Pager(
        config = PagingConfig(
            pageSize = 1,
            enablePlaceholders = false
        ),
        pagingSourceFactory = {
            BudgetDetailPagingSource(appPreference,
                moimLocalDataSource,
                accountItemLocalDataSource,
                isMonthly)
        }
    ).flow

    override suspend fun loadExpendReportForYear(date: Date): List<ExpendReport> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            val expendReportList = mutableListOf<ExpendReport>()
            val yearString = DateUtil.getQueryDateStringForDate(date, DateUtil.QueryDate.YEAR)
            val month =
                listOf("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
            val sumOfMonthMap = reportLocalDataSource.getYearReportMap(moim.id, yearString)

            for (i in month.indices) {
                val yearMonth = yearString + month[i]
                val sum = sumOfMonthMap.getOrDefault(yearMonth, 0)
                val dayOfWeekSumMap = reportLocalDataSource.getDayOfWeekSumMap(moim.id, yearMonth)

                val userReport = reportLocalDataSource.getReportByUser(moim, yearMonth)
                val categoryReport = reportLocalDataSource.getReportByCategory(moim, yearMonth)
                val assetsReport = reportLocalDataSource.getReportByAssets(moim, yearMonth)

                expendReportList.add(ExpendReport(
                    sum,
                    dayOfWeekSumMap,
                    userReport,
                    categoryReport,
                    assetsReport,
                ))
            }
            return expendReportList
        } ?: emptyList()
    }

    override suspend fun loadCompareReportForMonth(
        prevDate: Date,
        nextDate: Date,
    ): List<CompareItem> {
        fun createTotalItem(prevSum: Long, nextSum: Long, totalSum: Long, target: Target) =
            CompareItem(Category(appPreference.moimId, target, name = "합계"),
                prevAmount = prevSum,
                nextAmount = nextSum,
                sum = totalSum,
                target = target,
                isUnderLine = true)

        val compareList =
            reportLocalDataSource.getCompareList(appPreference.moimId, prevDate, nextDate)

        var prevSum: Long = 0
        var nextSum: Long = 0
        var totalSum: Long = 0
        val distinctCompareList = mutableListOf<CompareItem>()

        //TODO 이거의 정체는..? -> sum 으로 해결 못하나??
        compareList.forEachIndexed { i, item ->
            var addItem = item
            if (i > 0) {
                val prevItem = compareList[i - 1]
                if (item.category.id == prevItem.category.id) {
                    val lastItem = distinctCompareList.removeLastOrNull() ?: prevItem
                    addItem =
                        item.copy(prevAmount = item.prevAmount + lastItem.prevAmount,
                            nextAmount = item.nextAmount + lastItem.nextAmount,
                            sum = item.sum + lastItem.sum)
                }
            }
            distinctCompareList.add(addItem)
        }

        val sortedItemList = mutableListOf<CompareItem>()
        sortedItemList.addAll(distinctCompareList.filter { it.target == Target.INCOME }
            .sortedBy { it.category.id })
        sortedItemList.addAll(distinctCompareList.filter { it.target == Target.TRANSFER }
            .sortedBy { it.category.id })
        sortedItemList.addAll(distinctCompareList.filter { it.target == Target.EXPEND }
            .sortedBy { it.category.id })

        val itemList = mutableListOf<CompareItem>()
        sortedItemList.forEachIndexed { i, item ->
            if (i > 0) {
                if (item.target != sortedItemList[i - 1].target) {
                    itemList.add(createTotalItem(prevSum,
                        nextSum,
                        totalSum,
                        sortedItemList[i - 1].target))
                    prevSum = 0
                    nextSum = 0
                    totalSum = 0

                    item.isHeader = true
                }
            } else {
                item.isHeader = true
            }
            itemList.add(item)

            prevSum += item.prevAmount
            nextSum += item.nextAmount
            totalSum += item.sum

            //합계
            if (i == distinctCompareList.size - 1) {
                itemList.add(createTotalItem(prevSum, nextSum, totalSum, item.target))
            }
        }

        return itemList
    }

    private suspend fun isWeeklyAchievedGoal(totalBudget : Budget): Boolean {
        var achieve = false

        val calDate = DateUtil.convertPreferenceDate(appPreference.achieveCalDate)
        val canCalculator2 = calDate?.let { !DateUtil.containsThisWeek(it, appPreference.startWeek) } ?: true
        if (canCalculator2) {
            appPreference.saveAchieveCalDate(DateUtil.getPreferenceDate())

            val prevWeeklyCalendar = DateUtil.today.apply { add(Calendar.DATE, -7) }
            val prevWeeklyDate = prevWeeklyCalendar.time

            val achieveDate = DateUtil.convertPreferenceDate(appPreference.achieveDate)
            val canCalculator =
                achieveDate?.let { !DateUtil.containsTargetWeek(it, prevWeeklyDate, appPreference.startWeek) } ?: true
            if (canCalculator) {
                val prevWeeklyList =
                    accountItemLocalDataSource.getWeeklyList(appPreference.moimId,
                        prevWeeklyDate,
                        true,
                        appPreference.startWeek)
                if (prevWeeklyList.isEmpty()) {
                    return false
                }
                val prevWeeklySum = prevWeeklyList.sumOf { accountItem -> accountItem.amount }
                val prevWeeklyBudget =
                    MoneyUtil.convertFullMoney(MoneyUtil.getWeeklyBudget(
                        totalBudget.budget,
                        prevWeeklyCalendar))
                if (prevWeeklyBudget > prevWeeklySum) {
                    achieve = true
                    appPreference.saveAchieveDate(DateUtil.getPreferenceDate())
                }
            }
        }
        return achieve
    }
}