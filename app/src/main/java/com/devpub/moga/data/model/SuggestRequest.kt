package com.devpub.moga.data.model

import java.io.Serializable

data class SuggestRequest(
    val userId: Long,
    val content: String
) : Serializable
