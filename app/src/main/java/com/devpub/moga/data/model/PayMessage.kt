package com.devpub.moga.data.model

import com.devpub.moga.data.model.entity.CategoryEntity
import com.devpub.moga.domain.model.NoteType
import com.devpub.moga.domain.model.Target
import com.devpub.moga.util.DateUtil
import java.io.Serializable
import java.util.*

class PayMessage(var time: Date = DateUtil.todayDate) : Serializable {
    var target: Target? = null
    var assetsName: String = ""
    var payMoney: Long = 0
    var storeName: String = ""
    var content: String = ""
    var isBank: Boolean = false
    var category: CategoryEntity? = null
    var type: NoteType = NoteType.NONE
}