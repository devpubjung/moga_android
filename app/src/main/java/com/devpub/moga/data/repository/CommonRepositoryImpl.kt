package com.devpub.moga.data.repository

import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.remote.CommonService
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.repository.CommonRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CommonRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val commonService: CommonService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : CommonRepository {

    override fun ping() = flow {
        emit(commonService.ping().success)
    }

    override suspend fun sendSuggest(content: String): Boolean {
        val response = commonService.sendSuggest(appPreference.userId, content)
        return response.success
    }

    override suspend fun parseSms(smsRequest: SmsRequest): PayMessage {
        val response = commonService.parseSms(smsRequest)
        return response.data!!
    }

    override suspend fun parseNoti(notiRequest: NotiRequest): PayMessage {
        val response = commonService.parseNoti(notiRequest)
        return response.data!!
    }

    override suspend fun getCategoryInfo(accountItem: AccountItem) = flow {
        accountItem.content?.let { name ->
            val response = commonService.getCategoryInfo(name)
            response.data?.let {
                emit(response.data)
            } ?: emit("없음")
        } ?: emit("없음")

    }
}