package com.devpub.moga.data.model.entity

import java.io.Serializable
import com.devpub.moga.domain.model.Target

data class CompareItemEntity(
    val category: CategoryEntity,
    val target: Target,
    val prevAmount: Long,
    val nextAmount: Long,
    val sum: Long,
) : Serializable
