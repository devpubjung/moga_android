package com.devpub.moga.data.api

import com.devpub.moga.data.model.dto.BudgetSettingRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.dto.BudgetRequest
import com.devpub.moga.domain.model.Budget
import retrofit2.http.Body
import retrofit2.http.POST

interface BudgetApi {

    @POST("budget/save")
    suspend fun saveBudget(@Body budgetRequest: BudgetRequest) : ApiDataResponse<Budget>

    @POST("budget/saveList")
    suspend fun saveBudgetList(@Body budgetSettingRequest: BudgetSettingRequest) : ApiDataResponse<List<Budget>>
}