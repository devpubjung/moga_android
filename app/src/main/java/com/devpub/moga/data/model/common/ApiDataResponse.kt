package com.devpub.moga.data.model.common


class ApiDataResponse<T> : CommonResponse() {

    val data: T? = null
}