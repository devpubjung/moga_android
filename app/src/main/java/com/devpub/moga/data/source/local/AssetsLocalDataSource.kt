package com.devpub.moga.data.source.local

import com.devpub.moga.data.database.dao.AssetsDao
import com.devpub.moga.data.model.entity.AssetsEntity
import com.devpub.moga.data.model.mapper.mapToAssets
import com.devpub.moga.data.model.mapper.mapToEntity
import com.devpub.moga.domain.model.Assets
import javax.inject.Inject

class AssetsLocalDataSource @Inject constructor(
    private val assetsDao: AssetsDao,
) {

    suspend fun load() = assetsDao.getAssets().map { it.mapToAssets() }

    suspend fun add(moimId: Long, name:String) : List<Assets> {
        assetsDao.insert(AssetsEntity(moimId = moimId, name = name))
        return load()
    }

    suspend fun add(assets: Assets) {
        assetsDao.insert(assets.mapToEntity())
    }

    suspend fun remove(item: Assets) : List<Assets>{
        assetsDao.delete(item.mapToEntity())
        return load()
    }
}