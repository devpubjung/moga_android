package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.CategoryApi
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.mapper.mapToRequest
import com.devpub.moga.domain.model.Category
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CategoryService @Inject constructor(
    private val categoryApi: CategoryApi,
    private val ioDispatcher: CoroutineDispatcher,
) {
    suspend fun save(category: Category): ApiDataResponse<Category> {
        return categoryApi.saveCategory(category.mapToRequest())
    }

    suspend fun saveAll(categoryList: List<Category>): ApiDataResponse<List<Category>> {
        return categoryApi.saveCategoryList(categoryList.map { it.mapToRequest() })
    }

    suspend fun remove(category: Category) {
        categoryApi.deleteCategory(category.moimId, category.id)
    }
}