package com.devpub.moga.data.model.entity

import android.graphics.Color
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.TypeConverters
import com.devpub.moga.DEFAULT_MONTHLY_BUDGET
import com.devpub.moga.data.database.converter.MoimConverters
import com.devpub.moga.util.MoneyUtil
import java.io.Serializable

@Entity(tableName = "budget", primaryKeys = ["moimId", "category"])
@TypeConverters(MoimConverters::class)
class BudgetEntity(val moimId: Long) : Serializable {
    @Embedded
    var category: CategoryEntity = CategoryEntity(moimId)

    val categoryId: Long get() = category.id

    //예산은 만원(10,000) 뺀 숫자 (ex. 1,000,000원 -> 100)
    var budget: Int = DEFAULT_MONTHLY_BUDGET
    val weeklyBudget: Int get() = MoneyUtil.getWeeklyBudget(budget)
    var maxBudget: Int = (DEFAULT_MONTHLY_BUDGET * 1.5).toInt()

    var color: Int = Color.parseColor("#FEDBD0")

    var index: Int = 0
}
