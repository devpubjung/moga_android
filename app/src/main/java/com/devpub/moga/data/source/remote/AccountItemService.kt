package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.AccountItemApi
import com.devpub.moga.data.model.mapper.mapToRequest
import com.devpub.moga.domain.model.AccountItem
import javax.inject.Inject

class AccountItemService @Inject constructor(
    private val accountItemApi: AccountItemApi,
) {

    suspend fun saveAccountItem(accountItem: AccountItem) =
        accountItemApi.saveAccountItem(accountItem.mapToRequest())

    suspend fun deleteAccountItem(accountItem: AccountItem) =
        accountItemApi.deleteAccountItem(accountItem.id, accountItem.moimId)
}