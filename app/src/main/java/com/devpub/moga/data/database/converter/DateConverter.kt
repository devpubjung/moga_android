package com.devpub.moga.data.database.converter

import androidx.room.TypeConverter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateConverter {
    val dateFormat : DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    @TypeConverter
    fun toDate(timestamp: String?): Date? {
        return timestamp?.let { dateFormat.parse(it) }
    }

    @TypeConverter
    fun toTimestamp(date: Date?): String? {
        return dateFormat.format(date)
    }
}