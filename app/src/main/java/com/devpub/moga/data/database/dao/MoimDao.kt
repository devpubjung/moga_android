package com.devpub.moga.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.devpub.moga.data.model.entity.CategoryEntity
import com.devpub.moga.data.model.entity.MoimEntity

@Dao
interface MoimDao: BaseDao<MoimEntity> {
    @Query("SELECT * FROM moim WHERE id = :id")
    suspend fun getMoim(id: Long) : MoimEntity?

    @Query("SELECT categoryList FROM moim WHERE id = :moimId")
    suspend fun getCategoryList(moimId: Long) : List<CategoryEntity>
}