package com.devpub.moga.data.api

import com.devpub.moga.data.model.dto.AccountItemRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.common.CommonResponse
import com.devpub.moga.domain.model.AccountItem
import retrofit2.http.*

interface AccountItemApi {

    @POST("item/save")
    suspend fun saveAccountItem(@Body accountItem: AccountItemRequest): ApiDataResponse<*>

    @POST("item/saveList")
    suspend fun saveAccountItemList(@Body accountItemList: List<AccountItem>): ApiDataResponse<List<AccountItem>>

    @GET("item/{id}")
    suspend fun getAccountItem(@Path("id") id:Long, @Query("moimId") moimId: Long): ApiDataResponse<AccountItem>

    @DELETE("item/{id}")
    suspend fun deleteAccountItem(@Path("id") id:Long, @Query("moimId") moimId: Long): CommonResponse
}