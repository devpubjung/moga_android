package com.devpub.moga.data.source.remote

import com.devpub.moga.data.api.MoimApi
import com.devpub.moga.domain.model.Moim
import com.devpub.moga.data.model.dto.UpdatedMoimData
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.dto.JoinedMoimInfoRequest
import javax.inject.Inject

class MoimService @Inject constructor(
    private val moimApi: MoimApi
) {
    suspend fun saveMoim(moim: Moim): ApiDataResponse<Moim> {
        return moimApi.saveMoim(moim)
    }

    suspend fun joinMoim(joinedMoimInfoRequest: JoinedMoimInfoRequest): ApiDataResponse<Moim> {
        return moimApi.joinMoim(joinedMoimInfoRequest)
    }

    suspend fun syncMoimData(
        id: Long,
        userId: Long,
        date: String?,
        type: Int,
    ): ApiDataResponse<UpdatedMoimData> {
        return moimApi.getMoimData(id, userId, date, type)
    }
}