package com.devpub.moga.data.model.dto

import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.domain.model.AssetType
import com.devpub.moga.domain.model.NoteType
import com.devpub.moga.domain.model.Target
import java.io.Serializable

data class AccountItemRequest(
    val moimId: Long,
    val id: Long,
    val target: Target,
    val date: String,
    val userId: Long,
    val amount: Long = 0,
    val assets: String,
    val categoryId: Long,
    val content: String? = null,
    val memo: String? = null,
    val expend: Boolean = true,
    val share: Boolean = true,
    val report: Boolean = true,
    val ownerId: Long = DEFAULT_ID,
    val type: NoteType = NoteType.NONE,
    val assetType : AssetType = AssetType.CASH,
) : Serializable
