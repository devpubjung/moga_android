package com.devpub.moga.data.model.entity

import java.io.Serializable

data class UserSumEntity(
    val user: UserEntity,
    val sum: Long,
) : Serializable

data class CategorySumEntity(
    val category: CategoryEntity,
    val sum: Long,
) : Serializable

data class AssetsSumEntity(
    val assets: AssetsEntity,
    val sum: Long,
) : Serializable
