package com.devpub.moga.data.repository

import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.data.extension.launchBackground
import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.AccountItemLocalDataSource
import com.devpub.moga.data.source.local.MoimLocalDataSource
import com.devpub.moga.data.source.local.UserLocalDataSource
import com.devpub.moga.data.source.remote.AccountItemService
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.repository.AccountItemRepository
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.CoroutineDispatcher
import java.util.*
import javax.inject.Inject

class AccountItemRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val accountItemService: AccountItemService,
    private val accountItemLocalDataSource: AccountItemLocalDataSource,
    private val moimLocalDataSource: MoimLocalDataSource,
    private val userLocalDataSource: UserLocalDataSource,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : AccountItemRepository {

    override suspend fun loadAccountItem(itemId: Long?, date: Date?): AccountItem {
        return if (itemId != null) {
            accountItemLocalDataSource.getItem(itemId)
        } else {
            with(appPreference) {
                val user = userLocalDataSource.getUser(userId)
                AccountItem(
                    moimId = moimId,
                    id = "$moimId$moimUserIndex$accountItemIndex".toLong(),
                    user = user,
                    ownerId = user?.id ?: DEFAULT_ID,
                    date = date ?: DateUtil.todayDate,
                )
            }
        }.run {
            copy(isWriteMe = ownerId == appPreference.userId || id == appPreference.userId)
        }
    }

    override suspend fun saveAccountItem(accountItem: AccountItem) {
        if (accountItem.target == Target.TRANSFER) {
            accountItem.report = accountItem.expend
        }
        accountItem.ownerId = appPreference.userId
        accountItemLocalDataSource.save(accountItem)
        appPreference.saveAccountItemIndex(appPreference.accountItemIndex + 1)

        if (accountItem.share) {
            ioDispatcher.launchBackground { accountItemService.saveAccountItem(accountItem) }
        }
    }

    override suspend fun saveAccountItemList(accountItemList: List<AccountItem>) {
        accountItemLocalDataSource.saveAll(accountItemList)
    }

    override suspend fun updateAccountItemForUser(user: User) {
        accountItemLocalDataSource.updateUserInList(appPreference.moimId, user)
    }

    override suspend fun loadMonthlyAccountList(date: Date): List<AccountItem> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            accountItemLocalDataSource.getMonthlyList(moim.id, date)
        } ?: emptyList()
    }

    override suspend fun loadDayAccountList(date: Date): List<AccountItem> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            accountItemLocalDataSource.getDayList(moim.id, date)
        } ?: emptyList()
    }

    override suspend fun removeItem(accountItem: AccountItem) {
        accountItemLocalDataSource.remove(accountItem)
        ioDispatcher.launchBackground { accountItemService.deleteAccountItem(accountItem) }
    }

    //    override suspend fun getAccountItem(id: Long, moimId: Long): ApiDataResponse<AccountItem> {
//        return withContext(apiDispatcher) { accountItemApi.getAccountItem(id, moimId) }
//    }
}