package com.devpub.moga.data.api

import com.devpub.moga.data.model.external.KosisResponse
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface KosisApi {

    @GET("statisticsData.do")
    suspend fun loadByIncome(@QueryMap params: Map<String, String>): List<KosisResponse>

    @GET("statisticsData.do")
    suspend fun loadByUser(@QueryMap params: Map<String, String>): List<KosisResponse>
}
