package com.devpub.moga.data.repository

import com.devpub.moga.data.api.ApiService
import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.entity.AccountItemEntity
import com.devpub.moga.data.model.external.KakaoAddress
import com.devpub.moga.data.model.mapper.mapToRequest
import com.devpub.moga.domain.repository.MessageRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MessageRepositoryImpl : MessageRepository {
    private val commonApi = ApiService.commonApi
    private val accountItemApi = ApiService.accountItemApi
    private val kakaoApi = ApiService.kakaoApi
    private val apiDispatcher = Dispatchers.IO


    override suspend fun saveAccountItem(accountItemEntity: AccountItemEntity) {
        accountItemApi.saveAccountItem(accountItemEntity.mapToRequest())
    }

    override suspend fun searchKeyword(keyword: String): KakaoAddress {
        return withContext(apiDispatcher) {
            kakaoApi.searchKeyword(mapOf(
                "page" to "1",
                "size" to "15",
                "sort" to "accuracy",
                "query" to keyword
            ))
        }
    }

    override suspend fun parseSms(smsRequest: SmsRequest): ApiDataResponse<PayMessage> {
        return withContext(apiDispatcher) { commonApi.parseSms(smsRequest) }
    }

    override suspend fun parseNoti(notiRequest: NotiRequest): ApiDataResponse<PayMessage> {
        return withContext(apiDispatcher) { commonApi.paseNoti(notiRequest) }
    }
}