package com.devpub.moga.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.devpub.moga.data.model.dto.*
import com.devpub.moga.data.model.entity.AssetsSumEntity
import com.devpub.moga.data.model.entity.CategorySumEntity
import com.devpub.moga.data.model.entity.UserSumEntity

@Dao
interface ReportDao {
    @Query("SELECT strftime('%Y', date) year, strftime('%m', date) month, (sum(amount) / 10000) sum FROM accountItem WHERE moimId = :moimId AND target in ('EXPEND', 'TRANSFER') AND expend = 1 AND strftime('%Y', date) = :year GROUP BY strftime('%m', date) ORDER BY date ASC")
    suspend fun getYearReport(moimId: Long, year: String): List<MonthExpendReport>

    @Query("SELECT ((strftime(\"%w\", date)  + 6) % 7) dayOfWeek, (sum(amount) / 10000) sum FROM accountItem WHERE moimId = :moimId AND target in ('EXPEND', 'TRANSFER') AND expend = 1 AND strftime(\"%Y%m\", date) = :yearMonth GROUP BY strftime(\"%w\", date) ORDER BY 1 ASC")
    suspend fun getDayOfWeekSumReport(moimId: Long, yearMonth: String): List<DayOfWeekSum>

    @Query("SELECT category, sum(amount) sum FROM accountItem WHERE moimId = :moimId AND target in ('EXPEND', 'TRANSFER') AND expend = 1 AND strftime(\"%Y%m\", date) = :yearMonth GROUP BY category ORDER BY 1 ASC")
    suspend fun getCategorySumReport(moimId: Long, yearMonth: String): List<CategorySumEntity>

    @Query("SELECT user, sum(amount) sum FROM accountItem WHERE moimId = :moimId AND target in ('EXPEND', 'TRANSFER') AND expend = 1 AND strftime(\"%Y%m\", date) = :yearMonth GROUP BY user ORDER BY 1 ASC")
    suspend fun getUserSumReport(moimId: Long, yearMonth: String): List<UserSumEntity>

    @Query("SELECT assets, sum(amount) sum FROM accountItem WHERE moimId = :moimId AND target in ('EXPEND', 'TRANSFER') AND expend = 1 AND strftime(\"%Y%m\", date) = :yearMonth GROUP BY assets ORDER BY 1 ASC")
    suspend fun getAssetsSumReport(moimId: Long, yearMonth: String): List<AssetsSumEntity>

}
