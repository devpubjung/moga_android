package com.devpub.moga.data.model.external

import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
"meta": {
"same_name": {
"region": [],
"keyword": "카카오프렌즈",
"selected_region": ""
},
"pageable_count": 14,
"total_count": 14,
"is_end": true
},
"documents": [
{
"place_name": "카카오프렌즈 코엑스점",
"distance": "418",
"place_url": "http://place.map.kakao.com/26338954",
"category_name": "가정,생활 > 문구,사무용품 > 디자인문구 > 카카오프렌즈",
"address_name": "서울 강남구 삼성동 159",
"road_address_name": "서울 강남구 영동대로 513",
"id": "26338954",
"phone": "02-6002-1880",
"category_group_code": "",
"category_group_name": "",
"x": "127.05902969025047",
"y": "37.51207412593136"
},
}
 **/
data class KakaoAddress(
    val meta: Meta,
    val documents: List<Documents>
) : Serializable

data class Meta(
    @SerializedName("pageable_count")
    val pageableCount: Int = 0,
    @SerializedName("total_count")
    val totalCount: Int = 0,
    @SerializedName("is_end")
    val isEnd: Boolean = true,
): Serializable

data class Documents(
    @SerializedName("place_name")
    val placeName: String,
    @SerializedName("distance")
    val distance: String,
    @SerializedName("place_url")
    val placeUrl: String,
    @SerializedName("category_name")
    val categoryName: String,
    @SerializedName("address_name")
    val addressName: String,
    @SerializedName("road_address_name")
    val roadAddressName: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("category_group_code")
    val categoryGroupCode: String,
    @SerializedName("category_group_name")
    val categoryGroupName: String,
): Serializable

enum class GroupCode(val code: String, val codeName: String): Serializable {
//    MT1    대형마트
//    CS2    편의점
//    PS3    어린이집, 유치원
//    SC4    학교
//    AC5    학원
//    PK6    주차장
//    OL7    주유소, 충전소
//    SW8    지하철역
//    BK9    은행
//    CT1    문화시설
//    AG2    중개업소
//    PO3    공공기관
//    AT4    관광명소
//    AD5    숙박
//    FD6    음식점
//    CE7    카페
//    HP8    병원
//    PM9    약국
//    I99("M1"),
//    I199("M2"),
//    I299("M3"),
//    I399("M4"),
//    I499("M5"),
//    I599("M6"),
//    I699("M7"),
//    I700("M8"),
}
