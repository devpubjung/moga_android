package com.devpub.moga.data.interactor

import android.content.Context
import android.util.Log
import com.devpub.common.util.TAG
import com.devpub.moga.helper.KakaoHelper
import com.kakao.sdk.link.LinkClient
import com.kakao.sdk.template.model.Button
import com.kakao.sdk.template.model.Content
import com.kakao.sdk.template.model.FeedTemplate
import com.kakao.sdk.template.model.Link
import com.kakao.sdk.user.model.User
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class KakaoInteractor @Inject constructor(
    val context: Context,
    val kakaoHelper: KakaoHelper,
) {

    inline fun login(
        crossinline success: (User) -> Unit,
        crossinline fail: (String) -> Unit,
    ) {
        kakaoHelper.login(context) { token, error ->
            if (error != null) {
                fail("로그인 실패 $error")
            } else if (token != null) {
                kakaoHelper.getUserInfo { user, error ->
                    if (user != null) {
                        success(user)
                    } else {
                        fail("로그인 실패 $error")
                    }
                }
            }
        }
    }

    fun logout() {
        kakaoHelper.logout { }
    }

    inline fun getUserInfo(
        crossinline success: (User) -> Unit,
        crossinline fail: () -> Unit,
    ) {
        kakaoHelper.getTokenInfo { accessTokenInfo, _ ->
            if (accessTokenInfo != null) {
                kakaoHelper.getUserInfo { user, error ->
                    if (user != null) {
                        success(user)
                    } else {
                        fail()
                    }
                }
            } else {
                fail()
            }
        }
    }

    fun isKakaoLinkAvailable() = LinkClient.instance.isKakaoLinkAvailable(context)

    fun sendInviteMessage(code: String) {
        val params = mapOf("type" to "invite", "code" to code)
        val link = Link(
            androidExecutionParams = params,
            iosExecutionParams = params
        )
        val defaultFeed = FeedTemplate(
            content = Content(
                title = "가계부 함께쓰기",
                description = "가계부에 참여해서 수입/지출 내역을 함께 공유하고 관리하세요.",
                imageUrl = "https://lh3.googleusercontent.com/vOsQzaypRJCW8ZDh1vUF9e-X_qN6iUym5SM7y0RBCKixUzrNZ9QAl28L4K3hjUykR7aK",
                link = link,
                imageWidth = 512,
                imageHeight = 250
            ),
            buttons = listOf(Button("참여하기", link))
        )
        LinkClient.instance.defaultTemplate(context, defaultFeed) { linkResult, error ->
            if (error != null) {
                Log.e(TAG, "카카오링크 보내기 실패", error)
            } else if (linkResult != null) {
                Log.d(TAG, "카카오링크 보내기 성공 ${linkResult.intent}")
                context.startActivity(linkResult.intent)

                // 카카오링크 보내기에 성공했지만 아래 경고 메시지가 존재할 경우 일부 컨텐츠가 정상 동작하지 않을 수 있습니다.
                Log.w(TAG, "Warning Msg: ${linkResult.warningMsg}")
                Log.w(TAG, "Argument Msg: ${linkResult.argumentMsg}")
            }
        }
    }
}