package com.devpub.moga.data.repository

import com.devpub.common.model.ListItem
import com.devpub.moga.MONEY_SLICE
import com.devpub.moga.data.model.dto.Standard
import com.devpub.moga.data.model.external.Age
import com.devpub.moga.data.model.external.Income
import com.devpub.moga.data.model.external.KosisResponse
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.ReportLocalDataSource
import com.devpub.moga.data.source.remote.KosisService
import com.devpub.moga.domain.model.KosisData
import com.devpub.moga.domain.model.KosisItem
import com.devpub.moga.domain.repository.KosisRepository
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.CoroutineDispatcher
import java.util.*
import javax.inject.Inject

class KosisRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val kosisService: KosisService,
    private val reportLocalDataSource: ReportLocalDataSource,
    private val ioDispatcher: CoroutineDispatcher,
) : KosisRepository {

    override suspend fun getByIncomeData(date: Date): List<ListItem> {
        fun getIncome(totalIncome: Long): Income {
            val income = totalIncome / MONEY_SLICE
            return when {
                income in 100..199 -> Income.I199
                income in 200..299 -> Income.I299
                income in 300..399 -> Income.I399
                income in 400..499 -> Income.I499
                income in 500..599 -> Income.I599
                income in 600..699 -> Income.I699
                income >= 700 -> Income.I700
                else -> Income.I99
            }
        }

        val moimId = appPreference.moimId
        val standard = reportLocalDataSource.getOthersStandard(appPreference.userId, moimId, date)
        val incomeList = kosisService.getByIncomeData()
            .filter { it.columnId1 == getIncome(standard.income).code }
        val accountList = reportLocalDataSource.getOthersData(moimId, date)
        val groupByMap = mutableMapOf<Long, Long>()
        accountList.groupBy { it.category?.kosisId }.forEach {
            it.key?.let { key ->
                groupByMap[key] = it.value.sumOf { item -> item.amount }
            }
        }
        return convertKosisData(incomeList, standard, groupByMap)
    }

    override suspend fun getByAgeData(date: Date): List<ListItem> {
        fun getAge(ageRange: Int): Age {
            return when {
                ageRange == 4 -> Age.A49
                ageRange == 5 -> Age.A59
                ageRange >= 6 -> Age.A60
                else -> Age.A39
            }
        }

        val moimId = appPreference.moimId
        val standard = reportLocalDataSource.getOthersStandard(appPreference.userId, moimId, date)
        val ageList = kosisService.getByAgeData()
            .filter { it.columnId1 == getAge(standard.ageRange).code }
        val accountList = reportLocalDataSource.getOthersData(moimId, date)
        val groupByMap = mutableMapOf<Long, Long>()
        accountList.groupBy { it.category?.kosisId }.forEach {
            it.key?.let { key ->
                groupByMap[key] = it.value.sumOf { item -> item.amount }
            }
        }
        return convertKosisData(ageList, standard, groupByMap, true)
    }


    private fun convertKosisData(
        itemList: List<KosisResponse>,
        standard: Standard,
        groupByMap: Map<Long, Long>,
        isByAge: Boolean = false,
    ): List<ListItem> {
        val age = if (standard.ageRange <= 1) {
            "10대"
        } else {
            "${standard.ageRange}0대"
        }
        var compareTitle = if (isByAge) {
            "$age 소비그룹"
        } else {
            "소비그룹"
        }
        var compareSubTitle = "이번달(${DateUtil.thisMonth()}월) 1일~현재까지, 통계청 기준"
        var chartTitle = if (isByAge) {
            "$age 전체 평균"
        } else {
            "전체 평균"
        }
        var othersTitle = if (isByAge) {
            "$age 평균"
        } else {
            "평균"
        }
        var myTotal = groupByMap.map { it.value }.sum()
        var totalData = 0L
        val result = mutableListOf<KosisItem>()
        var userNum = 1F
        itemList.forEach { item ->
            when (item.columnId2) {
                "2" -> userNum = standard.userNumber / item.data //가구원수
                "7" -> {
                } //가구분포
                "D" -> {
                    if (!isByAge) {
                        compareTitle = "월 ${item.columnName1} 소득그룹"
                        chartTitle = "${item.columnName1} 전체 평균"
                        othersTitle = "${item.columnName1} 평균"
                    }
                    totalData = item.data.toLong()
                } //가계지출
                "F" -> {
                } //소비지출
                "W" -> {
                    val categoryPid = 13L
                    val columnName = item.columnName2
                    val value = item.data / userNum
                    result.add(KosisItem(categoryPid,
                        columnName,
                        groupByMap[categoryPid] ?: 0,
                        value.toLong()))
                } //비소비지출
                else -> {
                    val column = item.columnName2.replace(" · ", "/").split(".")
                    if (column.size > 1) {
                        val categoryPid = column[0].toLong()
                        val columnName = column[1]
                        val value = item.data / userNum
                        result.add(KosisItem(categoryPid,
                            columnName,
                            groupByMap[categoryPid] ?: 0,
                            value.toLong()))
                    }
                }
            }
        }
        return mutableListOf<ListItem>(KosisData(compareTitle,
            compareSubTitle,
            chartTitle,
            othersTitle,
            myTotal,
            totalData,
            isByAge))
            .apply {
                addAll(result)
            }
    }

}