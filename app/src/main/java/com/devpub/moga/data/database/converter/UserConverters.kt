package com.devpub.moga.data.database.converter

import androidx.room.TypeConverter
import com.devpub.moga.domain.model.Gender
import com.devpub.moga.domain.model.Provider
import java.lang.Exception
import java.util.*

class UserConverters {

    @TypeConverter
    fun toProvider(value: String) = try {
        enumValueOf<Provider>(value.toUpperCase(Locale.getDefault()))
    } catch (e : Exception) {
        Provider.KAKAO
    }

    @TypeConverter
    fun fromProvider(value: Provider) = value.name

    @TypeConverter
    fun toGender(value: String) = try {
        enumValueOf<Gender>(value.toUpperCase(Locale.getDefault()))
    } catch (e : Exception) {
        Gender.MALE
    }

    @TypeConverter
    fun fromGender(value: Gender) = value.name
}
