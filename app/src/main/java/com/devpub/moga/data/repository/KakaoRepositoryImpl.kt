package com.devpub.moga.data.repository

import com.devpub.moga.data.api.KakaoApi
import com.devpub.moga.data.model.external.KakaoAddress
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.repository.KakaoRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class KakaoRepositoryImpl @Inject constructor(
    private val kakaoApi: KakaoApi,
    @IoDispatcher private val apiDispatcher: CoroutineDispatcher
) : KakaoRepository {

    override suspend fun searchKeyword(keyword: String): KakaoAddress {
        return withContext(apiDispatcher) {
            kakaoApi.searchKeyword(mapOf(
                "page" to "1",
                "size" to "15",
                "sort" to "accuracy",
                "query" to keyword
            ))
        }
    }
}