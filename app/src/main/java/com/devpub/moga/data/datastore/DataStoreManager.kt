package com.devpub.moga.data.datastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

abstract class DataStoreManager(private val dataStore: DataStore<Preferences>) {

    protected suspend fun saveValue(key: String, value: String) {
        val wrappedKey = stringPreferencesKey(key)
        dataStore.edit {
            it[wrappedKey] = value
        }
    }

    protected suspend fun saveValue(key: String, value: Int) {
        val wrappedKey = intPreferencesKey(key)
        dataStore.edit {
            it[wrappedKey] = value
        }
    }

    protected suspend fun saveValue(key: String, value: Double) {
        val wrappedKey = doublePreferencesKey(key)
        dataStore.edit {
            it[wrappedKey] = value
        }
    }

    protected suspend fun saveValue(key: String, value: Long) {
        val wrappedKey = longPreferencesKey(key)
        dataStore.edit {
            it[wrappedKey] = value
        }
    }

    protected suspend fun saveValue(key: String, value: Boolean) {
        val wrappedKey = booleanPreferencesKey(key)
        dataStore.edit {
            it[wrappedKey] = value
        }
    }

    protected suspend fun getStringValue(key: String, default: String = ""): String {
        val wrappedKey = stringPreferencesKey(key)
        val valueFlow: Flow<String> = dataStore.data.map {
            it[wrappedKey] ?: default
        }
        return valueFlow.first()
    }

    protected suspend fun getIntValue(key: String, default: Int = 0): Int {
        val wrappedKey = intPreferencesKey(key)
        val valueFlow: Flow<Int> = dataStore.data.map {
            it[wrappedKey] ?: default
        }
        return valueFlow.first()
    }

    protected suspend fun getDoubleValue(key: String, default: Double = 0.0): Double {
        val wrappedKey = doublePreferencesKey(key)
        val valueFlow: Flow<Double> = dataStore.data.map {
            it[wrappedKey] ?: default
        }
        return valueFlow.first()
    }

    protected suspend fun getLongValue(key: String, default: Long = 0L): Long {
        val wrappedKey = longPreferencesKey(key)
        val valueFlow: Flow<Long> = dataStore.data.map {
            it[wrappedKey] ?: default
        }
        return valueFlow.first()
    }

    protected suspend fun getBooleanValue(key: String, default: Boolean = false): Boolean {
        val wrappedKey = booleanPreferencesKey(key)
        val valueFlow: Flow<Boolean> = dataStore.data.map {
            it[wrappedKey] ?: default
        }
        return valueFlow.first()
    }
}