package com.devpub.moga.data.database.dao

import androidx.room.*

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(objs: List<T>)

    @Delete
    suspend fun delete(obj: T)

    @Delete
    suspend fun deleteAll(objs: List<T>)

    @Update(onConflict = OnConflictStrategy.ABORT)
    suspend fun update(obj: T)
}
