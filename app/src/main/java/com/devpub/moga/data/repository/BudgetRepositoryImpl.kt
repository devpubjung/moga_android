package com.devpub.moga.data.repository

import com.devpub.moga.DEFAULT_MONTHLY_BUDGET
import com.devpub.moga.data.extension.launchBackground
import com.devpub.moga.data.model.entity.MoimEntity.Companion.createBudget
import com.devpub.moga.data.model.mapper.mapToBudget
import com.devpub.moga.data.model.mapper.mapToEntity
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.MoimLocalDataSource
import com.devpub.moga.domain.model.Budget
import com.devpub.moga.data.source.remote.BudgetService
import com.devpub.moga.di.IoDispatcher
import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.repository.BudgetRepository
import com.devpub.moga.util.ColorUtil
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import kotlin.math.max

class BudgetRepositoryImpl @Inject constructor(
    private val appPreference: AppPreference,
    private val moimLocalDataSource: MoimLocalDataSource,
    private val budgetService: BudgetService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : BudgetRepository {

    private val colors = ColorUtil.generatorColors()

    override suspend fun loadBudgetList(): List<Budget> {
        return moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
            if (moim.budgetList.isNotEmpty()) {
                moim.budgetList[0].maxBudget =
                    max((moim.budgetList[0].budget * 1.5).toInt(), DEFAULT_MONTHLY_BUDGET)

                moim.budgetList.forEachIndexed { index, budget ->
                    budget.index = index
                    budget.color = colors[index % colors.size]
                }
            }
            moim.budgetList
        } ?: emptyList()
    }

    override suspend fun addBudget(list: List<Budget>, category: Category): List<Budget> {
        if (list.find { item -> item.category.name == category.name } == null) {
            moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
                return list.toMutableList().apply {
                    add(moim.mapToEntity().createBudget(category.mapToEntity(), 0).apply {
                        if (list.isNullOrEmpty()) {
                            maxBudget = list[0].budget
                        }
                        color = colors[list.size % colors.size]
                    }.mapToBudget())
                }
            }
        }
        return list
    }

    override suspend fun updateBudgetList(list: List<Budget>) {
        moimLocalDataSource.getMoim(appPreference.moimId)?.let { moim ->
                val saveList = list.toMutableList()
                val removeList = mutableListOf<Budget>()
                for (i in saveList.size - 1 downTo 0) {
                    if (i > 0 && saveList[i].budget <= 0) {
                        removeList.add(saveList[i])
                        saveList.removeAt(i)
                    }
                }
            moimLocalDataSource.updateMoim(moim.copy(budgetList = saveList))

            ioDispatcher.launchBackground {
                budgetService.saveAllBudget(saveList, removeList)
            }
        }
    }

    override suspend fun saveBudget(budget: Budget) {
        ioDispatcher.launchBackground { budgetService.saveBudget(budget) }
    }

    override suspend fun saveBudgetList(
        addBudgetList: List<Budget>,
        removeBudgetList: List<Budget>,
    ) {
        ioDispatcher.launchBackground {
            budgetService.saveAllBudget(addBudgetList,
                removeBudgetList)
        }
    }


}