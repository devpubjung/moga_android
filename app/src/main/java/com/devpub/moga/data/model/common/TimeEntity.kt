package com.devpub.moga.data.model.common

import java.io.Serializable
import java.util.*

abstract class TimeEntity : Serializable {
    @Transient
    var createdDate: Date = Date(System.currentTimeMillis())
    @Transient
    var updatedDate: Date = Date(System.currentTimeMillis())
}