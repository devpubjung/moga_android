package com.devpub.moga.data.model.dto

import com.devpub.moga.domain.model.Target
import java.io.Serializable

data class CategoryRequest(
    val moimId: Long,
    val target: Target,
    val id: Long,
    val parentId: Long,
    val kosisId: Long,
    val name: String,
    val tag: String,
    val ownerId: Long?,
) : Serializable
