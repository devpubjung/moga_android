package com.devpub.moga.data.model.mapper

import com.devpub.moga.data.model.dto.CategoryRequest
import com.devpub.moga.data.model.entity.CategoryEntity
import com.devpub.moga.domain.model.Category

fun CategoryEntity.mapToCategory() =
    Category(
        id = this.id,
        parentId = this.parentId,
        moimId = this.moimId,
        target = this.target,
        kosisId = this.kosisId,
        name = this.name,
        tag = this.tag,
        ownerId = this.ownerId,
        index = this.index
    )

fun Category.mapToEntity() =
    CategoryEntity(
        moimId = this.moimId,
        target = this.target,
    ).also { entity ->
        entity.id = id
        entity.parentId = parentId
        entity.kosisId = kosisId
        entity.name = name
        entity.tag = tag
        entity.ownerId = ownerId
        entity.index = index
    }

fun Category.mapToRequest() =
    CategoryRequest(
        moimId = this.moimId,
        target = this.target,
        id = this.id,
        parentId = this.parentId,
        kosisId = this.kosisId,
        name = this.name,
        tag = this.tag,
        ownerId = this.ownerId
    )