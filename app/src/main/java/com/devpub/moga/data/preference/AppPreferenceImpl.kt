package com.devpub.moga.data.preference

import android.content.Context
import com.devpub.moga.DEFAULT_NUMBER

class AppPreferenceImpl(context: Context) : AppPreference {
    private val preference = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE)
    private val editor = preference.edit()

    override fun setWatchNotiPermission(flag: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_NOTI_PERMISSION, flag)
        editor.apply()
    }

    override val watchNotiPermission: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_NOTI_PERMISSION, false)

    override fun setWatchSmsPermission(flag: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_SMS_PERMISSION, flag)
        editor.apply()
    }

    override val watchSmsPermission: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_SMS_PERMISSION, false)

    override fun setNotiAutoInput(flag: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_LISTEN_NOTI, flag)
        editor.apply()
    }

    override val notiAutoInput: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_LISTEN_NOTI, false)

    override fun setShowNotiIcon(flag: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_SHOW_NOTI, flag)
        editor.apply()
    }

    override val notiIcon: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_SHOW_NOTI, true)

    override fun setPurchasedRemoveAds(flag: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_REMOVE_ADS, flag)
        editor.apply()
    }

    override val purchasedRemoveAds: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_REMOVE_ADS, false)

    override fun saveUserId(id: Long) {
        editor.putLong(KEY_LONG_USER_ID, id)
        editor.apply()
    }

    override val userId: Long
        get() = preference.getLong(KEY_LONG_USER_ID, -1)

    override fun saveMoimId(id: Long) {
        editor.putLong(KEY_LONG_MOIM_ID, id)
        editor.apply()
    }

    override val moimId: Long
        get() = preference.getLong(KEY_LONG_MOIM_ID, DEFAULT_NUMBER.toLong())

    override fun saveMoimUserIndex(index: Int) {
        editor.putInt(KEY_INT_MOIM_USER_INDEX, index)
        editor.apply()
    }

    override val moimUserIndex: Int
        get() = preference.getInt(KEY_INT_MOIM_USER_INDEX, DEFAULT_NUMBER)

    override fun saveAccountItemIndex(index: Long) {
        editor.putLong(KEY_LONG_ACCOUNT_ITEM_INDEX, index)
        editor.apply()
    }

    override val accountItemIndex: Long
        get() = preference.getLong(KEY_LONG_ACCOUNT_ITEM_INDEX, DEFAULT_NUMBER.toLong())

    override fun saveAchieveDate(dateString: String) {
        editor.putString(KEY_STRING_ACHIEVE_DATE, dateString)
        editor.apply()
    }

    override val achieveDate: String
        get() = preference.getString(KEY_STRING_ACHIEVE_DATE, "").toString()

    override fun saveAchieveCalDate(dateString: String) {
        editor.putString(KEY_STRING_ACHIEVE_CAL_DATE, dateString)
        editor.apply()
    }

    override val achieveCalDate: String
        get() = preference.getString(KEY_STRING_ACHIEVE_CAL_DATE, "").toString()

    override fun saveSyncMoimDataTime(dateString: String) {
        editor.putString(KEY_STRING_SYNC_MOIM_DATA_TIME, dateString)
        editor.apply()
    }

    override val syncMoimDataTime: String
        get() = preference.getString(KEY_STRING_SYNC_MOIM_DATA_TIME, "").toString()

    override fun saveUserDate(dateString: String) {
        editor.putString(KEY_STRING_USER_SAVE_DATE, dateString)
        editor.apply()
    }

    override val userSaveDate: String
        get() = preference.getString(KEY_STRING_USER_SAVE_DATE, "").toString()

    override fun saveJoinMoim(join: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_JOIN_MOIM, join)
        editor.apply()
    }

    override val joinMoim: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_JOIN_MOIM, false)

    override fun setNotiShare(flag: Boolean) {
        editor.putBoolean(KEY_BOOLEAN_NOTI_SHARE, flag)
        editor.apply()
    }

    override val notiShare: Boolean
        get() = preference.getBoolean(KEY_BOOLEAN_NOTI_SHARE, true)

    override fun saveMoimSyncDate(dateString: String) {
        editor.putString(KEY_STRING_MOIM_SYNC_DATE, dateString)
        editor.apply()
    }

    override val moimSyncDate: String
        get() = preference.getString(KEY_STRING_MOIM_SYNC_DATE, "").toString()

    override fun saveSyncDate(dateString: String) {
        editor.putString(KEY_STRING_SYNC_DATE, dateString)
        editor.apply()
    }

    override val syncDate: String
        get() = preference.getString(KEY_STRING_SYNC_DATE, "").toString()

    override fun setStartWeek(startWeek: Int) {
        editor.putInt(KEY_INT_START_WEEK, startWeek)
        editor.apply()
    }

    override val startWeek: Int
        get() = preference.getInt(KEY_INT_START_WEEK, 1)

    override fun setStartDayOfMonth(startDayOfMonth: Int) {
        editor.putInt(KEY_INT_START_DAY_OF_MONTH, startDayOfMonth)
        editor.apply()
    }

    override val startDayOfMonth: Int
        get() = preference.getInt(KEY_INT_START_DAY_OF_MONTH, 1)

    override fun clear() {
        editor.clear()
        editor.commit()
    }

    companion object {
        private const val APP_PREF = "APP_PREF"
        private const val KEY_BOOLEAN_NOTI_PERMISSION = "KEY_BOOLEAN_NOTI_PERMISSION"
        private const val KEY_BOOLEAN_SMS_PERMISSION = "KEY_BOOLEAN_SMS_PERMISSION"
        private const val KEY_BOOLEAN_LISTEN_NOTI = "KEY_BOOLEAN_LISTEN_NOTI"
        private const val KEY_BOOLEAN_SHOW_NOTI = "KEY_BOOLEAN_SHOW_NOTI"
        private const val KEY_BOOLEAN_REMOVE_ADS = "KEY_BOOLEAN_REMOVE_ADS"
        private const val KEY_LONG_USER_ID = "KEY_LONG_USER_ID"
        private const val KEY_LONG_MOIM_ID = "KEY_LONG_MOIM_ID"
        private const val KEY_INT_MOIM_USER_INDEX = "KEY_INT_MOIM_USER_INDEX"
        private const val KEY_LONG_ACCOUNT_ITEM_INDEX = "KEY_LONG_ACCOUNT_ITEM_INDEX"
        private const val KEY_STRING_ACHIEVE_DATE = "KEY_STRING_ACHIEVE_DATE"
        private const val KEY_STRING_ACHIEVE_CAL_DATE = "KEY_STRING_ACHIEVE_CAL_DATE"
        private const val KEY_STRING_SYNC_MOIM_DATA_TIME = "KEY_STRING_SYNC_MOIM_DATA_TIME"
        private const val KEY_STRING_USER_SAVE_DATE = "KEY_STRING_USER_SAVE_DATE"
        private const val KEY_BOOLEAN_JOIN_MOIM = "KEY_BOOLEAN_JOIN_MOIM"
        private const val KEY_BOOLEAN_NOTI_SHARE = "KEY_BOOLEAN_NOTI_SHARE"
        private const val KEY_STRING_MOIM_SYNC_DATE = "KEY_STRING_MOIM_SYNC_DATE"
        private const val KEY_STRING_SYNC_DATE = "KEY_STRING_SYNC_DATE"
        private const val KEY_INT_START_WEEK = "KEY_INT_START_WEEK"
        private const val KEY_INT_START_DAY_OF_MONTH = "KEY_INT_START_DAY_OF_MONTH"
    }
}