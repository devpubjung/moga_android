package com.devpub.moga.data.extension

import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException

fun CoroutineDispatcher.launchBackground(block: suspend CoroutineScope.() -> Unit) {
    CoroutineScope(this + coroutineExceptionHandler).launch {
        block()
    }
}

private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
    when (throwable) {
        is SocketTimeoutException -> {
            Log.d("OkHttpClient", "SocketTimeoutException : $throwable")
        }
        else -> {
            Log.d("OkHttpClient", "coroutineExceptionHandler : $throwable")
        }
    }
}