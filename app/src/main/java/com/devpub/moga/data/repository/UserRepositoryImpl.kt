package com.devpub.moga.data.repository

import com.devpub.moga.data.database.AppDatabase
import com.devpub.moga.data.extension.launchBackground
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.source.local.UserLocalDataSource
import com.devpub.moga.data.source.remote.UserService
import com.devpub.moga.domain.model.Provider
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.repository.UserRepository
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val appDatabase: AppDatabase,
    private val appPreference: AppPreference,
    private val userLocalDataSource: UserLocalDataSource,
    private val userService: UserService,
    private val ioDispatcher: CoroutineDispatcher,
) : UserRepository {

    override suspend fun getUser() = userLocalDataSource.getUser(appPreference.userId)

    override suspend fun getUserUid(uid: Long, provider: Provider) =
        userLocalDataSource.getUserUid(uid, provider)

    override suspend fun saveUser(user: User, me: Boolean): User {
        userLocalDataSource.saveUser(user)?.also {
            if (me) {
                appPreference.saveUserDate(DateUtil.getPreferenceDate())
                appPreference.saveUserId(it.id)
                ioDispatcher.launchBackground { userService.saveUser(it) }
            }
            return it
        }
        return user
    }

    override suspend fun updateUser(user: User) {
        userLocalDataSource.saveUser(user)?.also {
            appPreference.saveUserDate(DateUtil.getPreferenceDate())
            appPreference.saveUserId(it.id)
            ioDispatcher.launchBackground { userService.saveUser(it) }
        }
    }

    override suspend fun deleteUser(): Boolean {
        try {
            getUser()?.let {
                if (userService.deleteUser(it).success) {
                    appPreference.clear()
                    appDatabase.clearAllTables()
                    return true
                }
            }
        } catch (e: Exception) {}
        return false
    }
}