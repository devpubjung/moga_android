package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetMpPieChartReportBinding
import com.devpub.moga.ui.model.PieChartItem
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import java.text.DecimalFormat


class MpPieChartReportView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr), OnChartValueSelectedListener {

    private lateinit var binding: WidgetMpPieChartReportBinding
    private val textColor = ContextCompat.getColor(context, R.color.white)

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_mp_pie_chart_report, this)
        } else {
            binding =
                WidgetMpPieChartReportBinding.inflate(LayoutInflater.from(context), this, true)
            binding.pieChartView.apply {
                val offset = 5F
                setExtraOffsets(offset, offset, offset, offset)
                setUsePercentValues(true)
                setDrawEntryLabels(false)
//                setEntryLabelColor(textColor)
                isHighlightPerTapEnabled = true
                description.isEnabled = false
                isDrawHoleEnabled = false
                isRotationEnabled = false
                legend.apply {
                    setDrawInside(false)
                    isEnabled = false
                }
                animateY(1400, Easing.EaseInOutQuad)
                setOnChartValueSelectedListener(this@MpPieChartReportView)
            }
        }
    }

    fun setData(pieList: List<PieChartItem>, contentList: List<View>) {
        binding.contentLayout.removeAllViews()
        contentList.forEach {
            binding.contentLayout.addView(it)
        }

        val entries = mutableListOf<PieEntry>()
        val dataColors = mutableListOf<Int>()
        pieList.forEach {
            entries.add(PieEntry(it.value, it.label))
            dataColors.add(it.color)
        }
        val dataSet = PieDataSet(entries, "").apply {
            sliceSpace = 2F
            selectionShift = 5F
            colors = dataColors
            valueLinePart1OffsetPercentage = 80f
            valueLinePart1Length = 0.3f
            valueLinePart2Length = 0.5f
            valueLineColor = textColor
            yValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE
        }

        val data = PieData(dataSet).apply {
            setValueFormatter(PercentFormatter(binding.pieChartView))
            setValueTextSize(11f)
            setValueTextColor(textColor)
        }
        binding.pieChartView.data = data
        binding.pieChartView.invalidate()
    }

    override fun onValueSelected(entry: Entry?, highlight: Highlight) {
        entry?.let {
            Toast.makeText(context, "entry:$entry highlight:${highlight}", Toast.LENGTH_LONG).show()
        }
    }

    override fun onNothingSelected() {
    }

    inner class PercentFormatter() : ValueFormatter() {
        var mFormat: DecimalFormat = DecimalFormat("###,###,##0")
        private var pieChart: PieChart? = null

        // Can be used to remove percent signs if the chart isn't in percent mode
        constructor(pieChart: PieChart) : this() {
            this.pieChart = pieChart
        }

        override fun getFormattedValue(value: Float): String {
            return mFormat.format(value.toDouble()) + " %"
        }

        override fun getPieLabel(value: Float, pieEntry: PieEntry): String {
            return if (pieChart != null && pieChart!!.isUsePercentValuesEnabled) {
                // Converted to percent
                getFormattedValue(value)
            } else {
                // raw value, skip percent sign
                mFormat.format(value.toDouble())
            }
        }

    }
}