package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetReportUserDetailBinding
import com.devpub.moga.domain.model.UserSum
import com.devpub.moga.util.MoneyUtil.commaWon
import com.devpub.moga.util.bindingadapter.bindImage

class ReportUserDetailView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    lateinit var binding: WidgetReportUserDetailBinding

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_report_user_detail, this)
            view.findViewById<TextView>(R.id.titleText).text = "Title"
            view.findViewById<TextView>(R.id.contentText).text = "content"
        } else {
            binding =
                WidgetReportUserDetailBinding.inflate(LayoutInflater.from(context), this, true)
        }
    }

    fun setData(data: UserSum) {
        bindImage(binding.userProfile, data.user)
        binding.titleText.text = data.user.nickName
        binding.contentText.text = data.sum.commaWon()
    }
}