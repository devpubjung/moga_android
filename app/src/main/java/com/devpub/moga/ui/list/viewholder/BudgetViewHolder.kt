package com.devpub.moga.ui.list.viewholder

import android.app.AlertDialog
import android.content.res.ColorStateList
import android.text.InputFilter
import android.text.InputType
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.devpub.moga.R
import com.devpub.moga.domain.model.Budget
import com.devpub.moga.databinding.ItemBudgetBinding
import com.devpub.moga.ui.list.BudgetChangeListener
import com.devpub.moga.util.ColorUtil
import com.devpub.moga.util.MoneyUtil.commaManWon
import com.devpub.moga.util.extensions.showToast
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar.OnProgressChangeListener


class BudgetViewHolder(
    private val binding: ItemBudgetBinding,
) :
    RecyclerView.ViewHolder(binding.root) {

    var changeListener: BudgetChangeListener? = null

    init {
        binding.budgetLayout.budgetText = 0.commaManWon()
        binding.budgetLayout.budgetTextView.setOnClickListener {
            showInputBudgetPopup(binding.budgetLayout.budgetBar)
        }
    }

    fun bindViewHolder(item: Budget, position: Int) {
        binding.budgetLayout.apply {
            budgetBar.apply {
                max = item.maxBudget
                progress = item.budget - 1
                progress = item.budget
                this.postInvalidate()
                val lightColor = ColorUtil.lightColor(item.color)
                setRippleColor(lightColor)
                setTrackColor(lightColor)
                setThumbColor(item.color, item.color)
                setScrubberColor(item.color)

                setOnProgressChangeListener(object :
                    OnProgressChangeListener {
                    override fun onProgressChanged(
                        seekBar: DiscreteSeekBar,
                        value: Int,
                        fromUser: Boolean,
                    ) {

                    }

                    override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                    }

                    override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                        setBudget(seekBar.progress)
                    }
                })
            }
            budgetText = item.budget.commaManWon()
        }
        binding.item = item
        binding.executePendingBindings()
    }

    fun setBudget(progress: Int) {
        binding.budgetLayout.budgetText = progress.commaManWon()
        changeListener?.onStopTouch(progress, layoutPosition)
    }

    private fun showInputBudgetPopup(progressBar: DiscreteSeekBar) {
        val container = FrameLayout(binding.root.context)
        val editText = EditText(binding.root.context).apply {
            layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                leftMargin = resources.getDimensionPixelSize(R.dimen._16dp)
                rightMargin = resources.getDimensionPixelSize(R.dimen._16dp)
            }
            val colorStateList =
                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.primaryDarkColor))
            backgroundTintList = colorStateList
            inputType = InputType.TYPE_CLASS_NUMBER
            filters = arrayOf(InputFilter.LengthFilter(4))
            setSingleLine()
            if (progressBar.progress > 0) {
                setText(progressBar.progress.toString())
            }
        }
        container.addView(editText)

        AlertDialog.Builder(binding.root.context, R.style.AlertDialogStyle).apply {
            setTitle(binding.budgetLayout.title)
            setMessage("\n예산을 입력해주세요(최대 ${binding.budgetLayout.budgetBar.max}만원)")
            setView(container)
            setPositiveButton("설정") { _, _ ->
                try {
                    if (!editText.text.isNullOrEmpty()) {
                        val progress = editText.text.toString().toInt()
                        if (progress <= binding.budgetLayout.budgetBar.max) {
                            binding.budgetLayout.budgetBar.progress = progress
                            setBudget(progress)
                        } else {
                            "최대 예산을 벗어났습니다. 전체 예산을 조정하세요.".showToast(binding.root.context)
                        }
                    }
                } catch (e: Exception) {
                }
            }
            setNegativeButton("취소") { _, _ -> }
        }.create().show()
    }
}