package com.devpub.moga.ui.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.devpub.common.list.RecyclerHandler
import com.devpub.materialcalendarview.CalendarWeekDay
import com.devpub.materialcalendarview.EventDay
import com.devpub.materialcalendarview.listeners.OnCalendarPageChangeListener
import com.devpub.materialcalendarview.listeners.OnDayClickListener
import com.devpub.moga.R
import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.model.AccountItemNote
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.databinding.FragmentNoteBinding
import com.devpub.moga.ui.activity.InputActivity
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.extensions.*
import com.devpub.moga.util.MoneyUtil.commaWon
import com.devpub.moga.viewmodel.NoteViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class NoteFragment : BaseFragment<FragmentNoteBinding>() {

    private val viewModel: NoteViewModel by viewModels()
    private val adapter: ListAdapter by lazy {
        ListAdapter(Handler(requireContext(), viewModel))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadMonthNote(getSelectedDate().time)
    }

    override fun setUpViews() {
        bind {
            view = this@NoteFragment

            setCalendar()
            adListView.setAdapter(adapter, appPreference)
        }
    }

    private fun setCalendar() {
        bind {
            calendarView.apply {
                fun getCalendarPageChangeListener(): OnCalendarPageChangeListener {
                    return object : OnCalendarPageChangeListener {
                        override fun onChange() {
                            val month = currentPageDate.get(Calendar.MONTH)
                            val year = currentPageDate.get(Calendar.YEAR)
                            currentMonthTitle.text = "${year}년 ${month + 1}월"

                            viewModel.loadMonthNote(Calendar.getInstance(Locale.KOREA).apply {
                                set(Calendar.YEAR, year)
                                set(Calendar.MONTH, month)
                            }.time)
                        }
                    }
                }

                currentMonthTitle.text =
                    "${currentPageDate.get(Calendar.YEAR)}년 ${currentPageDate.get(Calendar.MONTH) + 1}월"

                setHeaderVisibility(View.GONE)
                setCalendarDayLayout(R.layout.row_custom_calendar_day)
                setOnForwardPageChangeListener(getCalendarPageChangeListener())
                setOnPreviousPageChangeListener(getCalendarPageChangeListener())
                setOnDayClickListener(object : OnDayClickListener {
                    override fun onDayClick(eventDay: EventDay) {
                        viewModel.loadDayNote(eventDay.calendar.time)
                    }
                })
            }
        }
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }

        viewModel.startWeek.observe {
            binding.calendarView.setFirstDayOfWeek(when(it) {
                0 -> CalendarWeekDay.SUNDAY
                else -> CalendarWeekDay.MONDAY
            })
        }

        viewModel.calendarData.observe {
            fun getMoneyText(money: Long): String? {
                return if (money != 0L) {
                    money.commaWon()
                } else {
                    null
                }
            }

            var incomeSum = 0L
            var expendSum = 0L
            var transferSum = 0L
            var transferExpendSum = 0L

            val events = it.map { item ->
                incomeSum += item.incomeMoney
                expendSum += item.expendMoney
                transferSum += item.transferMoney
                transferExpendSum += item.transferExpendMoney

                EventDay(item.calendar,
                    getMoneyText(item.incomeMoney),
                    getMoneyText(item.expendMoney),
                    getMoneyText(item.transferMoney))
            }

            bind {
                calendarView.setEvents(events)
                incomeLayout.moneyText.text = incomeSum.commaWon()
                expendLayout.moneyText.text = expendSum.commaWon()
                transferLayout.moneyText.text = transferSum.commaWon()

                val total = incomeSum - expendSum - transferExpendSum
                totalLayout.moneyText.text = total.commaWon()
                if (total >= 0) {
                    totalLayout.moneyText.setTextColor(ContextCompat.getColor(
                        requireContext(),
                        R.color.incomeColor))
                } else {
                    totalLayout.moneyText.setTextColor(ContextCompat.getColor(
                        requireContext(),
                        R.color.expendColor))
                }
            }
        }

        viewModel.noteItemData.observe {
            setListItem(it)
        }
    }

    private fun setListItem(items: List<AccountItemNote>) {
        bind {
            appBarLayout.setExpanded(true)
            adListView.setListItem(items)
        }
    }

    fun onClickToday() {
        binding.calendarView.setDate(DateUtil.today)
        viewModel.loadDayNote(getSelectedDate().time)
    }

    fun getSelectedDate(): Calendar {
        return binding.calendarView.firstSelectedDate
    }

    class Handler(private val context: Context, private val viewModel: NoteViewModel) :
        RecyclerHandler {

        fun onClickItem(accountItem: AccountItem) {
            accountItem.user?.let { user ->
                InputActivity.startForResult(context, accountItemId = accountItem.id)
            }
        }

        fun onLongClickItem(accountItem: AccountItem): Boolean {
            accountItem.user?.let { user ->
                val userId = AppPreferenceImpl(context).userId
                if (user.id == userId || accountItem.ownerId == userId) {
                    AlertDialog.Builder(context, R.style.AlertDialogStyle)
                        .apply {
                            setTitle("내역 삭제")
                            setMessage("모든 사용자에게서 해당 내용을 삭제합니다.\n\n정말 삭제하시겠습니까?")
                            setIcon(R.drawable.ic_item_check)
                            setPositiveButton("삭제") { _, _ ->
                                viewModel.removeNote(accountItem)
                            }
                            setNegativeButton("취소") { _, _ -> }
                        }.create().show()

                } else {
                    "직접 작성한 내역만 삭제 하실 수 있습니다.".showToast(context)
                }
            }
            return true
        }

        fun onLongClickCategory(accountItem: AccountItem): Boolean {
            viewModel.getCategoryInfo(accountItem)
            return true
        }
    }
}