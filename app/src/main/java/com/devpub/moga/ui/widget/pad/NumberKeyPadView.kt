package com.devpub.moga.ui.widget.pad

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetNumberKeypadBinding
import com.devpub.moga.util.MoneyUtil.isMaxLength

class NumberKeyPadView @JvmOverloads constructor(
    context: Context,
    attributes: AttributeSet? = null,
    defStyleAttr: Int = 0
) :  PadView<Long>(context, attributes, defStyleAttr) {

    private lateinit var binding: WidgetNumberKeypadBinding

    var isInit: Boolean = true

    val onClickListener: OnClickListener = OnClickListener {
        val number = it.tag
        if (number is String) {
            if (isInit) {
                item = 0
                isInit = false
            }
            val addAmount = item.toString() + number
            if (addAmount.isMaxLength()) {
                return@OnClickListener
            }
            item = addAmount.toLong()
            padListener?.onClickItem(item ?: 0)
        }
    }

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_number_keypad, this)
        } else {
            binding =
                WidgetNumberKeypadBinding.inflate(LayoutInflater.from(context), this, true)
            binding.onClickListener = this.onClickListener
            binding.view = this
            binding.keypadDelete.setOnLongClickListener {
                onLongClickBack()
                false
            }
        }
    }

    override fun show() {
        super.show()
        isInit = true
    }

    override var item: Long? = 0

    fun onClickBack() {
        item = if (item.toString().length == 1) {
            0
        } else {
            item.toString().dropLast(1).toLong()
        }
        padListener?.onClickItem(item ?: 0)
    }

    private fun onLongClickBack() {
        item = 0
        padListener?.onClickItem(item ?: 0)
    }

}

