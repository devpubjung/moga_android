package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemKosisBinding


class KosisViewHolder(
    binding: ItemKosisBinding,
) : BindingViewHolder<ItemKosisBinding>(binding)