package com.devpub.moga.ui.widget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devpub.moga.databinding.PopupJoinBinding

class JoinPopupDialog(context: Context) : Dialog(context) {

    private lateinit var binding: PopupJoinBinding
    private var clickListener: View.OnClickListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = PopupJoinBinding.inflate(LayoutInflater.from(context))
        binding.view = this
        setContentView(binding.root)
        setCancelable(true)
    }

    override fun onStart() {
        super.onStart()
        window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun setOnClickListener(clickListener: View.OnClickListener): JoinPopupDialog {
        this.clickListener = clickListener
        return this
    }

    fun onClickConfirm() {
        clickListener?.onClick(binding.confirmButton)
        dismiss()
    }

    fun onClickCancel() {
        dismiss()
    }
}