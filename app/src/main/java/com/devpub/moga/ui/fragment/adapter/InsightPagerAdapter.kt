package com.devpub.moga.ui.fragment.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.devpub.moga.ui.fragment.KosisFragment
import com.devpub.moga.ui.fragment.ComparePrevFragment


class InsightPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount() = 2

    override fun createFragment(position: Int) = when (position) {
        1 -> KosisFragment.newInstance()
        else -> ComparePrevFragment.newInstance()
    }
}