package com.devpub.moga.ui.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.devpub.moga.BuildConfig
import com.devpub.moga.R
import com.devpub.moga.databinding.FragmentSettingBinding
import com.devpub.moga.domain.model.User
import com.devpub.moga.manager.BillingManager
import com.devpub.moga.ui.activity.*
import com.devpub.moga.ui.widget.DefaultPopupDialog
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.PermissionUtil
import com.devpub.moga.util.bindingadapter.bindImage
import com.devpub.moga.util.extensions.toast
import com.devpub.moga.viewmodel.EtcViewModel
import com.google.android.gms.ads.AdRequest
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EtcFragment : BaseFragment<FragmentSettingBinding>() {

    @Inject
    internal lateinit var billingManager: BillingManager

    private val viewModel: EtcViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        billingManager.startConnection()
        removeAds = billingManager.queryPurchaseHistoryAsync()
    }

    override fun setUpViews() {
        bind {
            view = this@EtcFragment

            if (!removeAds) {
                adView.loadAd(AdRequest.Builder().build())
                bottomAdView.loadAd(AdRequest.Builder().build())
            }

            versionView.setSubTitle(getAppVersion())
        }
    }

    override fun onResume() {
        super.onResume()
        refreshData()
        viewModel.pingServer()
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }

        viewModel.userInfo.observe {
            setUserInfo(it)
        }

        viewModel.ping.observe {
            bind {
                requestFunctionButton.isVisible = it
                syncMoimButton.isVisible = it &&
                        !DateUtil.sameDate(DateUtil.todayDate,
                            DateUtil.convertPreferenceDate(appPreference.moimSyncDate))
                        && appPreference.joinMoim
                syncButton.isVisible = it &&
                        !DateUtil.sameDate(DateUtil.todayDate,
                            DateUtil.convertPreferenceDate(appPreference.syncDate))
            }
        }

        viewModel.syncMoimDataEvent.observe {
            refreshData()
        }
    }

    private fun refreshData() {
        bind {
            removeAdsButton.isVisible = !removeAds
            adView.isVisible = !removeAds
            bottomAdView.isVisible = !removeAds

            requestPermissionButton.isVisible = !PermissionUtil.isNotiPermissionAllowed(requireContext())
        }
    }

    private fun setUserInfo(user: User) {
        bind {
            bindImage(userProfile, user)
            userNameText.text = user.nickName
            userLayout.visibility = View.VISIBLE
        }
    }

    private fun getAppVersion() = "${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})"

    private fun goMarket() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse("market://details?id=${requireActivity().packageName}")
            })
        } catch (e: Exception) {
            val webIntent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse("https://play.google.com/store/apps/details?id=${requireActivity().packageName}")
            }
            if (webIntent.resolveActivity(requireActivity().packageManager) != null) {
                startActivity(webIntent)
            }
        }
    }

    private val userSettingLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            result.data?.let {
                val user = it.getSerializableExtra("user") as User
                setUserInfo(user)
            }
        }
    }

    fun onClickUserInfoButton() {
        UserInfoSettingActivity.startForResult(context, userSettingLauncher, false)
    }

    fun onClickSettingBudgetButton() {
        BudgetSettingActivity.startForResult(context)
    }

    fun onClickInviteMoimButton() {
        InviteActivity.start(requireContext())
    }

    fun onClickRequestPermissionButton() {
        if (!PermissionUtil.isNotiPermissionAllowed(requireContext())) {
            PermissionUtil.requireNotiPermission(requireActivity(), appPreference)
        } else {
            PermissionUtil.requireSmsPermission(requireActivity(), REQUEST_CODE_SMS_PERMISSION)
        }
    }

    fun onClickSettingButton() {
        SettingActivity.start(requireContext())
    }

    fun onClickSyncMoimButton() {
        viewModel.syncMoimData()
    }

    fun onClickSyncButton() {
        viewModel.syncMoimAllData()
    }

    fun onClickSyncSmsButton() {

    }

    fun onClickCheckUpdateButton() {
        goMarket()
    }

    fun onClickRequestFunctionButton() {
        SuggestActivity.start(requireContext())
    }

    fun onClickSendQnAButton() {
        val email = Intent(Intent.ACTION_SEND)
        email.type = "message/rfc822"
        val address = arrayOf("devpub21@gmail.com")
        email.putExtra(Intent.EXTRA_EMAIL, address)
        email.putExtra(Intent.EXTRA_SUBJECT, "[모두의가계부]오류/문의 내용")
        email.putExtra(Intent.EXTRA_TEXT,
            "앱 버전 : ${getAppVersion()}\n" +
                    "기기명 : ${Build.MODEL}\n" +
                    "OS 버전 : ${Build.VERSION.RELEASE}(${Build.VERSION.SDK_INT})\n" +
                    "내용  : \n")
        startActivity(email)
    }

    fun onClickGoReviewButton() {
        goMarket()
    }

    fun onClickRemoveAdsButton() {
        billingManager.purchaseItem(1) {
            showFirecracker("아이템 구매 완료!", R.string.removeAds, R.string.removeAds_content)
        }
    }

    fun onClickDonationButton() {
        billingManager.purchaseItem(0) {
            showFirecracker("응원해주기 완료!", R.string.donation, R.string.donation_content)
        }
    }

    private fun showFirecracker(topText: String, titleTextResId: Int, contentTextResId: Int) {
        lifecycleScope.launchWhenResumed {
            removeAds = appPreference.purchasedRemoveAds
            refreshData()
            activity?.let {
                if (it is MainActivity) {
                    it.showFirecracker(DefaultPopupDialog(it).apply {
                        setTopText(topText)
                        setTitleText(titleTextResId)
                        setContentText(contentTextResId)
                    })
                }

            }
        }
    }

    companion object {
        private const val REQUEST_CODE_SMS_PERMISSION = 1003
    }
}