package com.devpub.moga.ui.list

enum class ViewType {
    HomeReport, AccountNote, CompareItem, KosisHeader, Kosis, Category, CategoryHeader, User, Assets, AssetsHeader, SimpleText
}