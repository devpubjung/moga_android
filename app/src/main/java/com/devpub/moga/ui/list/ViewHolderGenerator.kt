package com.devpub.moga.ui.list

import android.view.ViewGroup
import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.common.list.viewholder.ItemEmptyViewHolder
import com.devpub.common.util.extension.toBinding
import com.devpub.moga.ui.list.viewholder.*

object ViewHolderGenerator {

    fun get(
        parent: ViewGroup,
        viewType: Int,
        handler: RecyclerHandler?,
    ): BindingViewHolder<*> {
        return when (viewType) {
            ViewType.HomeReport.ordinal -> BudgetReportViewHolder(parent.toBinding(), handler)
            ViewType.AccountNote.ordinal -> NoteViewHolder(parent.toBinding(), handler)
            ViewType.CompareItem.ordinal -> ComparePrevViewHolder(parent.toBinding())
            ViewType.KosisHeader.ordinal -> KosisHeaderViewHolder(parent.toBinding(), handler)
            ViewType.Kosis.ordinal -> KosisViewHolder(parent.toBinding())
            ViewType.CategoryHeader.ordinal -> CategoryHeaderViewHolder(parent.toBinding(), handler)
            ViewType.Category.ordinal -> CategoryViewHolder(parent.toBinding(), handler)
            ViewType.User.ordinal -> UserViewHolder(parent.toBinding(), handler)
            ViewType.AssetsHeader.ordinal -> AssetsHeaderViewHolder(parent.toBinding(), handler)
            ViewType.Assets.ordinal -> AssetsViewHolder(parent.toBinding(), handler)
            ViewType.SimpleText.ordinal -> SimpleTextViewHolder(parent.toBinding(), handler)
            else -> ItemEmptyViewHolder(parent.toBinding())
        }
    }
}