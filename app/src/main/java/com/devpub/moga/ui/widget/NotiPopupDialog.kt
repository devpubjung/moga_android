package com.devpub.moga.ui.widget

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.devpub.moga.R
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.databinding.PopupNotiBinding

class NotiPopupDialog(context: Context) : AlertDialog(context) {

    private lateinit var binding: PopupNotiBinding
    private var clickListener: View.OnClickListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = PopupNotiBinding.inflate(LayoutInflater.from(context))
        binding.view = this
        setContentView(binding.root)
        setCancelable(true)

        binding.contentText.text = SpannableString(context.getString(R.string.noti_content)).apply {
            setSpan(ForegroundColorSpan(ContextCompat.getColor(context, R.color.errorColor)), 0, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
            setSpan(RelativeSizeSpan(1.2f), 19, 21, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
            setSpan(StyleSpan(Typeface.BOLD), 19, 21, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        }
    }

    override fun onStart() {
        super.onStart()
        AppPreferenceImpl(context).setWatchNotiPermission(true)
    }

    fun setOnClickListener(clickListener: View.OnClickListener): NotiPopupDialog {
        this.clickListener = clickListener
        return this
    }

    fun onClickConfirm() {
        clickListener?.onClick(binding.confirmButton)
        dismiss()
    }

    fun onClickCancel() {
        dismiss()
    }
}