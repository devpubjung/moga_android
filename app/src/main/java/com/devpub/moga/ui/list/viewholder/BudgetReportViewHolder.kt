package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.common.model.ListItem
import com.devpub.moga.databinding.ItemHomeBinding
import com.devpub.moga.domain.model.BudgetDetailReport
import com.devpub.moga.util.MoneyUtil
import com.devpub.moga.util.MoneyUtil.commaManWon

class BudgetReportViewHolder(private val binding: ItemHomeBinding, handler: RecyclerHandler?) :
    BindingViewHolder<ItemHomeBinding>(binding, handler) {

    init {
        binding.usedText.text = 0.commaManWon()
    }

    override fun bind(item: ListItem) {
        super.bind(item)
        item as BudgetDetailReport
        binding.budgetGoalProgressBar.apply {
            max = if (item.isMonthly) {
                binding.budgetText.text = item.budget.budget.commaManWon()
                MoneyUtil.convertFullMoney(item.budget.budget).toInt()
            } else {
                binding.budgetText.text = item.budget.weeklyBudget.commaManWon()
                MoneyUtil.convertFullMoney(item.budget.weeklyBudget).toInt()
            }
            animateProgress(item.accountSum.toInt())
        }
    }

}