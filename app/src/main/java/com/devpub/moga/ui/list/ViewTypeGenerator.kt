package com.devpub.moga.ui.list

import com.devpub.common.model.ListItem
import com.devpub.moga.domain.model.*
import com.devpub.moga.ui.model.AssetsHeader
import com.devpub.moga.ui.model.CategoryHeader
import com.devpub.moga.ui.model.SimpleTextItem

object ViewTypeGenerator {

    fun get(item: ListItem): Int {
        return when (item.javaClass) {
            BudgetDetailReport::class.java -> ViewType.HomeReport.ordinal
            AccountItemNote::class.java -> ViewType.AccountNote.ordinal
            CompareItem::class.java -> ViewType.CompareItem.ordinal
            KosisData::class.java -> ViewType.KosisHeader.ordinal
            KosisItem::class.java -> ViewType.Kosis.ordinal
            Category::class.java -> ViewType.Category.ordinal
            CategoryHeader::class.java -> ViewType.CategoryHeader.ordinal
            User::class.java -> ViewType.User.ordinal
            Assets::class.java -> ViewType.Assets.ordinal
            AssetsHeader::class.java -> ViewType.AssetsHeader.ordinal
            SimpleTextItem::class.java -> ViewType.SimpleText.ordinal
            else -> {
                -1
            }
        }
    }
}