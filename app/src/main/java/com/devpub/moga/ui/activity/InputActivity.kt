package com.devpub.moga.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat
import com.devpub.common.util.StatusBarUtil
import com.devpub.common.mvvm.BaseActivity
import com.devpub.moga.R
import com.devpub.moga.databinding.ActivityInputBinding
import com.devpub.moga.domain.model.*
import com.devpub.moga.domain.model.Target
import com.devpub.moga.ui.widget.InputItemContainerLayout
import com.devpub.moga.ui.widget.pad.AssetsBottomSheetDialog
import com.devpub.moga.ui.widget.pad.CategoryBottomSheetDialog
import com.devpub.moga.ui.widget.pad.PadView
import com.devpub.moga.ui.widget.pad.UserBottomSheetDialog
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.extensions.*
import com.devpub.moga.util.MoneyUtil.commaWon
import com.devpub.moga.util.MoneyUtil.withoutCommaWon
import com.devpub.moga.util.bindingadapter.bindImage
import com.devpub.moga.viewmodel.InputViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_input.*
import java.util.*

@AndroidEntryPoint
class InputActivity : BaseActivity<ActivityInputBinding>() {

    private lateinit var layoutList: List<InputItemContainerLayout>
    private val calendar: Calendar = DateUtil.today
    private var isWriteMe = true

    private lateinit var dateTextView: TextView
    private lateinit var dateTimeTextView: TextView
    private lateinit var userImageView: ImageView
    private lateinit var userTextView: TextView
    private lateinit var amountTextView: TextView
    private lateinit var assetsTextView: TextView
    private lateinit var categoryTextView: TextView
    private lateinit var contentEditView: EditText
    private lateinit var memoEditView: EditText

    private val viewModel: InputViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeViewModel()
        var accountItemId = intent.getSerializableExtra(KEY_ITEM_ID) as? Long
        var cal = intent.getSerializableExtra(KEY_CALENDAR) as? Calendar
        if (savedInstanceState != null) {
            accountItemId = savedInstanceState.getSerializable(KEY_ITEM_ID) as? Long
            cal = savedInstanceState.getSerializable(KEY_CALENDAR) as? Calendar
        }
        viewModel.loadInputInfo(accountItemId, cal)
    }

    private fun setUpViews() {
        StatusBarUtil.setStatusBarColorForLollipop(window,
            ContextCompat.getColor(this, R.color.primaryColor))
        binding.view = this
        binding.viewModel = this.viewModel

        dateTextView = findViewById(R.id.dateText)
        dateTimeTextView = findViewById(R.id.dateTimeText)
        userImageView = findViewById(R.id.userImage)
        userTextView = findViewById(R.id.userText)
        amountTextView = findViewById(R.id.amountText)
        assetsTextView = findViewById(R.id.assetsText)
        categoryTextView = findViewById(R.id.categoryText)
        contentEditView = findViewById(R.id.contentEdit)
        memoEditView = findViewById(R.id.memoEdit)
        bind {
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowTitleEnabled(false)
                toolbarTitle.text = "내역 추가하기"
            }

            layoutList = listOf(
                dateLayout,
                assetsLayout,
                amountLayout,
                userLayout,
                categoryLayout,
                contentLayout,
                memoLayout
            )

            dateTextView.setOnClickListener {
                onClickDate()
            }
            dateTimeTextView.setOnClickListener {
                onClickTime()
            }

            userImageView.setOnClickListener {
                showUserPad()
            }

            userTextView.setOnClickListener {
                showUserPad()
            }

            assetsTextView.setOnClickListener {
                showAssetsPad()
            }

            categoryTextView.setOnClickListener {
                showCategoryPad()
            }

            numberKeyPad.apply {
                item = amountTextView.text.toString().withoutCommaWon()
                onPadClickItem {
                    amountTextView.text = it.commaWon()
                    this@InputActivity.viewModel.saveAmount(it)
                }
                onPadClickDone {
                    showAssetsPad()
                }
                setUpItemView(amountLayout, amountTextView, this)
            }

            setBottomSheetResultListener(UserBottomSheetDialog.TAG) { _, bundle ->
                (bundle.getSerializable(UserBottomSheetDialog.itemKey) as User).let { user ->
                    bindImage(userImageView, user)
                    userTextView.text = user.nickName
                    this@InputActivity.viewModel.saveUser(user)
                    setFocus(amountLayout)
                    numberKeyPad.show()
                }
            }

            setBottomSheetResultListener(AssetsBottomSheetDialog.TAG) { _, bundle ->
                (bundle.getSerializable(AssetsBottomSheetDialog.itemKey) as Assets).let { assets ->
                    assetsTextView.text = assets.name
                    this@InputActivity.viewModel.saveAssets(assets)
                    showCategoryPad()
                }
            }

            setBottomSheetResultListener(CategoryBottomSheetDialog.TAG) { _, bundle ->
                (bundle.getSerializable(CategoryBottomSheetDialog.itemKey) as Category).let { category ->
                    categoryTextView.text = category.name
                    this@InputActivity.viewModel.saveCategory(category)
                    setFocus(contentLayout)
                    contentEditView.postDelayed({
                        contentEditView.showKeyboard()
                    }, 200)
                }
            }

            setUpEditView(contentLayout, contentEditView)
            setUpEditView(memoLayout, memoEditView)
        }
    }

    private fun observeViewModel() {
        viewModel.accountItemData.observe { item ->
            fun setEnableItems(enable: Boolean) {
                with(binding) {
                    shareCheckBox.isEnabled = enable
                    expendCheckBox.isEnabled = enable
                    incomeButton.isEnabled = enable
                    expendButton.isEnabled = enable
                    transferButton.isEnabled = enable
                    dateTextView.isEnabled = enable
                    dateTimeTextView.isEnabled = enable
                    userImageView.isEnabled = enable
                    userTextView.isEnabled = enable
                    assetsTextView.isEnabled = enable
                    categoryTextView.isEnabled = enable
                    contentEditView.isEnabled = enable
                    amountTextView.isEnabled = enable
                    memoEditView.isEnabled = enable

                    supportActionBar?.apply {
                        toolbarTitle.text = if (enable) "내역 추가하기" else "내역 상세보기"
                    }
                }
            }

            item.apply {
                layoutList.forEach { layout ->
                    layout.setFocusColor(target.color)
                }
                calendar.time = date
                dateTextView.text = DateUtil.getDateString(date)
                dateTimeTextView.text = DateUtil.getTimeString(date)
                user?.let { user ->
                    bindImage(userImageView, user)
                    userTextView.text = user.nickName
                }
                if (amount != 0L) {
                    amountTextView.text = amount.commaWon()
                } else {
                    amountTextView.text = ""
                }
                assetsTextView.text = assets?.name
                categoryTextView.text = category?.name
                contentEditView.setText(content)
                memoEditView.setText(memo)

                numberKeyPad.item = amount
                setExpendCheckBox(target, this)
            }
            setFocus(amountLayout)
            if (item.isWriteMe) {
                numberKeyPad.show()
            }
            setEnableItems(item.isWriteMe)
        }

        viewModel.changeCategoryEvent.observe { message ->
            if (message.isNotEmpty()) {
                toast(message)
            }
            showCategoryPad()
        }

        viewModel.changeAmountEvent.observe { message ->
            if (message.isNotEmpty()) {
                toast(message)
            }
            showAmountPad()
        }

        viewModel.successSaveEvent.observe {
            toast(it)
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.input_menu, menu)
        menu.findItem(R.id.action_save)?.isVisible = isWriteMe
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.action_save -> {
            if (canNext()) {
                viewModel.saveItem(contentEditView.text.toString(), memoEditView.text.toString())
            }
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun finish() {
        closeAllPad()
        super.finish()
    }

    private fun canNext(): Boolean {
        if (userTextView.text.isEmpty()) {
            toast("대상을 선택하셔야 합니다.")
            showUserPad()
            return false
        }

        if (amountTextView.text.isEmpty()) {
            toast("금액을 입력하셔야 합니다.")
            showAmountPad()
            return false
        }

        if (assetsTextView.text.isEmpty()) {
            toast("자산을 선택하셔야 합니다.")
            showAssetsPad()
            return false
        }

        if (categoryTextView.text.isEmpty()) {
            toast("분류를 선택하셔야 합니다.")
            showCategoryPad()
            return false
        }
        return true
    }

    private fun showAmountPad() {
        closeAllPad()
        setFocus(binding.amountLayout)
        binding.numberKeyPad.show()
    }

    private fun showUserPad() {
        closeAllPad()
        setFocus(binding.userLayout)
        UserBottomSheetDialog.newInstance()
            .also {
                it.dialog?.setOnDismissListener {
                    setFocus(null)
                }
            }
            .show(supportFragmentManager, UserBottomSheetDialog.TAG)
    }

    private fun showAssetsPad() {
        closeAllPad()
        setFocus(binding.assetsLayout)
        AssetsBottomSheetDialog.newInstance()
            .also {
                it.dialog?.setOnDismissListener {
                    setFocus(null)
                }
            }
            .show(supportFragmentManager, AssetsBottomSheetDialog.TAG)
    }

    private fun showCategoryPad() {
        viewModel.accountItemData.value?.let {
            closeAllPad()
            setFocus(binding.categoryLayout)
            CategoryBottomSheetDialog.newInstance(it.target)
                .also { bottomSheet ->
                    bottomSheet.dialog?.setOnDismissListener {
                        setFocus(null)
                    }
                }
                .show(supportFragmentManager, CategoryBottomSheetDialog.TAG)
        }
    }

    private fun setFocus(layout: InputItemContainerLayout?) {
        layoutList.forEach {
            it.setFocus(it == layout)
        }
    }

    private fun closeSelectPad() {
        binding.numberKeyPad.hide()
    }

    private fun closeAllPad() {
        closeSelectPad()
        contentEditView.closeKeyboard()
        memoEditView.closeKeyboard()
    }

    private fun onClickDate() {
        closeAllPad()
        setFocus(binding.dateLayout)
        val dialog =
            DatePickerDialog(this,
                R.style.DatePickerStyle,
                { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(year, monthOfYear, dayOfMonth)
                    dateTextView.text = DateUtil.getDateString(calendar.time)
                    viewModel.saveDateTime(calendar.time)
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE))

        dialog.show()
    }

    private fun onClickTime() {
        closeAllPad()
        setFocus(binding.dateLayout)
        val dialog = TimePickerDialog(this, R.style.DatePickerStyle, { _, hour, minute ->
            calendar.set(Calendar.HOUR, hour)
            calendar.set(Calendar.MINUTE, minute)
            dateTimeTextView.text = DateUtil.getTimeString(calendar.time)
            viewModel.saveDateTime(calendar.time)
        }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), false)
        dialog.show()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpItemView(
        layout: InputItemContainerLayout, view: View, padView: PadView<*>,
    ) {
        padView.onPadClickCancel { setFocus(null) }
        view.setOnClickListener {
            closeAllPad()
            setFocus(layout)
            padView.show()
        }
        view.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                clearFocus(contentEditView, event)
                clearFocus(memoEditView, event)
            }
            false
        }
    }

    private fun setUpEditView(layout: InputItemContainerLayout, view: EditText) {
        view.setOnFocusChangeListener { _, focus ->
            if (focus) {
                closeSelectPad()
                setFocus(layout)
            }
        }
    }

    private fun clearFocus(view: EditText, event: MotionEvent) {
        if (view.isFocused) {
            val outRect = Rect()
            view.getGlobalVisibleRect(outRect)
            if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                view.closeKeyboard()
            }
        }
    }

    fun onClickTargetButton(target: Target) {
        layoutList.forEach {
            it.setFocusColor(target.color)
            it.notiChangeFocusColor()
        }
        setExpendCheckBox(target)
        viewModel.onClickTarget(target)
    }

    private fun setExpendCheckBox(target: Target, accountItem: AccountItem? = null) {
        with(binding.expendCheckBox) {
            isChecked = accountItem?.expend ?: (target == Target.EXPEND)
            if (target == Target.TRANSFER) {
                alpha = 1.0f
                isEnabled = true
            } else {
                alpha = 0.3f
                isEnabled = false
            }
        }
    }

    companion object {
        private const val KEY_CALENDAR = "calendar"
        private const val KEY_ITEM_ID = "itemId"

        fun startForResult(
            context: Context?,
            resultLauncher: ActivityResultLauncher<Intent>? = null,
            calendar: Calendar? = null,
            accountItemId: Long? = null,
        ) {
            context?.run {
                Intent(this, InputActivity::class.java).apply {
                    putExtra(KEY_CALENDAR, calendar)
                    putExtra(KEY_ITEM_ID, accountItemId)

                    resultLauncher?.launch(this) ?: context.startActivity(this)
                }
            }
        }
    }
}