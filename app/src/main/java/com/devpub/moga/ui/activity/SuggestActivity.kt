package com.devpub.moga.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.content.ContextCompat
import com.devpub.common.util.StatusBarUtil
import com.devpub.common.mvvm.BaseActivity
import com.devpub.moga.R
import com.devpub.moga.databinding.ActivitySuggestBinding
import com.devpub.moga.util.extensions.toast
import com.devpub.moga.viewmodel.SuggestViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SuggestActivity : BaseActivity<ActivitySuggestBinding>() {

    private val viewModel: SuggestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.ping()
    }

    private fun setUpViews() {
        StatusBarUtil.setStatusBarColorForLollipop(window,
            ContextCompat.getColor(this, R.color.primaryColor))
        binding.view = this
        with(binding) {
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowTitleEnabled(false)
                toolbarTitle.text = getString(R.string.suggest_title)
            }
        }
    }

    private fun observeViewModel() {
        viewModel.sendSuccessEvent.observe {
            toast("성공적으로 전달되었습니다.")
            finish()
        }
        viewModel.errorEvent.observe {
            toast(it.message)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.input_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.action_save -> {
            val content = binding.contentEdit.text?.trim()
            if (content.isNullOrEmpty()) {
                toast("내용을 입력하여 주세요.")
            } else {
                viewModel.send(content.toString())
            }
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun start(context: Context) {
            Intent(context, SuggestActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }
}