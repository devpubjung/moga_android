package com.devpub.moga.ui.model.state

import com.devpub.moga.domain.model.User

sealed class UserInfoState {
    object Loading: UserInfoState()
    data class Load(val user: User): UserInfoState()
    data class Save(val user: User): UserInfoState()
    data class Delete(val message : String): UserInfoState()
    data class Error(val error: Throwable?): UserInfoState()
}
