package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemCompareBinding

class ComparePrevViewHolder(binding: ItemCompareBinding) :
    BindingViewHolder<ItemCompareBinding>(binding)