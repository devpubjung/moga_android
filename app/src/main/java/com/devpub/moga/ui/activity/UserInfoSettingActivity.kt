package com.devpub.moga.ui.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import com.devpub.common.mvvm.BaseActivity
import com.devpub.moga.R
import com.devpub.moga.data.interactor.KakaoInteractor
import com.devpub.moga.databinding.ActivityUserInfoSettingBinding
import com.devpub.moga.domain.model.Gender
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MoneyUtil.commaManWon
import com.devpub.moga.util.extensions.*
import com.devpub.moga.viewmodel.UserInfoSettingViewModel
import com.whiteelephant.monthpicker.MonthPickerDialog
import dagger.hilt.android.AndroidEntryPoint
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import javax.inject.Inject


@AndroidEntryPoint
class UserInfoSettingActivity : BaseActivity<ActivityUserInfoSettingBinding>() {

    @Inject
    internal lateinit var kakaoInteractor: KakaoInteractor

    private val viewModel: UserInfoSettingViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    private fun setUpViews() {
        val forStartPage = intent?.getBooleanExtra(FOR_START_PAGE, false) ?: false
        binding.view = this
        binding.viewModel = this.viewModel
        bind {
            if (forStartPage) {
                toolbar.title = "초기 설정"
                toolbar.navigationIcon = null
                toolbar.inflateMenu(R.menu.user_info_setting_menu)
            } else {
                toolbar.title = "정보 수정"
                toolbar.navigationIcon =
                    ContextCompat.getDrawable(this@UserInfoSettingActivity, R.drawable.ic_back)
                toolbar.setNavigationOnClickListener { finish() }
                toolbar.inflateMenu(R.menu.user_info_menu)
            }

            toolbar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.action_save -> {
                        bind {
                            if (nameEdit.text.isNullOrEmpty()) {
                                nameField.error = "이름을 작성해주세요."
                            } else {
                                val email = emailEdit.text.toString()
                                val name = nameEdit.text.toString()
                                val birthYear = birthdayText.text.toString().toInt()
                                val gender = if (maleRadioButton.isChecked) Gender.MALE else Gender.FEMALE
                                val totalBudget = if (forStartPage) budgetBar.progress else null
                                viewModel?.saveUserInfo(
                                    email,
                                    name,
                                    birthYear,
                                    gender,
                                    totalBudget)
                            }
                        }
                        true
                    }
                    else -> false
                }
            }

            nameEdit.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    nameField.error = null
                }
            }
            genderRadioGroup.setOnCheckedChangeListener { _, _ ->
                nameEdit.closeKeyboard()
            }
            totalBudgetLayout.isVisible = forStartPage
            deleteUserTextView.isVisible = !forStartPage
            budgetBar.setOnProgressChangeListener(object :
                DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(
                    seekBar: DiscreteSeekBar,
                    value: Int,
                    fromUser: Boolean,
                ) {
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {}

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    val process = seekBar.progress
                    binding.budgetText.text = process.commaManWon()
                }
            })
        }
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.saveSuccessEvent.observe {
            setResult(RESULT_OK, Intent().putExtra("user", it))
            finish()
        }

        viewModel.deleteCompleteEvent.observe {
            toast("정상적으로 탈퇴되었습니다.\n이용해주셔서 감사합니다.")
            kakaoInteractor.logout()
            finishAffinity()
        }

        viewModel.errorEvent.observe {
            toast(it.message)
        }
    }

    fun onClickBirthYearText() {
        binding.nameEdit.closeKeyboard()
        val year = Integer.parseInt(binding.birthdayText.text.toString())
        val builder = MonthPickerDialog.Builder(this,
            { _, selectedYear ->
                binding.birthdayText.text = selectedYear.toString()
            }, year, 0)

        builder.setActivatedYear(year)
            .setTitle("태어난 연도를 선택해주세요.")
            .setYearRange(1920, DateUtil.thisYear())
            .showYearOnly()
            .build()
            .show()
    }

    fun onClickDeleteUserText() {
        AlertDialog.Builder(this, R.style.AlertDialogStyle)
            .apply {
                setTitle("회원 탈퇴")
                setMessage("탈퇴하시면 모든 데이터가 삭제되어 복구 불가합니다.\n\n정말 탈퇴하시겠습니까?")
                setIcon(R.drawable.ic_cancel)
                setPositiveButton("탈퇴") { _, _ ->
                    viewModel.deleteUser()
                }
                setNegativeButton("취소") { _, _ -> }
            }.create().show()
    }

    companion object {
        private const val FOR_START_PAGE = "forStartPage"

        fun startForResult(
            context: Context?,
            resultLauncher: ActivityResultLauncher<Intent>,
            forStartPage: Boolean = true,
        ) {
            context?.run {
                Intent(this, UserInfoSettingActivity::class.java).apply {
                    putExtra(FOR_START_PAGE, forStartPage)
                }.run {
                    resultLauncher.launch(this)
                }
            }
        }
    }
}