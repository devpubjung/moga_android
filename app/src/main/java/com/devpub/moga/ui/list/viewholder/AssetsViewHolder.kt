package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemAssetsPadBinding


class AssetsViewHolder(
    binding: ItemAssetsPadBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemAssetsPadBinding>(binding, handler)