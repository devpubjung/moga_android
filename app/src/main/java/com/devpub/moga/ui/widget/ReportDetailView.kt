package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetReportDetailBinding

class ReportDetailView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    lateinit var binding: WidgetReportDetailBinding

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_report_detail, this)
            view.findViewById<TextView>(R.id.titleText).text = "Title"
            view.findViewById<TextView>(R.id.contentText).text = "content"
        } else {
            binding =
                WidgetReportDetailBinding.inflate(LayoutInflater.from(context), this, true)
        }
    }
}