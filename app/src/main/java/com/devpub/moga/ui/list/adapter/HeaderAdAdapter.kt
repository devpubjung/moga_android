package com.devpub.moga.ui.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.devpub.moga.databinding.ItemAdHeaderBinding
import com.google.android.gms.ads.AdRequest


class HeaderAdAdapter : RecyclerView.Adapter<HeaderAdAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemAdHeaderBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }


    override fun getItemCount() = 1

    class ViewHolder(binding: ItemAdHeaderBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            AdRequest.Builder().build().apply {
                binding.adView.loadAd(this)
            }
        }
    }
}