package com.devpub.moga.ui.list.adapter

import android.view.ViewGroup
import com.devpub.common.list.BasePageListAdapter
import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.common.model.ListItem
import com.devpub.moga.domain.model.BudgetReport
import com.devpub.moga.ui.list.ViewHolderGenerator
import com.devpub.moga.ui.list.ViewType

class PageListAdapter(
    recyclerHandler: RecyclerHandler? = Handler(),
) : BasePageListAdapter(recyclerHandler) {

    override fun getViewType(item: ListItem?): Int {
        return when (item) {
            BudgetReport::class.java -> {
                ViewType.HomeReport.ordinal
            }
            else -> {
                -1
            }
        }
    }

    override fun getCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
        handler: RecyclerHandler?,
    ): BindingViewHolder<*> {
        return ViewHolderGenerator.get(parent, viewType, handler)
    }

    class Handler : RecyclerHandler
}