package com.devpub.moga.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.devpub.moga.R
import com.devpub.moga.domain.model.Budget
import com.devpub.moga.databinding.ItemBudgetBinding
import com.devpub.moga.databinding.ItemBudgetHeaderBinding
import com.devpub.moga.ui.list.BudgetChangeListener
import com.devpub.moga.ui.list.BudgetHeaderChangeListener
import com.devpub.moga.ui.list.viewholder.BudgetHeaderViewHolder
import com.devpub.moga.ui.list.viewholder.BudgetViewHolder

class BudgetSettingAdapter(
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<Budget> = mutableListOf()
    var itemChangeListener: ItemChangeListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_FOOTER -> FooterViewHolder(View(parent.context).apply {
                layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    resources.getDimensionPixelSize(R.dimen._36dp))
            })
            ITEM_VIEW_TYPE_HEADER -> BudgetHeaderViewHolder(
                ItemBudgetHeaderBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            ).also {
                it.changeListener = object : BudgetHeaderChangeListener {
                    override fun onStopTouch(progress: Int, position: Int) {
                        items.forEachIndexed { index, budget ->
                            if (index > 0) {
                                items[index].maxBudget = progress
                            } else {
                                items[index].budget = progress
                            }
                        }
                        notifyItemRangeChanged(1, itemCount)
                        itemChangeListener?.onChanged(items, 0)
                    }

                    override fun onOverTotalBudget(totalBudget: Int, budgetSum: Int) {
                        items.forEachIndexed { index, budget ->
                            if (index > 0) {
                                val ratio = budget.budget.toFloat() / budgetSum
                                items[index].budget = (totalBudget * ratio).toInt()
                            }
                        }
                        notifyDataSetChanged()
                        itemChangeListener?.onChanged(items, 0)
                    }
                }
            }
            else -> BudgetViewHolder(
                ItemBudgetBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            ).also {
                it.changeListener = object : BudgetChangeListener {
                    override fun onStopTouch(progress: Int, position: Int) {
                        items[position].budget = progress
                        notifyItemChanged(0)
                        itemChangeListener?.onChanged(items, position)
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> ITEM_VIEW_TYPE_HEADER
            items.size -> ITEM_VIEW_TYPE_FOOTER
            else -> ITEM_VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BudgetHeaderViewHolder -> {
                holder.bindViewHolder(items)
            }
            is BudgetViewHolder -> {
                holder.bindViewHolder(items[holder.layoutPosition], holder.layoutPosition)
            }
            is FooterViewHolder -> {
            }
        }
    }

    override fun getItemCount() = items.size + 1

    fun setItems(budgets: List<Budget>) {
        items = budgets.toMutableList()
        notifyDataSetChanged()
    }

    fun addItem(budget: Budget) {
        items.add(budget)
        notifyDataSetChanged()
    }

    interface ItemChangeListener {
        fun onChanged(budgets: List<Budget>, position: Int)
    }

    inner class FooterViewHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        private const val ITEM_VIEW_TYPE_HEADER = 0
        private const val ITEM_VIEW_TYPE_ITEM = 1
        private const val ITEM_VIEW_TYPE_FOOTER = 2
    }
}