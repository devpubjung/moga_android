package com.devpub.moga.ui.fragment

import com.devpub.moga.databinding.FragmentInsightBinding
import com.devpub.moga.ui.fragment.adapter.InsightPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InsightFragment : BaseFragment<FragmentInsightBinding>() {

    private  val adapter: InsightPagerAdapter by lazy { InsightPagerAdapter(this) }


    override fun setUpViews() {
        binding.pager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = when(position) {
                1 -> "소비그룹 비교"
                else -> "월별 비교"
            }
        }.attach()
    }

}