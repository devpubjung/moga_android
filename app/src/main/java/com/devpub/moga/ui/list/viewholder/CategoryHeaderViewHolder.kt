package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemCategoryPadHeaderBinding

class CategoryHeaderViewHolder(
    binding: ItemCategoryPadHeaderBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemCategoryPadHeaderBinding>(binding, handler)