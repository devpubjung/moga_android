package com.devpub.moga.ui.widget

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.devpub.common.binidng.BindingBottomSheetDialog
import com.devpub.common.list.RecyclerHandler
import com.devpub.common.model.ListItem
import com.devpub.moga.util.extensions.stateExpanded
import com.devpub.moga.databinding.WidgetSelectItemBottomSheetBinding
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.extensions.dragFixed

class SelectItemBottomSheetDialog : BindingBottomSheetDialog<WidgetSelectItemBottomSheetBinding>() {

    private val title by lazy {
        arguments?.getString(TITLE) ?: throw NullPointerException("타이틀을 지정해주세요.")
    }
    private val listItems by lazy {
        arguments?.getSerializable(LIST_ITEM) as? List<ListItem>
            ?: throw NullPointerException("listItem을 넣어주세요.")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews() {
        stateExpanded()
        val adapter = ListAdapter(Handler { item ->
            setFragmentResult(TAG, bundleOf(itemKey to item))
            dismiss()
        })
        bind {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter

            titleTextView.text = title
            adapter.submitList(listItems)
        }
    }

    class Handler(private val listener: (item: ListItem) -> Unit) : RecyclerHandler {
        fun onClickItem(item: ListItem) {
            listener(item)
        }
    }

    companion object {
        const val TAG = "SelectItemBottomSheetDialog"
        const val itemKey = "itemKey"

        private const val TITLE = "title"
        private const val LIST_ITEM = "listItem"

        fun newInstance(title: String, listItem: List<ListItem>) =
            SelectItemBottomSheetDialog().apply {
                arguments = Bundle().apply {
                    putString(TITLE, title)
                    putSerializable(LIST_ITEM, ArrayList(listItem))
                }
            }
    }
}