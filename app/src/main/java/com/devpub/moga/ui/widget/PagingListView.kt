package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import androidx.core.view.isVisible
import androidx.paging.LoadState
import com.devpub.common.binidng.BindingComponent
import com.devpub.common.list.FooterStateAdapter
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetPageListBinding
import com.devpub.moga.ui.list.adapter.PageListAdapter


class PagingListView @JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    BindingComponent<WidgetPageListBinding>(context, attrs, R.layout.widget_page_list) {

    fun setAdapter(adapter: PageListAdapter) {
        binding.apply {
//            val animator = recyclerView.itemAnimator
//            if (animator is SimpleItemAnimator) {
//                animator.supportsChangeAnimations = false
//            }
//recyclerView.setHasFixedSize(true)
            recyclerView.adapter =
                adapter.withLoadStateFooter(FooterStateAdapter { adapter.retry() })
            adapter.apply {
                addLoadStateListener { loadState ->
                    val isListEmpty = (loadState.refresh is LoadState.NotLoading && itemCount == 0)
                        .also { empty ->
                            if (empty) {
                                infoIconImageView.setImageResource(R.drawable.ic_component_none)
                                infoTextView.text = context.getString(R.string.no_data)
                            }
                        }
                    val errorState = loadState.refresh as? LoadState.Error
                        ?: loadState.prepend as? LoadState.Error
                    errorState?.let {
                        infoIconImageView.setImageResource(R.drawable.ic_component_error)
                        infoTextView.text = it.error.localizedMessage
                    }

                    infoLayout.isVisible = isListEmpty || errorState != null
                    progressBar.isVisible = loadState.refresh is LoadState.Loading
                            && itemCount == 0
                    recyclerView.isVisible =
                        errorState !is LoadState.Error && itemCount != 0
                    retryButton.isVisible = errorState is LoadState.Error
                }
            }
            retryButton.setOnClickListener { adapter.retry() }
            swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor)
            swipeRefreshLayout.setOnRefreshListener {
                swipeRefreshLayout.isRefreshing = true
                adapter.refresh()
                swipeRefreshLayout.isRefreshing = false
            }
        }
    }
}