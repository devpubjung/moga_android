package com.devpub.moga.ui.model

import java.io.Serializable
import java.util.*

data class NoteCalendarItem(
    val calendar: Calendar,
    val incomeMoney: Long = 0,
    val expendMoney: Long = 0,
    val transferMoney: Long = 0,
    val transferExpendMoney: Long = 0,
) : Serializable