package com.devpub.moga.ui.model

import java.io.Serializable

data class PieChartItem(
    val value: Float,
    val color: Int,
    val label: String,
) : Serializable