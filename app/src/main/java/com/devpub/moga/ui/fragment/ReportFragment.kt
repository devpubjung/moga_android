package com.devpub.moga.ui.fragment

import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.devpub.moga.R
import com.devpub.moga.databinding.FragmentReportBinding
import com.devpub.moga.domain.model.AssetsSum
import com.devpub.moga.domain.model.CategorySum
import com.devpub.moga.domain.model.ExpendReport
import com.devpub.moga.domain.model.UserSum
import com.devpub.moga.ui.model.PieChartItem
import com.devpub.moga.ui.widget.ReportDetailView
import com.devpub.moga.ui.widget.ReportUserDetailView
import com.devpub.moga.util.ColorUtil
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MoneyUtil.commaWon
import com.devpub.moga.util.bindingadapter.setVisible
import com.devpub.moga.util.extensions.toast
import com.devpub.moga.viewmodel.ReportViewModel
import com.google.android.gms.ads.AdRequest
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_report.*
import view.hellocharts.formatter.SimpleAxisValueFormatter
import view.hellocharts.listener.ColumnChartOnValueSelectListener
import view.hellocharts.model.*
import java.util.*


@AndroidEntryPoint
class ReportFragment : BaseFragment<FragmentReportBinding>() {

    private val calendar: Calendar = DateUtil.today
    private val colors = ColorUtil.generatorColors()

    private val viewModel: ReportViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        loadYearData()
    }

    override fun setUpViews() {
        if (!removeAds) {
            binding.adView.loadAd(AdRequest.Builder().build())
        }
        binding.swipeLayout.setColorSchemeColors(requireContext().getColor(R.color.primaryDarkColor))
        binding.swipeLayout.setOnRefreshListener {
            viewModel.loadYearReport(calendar.time)
            binding.swipeLayout.isRefreshing = false
        }
    }

    override fun onResume() {
        super.onResume()
        setVisible(binding.adView, !removeAds)
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }

        viewModel.reportGraphData.observe {
            initColumnGraph(it.expendReportList, it.monthSumMax.toFloat(), it.startWeek)
            initLineGraph(it.weekSumMax.toFloat(), it.startWeek)
            binding.chartLayout.visibility = View.VISIBLE
            if (it.monthSumMax > 0) {
                selectMonth()
            } else {
                resetPieChart()
            }
        }
    }

    private fun loadYearData() {
        binding.currentMonthTitle.text = "${DateUtil.getYearText(calendar)}년"
        viewModel.loadYearReport(calendar.time)
    }

    private fun initLineGraph(max: Float = 100f, startWeek: Int) {
        val days = DateUtil.getWeekSymbol(startWeek)
        val axisValues = mutableListOf<AxisValue>()
        val values = mutableListOf<PointValue>()

        for (i in days.indices) {
            values.add(PointValue(i.toFloat(), 0f))
            axisValues.add(AxisValue(i.toFloat()).setLabel(days[i]))
        }

        lineChart.apply {
            isViewportCalculationEnabled = false
            isZoomEnabled = false
            val line = Line(values).apply {
                setHasLabelsOnlyForSelected(true)
                isValueSelectionEnabled = true
                color = ContextCompat.getColor(requireContext(), R.color.primaryDarkColor)
                isCubic = true
            }
            lineChartData = LineChartData(listOf(line)).apply {
                axisXBottom = Axis(axisValues).apply {
                    setHasLines(true)
                }
                axisYLeft = Axis().apply {
                    setHasLines(true)
                    maxLabelChars = 3
                    name = "만원"
                    formatter = SimpleAxisValueFormatter(0)
                }
            }

            val top = if (max <= 0) {
                100f
            } else {
                max
            }
            val v = Viewport(0f, top, (days.size - 1).toFloat(), 0f)
            maximumViewport = v
            currentViewport = v
        }
    }

    private fun initColumnGraph(expendReportList: List<ExpendReport>, max: Float, startWeek: Int) {
        val months = DateUtil.getMonthSymbol()
        val axisValues = mutableListOf<AxisValue>()
        val columns = mutableListOf<Column>()

        for (i in months.indices) {
            axisValues.add(AxisValue(i.toFloat()).setLabel(months[i]))
            columns.add(Column(listOf(SubcolumnValue(expendReportList[i].monthlySum.toFloat(),
                colors[i % colors.size]))).setHasLabelsOnlyForSelected(true))
        }
        columnChart.apply {
            isValueSelectionEnabled = true
            isZoomEnabled = false
            columnChartData = ColumnChartData(columns).apply {
                axisXBottom = Axis(axisValues).apply {
                    setHasLines(true)
                }
                axisYLeft = Axis().apply {
                    setHasLines(true)
                    maxLabelChars = 3
                    name = "만원"
                    formatter = SimpleAxisValueFormatter(0)
                }
            }
            val top = if (max <= 0) {
                100f
            } else {
                max
            }
            val viewport = Viewport(0f,
                top,
                (months.size - 1).toFloat(),
                0f)
            maximumViewport = viewport
            currentViewport = viewport
            onValueTouchListener = object : ColumnChartOnValueSelectListener {
                override fun onValueSelected(
                    columnIndex: Int,
                    subcolumnIndex: Int,
                    value: SubcolumnValue,
                ) {
                    calendar.set(Calendar.MONTH, columnIndex)
                    val sourceString = "지출 통계 (<b>${DateUtil.getMonthText(calendar)}월</b>)"
                    binding.titleText.text = Html.fromHtml(sourceString, Html.FROM_HTML_MODE_LEGACY)
                    with(expendReportList[columnIndex]) {
                        setLineData(value.color, dayOfWeekSumMap, startWeek)
                        setUserChart(sumOfUserList)
                        setCategoryChart(sumOfCategoryList)
                        setAssetsChart(sumOfAssetsList)
                        setVisible(binding.emptyView.root,
                            sumOfUserList.isNullOrEmpty()
                                    && sumOfCategoryList.isNullOrEmpty()
                                    && sumOfAssetsList.isNullOrEmpty())
                    }
                }

                override fun onValueDeselected() {
                    setLineData(ContextCompat.getColor(requireContext(), R.color.primaryDarkColor),
                        null, startWeek)
                    resetPieChart()
                }
            }
        }
    }

    private fun resetPieChart() {
        bind {
            titleText.text = "지출 통계"
            userReportView.visibility = View.GONE
            categoryReportView.visibility = View.GONE
            assetsReportView.visibility = View.GONE
            emptyView.root.visibility = View.VISIBLE
        }

    }

    private fun setLineData(color: Int, items: Map<Int, Long>?, startWeek: Int) {
        lineChart.cancelDataAnimation()
        val line = lineChart.lineChartData.lines[0]
        line.color = color
        line.values.forEachIndexed { index, pointValue ->
            val value = if (items != null) {
                items[(index + 6 + startWeek) % 7]?.toFloat() ?: 0f
            } else {
                0f
            }
            pointValue.setTarget(pointValue.x, value)
        }
        lineChart.startDataAnimation(300)
    }

    private fun selectMonth() {
        val month = calendar.get(Calendar.MONTH)
        binding.columnChart.selectValue(
            SelectedValue(
                month,
                0,
                SelectedValue.SelectedValueType.COLUMN)
        )
    }

    private fun setUserChart(userList: List<UserSum>) {
        val items = mutableListOf<PieChartItem>()
        val views = mutableListOf<ReportUserDetailView>()
        val sum = userList.sumOf { it.sum }

        userList.forEachIndexed { i, report ->
            val color = getColor(i)
            val percent = getPercent(report.sum.toFloat(), sum)
            if (percent > 0) {
                items.add(PieChartItem(percent,
                    color,
                    report.user.nickName))
            }

            views.add(ReportUserDetailView(requireContext()).apply {
                setData(report)
                binding.titleText.setTextColor(color)
            })
        }
        binding.userReportView.visibility = View.VISIBLE
        binding.userReportView.setData(items, views)
    }

    private fun setCategoryChart(categoryList: List<CategorySum>) {
        val items = mutableListOf<PieChartItem>()
        val views = mutableListOf<ReportDetailView>()
        val sum = categoryList.sumOf { it.sum }

        categoryList.forEachIndexed { i, report ->
            val color = getColor(i)
            val percent = getPercent(report.sum.toFloat(), sum)
            if (percent > 0) {
                items.add(PieChartItem(percent,
                    color,
                    report.category.name))
            }

            views.add(ReportDetailView(requireContext()).apply {
                binding.legendView.setBackgroundColor(color)
                binding.titleText.text = report.category.name
                binding.contentText.text = report.sum.commaWon()
            })
        }

        binding.categoryReportView.visibility = View.VISIBLE
        binding.categoryReportView.setData(items, views)
    }

    private fun setAssetsChart(assetsList: List<AssetsSum>) {
        val items = mutableListOf<PieChartItem>()
        val views = mutableListOf<ReportDetailView>()
        val sum = assetsList.sumOf { it.sum }

        assetsList.forEachIndexed { i, report ->
            val color = getColor(i)
            val percent = getPercent(report.sum.toFloat(), sum)
            if (percent > 0) {
                items.add(PieChartItem(percent,
                    color,
                    report.assets.name))
            }

            views.add(ReportDetailView(requireContext()).apply {
                binding.legendView.setBackgroundColor(color)
                binding.titleText.text = report.assets.name
                binding.contentText.text = report.sum.commaWon()
            })
        }

        binding.assetsReportView.visibility = View.VISIBLE
        binding.assetsReportView.setData(items, views)
    }

    private fun getPercent(data: Float, sum: Long) = (data / sum) * 100

    private fun getColor(i: Int): Int {
        return if (i == 0) {
            ContextCompat.getColor(requireContext(), R.color.primaryDarkColor)
        } else {
            colors.shuffled()[i % colors.size]
        }
    }

    fun onClickPrevYear() {
        calendar.add(Calendar.YEAR, -1)
        loadYearData()
    }

    fun onClickNextYear() {
        calendar.add(Calendar.YEAR, 1)
        loadYearData()
    }
}