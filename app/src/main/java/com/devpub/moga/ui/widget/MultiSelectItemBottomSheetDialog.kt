package com.devpub.moga.ui.widget

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.devpub.common.binidng.BindingBottomSheetDialog
import com.devpub.common.list.BaseListAdapter
import com.devpub.common.list.RecyclerHandler
import com.devpub.common.model.ListItem
import com.devpub.moga.databinding.WidgetMultiSelectItemBottomSheetBinding
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.ui.model.CheckTextItem

class MultiSelectItemBottomSheetDialog :
    BindingBottomSheetDialog<WidgetMultiSelectItemBottomSheetBinding>() {

    private val title by lazy {
        arguments?.getString(TITLE) ?: throw NullPointerException("타이틀을 지정해주세요.")
    }
    private val listItems by lazy {
        arguments?.getSerializable(LIST_ITEM) as? List<ListItem>
            ?: throw NullPointerException("listItem을 넣어주세요.")
    }

    private val handler = Handler {
        binding.completeButton.isEnabled =
            adapter.currentList.map { it as CheckTextItem }.any { it.checked }
    }
    private val adapter: BaseListAdapter = ListAdapter(handler)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews() {
        binding.view = this
        bind {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter

            titleTextView.text = title
            adapter.submitList(listItems)
        }
    }

    fun onClickComplete() {
        setFragmentResult(TAG, bundleOf(itemKey to adapter.currentList.map { it as CheckTextItem }.filter { it.checked }))
        dismiss()
    }

    class Handler(private val listener: (item: CheckTextItem) -> Unit) : RecyclerHandler {
        fun onClickItem(item: ListItem) {
            listener(item as CheckTextItem)
        }
    }

    companion object {
        const val TAG = "MultiSelectItemBottomSheetDialog"
        const val itemKey = "itemKey"

        private const val TITLE = "title"
        private const val LIST_ITEM = "listItem"

        fun newInstance(title: String, listItem: List<ListItem>) =
            MultiSelectItemBottomSheetDialog().apply {
                arguments = Bundle().apply {
                    putString(TITLE, title)
                    putSerializable(LIST_ITEM, ArrayList(listItem))
                }
            }
    }
}