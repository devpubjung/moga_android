package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemSimpleTextBinding


class SimpleTextViewHolder(
    binding: ItemSimpleTextBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemSimpleTextBinding>(binding, handler)