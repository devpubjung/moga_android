package com.devpub.moga.ui.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.devpub.moga.R
import com.devpub.moga.databinding.ViewInputItemContainerBinding


class InputItemContainerLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var binding: ViewInputItemContainerBinding
    private var focusColor: Int = R.color.primaryColor
    private var defaultColor: Int = R.color.divider
    private var isFocus: Boolean = false

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.view_input_item_container, this)
            view.findViewById<TextView>(R.id.titleText).text = "Title"
        } else {
            binding =
                ViewInputItemContainerBinding.inflate(LayoutInflater.from(context), this, true)
            if (attrs != null) {
                val attributes =
                    context.obtainStyledAttributes(attrs, R.styleable.InputItemContainerLayout)
                if (attributes.hasValue(R.styleable.InputItemContainerLayout_title)) {
                    setTitle(attributes.getString(R.styleable.InputItemContainerLayout_title))
                }
                attributes.recycle()
            }
        }
    }

    fun setTitle(title: String?) {
        binding.titleText.text = title ?: "title"
    }

    fun setFocusColor(color: Int) {
        focusColor = color
    }

    @SuppressLint("ResourceAsColor")
    fun setFocus(isFocus: Boolean) {
        this.isFocus = isFocus
        notiChangeFocusColor()
    }

    fun notiChangeFocusColor() {
        if (isFocus) {
            binding.underLine.setBackgroundColor(ContextCompat.getColor(context, focusColor))
        } else {
            binding.underLine.setBackgroundColor(ContextCompat.getColor(context, defaultColor))
        }
    }

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams?) {
        val id = child.id
        if (id == R.id.titleText || id == R.id.guideLine || id == R.id.rootLayout || id == R.id.underLine || id == R.id.contentLayout) {
            super.addView(child, index, params)
        } else {
            binding.contentLayout.addView(child, index, params)
        }
    }

}