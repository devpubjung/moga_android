package com.devpub.moga.ui.model

import com.devpub.common.model.ListItem

data class SimpleTextItem(
    val value: Int,
    val unit: String = "",
    val text: String? = null
) : ListItem {

}