package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemNoteBinding

class NoteViewHolder(private val binding: ItemNoteBinding, handler: RecyclerHandler?) :
    BindingViewHolder<ItemNoteBinding>(binding, handler) {

}