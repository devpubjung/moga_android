package com.devpub.moga.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import com.devpub.common.mvvm.BaseActivity
import com.devpub.common.util.extension.toast
import com.devpub.moga.databinding.ActivitySettingBinding
import com.devpub.moga.ui.model.SimpleTextItem
import com.devpub.moga.ui.widget.SelectItemBottomSheetDialog
import com.devpub.moga.util.PermissionUtil
import com.devpub.moga.util.extensions.setBottomSheetResultListener
import com.devpub.moga.viewmodel.SettingViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingActivity : BaseActivity<ActivitySettingBinding>() {

    private val viewModel: SettingViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.pingServer()
    }

    private fun setUpViews() {
        binding.view = this
        binding.viewModel = viewModel
        bind {
            toolbar.setNavigationOnClickListener { finish() }
            notiAllowSwitch.setOnCheckedChangeListener { _, isChecked ->
                viewModel?.notiAutoInput?.value = isChecked
                notiIconAllowSwitch.isChecked = isChecked
                notiShareAllowSwitch.isChecked = isChecked
            }

            notiAllowLayout.isVisible = PermissionUtil.isNotiPermissionAllowed(this@SettingActivity)
            binding.startWeekButton.setSubTitle(when(viewModel?.getStartWeek()) {
                0 -> "일요일"
                else -> "월요일"
            })

            binding.startDayOfMonthButton.setSubTitle("${viewModel?.getStartDayOfMonth() ?: 1}일")
        }
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            toast(it, Toast.LENGTH_LONG)
        }

        viewModel.errorEvent.observe {
            toast(it.localizedMessage, Toast.LENGTH_LONG)
        }
    }


    fun onClickStartWeekButton() {
        SelectItemBottomSheetDialog.newInstance("한 주의 시작을 설정해주세요.", listOf(
            SimpleTextItem(1, text = "월요일"),
            SimpleTextItem(0, text = "일요일")
        )).show(supportFragmentManager, SelectItemBottomSheetDialog.TAG)

        setBottomSheetResultListener(SelectItemBottomSheetDialog.TAG) { _, bundle ->
            val selectItem =
                bundle.getSerializable(SelectItemBottomSheetDialog.itemKey) as SimpleTextItem
            binding.startWeekButton.setSubTitle(selectItem.text)
            viewModel.setStartWeek(selectItem.value)
        }
    }

    fun onClickStartDayOfMonthButton() {
        val list = mutableListOf<SimpleTextItem>()
        for (i in 1..31) {
            list.add(SimpleTextItem(i, "일"))
        }
        SelectItemBottomSheetDialog.newInstance("매월 시작 일을 설정해주세요.", list)
            .show(supportFragmentManager, SelectItemBottomSheetDialog.TAG)

        setBottomSheetResultListener(SelectItemBottomSheetDialog.TAG) { _, bundle ->
            val selectItem =
                bundle.getSerializable(SelectItemBottomSheetDialog.itemKey) as SimpleTextItem
            binding.startDayOfMonthButton.setSubTitle("${selectItem.value}${selectItem.unit}")
            viewModel.setStartDayOfMonth(selectItem.value)
        }
    }


    companion object {
        fun start(context: Context) {
            Intent(context, SettingActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }
}