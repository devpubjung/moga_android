package com.devpub.moga.ui.fragment

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.devpub.moga.databinding.FragmentHomeBudgetListBinding
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.ui.model.BudgetDetailType
import com.devpub.moga.ui.model.HomePageModel
import com.devpub.moga.viewmodel.HomeBudgetListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeBudgetListFragment : BaseFragment<FragmentHomeBudgetListBinding>() {

    private val viewModel: HomeBudgetListViewModel by viewModels()

    private val adapter: ListAdapter = ListAdapter()

    private var pageModel: HomePageModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            pageModel = it.getSerializable(PAGE_MODEL) as HomePageModel
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadBudgetReport(pageModel?.type == BudgetDetailType.MONTHLY)
    }
    override fun setUpViews() {
        bind {
            view = this@HomeBudgetListFragment
            viewModel = this@HomeBudgetListFragment.viewModel
            adListView.setAdapter(adapter, appPreference)
        }
    }

    companion object {
        private const val PAGE_MODEL = "page_model"

        fun newInstance(pageModel: HomePageModel) =
            HomeBudgetListFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(PAGE_MODEL, pageModel)
                }
            }
    }
}