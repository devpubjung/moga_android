package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import com.devpub.common.binidng.BindingComponent
import com.devpub.moga.R
import com.devpub.moga.domain.model.BudgetReport
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.databinding.WidgetHomeHeaderBinding
import com.devpub.moga.ui.activity.BudgetSettingActivity
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MoneyUtil
import com.devpub.moga.util.MoneyUtil.commaManWon
import com.devpub.moga.util.MoneyUtil.commaWon
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

class HomeHeaderView @JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null) :
    BindingComponent<WidgetHomeHeaderBinding>(context, attrs, R.layout.widget_home_header) {

    private var isShowBudget = true

    fun openBudgetSetting() {
        BudgetSettingActivity.startForResult(context)
    }

    fun setReportData(budgetReport: BudgetReport) {
        bind {
            view = this@HomeHeaderView
            weeklyPeriodText.text = "(${DateUtil.getWeekPeriod(startWeek = AppPreferenceImpl(context).startWeek)})"
            weeklyProgressBar.apply {
                setGoal(budgetReport.totalBudget.weeklyBudget.commaManWon())
                showWeekBudget(budgetReport, isShowBudget)

                setOnClickListener {
                    isShowBudget = !isShowBudget
                    showWeekBudget(budgetReport, isShowBudget)
                }
            }

            monthlyTitleText.text = "${DateUtil.thisMonth()}월 지출 현황"
            monthlyProgressBar.apply {
                setProgressPercentage(budgetReport.monthSum.toDouble() / MoneyUtil.convertFullMoney(
                    budgetReport.totalBudget.budget) * 100)
            }
            monthlyUsedText.text = budgetReport.monthSum.commaWon()
            monthlyBudgetText.text = budgetReport.totalBudget.budget.commaManWon()
        }
    }

    private fun showWeekBudget(budgetReport: BudgetReport, showBudget: Boolean) {
        binding.weeklyProgressBar.apply {
            val weeklyBudget = MoneyUtil.convertFullMoney(budgetReport.totalBudget.weeklyBudget)
            if (showBudget) {
                setTitle("남은 금액")
                setUse(budgetReport.thisWeekSum.commaWon())
                setMaxValues(weeklyBudget)
                setCurrentValues(weeklyBudget - budgetReport.thisWeekSum)
            } else {
                setTitle("지출 금액")
                setUse(null)
                setMaxValues(Math.max(weeklyBudget, budgetReport.thisWeekSum))
                setCurrentValues(budgetReport.thisWeekSum)
            }
        }
    }
}