package com.devpub.moga.ui.fragment

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.devpub.common.binidng.BindingFragment
import com.devpub.common.mvvm.BaseActivity
import com.devpub.common.util.StatusBarUtil
import com.devpub.moga.R
import com.devpub.moga.data.preference.AppPreference
import javax.inject.Inject

abstract class BaseFragment<VB : ViewDataBinding> : BindingFragment<VB>() {

    @Inject
    internal lateinit var appPreference: AppPreference

    protected var removeAds: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        StatusBarUtil.setStatusBarColorForLollipop(requireActivity().window,
            ContextCompat.getColor(requireContext(), R.color.backgroundColor))
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        removeAds = appPreference.purchasedRemoveAds
    }

    protected fun setLoading(show: Boolean) {
        baseActivity {
            if (show) {
                showLoading()
            } else {
                hideLoading()
            }
        }
    }

    protected fun baseActivity(block: BaseActivity<*>.() -> Unit) {
        activity?.let {
            block(it as BaseActivity<*>)
        }
    }

}