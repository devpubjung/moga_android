package com.devpub.moga.ui.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.devpub.common.mvvm.BaseActivity
import com.devpub.moga.R
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.databinding.ActivityMainBinding
import com.devpub.moga.ui.activity.behavior.ExpendFloatingBehavior
import com.devpub.moga.ui.fragment.NoteFragment
import com.devpub.moga.ui.model.InternalDeepLink
import com.devpub.moga.ui.model.MainPage
import com.devpub.moga.ui.widget.DefaultPopupDialog
import com.devpub.moga.util.ColorUtil
import com.devpub.moga.util.DisplayUtil
import com.devpub.moga.util.PermissionUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    @Inject
    internal lateinit var appPreference: AppPreference

    private val navController by lazy { findNavController(R.id.navHost) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpViews()
        if (!appPreference.watchNotiPermission && !PermissionUtil.isNotiPermissionAllowed(this)) {
            PermissionUtil.requireNotiPermission(this, appPreference)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        navController.handleDeepLink(intent)
    }

    private fun setUpViews() {
        binding.view = this
        bind {
            bottomNavigationView.setupWithNavController(navController)
            (intent.getSerializableExtra(TAB) as? MainPage)?.let {
                navController.navigate(InternalDeepLink.makeMainTabDeepLink(it))
            }
            navController.addOnDestinationChangedListener { controller, destination, arguments ->
                addInputButton.isVisible = when (destination.id) {
                    R.id.homeFragment,
                    R.id.noteFragment,
                    R.id.insightFragment,
                    -> true
                    else -> false
                }
            }
            addInputButton.setOnClickListener {
                currentFragment {
                    var calendar: Calendar? = null
                    if (it is NoteFragment) {
                        calendar = it.getSelectedDate()
                    }
                    InputActivity.startForResult(this@MainActivity, calendar = calendar)
                }
            }
        }
    }

    private inline fun currentFragment(invoke: (Fragment) -> Unit) {
        supportFragmentManager.findFragmentById(R.id.navHost)?.let { navFragment ->
            navFragment.childFragmentManager.primaryNavigationFragment?.let { fragment ->
                invoke(fragment)
            }
        }
    }

    fun showFirecracker(dialog: Dialog) {
        lifecycleScope.launchWhenStarted {
            val deviceWith = DisplayUtil.getDeviceWith(this@MainActivity).toFloat()
            val deviceHeight = DisplayUtil.getDeviceHeight(this@MainActivity).toFloat()

            showKonfetti(deviceWith / 2, deviceHeight / 6)
            delay(200)
            showKonfetti(deviceWith / 3, deviceHeight / 5)
            delay(300)
            showKonfetti(deviceWith / 1.5f, deviceHeight / 4)
            delay(1000)
            dialog.show()
        }
    }

    fun showFirecracker(topText: String, titleTextResId: Int, contentTextResId: Int) {
        lifecycleScope.launchWhenStarted {
            showFirecracker(DefaultPopupDialog(this@MainActivity).apply {
                setTopText(topText)
                setTitleText(titleTextResId)
                setContentText(contentTextResId)
            })
        }
    }

    private fun showKonfetti(x: Float, y: Float) {
        binding.viewKonfetti.build()
            .addColors(ColorUtil.generatorColors())
            .setDirection(0.0, 360.0)
            .setSpeed(2f, 7f)
            .setFadeOutEnabled(true)
            .setTimeToLive(1500)
            .addShapes(Shape.Square, Shape.Circle)
            .addSizes(Size(10), Size(12, 6f))
            .setPosition(x, y)
            .burst(100)
    }

    companion object {
        private const val TAB = "tab"
        fun start(context: Context, tab: String?) {
            Intent(context, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_CLEAR_TOP)
                putExtra("TAB", tab)
                context.startActivity(this)
            }
        }
    }
}