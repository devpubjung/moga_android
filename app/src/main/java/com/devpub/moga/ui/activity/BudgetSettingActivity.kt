package com.devpub.moga.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.devpub.common.util.StatusBarUtil
import com.devpub.common.mvvm.BaseActivity
import com.devpub.moga.R
import com.devpub.moga.domain.model.Budget
import com.devpub.moga.databinding.ActivityBudgetSettingBinding
import com.devpub.moga.domain.model.Category
import com.devpub.moga.ui.list.adapter.BudgetSettingAdapter
import com.devpub.moga.ui.widget.pad.CategoryBottomSheetDialog
import com.devpub.moga.util.extensions.setBottomSheetResultListener
import com.devpub.moga.viewmodel.BudgetSettingViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class BudgetSettingActivity : BaseActivity<ActivityBudgetSettingBinding>() {

    private val adapter: BudgetSettingAdapter by lazy { BudgetSettingAdapter() }

    private val viewModel: BudgetSettingViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    private fun setUpViews() {
        StatusBarUtil.setStatusBarColorForLollipop(window,
            ContextCompat.getColor(this, R.color.primaryColor))
        with(binding) {
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowTitleEnabled(false)
                toolbarTitle.text = "예산 설정"
            }
            view = this@BudgetSettingActivity

            recyclerView.adapter = adapter

            adapter.itemChangeListener = object : BudgetSettingAdapter.ItemChangeListener {
                override fun onChanged(budgets: List<Budget>, position: Int) {
                    lifecycleScope.launch {
                        delay(500)
                        viewModel.changeBudget(budgets)
                    }
                }
            }

            setBottomSheetResultListener(CategoryBottomSheetDialog.TAG) { _, bundle ->
                (bundle.getSerializable(CategoryBottomSheetDialog.itemKey) as Category).let { category ->
                    viewModel.addBudget(category)
                }
            }
        }
    }

    private fun observeViewModel() {
        viewModel.budgetListLiveData.observe {
            adapter.setItems(it)
        }

        viewModel.budgetSaveEvent.observe {
            if (it) setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.budget_setting_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_budget_add -> {
            CategoryBottomSheetDialog.newInstance()
                .show(supportFragmentManager, CategoryBottomSheetDialog.TAG)
            true
        }
        android.R.id.home -> {
            viewModel.saveItem()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        viewModel.saveItem()
    }

    companion object {
        fun startForResult(
            context: Context?,
            resultLauncher: ActivityResultLauncher<Intent>? = null,
        ) {
            context?.run {
                Intent(this, BudgetSettingActivity::class.java).apply {
                    resultLauncher?.launch(this) ?: context.startActivity(this)
                }
            }
        }
    }
}