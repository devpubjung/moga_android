package com.devpub.moga.ui.widget.pad

import android.app.AlertDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.devpub.common.binidng.BindingBottomSheetDialog
import com.devpub.common.list.RecyclerHandler
import com.devpub.moga.databinding.WidgetAssetsPadBinding
import com.devpub.moga.domain.model.Assets
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.extensions.*
import com.devpub.moga.viewmodel.pad.AssetsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AssetsBottomSheetDialog : BindingBottomSheetDialog<WidgetAssetsPadBinding>() {

    private val viewModel: AssetsViewModel by viewModels()
    private val adapter: ListAdapter by lazy { ListAdapter(Handler(this, viewModel)) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    private fun setUpViews() {
        peekHeight(350f)
        bind {
            recyclerView.adapter = adapter
        }
    }

    private fun observeViewModel() {
        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }

        viewModel.assetsListData.observe {
            adapter.submitList(it)
        }
    }

    private fun showAddAssetsPopup() {
        val container = FrameLayout(requireContext())
        val editText = EditText(context).apply {
            layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                leftMargin = resources.getDimensionPixelSize(com.devpub.moga.R.dimen._16dp)
                rightMargin = resources.getDimensionPixelSize(com.devpub.moga.R.dimen._16dp)
            }
            val colorStateList =
                ColorStateList.valueOf(ContextCompat.getColor(context, com.devpub.moga.R.color.primaryDarkColor))
            backgroundTintList = colorStateList
            filters = arrayOf(InputFilter.LengthFilter(25))
            setSingleLine()
        }
        container.addView(editText)

        AlertDialog.Builder(context, com.devpub.moga.R.style.AlertDialogStyle).apply {
            setTitle("자산 추가")
            setMessage("\n추가할 자산을 입력해주세요")
            setIcon(com.devpub.moga.R.drawable.ic_item_add)
            setView(container)
            setPositiveButton("추가") { _, _ ->
                editText.closeKeyboard()
                val name = editText.text.toString()
                if (name.isNotEmpty()) {
                    viewModel.addItem(name)
                }
            }
            setNegativeButton("취소") { _, _ -> }
        }.create().show()
    }

    class Handler(
        private val fragment: AssetsBottomSheetDialog,
        private val viewModel: AssetsViewModel,
    ) : RecyclerHandler {
        fun onClickAddItem() {
            fragment.showAddAssetsPopup()
        }

        fun onClickItem(item: Assets) {
            fragment.setFragmentResult(TAG, bundleOf(itemKey to item))
            fragment.dismiss()
        }

        fun onLongClickItem(item: Assets) :Boolean{
            AlertDialog.Builder(fragment.context, com.devpub.moga.R.style.AlertDialogStyle)
                .apply {
                    setTitle("자산 삭제")
                    setMessage("\n[${item.name}] 자산을 삭제하시겠습니까?")
                    setIcon(com.devpub.moga.R.drawable.ic_item_check)
                    setPositiveButton("삭제") { _, _ ->
                        viewModel.deleteItem(item)
                    }
                    setNegativeButton("취소") { _, _ -> }
                }.create().show()
            return true
        }
    }

    companion object {
        const val TAG = "AssetsBottomSheetDialog"
        const val itemKey = "itemKey"

        fun newInstance() = AssetsBottomSheetDialog()
    }
}