package com.devpub.moga.ui.model

import java.io.Serializable

data class HomePageModel(val title: String, val type: BudgetDetailType) : Serializable

enum class BudgetDetailType {
    WEEKLY, MONTHLY
}
