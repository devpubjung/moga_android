package com.devpub.moga.ui.fragment.adapter

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.devpub.moga.ui.fragment.HomeBudgetListFragment
import com.devpub.moga.ui.model.HomePageModel

class HomePagerAdapter(fragmentActivity: FragmentActivity, private val dataList: List<HomePageModel>) :
    FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount() = dataList.size

    override fun createFragment(position: Int) = HomeBudgetListFragment.newInstance(dataList[position])
}