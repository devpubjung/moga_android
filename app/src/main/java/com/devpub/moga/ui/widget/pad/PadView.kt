package com.devpub.moga.ui.widget.pad

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider

abstract class PadView<T> @JvmOverloads constructor(
    context: Context,
    attributes: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attributes, defStyleAttr) {

    abstract var item: T?
    var padListener: PadClickListener<T>? = null
    var padCancelListener: PadClickCancelListener? = null
    var padDoneListener: PadClickDoneListener<T>? = null

    open fun show() {
        this.visibility = VISIBLE
    }

    open fun hide() {
        this.visibility = GONE
    }

    fun onClickCancel() {
        hide()
        padCancelListener?.onClickCancel()
    }

    fun onClickDone() {
        hide()
        padDoneListener?.onClickDone(item)
    }

    open fun onPadClickCancel(cancel: () -> Unit) {
        this.padCancelListener = object : PadClickCancelListener {
            override fun onClickCancel() {
                cancel()
            }
        }
    }

    open fun onPadClickDone(done: (T?) -> Unit) {
        this.padDoneListener = object : PadClickDoneListener<T> {
            override fun onClickDone(item: T?) {
                done(item)
            }
        }
    }

    fun onPadClickItem(listener: (T) -> Unit) {
        this.padListener = object : PadClickListener<T> {

            override fun onClickItem(item: T) {
                listener(item)
            }
        }
    }

    interface PadClickListener<T> {
        fun onClickItem(item: T)
    }

    interface PadClickCancelListener {
        fun onClickCancel()
    }

    interface PadClickDoneListener<T> {
        fun onClickDone(item: T?)
    }

    @MainThread
    protected inline fun <reified VM : ViewModel> viewModels(activity: AppCompatActivity,
        noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null,
    ): Lazy<VM> {
        val factoryPromise = factoryProducer ?: {
            activity.defaultViewModelProviderFactory
        }

        return ViewModelLazy(VM::class, { activity.viewModelStore }, factoryPromise)
            .also {
            }
    }
}