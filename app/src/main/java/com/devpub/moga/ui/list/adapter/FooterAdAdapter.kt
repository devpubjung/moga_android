package com.devpub.moga.ui.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.devpub.moga.databinding.ItemAdFooterBinding
import com.google.android.gms.ads.AdRequest


class FooterAdAdapter : RecyclerView.Adapter<FooterAdAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemAdFooterBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }


    override fun getItemCount() = 1

    class ViewHolder(binding: ItemAdFooterBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            AdRequest.Builder().build().apply {
                binding.adView.loadAd(this)
            }
        }
    }
}