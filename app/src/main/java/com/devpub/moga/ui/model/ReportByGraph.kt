package com.devpub.moga.ui.model

import com.devpub.moga.domain.model.ExpendReport


data class ReportByGraph(
    val expendReportList: List<ExpendReport>,
    val monthSumMax: Long,
    val weekSumMax: Long,
    val startWeek: Int,
)