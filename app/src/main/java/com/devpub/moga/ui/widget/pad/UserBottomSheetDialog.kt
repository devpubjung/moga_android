package com.devpub.moga.ui.widget.pad

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.devpub.common.binidng.BindingBottomSheetDialog
import com.devpub.common.list.RecyclerHandler
import com.devpub.moga.databinding.WidgetUserPadBinding
import com.devpub.moga.domain.model.User
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.extensions.*
import com.devpub.moga.viewmodel.pad.UserPadViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserBottomSheetDialog : BindingBottomSheetDialog<WidgetUserPadBinding>() {

    private val viewModel: UserPadViewModel by viewModels()
    private val adapter: ListAdapter by lazy { ListAdapter(Handler(this)) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    private fun setUpViews() {
        peekHeight(350f)
        bind {
            recyclerView.adapter = adapter
        }
    }

    private fun observeViewModel() {
        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }

        viewModel.userListData.observe {
            adapter.submitList(it)
        }
    }

    class Handler(
        private val fragment: UserBottomSheetDialog,
    ) : RecyclerHandler {

        fun onClickItem(item: User) {
            fragment.setFragmentResult(TAG, bundleOf(itemKey to item))
            fragment.dismiss()
        }
    }

    companion object {
        const val TAG = "UserBottomSheetDialog"
        const val itemKey = "itemKey"

        fun newInstance() = UserBottomSheetDialog()
    }
}