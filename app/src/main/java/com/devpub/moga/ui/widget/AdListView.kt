package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.SimpleItemAnimator
import com.devpub.common.binidng.BindingComponent
import com.devpub.common.model.ListItem
import com.devpub.moga.R
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.databinding.WidgetListBinding
import com.devpub.moga.ui.list.adapter.FooterAdAdapter
import com.devpub.moga.ui.list.adapter.HeaderAdAdapter
import com.devpub.moga.ui.list.adapter.ListAdapter


class AdListView @JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    BindingComponent<WidgetListBinding>(context, attrs, R.layout.widget_list) {

    private var adapter: ListAdapter? = null

    init {
        bind {
            val animator = recyclerView.itemAnimator
            if (animator is SimpleItemAnimator) {
                animator.supportsChangeAnimations = false
            }
            recyclerView.setHasFixedSize(true)
            swipeRefreshLayout.setColorSchemeResources(R.color.primaryColor)
            attrs?.let {
                val typedArray = context.obtainStyledAttributes(it, R.styleable.AdListView)

                typedArray.getString(R.styleable.AdListView_empty_text)?.apply {
                    binding.emptyTextView.text = this
                }

                typedArray.getDrawable(R.styleable.AdListView_empty_icon)?.apply {
                    binding.emptyIconView.setImageDrawable(this)
                }

                typedArray.getBoolean(R.styleable.AdListView_refresh, false).apply {
                    binding.swipeRefreshLayout.isEnabled = this
                }
                typedArray.recycle()
            }
        }
    }

    fun setRefreshListener(block: () -> Unit) {
        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.swipeRefreshLayout.isRefreshing = true
            block()
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    fun setAdapter(adapter: ListAdapter, appPreference: AppPreference) {
        this.adapter = adapter
        bind {
            recyclerView.adapter = if (appPreference.purchasedRemoveAds) {
                adapter
            } else {
                ConcatAdapter(
                    HeaderAdAdapter(),
                    adapter,
                    FooterAdAdapter()
                )
            }
        }
    }

    fun setListItem(items: List<ListItem>) {
        adapter?.let {
            bind {
                swipeRefreshLayout.isRefreshing = false
                emptyView.isVisible = items.isNullOrEmpty()
                recyclerView.isVisible = !items.isNullOrEmpty()
                it.submitList(items)
            }
        }
    }
}