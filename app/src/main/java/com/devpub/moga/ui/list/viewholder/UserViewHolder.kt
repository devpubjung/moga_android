package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemUserPadBinding


class UserViewHolder(
    binding: ItemUserPadBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemUserPadBinding>(binding, handler)