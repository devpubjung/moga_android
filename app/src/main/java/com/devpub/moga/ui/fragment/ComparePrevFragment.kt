package com.devpub.moga.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.devpub.moga.databinding.FragmentComparePrevBinding
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.extensions.toast
import com.devpub.moga.viewmodel.ComparePrevViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ComparePrevFragment : BaseFragment<FragmentComparePrevBinding>() {

    private val adapter by lazy { ListAdapter() }

    private val viewModel : ComparePrevViewModel by viewModels()
    private var preCalendar : Calendar = DateUtil.today.apply { add(Calendar.MONTH, -1) }
    private var nextCalendar : Calendar = DateUtil.today

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        loadCompareItems()
    }

    override fun setUpViews() {
        bind {
            viewModel = this@ComparePrevFragment.viewModel
            adListView.setAdapter(adapter, appPreference)
            adListView.setRefreshListener {
                loadCompareItems()
            }
        }
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }
    }

    private fun loadCompareItems() {
        binding.prevTitleText.text = "${DateUtil.getFullMonthText(preCalendar)}"
        binding.thisTitleText.text = "${DateUtil.getFullMonthText(nextCalendar)}"
        viewModel.loadCompareData(preCalendar.time, nextCalendar.time)
    }

    companion object {
        fun newInstance() = ComparePrevFragment()
    }
}