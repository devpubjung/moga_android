package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetSelectTitleBinding


class SelectedTitle @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    private lateinit var binding: WidgetSelectTitleBinding

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_select_title, this)
            view.findViewById<TextView>(R.id.titleText).text = "Title"
        } else {
            binding =
                WidgetSelectTitleBinding.inflate(LayoutInflater.from(context), this, true)
            if (attrs != null) {
                val attributes =
                    context.obtainStyledAttributes(attrs, R.styleable.SelectedTitle)
                if (attributes.hasValue(R.styleable.SelectedTitle_titleText)) {
                    setTitle(attributes.getString(R.styleable.SelectedTitle_titleText))
                }
                setSelect(attributes.getBoolean(R.styleable.SelectedTitle_select, isSelected))
                attributes.recycle()
            }
        }
    }

    fun setTitle(title: String?) {
        binding.titleText.text = title ?: "title"
    }

    fun setSelect(isSelected: Boolean) {
        if (isSelected) {
            binding.selectedView.setBackgroundColor(ContextCompat.getColor(context,
                R.color.primaryColor))
        } else {
            binding.selectedView.setBackgroundColor(ContextCompat.getColor(context,
                R.color.transparent))
        }
        this.isSelected = isSelected
    }
}