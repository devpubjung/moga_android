package com.devpub.moga.ui.list.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.devpub.moga.domain.model.Budget
import com.devpub.moga.databinding.ItemBudgetHeaderBinding
import com.devpub.moga.ui.list.BudgetHeaderChangeListener
import com.devpub.moga.util.MoneyUtil.commaManWon
import com.devpub.moga.util.bindingadapter.setVisible
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import view.hellocharts.formatter.SimplePieChartValueFormatter
import view.hellocharts.model.PieChartData
import view.hellocharts.model.SliceValue


class BudgetHeaderViewHolder(private val binding: ItemBudgetHeaderBinding) :
    RecyclerView.ViewHolder(binding.root) {

    private val data: PieChartData = PieChartData().apply {
        setHasLabels(true)
        isValueLabelBackgroundEnabled = false
        slicesSpacing = 1
        formatter = SimplePieChartValueFormatter().setAppendedText("%".toCharArray())
    }
    var changeListener: BudgetHeaderChangeListener? = null
    var budgetSum = 0

    init {
        binding.view = this
        binding.pieChartView.apply {
            isValueSelectionEnabled = true
            isChartRotationEnabled = false
        }
        binding.totalBudgetLayout.budgetBar.apply {
            setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(
                    seekBar: DiscreteSeekBar,
                    value: Int,
                    fromUser: Boolean,
                ) {
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {}

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    val process = seekBar.progress
                    binding.totalBudgetLayout.budgetText = process.commaManWon()
                    changeListener?.onStopTouch(process, 0)
                    if (budgetSum > process) {
                        changeListener?.onOverTotalBudget(process, budgetSum)
                    }
                }
            })
        }
    }

    fun bindViewHolder(itemList: List<Budget>) {
        if (itemList.isEmpty()) {
            return
        }
        val total = itemList[0]
        binding.totalBudgetLayout.apply {
            val totalBudget = total.budget
            budgetText = "${totalBudget}만원"
            budgetBar.max = total.maxBudget
            budgetBar.progress = totalBudget
        }

        binding.item = total
        binding.executePendingBindings()

        if (itemList.size < 2) {
            binding.pieChartView.visibility = View.GONE
            binding.divider.visibility = View.GONE
        } else {
            var existData = false
            calculatorBudgetSum(itemList)

            if (data.values.size == itemList.size - 1) {
                itemList.forEachIndexed { index, budget ->
                    if (index > 0) {
                        data.values[index - 1].setTarget((budget.budget.toFloat() / budgetSum) * 100)
                        if (budget.budget > 0) {
                            existData = true
                        }
                    }
                }

                setVisible(binding.pieChartView, existData)
                setVisible(binding.divider, existData)
            } else {
                data.values = createChartData(itemList, budgetSum)
                binding.pieChartView.visibility = View.VISIBLE
                binding.divider.visibility = View.VISIBLE
            }

            binding.pieChartView.apply {
                pieChartData = data
                startDataAnimation()
            }

            if (budgetSum > total.budget) {
                total.budget = budgetSum
                binding.totalBudgetLayout.budgetBar.apply {
                    progress = Math.min(total.budget, max)
                    binding.totalBudgetLayout.budgetText = "${progress}만원"
                }
            }
        }
    }

    private fun calculatorBudgetSum(itemList: List<Budget>) {
        budgetSum = 0
        itemList.forEachIndexed { index, budget ->
            if (index > 0) {
                budgetSum += budget.budget
            }
        }
    }

    private fun createChartData(itemList: List<Budget>, budgetSum: Int): MutableList<SliceValue> {
        val list = mutableListOf<SliceValue>()
        itemList.forEachIndexed { index, budget ->
            if (index > 0) {
                list.add(createSliceData(itemList, budget, budgetSum))
            }
        }
        return list
    }

    private fun createSliceData(
        itemList: List<Budget>,
        budget: Budget,
        budgetSum: Int,
    ): SliceValue {
        return SliceValue(100f / (itemList.size - 1),
            budget.color).apply {
            setLabel(budget.category.name)
            if (budget.budget > 0) {
                setTarget((budget.budget.toFloat() / budgetSum) * 100)
            } else if (budgetSum > 0) {
                setTarget(0f)
            }
        }
    }
}