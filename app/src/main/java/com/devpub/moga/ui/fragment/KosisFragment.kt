package com.devpub.moga.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.devpub.common.list.RecyclerHandler
import com.devpub.moga.databinding.FragmentKosisBinding
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.extensions.toast
import com.devpub.moga.viewmodel.KosisViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class KosisFragment : BaseFragment<FragmentKosisBinding>() {

    private val viewModel : KosisViewModel by viewModels()

    private val adapter by lazy { ListAdapter(Handler(viewModel)) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewModel.loadKosisByAge()
    }

    override fun setUpViews() {
        bind {
            viewModel = this@KosisFragment.viewModel
            adListView.setAdapter(adapter, appPreference)
            adListView.setRefreshListener {
                viewModel?.loadKosisByAge()
            }
        }
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }
    }

    class Handler(private val viewModel : KosisViewModel) : RecyclerHandler {
        fun onClickByAge() {
            viewModel.loadKosisByAge()
        }

        fun onClickByIncome() {
            viewModel.loadKosisByIncome()
        }
    }

    companion object {

        fun newInstance() = KosisFragment()
    }

}