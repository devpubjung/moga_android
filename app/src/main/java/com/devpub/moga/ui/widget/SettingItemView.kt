package com.devpub.moga.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetSettingBinding

class SettingItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    private lateinit var binding: WidgetSettingBinding
    var showArrow = true

    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_setting, this)
            view.findViewById<TextView>(R.id.titleText).text = "Title"
            view.findViewById<TextView>(R.id.subTitleText).text = "subTitle"
        } else {
            binding =
                WidgetSettingBinding.inflate(LayoutInflater.from(context), this, true)
            if (attrs != null) {
                val attributes =
                    context.obtainStyledAttributes(attrs, R.styleable.SettingItemView)
                if (attributes.hasValue(R.styleable.SettingItemView_menu_title)) {
                    setTitle(attributes.getString(R.styleable.SettingItemView_menu_title))
                }
                if (attributes.hasValue(R.styleable.SettingItemView_menu_subTitle)) {
                    setSubTitle(attributes.getString(R.styleable.SettingItemView_menu_subTitle))
                }

                showArrow = attributes.getBoolean(R.styleable.SettingItemView_show_arrow, true)
                setVisibleArrow(showArrow)

                attributes.recycle()
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        alpha = if (enabled) {
            1f
        } else {
            0.3f
        }
    }

    fun setTitle(title: String?) {
        binding.titleText.text = title ?: "title"
    }

    fun setSubTitle(subTitle: String?) {
        binding.subTitleText.text = subTitle ?: "subTitle"
    }

    fun setVisibleArrow(show: Boolean) {
        if (show) {
            binding.arrowImage.visibility = View.VISIBLE
        } else {
            binding.arrowImage.visibility = View.INVISIBLE
        }
    }
}