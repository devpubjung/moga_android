package com.devpub.moga.ui.list


interface BudgetChangeListener {
    fun onStopTouch(progress: Int, position: Int)
}

interface BudgetHeaderChangeListener : BudgetChangeListener {

    fun onOverTotalBudget(totalBudget: Int, budgetSum: Int)
}