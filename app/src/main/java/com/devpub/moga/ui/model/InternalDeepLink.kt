package com.devpub.moga.ui.model

import android.net.Uri
import androidx.core.net.toUri

object InternalDeepLink {
    const val DOMAIN = "app://"

    const val HOME = "${DOMAIN}home"
    const val NOTE = "${DOMAIN}note"
    const val REPORT = "${DOMAIN}report"
    const val INSIGHT = "${DOMAIN}insight"
    const val ETC = "${DOMAIN}etc"

    fun makeMainTabDeepLink(type: MainPage): Uri {
        return when(type){
            MainPage.NOTE -> NOTE
            MainPage.REPORT -> REPORT
            MainPage.INSIGHT -> INSIGHT
            MainPage.ETC -> ETC
            else -> HOME
        }.toUri()
    }

    fun makeCustomDeepLink(id: String): String {
        return "${DOMAIN}customDeepLink?id=${id}"
    }
}