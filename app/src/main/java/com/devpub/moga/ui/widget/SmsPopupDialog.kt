package com.devpub.moga.ui.widget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.databinding.PopupSmsBinding

class SmsPopupDialog(context: Context) : Dialog(context) {

    private lateinit var binding: PopupSmsBinding
    private var clickListener: View.OnClickListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = PopupSmsBinding.inflate(LayoutInflater.from(context))
        binding.view = this
        setContentView(binding.root)
        setCancelable(true)
    }

    override fun onStart() {
        super.onStart()
        AppPreferenceImpl(context).setWatchSmsPermission(true)
    }

    fun setOnClickListener(clickListener: View.OnClickListener): SmsPopupDialog {
        this.clickListener = clickListener
        return this
    }

    fun onClickConfirm() {
        clickListener?.onClick(binding.confirmButton)
        dismiss()
    }
}