package com.devpub.moga.ui.list.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {

    open fun bindViewHolder(item: T, position: Int) {

    }
}