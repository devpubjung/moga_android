package com.devpub.moga.ui.list.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.devpub.moga.databinding.ItemAdHeaderBinding
import com.google.android.gms.ads.AdRequest

class HeaderAdViewHolder(binding: ItemAdHeaderBinding) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.adView.loadAd(AdRequest.Builder().build())
    }
}