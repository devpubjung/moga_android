package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.common.model.ListItem
import com.devpub.moga.databinding.ItemKosisHeaderBinding
import com.devpub.moga.domain.model.KosisData

class KosisHeaderViewHolder(
    private val binding: ItemKosisHeaderBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemKosisHeaderBinding>(binding, handler) {

    override fun bind(item: ListItem) {
        super.bind(item)
        item as KosisData
        setData(item)
    }
    private fun setData(data: KosisData) {
        with(binding) {
            byAgeButton.isSelected = data.isByAge
            byIncomeButton.isSelected = !data.isByAge

            compareTitleText.text = data.compareTitle
            compareSubTitleText.text = data.compareSubTitle
            othersChartView.setTitle(data.chartTitle)
            othersTitleText.text = data.othersTitle

            val myChartPercent: Float
            val othersChartPercent: Float
            if (data.myTotal > data.totalData) {
                myChartPercent = 100f
                othersChartPercent = data.totalData.toFloat() / data.myTotal * 100
            } else {
                myChartPercent = data.myTotal.toFloat() / data.totalData * 100
                othersChartPercent = 100f
            }
            myChartView.setChartData(myChartPercent, data.myTotal)
            othersChartView.setChartData(othersChartPercent, data.totalData)
        }
    }
}