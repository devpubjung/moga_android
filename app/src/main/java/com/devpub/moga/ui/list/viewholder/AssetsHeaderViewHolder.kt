package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemAssetsPadHeaderBinding


class AssetsHeaderViewHolder(
    binding: ItemAssetsPadHeaderBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemAssetsPadHeaderBinding>(binding, handler)