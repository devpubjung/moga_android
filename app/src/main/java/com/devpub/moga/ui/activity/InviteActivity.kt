package com.devpub.moga.ui.activity

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import com.devpub.common.util.StatusBarUtil
import com.devpub.common.mvvm.BaseActivity
import com.devpub.common.util.extension.closeKeyboard
import com.devpub.common.util.extension.toast
import com.devpub.moga.R
import com.devpub.moga.data.interactor.KakaoInteractor
import com.devpub.moga.databinding.ActivityInviteBinding
import com.devpub.moga.ui.widget.JoinPopupDialog
import com.devpub.moga.viewmodel.InviteViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InviteActivity : BaseActivity<ActivityInviteBinding>() {

    @Inject
    lateinit var kakaoInteractor: KakaoInteractor

    private val viewModel: InviteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    private fun setUpViews() {
        StatusBarUtil.setStatusBarColorForLollipop(window,
            ContextCompat.getColor(this, R.color.primaryColor))
        binding.view = this
        binding.viewModel = viewModel
        with(binding) {
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowTitleEnabled(false)
                toolbarTitle.text = getString(R.string.invite_title)
            }
            codeEdit.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    codeField.error = null
                }
            }

            kakaoTalkButton.isVisible = kakaoInteractor.isKakaoLinkAvailable()
        }
    }

    private fun observeViewModel() {
        viewModel.isLoading.observe {
            setLoading(it)
        }

        viewModel.toast.observe {
            toast(it, Toast.LENGTH_LONG)
        }

        viewModel.errorEvent.observe {
            toast(it.localizedMessage, Toast.LENGTH_LONG)
        }

        viewModel.successJoinEvent.observe {
            toast("이제부터 가계부를 공유하실 수 있습니다.")
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    fun join() {
        binding.codeEdit.closeKeyboard()
        val joinCode = binding.codeEdit.text?.toString()
        if (joinCode.isNullOrEmpty()) {
            binding.codeField.error = "초대 코드를 입력해주세요."
            return
        }

        JoinPopupDialog(this).setOnClickListener {
            viewModel.join(joinCode)
        }.show()
    }

    fun copyCode() {
        binding.codeEdit.closeKeyboard()
        val clipboard: ClipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("mogaCode", binding.inviteCodeText.text.toString())
        clipboard.setPrimaryClip(clip)
        toast("초대 코드가 복사되었습니다.")
    }

    fun shareKakaoTalk() {
        binding.codeEdit.closeKeyboard()
        kakaoInteractor.sendInviteMessage(binding.inviteCodeText.text.toString())
    }

    companion object {
        fun start(context: Context) {
            Intent(context, InviteActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }
}