package com.devpub.moga.ui.model

import com.devpub.common.model.ListItem


data class CheckTextItem(
    val value: Int,
    val unit: String = "",
    val text: String? = null,
    var checked : Boolean = false
) : ListItem {
}