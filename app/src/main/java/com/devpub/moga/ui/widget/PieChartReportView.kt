package com.devpub.moga.ui.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.forEachIndexed
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetPieChartReportBinding
import com.devpub.moga.ui.model.PieChartItem
import kotlinx.android.synthetic.main.view_input_item_container.view.*
import view.hellocharts.formatter.SimplePieChartValueFormatter
import view.hellocharts.listener.PieChartOnValueSelectListener
import view.hellocharts.model.PieChartData
import view.hellocharts.model.SelectedValue
import view.hellocharts.model.SliceValue

class PieChartReportView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr), PieChartOnValueSelectListener {

    private lateinit var binding: WidgetPieChartReportBinding
    private lateinit var data: PieChartData
    private var pieList = mutableListOf<PieChartItem>()
    init {
        if (isInEditMode) {
            val view = View.inflate(context, R.layout.widget_pie_chart_report, this)
        } else {
            binding =
                WidgetPieChartReportBinding.inflate(LayoutInflater.from(context), this, true)
            binding.pieChartView.apply {
                isValueSelectionEnabled = true
                isChartRotationEnabled = false
                isValueTouchEnabled = true
                onValueTouchListener = this@PieChartReportView
            }
            data = PieChartData().apply {
                setHasLabels(true)
                isValueLabelBackgroundEnabled = false
                valueLabelTextSize = 11
                slicesSpacing = 1
                showMinValue = 4F
                formatter = SimplePieChartValueFormatter().setAppendedText("%".toCharArray()).setMaxText(7)
            }
        }
    }

    override fun onValueDeselected() {
        resetSelectedViews()
    }

    override fun onValueSelected(arcIndex: Int, value: SliceValue?) {
        resetSelectedViews()
        val selectedView = binding.contentLayout.getChildAt(arcIndex)
        if (selectedView is ReportUserDetailView) {
            selectedView.binding.titleText.setTextColor(Color.WHITE)
            selectedView.binding.contentText.setTextColor(Color.WHITE)
        } else if (selectedView is ReportDetailView) {
            selectedView.binding.titleText.setTextColor(Color.WHITE)
            selectedView.binding.contentText.setTextColor(Color.WHITE)
        }
        selectedView.setBackgroundColor(data.values[arcIndex].darkenColor)
    }

    fun setData(pieList: List<PieChartItem>, contentList: List<View>) {
        binding.contentLayout.removeAllViews()
        contentList.forEachIndexed { index, view ->
            binding.contentLayout.addView(view.apply {
                setOnClickListener {
                    resetSelectedViews()
                    binding.pieChartView.selectValue(SelectedValue(index,
                        0,
                        SelectedValue.SelectedValueType.NONE))
                }
            })
        }

        if (this.pieList.size == pieList.size) {
            data.values.forEachIndexed { index, sliceValue ->
                sliceValue.setTarget(pieList[index].value)
                sliceValue.setLabel(pieList[index].label)
                sliceValue.color = pieList[index].color
            }
        } else {
            if (pieList.size == 1) {
                data.values = listOf(SliceValue(pieList[0].value, ContextCompat.getColor(context, R.color.primaryDarkColor)).apply {
                    setLabel(pieList[0].label)
                })
            } else {
                data.values = createChartData(pieList)
            }
        }

        binding.pieChartView.apply {
            visibility = View.VISIBLE
            pieChartData = data
            startDataAnimation()
        }
        this.pieList = pieList.toMutableList()
    }

    private fun resetSelectedViews() {
        binding.contentLayout.forEachIndexed { index, view ->
            if (view is ReportUserDetailView) {
                view.binding.titleText.setTextColor(data.values[index].color)
                view.binding.contentText.setTextColor(ContextCompat.getColor(context,
                    R.color.primaryTextColor))
            } else if (view is ReportDetailView) {
                view.binding.titleText.setTextColor(ContextCompat.getColor(context,
                    R.color.secondaryTextLightColor))
                view.binding.contentText.setTextColor(ContextCompat.getColor(context,
                    R.color.primaryTextColor))
            }
            view.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    private fun createChartData(itemList: List<PieChartItem>): MutableList<SliceValue> {
        val list = mutableListOf<SliceValue>()
        itemList.forEach { item ->
            list.add(createSliceData(item, 100f / (itemList.size - 1)))
        }
        return list
    }

    private fun createSliceData(item: PieChartItem, defaultValue: Float): SliceValue {
        return SliceValue(defaultValue, item.color).apply {
            setLabel(item.label)
            setTarget(item.value)
        }
    }
}