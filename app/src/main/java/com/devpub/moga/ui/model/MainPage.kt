package com.devpub.moga.ui.model

enum class MainPage {
    HOME, NOTE, REPORT, INSIGHT, ETC
}
