package com.devpub.moga.ui.widget

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import com.devpub.common.binidng.BindingComponent
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetOthersChartBinding
import com.devpub.moga.util.MoneyUtil.commaWon

class VerticalChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int = 0,
) : BindingComponent<WidgetOthersChartBinding>(context, attrs, R.layout.widget_others_chart) {


    private var preMoney: Int = 0
    private var prePercent: Float = 0f

    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.VerticalChartView)

            typedArray.getString(R.styleable.VerticalChartView_title)?.apply {
                setTitle(this)
            }

            typedArray.recycle()
        }
    }

    fun setTitle(title: String?) {
        binding.titleText.text = title ?: "title"
    }

    fun setChartData(percent: Float, money: Long) {
        startAnimationChart(percent)
        startAnimationText(money)
    }

    private fun startAnimationChart(percent: Float) {
        val animator = ValueAnimator.ofFloat(prePercent, percent)
        animator.duration = 700
        animator.addUpdateListener { animation ->
            val value = animation.animatedValue as Float
            (binding.chartBackground.layoutParams as LayoutParams).weight = 100 - value
            (binding.chartForeground.layoutParams as LayoutParams).weight = value
        }
        animator.start()
        prePercent = percent
    }

    private fun startAnimationText(money: Long) {
        val animator = ValueAnimator.ofInt(preMoney, money.toInt())
        animator.duration = 500
        animator.addUpdateListener { animation ->
            binding.moneyText.text = (animation.animatedValue as Int).commaWon()
        }
        animator.start()
        preMoney = money.toInt()
    }
}