package com.devpub.moga.ui.widget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import com.devpub.moga.databinding.PopupDefaultBinding

class DefaultPopupDialog(context: Context): Dialog(context) {

    private var binding = PopupDefaultBinding.inflate(LayoutInflater.from(context))

    init {
        binding.view = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setCancelable(true)
    }

    fun setTopText(resId : Int) {
        binding.topText.setText(resId)
    }

    fun setTopText(text : String) {
        binding.topText.text = text
    }

    fun setTitleText(resId : Int) {
        binding.titleText.setText(resId)
    }

    fun setContentText(resId : Int) {
        binding.contentText.setText(resId)
    }

    fun onClickClose() {
        dismiss()
    }
}