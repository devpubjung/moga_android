package com.devpub.moga.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.devpub.common.util.TAG
import com.devpub.common.mvvm.BaseActivity
import com.devpub.moga.data.interactor.KakaoInteractor
import com.devpub.moga.data.model.mapper.convertForUser
import com.devpub.moga.databinding.ActivityStartBinding
import com.devpub.moga.domain.model.state.LoginState
import com.devpub.moga.util.extensions.setStatusBarTransparent
import com.devpub.moga.util.extensions.showToast
import com.devpub.moga.util.extensions.toast
import com.devpub.moga.viewmodel.StartViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@AndroidEntryPoint
@SuppressLint("CustomSplashScreen")
class StartActivity : BaseActivity<ActivityStartBinding>() {

    @Inject
    internal lateinit var kakaoInteractor: KakaoInteractor

    private val viewModel: StartViewModel by viewModels()
    private var joinCode: String? = null

    private val resultLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            goMain()
        } else {
            finish()
        }
    }

    override fun beforeBindView() {
        installSplashScreen()
        setStatusBarTransparent()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.activity = this
        binding.viewModel = this.viewModel
        observeViewModel()
        try {
            if (Intent.ACTION_VIEW == intent.action) {
                joinCode = intent.data?.let {
                    when (it.getQueryParameter("type")) {
                        "invite" -> it.getQueryParameter("code")
                        else -> null
                    }
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        viewModel.loginUser(joinCode)
    }

    fun signUpKakaoUserInfo() {
        kakaoInteractor.login({ user ->
            viewModel.loginUserForSocial(user.convertForUser(), joinCode)
        }, { error ->
            toast(error)
        })
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.loginState.collect {
                when (it) {
                    LoginState.Loading -> {
                        showLoading()
                    }
                    LoginState.Login -> {
                        hideLoading()
                        goMain()
                    }
                    LoginState.SingUp -> {
                        hideLoading()
                        UserInfoSettingActivity.startForResult(this@StartActivity, resultLauncher)
                    }
                    LoginState.SocialLogin -> {
                        hideLoading()
                        kakaoInteractor.getUserInfo({ user ->
                            viewModel.loginUserForSocial(user.convertForUser(), joinCode)
                        }, {
                            viewModel.loginUser(joinCode, true)
                        })
                    }
                    LoginState.Fail -> {
                        hideLoading()
                        bind {
                            backgroundLayout.isVisible = true
                            loginLayout.isVisible = true
                        }
                    }
                    is LoginState.Error -> {
                        it.error?.localizedMessage?.showToast(this@StartActivity)
                    }
                }
            }
        }

    }

    private fun goMain() {
        MainActivity.start(this@StartActivity, intent.extras?.getString("tab"))
        overridePendingTransition(0, 0)
        finish()
    }
}