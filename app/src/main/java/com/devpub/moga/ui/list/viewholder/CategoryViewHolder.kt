package com.devpub.moga.ui.list.viewholder

import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.moga.databinding.ItemCategoryPadBinding


class CategoryViewHolder(
    binding: ItemCategoryPadBinding,
    handler: RecyclerHandler?
) : BindingViewHolder<ItemCategoryPadBinding>(binding, handler)