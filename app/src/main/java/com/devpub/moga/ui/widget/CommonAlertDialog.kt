package com.devpub.moga.ui.widget

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import com.devpub.moga.R
import com.devpub.moga.databinding.WidgetAlertDialogBinding

class CommonAlertDialog private constructor(context: Context, builder: Builder) {
    private val alertDialog = AlertDialog.Builder(context).create()

    var title: String? = null
    var content: String? = null
    var desc: String? = null
    var negative: String? = null
    var positive: String? = null
    var negativeClickListener: DialogInterface.OnClickListener? = null
    var positiveClickListener: DialogInterface.OnClickListener? = null

    init {
        title = builder.getTitle()
        content = builder.getContent()
        desc = builder.getDescription()
        negative = builder.getNegative()
        positive = builder.getPositive()
        negativeClickListener = builder.getNegativeListener()
        positiveClickListener = builder.getPositiveListener()

        val binding = DataBindingUtil.inflate<WidgetAlertDialogBinding>(
            LayoutInflater.from(context),
            R.layout.widget_alert_dialog,
            null,
            false
        ).apply {
            titleTextView.text = title
            contentTextView.text = content
            descTextView.setText(desc)
            negativeButton.text = negative
            positiveButton.text = positive

            titleTextView.isVisible = !title.isNullOrEmpty()
            contentTextView.isVisible = !content.isNullOrEmpty()
            descTextView.isVisible = !desc.isNullOrEmpty()
            positiveButton.isVisible = !positive.isNullOrEmpty()

            negativeButton.setOnClickListener {
                negativeClickListener?.onClick(alertDialog, AlertDialog.BUTTON_NEGATIVE)
                alertDialog.dismiss()
            }
            positiveButton.setOnClickListener {
                positiveClickListener?.onClick(alertDialog, AlertDialog.BUTTON_POSITIVE)
                alertDialog.dismiss()
            }
        }

        alertDialog.setView(binding.root)
    }

    fun show() {
        alertDialog.show()
    }

    class Builder(private val context: Context) {
        private var title: String? = null
        private var content: String? = null
        private var desc: String? = null
        private var negative: String? = null
        private var positive: String? = null
        private var negativeClickListener: DialogInterface.OnClickListener? = null
        private var positiveClickListener: DialogInterface.OnClickListener? = null

        fun setTitle(title: String?) = apply { this.title = title }
        fun setContent(content: String?) = apply { this.content = content }
        fun setDescription(desc: String?) = apply { this.desc = desc }
        fun setNegative(negative: String?) = apply { this.negative = negative }
        fun setPositive(positive: String?) = apply { this.positive = positive }
        fun setNegativeListener(negativeListener: DialogInterface.OnClickListener?) =
            apply { this.negativeClickListener = negativeListener }

        fun setPositiveListener(positiveListener: DialogInterface.OnClickListener?) =
            apply { this.positiveClickListener = positiveListener }

        fun build() = CommonAlertDialog(context, this)

        fun getTitle() = title
        fun getContent() = content
        fun getDescription() = desc
        fun getNegative() = negative
        fun getPositive() = positive
        fun getNegativeListener() = negativeClickListener
        fun getPositiveListener() = positiveClickListener
    }
}