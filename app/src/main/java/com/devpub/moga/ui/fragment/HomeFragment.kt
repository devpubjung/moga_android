package com.devpub.moga.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.devpub.moga.R
import com.devpub.moga.databinding.FragmentHomeBinding
import com.devpub.moga.manager.ReviewManager
import com.devpub.moga.ui.activity.MainActivity
import com.devpub.moga.ui.model.BudgetDetailType
import com.devpub.moga.ui.model.HomePageModel
import com.devpub.moga.ui.fragment.adapter.HomePagerAdapter
import com.devpub.moga.ui.widget.DefaultPopupDialog
import com.devpub.moga.util.extensions.*
import com.devpub.moga.viewmodel.HomeViewModel
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val viewModel: HomeViewModel by viewModels()
    private val pagerList = listOf(
        HomePageModel("주간 예산", BudgetDetailType.WEEKLY),
        HomePageModel("월간 예산", BudgetDetailType.MONTHLY)
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadBudgetReport()
    }

    override fun setUpViews() {
        bind {
            view = this@HomeFragment
            viewModel = this@HomeFragment.viewModel
            viewpager.adapter = HomePagerAdapter(requireActivity(), pagerList)
            TabLayoutMediator(tab, viewpager) { tab, position ->
                tab.text = pagerList[position].title
            }.attach()
        }
    }

    private fun observeViewModel() {
        viewModel.budgetReportData.observe {
            binding.headerView.setReportData(it)
            showAchieveGoalView(it.isWeeklyAchievedGoal)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }
    }

    private fun showAchieveGoalView(achieveGoal: Boolean) {
        activity?.let { activity ->
            activity as MainActivity
            if (achieveGoal) {
                activity.showFirecracker(DefaultPopupDialog(activity).apply {
                    setTopText("예산 달성!")
                    setTitleText(R.string.celebration)
                    setContentText(R.string.celebration_content)
                    setOnDismissListener {
                        ReviewManager(activity).showReviewPopup()
                    }
                })
            }
        }
    }
}