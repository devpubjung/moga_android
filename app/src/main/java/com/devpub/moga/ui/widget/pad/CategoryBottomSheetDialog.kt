package com.devpub.moga.ui.widget.pad

import android.R
import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.devpub.common.binidng.BindingBottomSheetDialog
import com.devpub.common.list.RecyclerHandler
import com.devpub.moga.databinding.ItemCategoryPadPopupBinding
import com.devpub.moga.databinding.WidgetCategoryPadBinding
import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.model.Target
import com.devpub.moga.ui.list.adapter.ListAdapter
import com.devpub.moga.util.extensions.*
import com.devpub.moga.viewmodel.pad.CategoryViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoryBottomSheetDialog : BindingBottomSheetDialog<WidgetCategoryPadBinding>() {

    private val viewModel: CategoryViewModel by viewModels()
    private val adapter: ListAdapter by lazy { ListAdapter(Handler(this, viewModel)) }

    private var target: Target? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        observeViewModel()
        arguments?.let {
            target = it.getSerializable(TARGET) as Target
        }
        viewModel.loadCategoryList(target)
    }

    private fun setUpViews() {
        peekHeight(350f)
        bind {
            recyclerView.adapter = adapter
        }
    }

    private fun observeViewModel() {
        viewModel.toast.observe {
            context?.toast(it)
        }

        viewModel.errorEvent.observe {
            context?.toast(it.message)
        }

        viewModel.categoryListData.observe {
            adapter.submitList(it)
        }

        viewModel.parentCategoryListData.observe {
            showAddCategoryPopup(it.first, it.second)
        }
    }

    private fun showAddCategoryPopup(categoryList: List<Category>, item: Category?) {
        var selectedParentItem = item
        val categoryPopupBinding = ItemCategoryPadPopupBinding.inflate(LayoutInflater.from(context))
        categoryPopupBinding.prentCategorySpinner.adapter =
            ArrayAdapter(requireContext(), R.layout.simple_list_item_1, categoryList)
        categoryPopupBinding.prentCategorySpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long,
                ) {
                    selectedParentItem = categoryList[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        var status = "추가"
        if (item != null) {
            status = "수정"
            categoryPopupBinding.categoryEdit.setText(item.name)
            categoryPopupBinding.prentCategorySpinner.setSelection(categoryList.indexOfFirst { it.id == item.parentId })
        }

        AlertDialog.Builder(context, com.devpub.moga.R.style.AlertDialogStyle).apply {
            setTitle("카테고리 $status")
            setMessage("\n$status 할 카테고리 정보를 입력해주세요")
            setIcon(com.devpub.moga.R.drawable.ic_item_add)
            setView(categoryPopupBinding.root)
            setPositiveButton(status) { _, _ ->
                categoryPopupBinding.categoryEdit.closeKeyboard()
                val name = categoryPopupBinding.categoryEdit.text.toString()
                if (name.isNotEmpty()) {
                    if (item != null) {
                        viewModel.updateCategory(name, selectedParentItem, item)
                    } else {
                        viewModel.addCategory(name, selectedParentItem)
                    }
                }
            }
            setNegativeButton("취소") { _, _ -> }
        }.create().show()
    }

    class Handler(
        private val fragment: CategoryBottomSheetDialog,
        private val viewModel: CategoryViewModel,
    ) : RecyclerHandler {
        fun onClickAddItem() {
            viewModel.loadParentCategoryList()
        }

        fun onClickItem(item: Category) {
            fragment.setFragmentResult(TAG, bundleOf(itemKey to item))
            fragment.dismiss()
        }

        fun onLongClickItem(item: Category) :Boolean{
            if (item.canDelete) {
                AlertDialog.Builder(fragment.context, com.devpub.moga.R.style.AlertDialogStyle)
                    .apply {
                        setTitle("카테고리 수정/삭제")
                        setMessage("\n[${item.name}] 카테고리를 수정/삭제 하시겠습니까?")
                        setIcon(com.devpub.moga.R.drawable.ic_item_check)
                        setPositiveButton("삭제") { _, _ ->
                            viewModel.deleteCategory(item)
                        }
                        setNegativeButton("수정") { _, _ ->
                            viewModel.loadParentCategoryList(item)
                        }
                        setNeutralButton("취소") { _, _ -> }
                    }.create().show()
            } else {
                "기본 카테고리는 수정/삭제가 불가합니다.".showToast(fragment.context)
            }
            return true
        }
    }

    companion object {
        const val TAG = "CategoryBottomSheetDialog"
        const val itemKey = "itemKey"

        private const val TARGET = "target"

        fun newInstance(target: Target? = Target.EXPEND)  =
            CategoryBottomSheetDialog().apply {
                arguments = Bundle().apply {
                    putSerializable(TARGET, target)
                }
            }
    }
}