package com.devpub.moga.ui.list.adapter

import android.view.ViewGroup
import com.devpub.common.list.BaseListAdapter
import com.devpub.common.list.RecyclerHandler
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.common.model.ListItem
import com.devpub.moga.ui.list.ViewHolderGenerator
import com.devpub.moga.ui.list.ViewTypeGenerator

class ListAdapter(
    recyclerHandler: RecyclerHandler? = Handler(),
) : BaseListAdapter(recyclerHandler) {

    override fun getViewType(item: ListItem): Int {
        return ViewTypeGenerator.get(item)
    }

    override fun getCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
        handler: RecyclerHandler?,
    ): BindingViewHolder<*> {
        return ViewHolderGenerator.get(parent, viewType, handler)
    }

    class Handler : RecyclerHandler
}