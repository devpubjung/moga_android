package com.devpub.moga.viewmodel.pad

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.model.ListItem
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.Assets
import com.devpub.moga.domain.usecase.AssetsUseCase
import com.devpub.moga.ui.model.AssetsHeader
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AssetsViewModel @Inject constructor(
    private val assetsUseCase: AssetsUseCase
) : BaseViewModel() {

    val assetsListData: LiveData<List<ListItem>> by lazy { _assetsListData }

    private val _assetsListData = MutableLiveData<List<ListItem>>()

    init {
        loadAssetsList()
    }

    private fun loadAssetsList() {
        viewModelScope.launch {
            assetsUseCase.loadAssetsList()
                .catch { error(it) }
                .collectLatest {
                    setAssetsList(it)
                }
        }
    }

    fun addItem(name: String) {
        viewModelScope.launch {
            assetsUseCase.addAssets(name)
                .catch { error(it) }
                .collectLatest {
                    setAssetsList(it)
                }
        }
    }

    fun deleteItem(item: Assets) {
        viewModelScope.launch {
            assetsUseCase.removeAssets(item)
                .catch { error(it) }
                .collectLatest {
                    setAssetsList(it)
                }
        }
    }

    private fun setAssetsList(list: List<Assets>) {
        _assetsListData.value = list.toMutableList<ListItem>().apply { add(0, AssetsHeader()) }
    }
}