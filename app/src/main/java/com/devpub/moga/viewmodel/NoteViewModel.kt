package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.domain.model.*
import com.devpub.moga.domain.usecase.NoteUseCase
import com.devpub.moga.ui.model.NoteCalendarItem
import com.devpub.moga.util.DateUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*
import javax.inject.Inject

@HiltViewModel
class NoteViewModel @Inject constructor(
    appPreference: AppPreference,
    private val noteUseCase: NoteUseCase,
) : BaseViewModel() {

    val calendarData: LiveData<List<NoteCalendarItem>> by lazy { _calendarData }
    val noteItemData: LiveData<List<AccountItemNote>> by lazy { _noteItemData }
    val startWeek: LiveData<Int> by lazy { _startWeek }

    private val _calendarData = MutableLiveData<List<NoteCalendarItem>>()
    private val _noteItemData = MutableLiveData<List<AccountItemNote>>()
    private val _startWeek = MutableLiveData<Int>()

    init {
        _startWeek.value = appPreference.startWeek
    }

    fun loadMonthNote(date: Date = DateUtil.todayDate) {
        viewModelScope.launch {
            noteUseCase.loadMonthNote(date)
                .catch { error(it) }
                .map { it.data }
                .collectLatest { list ->
                    _calendarData.value = list.filter { it.showHeader }.map {
                        val cal = Calendar.getInstance(Locale.KOREA)
                        cal.time = it.accountItem.date
                        NoteCalendarItem(
                            cal,
                            it.incomeMoney,
                            it.expendMoney,
                            it.transferMoney)
                    }
                    _noteItemData.value = list
                }
        }
    }

    fun loadDayNote(date: Date = DateUtil.todayDate) {
        viewModelScope.launch {
            noteUseCase.loadDayNote(date)
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .map { it.data }
                .collectLatest { list ->
                    _noteItemData.value = list
                }
        }
    }

    fun removeNote(accountItem: AccountItem) {
        viewModelScope.launch {
            noteUseCase.removeNote(accountItem)
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .map { it.data }
                .collectLatest { list ->
                    _calendarData.value = list.filter { it.showHeader }.map {
                        val cal = Calendar.getInstance(Locale.KOREA)
                        cal.time = it.accountItem.date
                        NoteCalendarItem(
                            cal,
                            it.incomeMoney,
                            it.expendMoney,
                            it.transferMoney)
                    }
                    _noteItemData.value = list
                }
        }
    }

    fun getCategoryInfo(accountItem: AccountItem) {
        viewModelScope.launch {
            noteUseCase.getCategoryInfo(accountItem)
                .catch { error(it) }
                .collect {
                    showToast(it)
                }
        }
    }
}