package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.common.mvvm.SingleLiveEvent
import com.devpub.moga.domain.model.Gender
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.model.state.DataState
import com.devpub.moga.domain.usecase.UserInfoSettingUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class UserInfoSettingViewModel @Inject constructor(
    private val userInfoSettingUseCase: UserInfoSettingUseCase,
) : BaseViewModel() {

    val myInfo: LiveData<User> by lazy { _myInfoLiveData }
    val saveSuccessEvent: LiveData<User> by lazy { _saveSuccessEvent }
    val deleteCompleteEvent: LiveData<Nothing> by lazy { _deleteCompleteEvent }

    private val _myInfoLiveData = MutableLiveData<User>()
    private val _saveSuccessEvent = SingleLiveEvent<User>()
    private val _deleteCompleteEvent = SingleLiveEvent<Nothing>()

    init {
        loadUserInfo()
    }

    private fun loadUserInfo() {
        viewModelScope.launch {
            userInfoSettingUseCase.loadUserInfo()
                .onStart { setLoading(false) }
                .catch { error(it)}
                .onCompletion { setLoading(false) }
                .collectLatest {
                    when (it) {
                        is DataState.Success<*> -> _myInfoLiveData.value = it.data as User
                        is DataState.Error -> error(it.error)
                    }
                }
        }
    }

    fun saveUserInfo(email: String, name: String, birthYear: Int, gender: Gender, totalBudget: Int?) {
        viewModelScope.launch {
            userInfoSettingUseCase.saveUserInfo(email, name, birthYear, gender, totalBudget)
                .onStart { setLoading(true) }
                .catch { error(Exception("수정내용 불가! 나중에 다시 시도해주세요.", it))}
                .onCompletion { setLoading(false) }
                .collectLatest {
                    _saveSuccessEvent.value = it.data as User
                }
        }
    }

    fun deleteUser() {
        viewModelScope.launch {
            userInfoSettingUseCase.deleteUser()
                .onStart { setLoading(true) }
                .catch { error(it)}
                .onCompletion { setLoading(false) }
                .collectLatest {
                    when (it) {
                        is DataState.Success<*> -> _deleteCompleteEvent.call()
                        is DataState.Error -> error(Exception("일시적인 문제로 탈퇴에 실패하였습니다. 문의하기로 직접 요청하세요."))
                    }
                }
        }
    }
}