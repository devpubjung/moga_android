package com.devpub.moga.viewmodel

import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.state.UiState
import com.devpub.moga.domain.usecase.CompareMonthlyUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ComparePrevViewModel @Inject constructor(
    private val compareMonthlyUseCase: CompareMonthlyUseCase,
) : BaseViewModel() {

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState: StateFlow<UiState> = _uiState

    fun loadCompareData(prevDate: Date, nextDate: Date) {
        viewModelScope.launch {
            compareMonthlyUseCase.loadCompareData(prevDate, nextDate)
                .map { UiState.Success(it) }
                .collectLatest { uiState ->
                    _uiState.value = uiState
                }
        }
    }
}