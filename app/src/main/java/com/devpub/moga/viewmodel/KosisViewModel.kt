package com.devpub.moga.viewmodel

import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.state.UiState
import com.devpub.moga.domain.usecase.KosisUseCase
import com.devpub.moga.util.DateUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class KosisViewModel @Inject constructor(
    private val kosisUseCase: KosisUseCase,
) : BaseViewModel() {

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState: StateFlow<UiState> = _uiState

    fun loadKosisByAge(date: Date = DateUtil.todayDate) {
        viewModelScope.launch {
            kosisUseCase.loadKosisByAge(date)
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .map { UiState.Success(it) }
                .collectLatest {
                    _uiState.value = it
                }
        }
    }

    fun loadKosisByIncome(date: Date = DateUtil.todayDate) {
        viewModelScope.launch {
            kosisUseCase.loadKosisByIncome(date)
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .map { UiState.Success(it) }
                .collectLatest {
                    _uiState.value = it
                }
        }
    }
}