package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.*
import com.devpub.moga.domain.model.state.DataState
import com.devpub.moga.domain.usecase.HomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeUseCase: HomeUseCase,
) : BaseViewModel() {

    val budgetReportData: LiveData<BudgetReport> by lazy { _budgetReportData }
    val achieve: LiveData<Boolean> by lazy { _achieve }

    private val _budgetReportData = MutableLiveData<BudgetReport>()
    private val _achieve = MutableLiveData<Boolean>()

    fun loadBudgetReport() {
        viewModelScope.launch {
            homeUseCase.loadBudgetReport()
                .catch { error(it) }
                .collectLatest {
                    when (it) {
                        is DataState.Success<*> -> _budgetReportData.value = it.data as BudgetReport
                        is DataState.Error -> error(it.error)
                    }
                }
        }
    }

}