package com.devpub.moga.viewmodel

import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.state.LoginState
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.usecase.StartUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class StartViewModel @Inject constructor(
    private val startUseCase: StartUseCase,
) : BaseViewModel() {

    private val _loginState = MutableStateFlow<LoginState>(LoginState.Loading)
    val loginState: StateFlow<LoginState> = _loginState

    fun loginUser(joinCode: String?, always: Boolean = false) {
        viewModelScope.launch {
            startUseCase.loginUser(joinCode, always)
                .onStart { LoginState.Loading }
                .catch { LoginState.Error(it) }
                .collectLatest {
                    _loginState.value = it
                }
        }
    }

    fun loginUserForSocial(user: User, joinCode: String?) {
        viewModelScope.launch {
            startUseCase.loginUserForSocial(user, joinCode)
                .onStart { LoginState.Loading }
                .catch { LoginState.Error(it) }
                .collectLatest {
                    _loginState.value = it
                }
        }
    }
}