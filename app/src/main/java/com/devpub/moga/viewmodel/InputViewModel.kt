package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.domain.model.*
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.usecase.InputUseCase
import com.devpub.moga.util.MogaUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*
import javax.inject.Inject

@HiltViewModel
class InputViewModel @Inject constructor(
    private val appPreference: AppPreference,
    private val inputUseCase: InputUseCase,
) : BaseViewModel() {

    val joinMoim: Boolean by lazy { appPreference.joinMoim }
    val accountItemData: LiveData<AccountItem> by lazy { _accountItemData }
    val changeCategoryEvent: LiveData<String> by lazy { _changeCategoryEvent }
    val changeAmountEvent: LiveData<String> by lazy { _changeAmountEvent }
    val successSaveEvent: LiveData<String> by lazy { _successSaveEvent }

    private val _accountItemData = MutableLiveData<AccountItem>()
    private val _changeCategoryEvent = MutableLiveData<String>()
    private val _changeAmountEvent = MutableLiveData<String>()
    private val _successSaveEvent = MutableLiveData<String>()

    fun loadInputInfo(itemId: Long?, calendar: Calendar?) {
        viewModelScope.launch {
            inputUseCase.loadAccountItem(itemId, calendar?.time)
                .catch { error(it) }
                .collectLatest {
                    _accountItemData.value = it
                }
        }
    }

    fun onClickTarget(target: Target) {
        _accountItemData.value?.let { accountItem ->
            val changeCategory = !MogaUtil.isSameTarget(accountItem.target, target)
                    && accountItem.category != null
            accountItem.expend = target != Target.INCOME
            accountItem.target = target
            _accountItemData.value = accountItem

            if (changeCategory) {
                _changeCategoryEvent.value = ""
            }
        }
    }

    fun saveDateTime(date: Date) {
        _accountItemData.value?.date = date
    }

    fun saveCategory(category: Category) {
        _accountItemData.value?.category = category
    }

    fun saveAssets(assets: Assets) {
        _accountItemData.value?.assets = assets
    }

    fun saveAmount(amount: Long) {
        _accountItemData.value?.amount = amount
    }

    fun saveUser(user: User) {
        _accountItemData.value?.user = user
    }

    fun saveItem(content: String, memo: String) {
        _accountItemData.value?.let { accountItem ->
            if (accountItem.amount <= 0
                && accountItem.content.isNullOrEmpty()
                && accountItem.memo.isNullOrEmpty()) {
                _changeAmountEvent.value = "금액을 입력하거나 내용을 입력하셔야 합니다."
                return
            }

            if (!MogaUtil.isSameTarget(accountItem.target, accountItem.category?.target)) {
                _changeCategoryEvent.value = "카테고리 설정을 다시 하셔야 합니다."
                return
            }

            viewModelScope.launch {
                inputUseCase.saveAccountItem(accountItem.apply {
                    this.content = content
                    this.memo = memo
                }).onStart { setLoading(true) }
                    .catch { error(it) }
                    .onCompletion { setLoading(false) }
                    .collectLatest {
                        _successSaveEvent.value = it
                    }
            }
        }
    }
}