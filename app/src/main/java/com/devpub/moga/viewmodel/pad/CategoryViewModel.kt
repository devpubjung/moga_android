package com.devpub.moga.viewmodel.pad

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.model.ListItem
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.usecase.CategoryUseCase
import com.devpub.moga.ui.model.CategoryHeader
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(
    private val categoryUseCase: CategoryUseCase,
) : BaseViewModel() {

    val categoryListData: LiveData<List<ListItem>> by lazy { _categoryListData }
    val parentCategoryListData: LiveData<Pair<List<Category>, Category?>> by lazy { _parentCategoryListData }

    private val _categoryListData = MutableLiveData<List<ListItem>>()
    private val _parentCategoryListData = MutableLiveData<Pair<List<Category>, Category?>>()

    private var target: Target = Target.EXPEND

    fun loadCategoryList(target: Target? = Target.EXPEND) {
        this.target = target!!
        viewModelScope.launch {
            categoryUseCase.loadCategoryList(target)
                .catch { error(it) }
                .collectLatest {
                    setCategoryList(it)
                }
        }
    }

    fun loadParentCategoryList(item: Category? = null) {
        viewModelScope.launch {
            categoryUseCase.loadParentCategoryList(target)
                .catch { error(it) }
                .collectLatest {
                    _parentCategoryListData.value = Pair(it, item)
                }
        }
    }

    fun addCategory(name: String, parentItem: Category?) {
        viewModelScope.launch {
            categoryUseCase.addCategory(target, name, parentItem)
                .catch { error(it) }
                .collectLatest {
                    setCategoryList(it)
                }
        }
    }

    fun updateCategory(name: String, parentItem: Category?, item: Category) {
        viewModelScope.launch {
            categoryUseCase.updateCategory(target, name, parentItem, item)
                .catch { error(it) }
                .collectLatest {
                    setCategoryList(it)
                }
        }
    }

    fun deleteCategory(item: Category) {
        viewModelScope.launch {
            categoryUseCase.deleteCategory(target, item)
                .catch { error(it) }
                .collectLatest {
                    setCategoryList(it)
                }
        }
    }

    private fun setCategoryList(list: List<Category>) {
        _categoryListData.value = list.toMutableList<ListItem>().apply { add(0, CategoryHeader()) }
    }
}
