package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.common.mvvm.SingleLiveEvent
import com.devpub.moga.BuildConfig
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.usecase.EtcUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

@HiltViewModel
class EtcViewModel @Inject constructor(
    private val etcUseCase: EtcUseCase,
) : BaseViewModel() {

    val userInfo: LiveData<User> by lazy { _userInfo }
    val ping: LiveData<Boolean> by lazy { _ping }
    val syncMoimDataEvent: LiveData<Nothing> by lazy { _syncMoimDataEvent }

    private val _userInfo = MutableLiveData<User>()
    private val _ping = MutableLiveData<Boolean>()
    private val _syncMoimDataEvent = SingleLiveEvent<Nothing>()

    init {
        loadUserInfo()
    }

    private fun loadUserInfo() {
        viewModelScope.launch {
            etcUseCase.loadUserInfo()
                .catch { error(it) }
                .collectLatest {
                    _userInfo.value = it
                }
        }
    }

    fun pingServer() {
        viewModelScope.launch {
            try {
                withTimeout(1000L) {
                    etcUseCase.ping()
                        .collectLatest {
                            _ping.value = it
                        }
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    _ping.value = false
                    showToast(e.localizedMessage)
                }
            }
        }
    }

    fun syncMoimData() {
        syncMoimData(0)
    }

    fun syncMoimAllData() {
        syncMoimData(1)
    }

    private fun syncMoimData(type: Int) {
        viewModelScope.launch {
            etcUseCase.syncMoimData(type)
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .collectLatest {
                    if (it) {
                        _syncMoimDataEvent.call()
                        showToast("동기화가 완료되었습니다.")
                    } else {
                        showToast("오류로 인해 동기화에 실패하였습니다.")
                    }
                }
        }
    }
}