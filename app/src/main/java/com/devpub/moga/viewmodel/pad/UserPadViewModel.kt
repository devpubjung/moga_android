package com.devpub.moga.viewmodel.pad

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.usecase.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserPadViewModel @Inject constructor(
    private val userUseCase: UserUseCase
) : BaseViewModel() {

    val userListData: LiveData<List<User>> by lazy { _userListData }
    private val _userListData = MutableLiveData<List<User>>()

    init {
        loadUsers()
    }

    private fun loadUsers() {
        viewModelScope.launch {
            userUseCase.loadUsers()
                .catch { error(it) }
                .collectLatest {
                    _userListData.value = it
                }
        }
    }
}
