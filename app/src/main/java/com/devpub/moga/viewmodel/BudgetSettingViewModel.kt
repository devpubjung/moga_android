package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.common.mvvm.SingleLiveEvent
import com.devpub.moga.domain.model.Budget
import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.usecase.BudgetUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class BudgetSettingViewModel @Inject constructor(
    private val budgetUseCase: BudgetUseCase,
) : BaseViewModel() {

    val budgetListLiveData: LiveData<List<Budget>> by lazy { _budgetListLiveData }
    val budgetSaveEvent: LiveData<Boolean> by lazy { _budgetSaveEvent }

    private val _budgetListLiveData = MutableLiveData<List<Budget>>()
    private val _budgetSaveEvent = SingleLiveEvent<Boolean>()

    private var isChange: Boolean = false

    init {
        loadBudgetList()
    }

    private fun loadBudgetList() {
        viewModelScope.launch {
            budgetUseCase.loadBudgetList()
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .collectLatest { list ->
                    _budgetListLiveData.value = list
                }
        }
    }

    fun addBudget(category: Category) {
        _budgetListLiveData.value?.let { list ->
            viewModelScope.launch {
                budgetUseCase.addBudget(list, category)
                    .onStart { setLoading(true) }
                    .catch { error(it) }
                    .onCompletion { setLoading(false) }
                    .collectLatest { list ->
                        isChange = list.isNotEmpty()
                        _budgetListLiveData.value = list
                    }
            }
        }
    }

    fun changeBudget(budgets: List<Budget>) {
        isChange = true
        _budgetListLiveData.value = budgets
    }

    fun saveItem() {
        if (isChange && !_budgetListLiveData.value.isNullOrEmpty()) {
            viewModelScope.launch {
                budgetUseCase.updateBudgetList(_budgetListLiveData.value!!)
                    .onStart { setLoading(true) }
                    .catch { error(it) }
                    .onCompletion { setLoading(false) }
                    .collectLatest {
                        _budgetSaveEvent.value = true
                    }
            }
        } else {
            _budgetSaveEvent.value = false
        }
    }
}