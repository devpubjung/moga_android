package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.data.model.*
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.domain.model.ExpendReport
import com.devpub.moga.domain.usecase.ReportUseCase
import com.devpub.moga.ui.model.ReportByGraph
import com.devpub.moga.util.DateUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ReportViewModel @Inject constructor(
    private val appPreference: AppPreference,
    private val reportUseCase: ReportUseCase,
) : BaseViewModel() {

    val reportGraphData: LiveData<ReportByGraph> by lazy { _reportGraphData }
    val expendReportData: LiveData<ExpendReport> by lazy { _expendReportData }

    private val _reportGraphData = MutableLiveData<ReportByGraph>()
    private val _expendReportData = MutableLiveData<ExpendReport>()

    fun loadYearReport(date: Date = DateUtil.todayDate) {
        viewModelScope.launch {
            reportUseCase.loadExpendReportForYear(date)
                .catch { error(it) }
                .collectLatest { list ->
                    val monthMax = (list.maxOf { it.monthlySum } * 1.1).toLong()
                    val weekMax =
                        (list.maxOf {
                            it.dayOfWeekSumMap.maxByOrNull { map -> map.value }?.value ?: 0L
                        } * 1.1).toLong()
                    _reportGraphData.value = ReportByGraph(list, monthMax, weekMax, appPreference.startWeek)
                }
        }
    }
}