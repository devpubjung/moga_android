package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.common.mvvm.SingleLiveEvent
import com.devpub.moga.domain.usecase.InviteUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class InviteViewModel @Inject constructor(
    private val inviteUseCase: InviteUseCase
) : BaseViewModel() {

    val joinCode: LiveData<String> by lazy { _joinCode }
    val successJoinEvent: LiveData<Nothing> by lazy { _successJoinEvent }

    private val _joinCode = MutableLiveData<String>()
    private val _successJoinEvent = SingleLiveEvent<Nothing>()

    init {
        init()
    }

    private fun init() {
        viewModelScope.launch {
            inviteUseCase.loadJoinCode()
                .catch { error(it) }
                .collectLatest {
                    _joinCode.value = it
                }
        }
    }

    fun join(joinCode: String) {
        viewModelScope.launch {
            inviteUseCase.join(joinCode)
                .onStart { setLoading(true) }
                .catch { error(it) }
                .onCompletion { setLoading(false) }
                .collectLatest {
                    if (it) {
                        _successJoinEvent.call()
                    } else {
                        showToast("code가 잘못되었습니다.")
                    }
                }
        }
    }
}