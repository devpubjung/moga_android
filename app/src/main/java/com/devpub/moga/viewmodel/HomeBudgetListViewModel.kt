package com.devpub.moga.viewmodel

import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.domain.model.state.UiState
import com.devpub.moga.domain.usecase.HomeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeBudgetListViewModel @Inject constructor(
    private val homeUseCase: HomeUseCase,
) : BaseViewModel() {

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState: StateFlow<UiState> = _uiState

    fun loadBudgetReport(isMonthly: Boolean) {
        viewModelScope.launch {
            homeUseCase.loadBudgetDetailReport(isMonthly)
                .map { UiState.Success(it) }
                .collectLatest { uiState ->
                    _uiState.value = uiState
                }
        }
    }
}