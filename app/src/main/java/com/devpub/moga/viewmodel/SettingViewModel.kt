package com.devpub.moga.viewmodel

import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.domain.usecase.EtcUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import javax.inject.Inject


@HiltViewModel
class SettingViewModel @Inject constructor(
    private val appPreference: AppPreference,
    private val etcUseCase: EtcUseCase,
) : BaseViewModel() {

    var notiAutoInput = MutableStateFlow(appPreference.notiAutoInput)
    var notiShare = MutableStateFlow(appPreference.notiShare)
    var notiIcon = MutableStateFlow(appPreference.notiIcon)
    val visibleNotiShare: StateFlow<Boolean> by lazy { _visibleNotiShare }

    private val _visibleNotiShare = MutableStateFlow(false)

    fun pingServer() {
        viewModelScope.launch {
            try {
                withTimeout(1000L) {
                    etcUseCase.ping()
                        .collectLatest {
                            _visibleNotiShare.value = it && appPreference.joinMoim
                        }
                }
            } catch (e: Exception) {
                _visibleNotiShare.value = false
            }
        }
    }

    fun setStartWeek(week: Int) {
        appPreference.setStartWeek(week)
    }

    fun setStartDayOfMonth(day: Int) {
        appPreference.setStartDayOfMonth(day)
    }

    fun getStartWeek() = appPreference.startWeek

    fun getStartDayOfMonth() = appPreference.startDayOfMonth

    override fun onCleared() {
        appPreference.setNotiAutoInput(notiAutoInput.value)
        appPreference.setShowNotiIcon(notiIcon.value)
        appPreference.setNotiShare(notiShare.value)

        super.onCleared()
    }
}