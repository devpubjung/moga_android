package com.devpub.moga.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.devpub.common.mvvm.BaseViewModel
import com.devpub.common.mvvm.SingleLiveEvent
import com.devpub.moga.domain.usecase.SuggestUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SuggestViewModel @Inject constructor(
    private val suggestUseCase: SuggestUseCase,
) : BaseViewModel() {

    val sendSuccessEvent: LiveData<Nothing> by lazy { _sendSuccessEvent }
    private val _sendSuccessEvent = SingleLiveEvent<Nothing>()

    fun ping() {
        viewModelScope.launch {
            suggestUseCase.ping()
                .catch { error(Exception("서버와의 통신이 원할하지 않아 해당 메뉴는 이용하실 수 없습니다.", it)) }
                .collectLatest {
                    if (!it) {
                        error(Exception(("서버와의 통신이 원할하지 않아 해당 메뉴는 이용하실 수 없습니다.")))
                    }
                }
        }
    }

    fun send(content: String) {
        viewModelScope.launch {
            suggestUseCase.sendSuggest(content)
                .onStart { setLoading(true) }
                .catch { error(Exception("서버와의 통신이 원할하지 않아 전달 할 수 없습니다.", it)) }
                .onCompletion { setLoading(false) }
                .collectLatest {
                    if (it) {
                        _sendSuccessEvent.call()
                    } else {
                        throw Exception("서버와의 통신이 원할하지 않아 전달 할 수 없습니다.")
                    }
                }
        }
    }
}