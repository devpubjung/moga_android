package com.devpub.moga.domain.repository

import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.domain.model.AccountItem
import kotlinx.coroutines.flow.Flow

interface CommonRepository {
    fun ping() : Flow<Boolean>

    suspend fun parseSms(smsRequest: SmsRequest) : PayMessage

    suspend fun parseNoti(notiRequest: NotiRequest) : PayMessage

    suspend fun getCategoryInfo(accountItem: AccountItem): Flow<String>

    suspend fun sendSuggest(content: String): Boolean
}