package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem
import java.io.Serializable

data class BudgetReport(
    val totalBudget: Budget,
    val monthSum: Long,
    val thisWeekSum: Long,
    val isWeeklyAchievedGoal: Boolean,
) : Serializable

data class BudgetDetailReport(
    val budget: Budget,
    val accountSum: Long,
    val accountItemList: List<AccountItem>,
    val userList: List<User>,
    val isMonthly: Boolean = true,
) : ListItem

data class ExpendReport(
    val monthlySum: Long,
    val dayOfWeekSumMap: Map<Int, Long>,
    val sumOfUserList: List<UserSum>,
    val sumOfCategoryList: List<CategorySum>,
    val sumOfAssetsList: List<AssetsSum>,
) : Serializable


data class UserSum(
    val user: User,
    val sum: Long,
) : Serializable

data class CategorySum(
    val category: Category,
    val sum: Long,
) : Serializable

data class AssetsSum(
    val assets: Assets,
    val sum: Long,
) : Serializable
