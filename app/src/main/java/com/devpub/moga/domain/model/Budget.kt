package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem

class Budget(
    val moimId: Long,
    var category: Category,
    val categoryId: Long,
    var budget: Int,
    val weeklyBudget: Int,
    var maxBudget: Int,
    var color: Int,
    var index: Int,
) : ListItem
