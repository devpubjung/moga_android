package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import java.util.*
import javax.inject.Inject

class CompareMonthlyUseCase @Inject constructor(
    private val reportRepository: ReportRepository,
) {

    suspend fun loadCompareData(prevDate: Date, nextDate: Date) = flow {
        emit(reportRepository.loadCompareReportForMonth(prevDate, nextDate))
    }
}