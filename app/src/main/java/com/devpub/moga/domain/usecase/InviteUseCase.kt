package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class InviteUseCase @Inject constructor(
    private val moimRepository: MoimRepository
) {

    suspend fun loadJoinCode() = flow {
        emit(moimRepository.loadJoinCode())
    }

    suspend fun join(joinCode: String?)= flow {
        emit(moimRepository.joinMoim(joinCode))
    }
}