package com.devpub.moga.domain.repository

import androidx.paging.PagingData
import com.devpub.moga.domain.model.BudgetDetailReport
import com.devpub.moga.domain.model.BudgetReport
import com.devpub.moga.domain.model.CompareItem
import com.devpub.moga.domain.model.ExpendReport
import kotlinx.coroutines.flow.Flow
import java.util.*

interface ReportRepository {
    suspend fun loadBudgetReport() : BudgetReport?

    suspend fun loadBudgetDetailReport(isMonthly: Boolean): List<BudgetDetailReport>

    suspend fun loadBudgetDetailReportPaging(isMonthly: Boolean): Flow<PagingData<BudgetDetailReport>>

    suspend fun loadExpendReportForYear(date: Date) : List<ExpendReport>

    suspend fun loadCompareReportForMonth(prevDate: Date, nextDate: Date) : List<CompareItem>
}