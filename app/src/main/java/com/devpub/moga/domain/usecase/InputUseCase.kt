package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import java.util.*
import javax.inject.Inject

class InputUseCase @Inject constructor(
    private val accountItemRepository: AccountItemRepository
) {

    suspend fun loadAccountItem(itemId: Long?, date: Date?) = flow {
        emit(accountItemRepository.loadAccountItem(itemId, date))
    }

    suspend fun saveAccountItem(item: AccountItem) = flow {
        accountItemRepository.saveAccountItem(item)
        emit("저장하였습니다.")
    }
}