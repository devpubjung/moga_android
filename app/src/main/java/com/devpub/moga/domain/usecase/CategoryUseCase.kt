package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CategoryUseCase @Inject constructor(
    private val categoryRepository: CategoryRepository,
) {

    suspend fun loadCategoryList(target: Target) = flow {
        emit(categoryRepository.loadCategoryList(target))
    }

    suspend fun loadParentCategoryList(target: Target) = flow {
        emit(categoryRepository.loadParentCategoryList(target))
    }

    suspend fun addCategory(target: Target, name: String, parentItem: Category?) = flow {
        emit(categoryRepository.addCategory(target, name, parentItem))
    }

    suspend fun updateCategory(target: Target, name: String, parentItem: Category?, item: Category) = flow {
        emit(categoryRepository.updateCategory(target, name, parentItem, item))
    }

    suspend fun deleteCategory(target: Target, item: Category) = flow {
        emit(categoryRepository.deleteCategory(target, item))
    }
}