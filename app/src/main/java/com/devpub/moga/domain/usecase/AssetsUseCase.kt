package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.Assets
import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AssetsUseCase @Inject constructor(
    private val assetsRepository: AssetsRepository,
) {

    suspend fun loadAssetsList() = flow {
        emit(assetsRepository.loadAssetsList())
    }

    suspend fun addAssets(name: String) = flow {
        emit(assetsRepository.addAssets(name))
    }

    suspend fun removeAssets(item: Assets) = flow {
        emit(assetsRepository.removeAssets(item))
    }
}