package com.devpub.moga.domain.repository

import com.devpub.moga.domain.model.Budget
import com.devpub.moga.domain.model.Category

interface BudgetRepository {

    suspend fun loadBudgetList() : List<Budget>

    suspend fun addBudget(list : List<Budget>, category: Category) : List<Budget>

    suspend fun updateBudgetList(list : List<Budget>)

    suspend fun saveBudget(budget: Budget)

    suspend fun saveBudgetList(addBudgetList: List<Budget>, removeBudgetList : List<Budget> = listOf())

}