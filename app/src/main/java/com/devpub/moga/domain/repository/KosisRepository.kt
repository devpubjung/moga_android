package com.devpub.moga.domain.repository

import com.devpub.common.model.ListItem
import java.util.*

interface KosisRepository {

    suspend fun getByAgeData(date: Date) : List<ListItem>

    suspend fun getByIncomeData(date: Date): List<ListItem>
}