package com.devpub.moga.domain.repository

import com.devpub.moga.domain.model.Moim
import com.devpub.moga.domain.model.User

interface MoimRepository {

    suspend fun getMoim() : Moim?

    suspend fun saveMoim(moim: Moim)

    suspend fun updateMoim(moim: Moim)

    suspend fun createMoim(user: User)

    suspend fun joinMoim(joinCode: String?): Boolean

    suspend fun syncMoimData(type: Int = 0): Boolean

    fun syncMoimDataForBackground(type: Int = 0)

    suspend fun loadJoinCode() : String

    suspend fun loadUsers() : List<User>
}