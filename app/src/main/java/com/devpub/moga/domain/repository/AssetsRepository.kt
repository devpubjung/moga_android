package com.devpub.moga.domain.repository

import com.devpub.moga.domain.model.Assets

interface AssetsRepository {

    suspend fun loadAssetsList(): List<Assets>

    suspend fun addAssets(name: String): List<Assets>

    suspend fun removeAssets(item: Assets): List<Assets>

    suspend fun findAssets(assetsName: String) : Assets
}