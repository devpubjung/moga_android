package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem

data class KosisItem(
    val categoryPid: Long,
    val columnName: String,
    val myExpend: Long,
    val data: Long,
) : ListItem

data class KosisData(
    val compareTitle: String,
    val compareSubTitle: String,
    val chartTitle: String,
    val othersTitle: String,
    val myTotal: Long,
    val totalData: Long,
    val isByAge: Boolean,
) : ListItem