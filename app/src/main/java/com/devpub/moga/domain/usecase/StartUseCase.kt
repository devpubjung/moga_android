package com.devpub.moga.domain.usecase

import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.domain.model.state.LoginState
import com.devpub.moga.domain.model.User
import com.devpub.moga.domain.repository.MoimRepository
import com.devpub.moga.domain.repository.UserRepository
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class StartUseCase @Inject constructor(
    private val appPreference: AppPreference,
    private val userRepository: UserRepository,
    private val moimRepository: MoimRepository,
) {

    suspend fun loginUser(joinCode: String?, always: Boolean = false) = flow {
        if (appPreference.userId != DEFAULT_ID) {
            if (always ||
                DateUtil.getBeforeDay(3).time <
                DateUtil.convertPreferenceDate(appPreference.userSaveDate)?.time ?: Long.MAX_VALUE
            ) {
                userRepository.getUser()?.let {
                    moimRepository.joinMoim(joinCode)
                    moimRepository.syncMoimDataForBackground()
                    emit(LoginState.Login)
                } ?: emit(LoginState.Fail)
            } else {
                emit(LoginState.SocialLogin)
            }
        } else {
            emit(LoginState.Fail)
        }
    }

    suspend fun loginUserForSocial(user: User, joinCode: String?) = flow {
        val loadUser = userRepository.getUserUid(user.uid, user.provider)
        if (loadUser != null) {
            userRepository.updateUser(loadUser.apply {
                profileImgUrl = user.profileImgUrl
                thumbNailImgUrl = user.thumbNailImgUrl
            })
            if (!moimRepository.joinMoim(joinCode)) {
                if (moimRepository.getMoim() == null) {
                    moimRepository.createMoim(loadUser)
                }
            }
            moimRepository.syncMoimDataForBackground()
            emit(LoginState.Login)
        } else {
            createUser(user, joinCode)
            emit(LoginState.SingUp)
        }
    }

    private suspend fun createUser(user: User, joinCode: String?) {
        userRepository.saveUser(user).also {
            if (moimRepository.joinMoim(joinCode)) {
                moimRepository.syncMoimDataForBackground()
            } else {
                moimRepository.createMoim(it)
            }
        }
    }
}