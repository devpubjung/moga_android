package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import java.util.*
import javax.inject.Inject

class ReportUseCase @Inject constructor(
    private val reportRepository: ReportRepository,
) {

    suspend fun loadExpendReportForYear(date: Date) = flow {
        emit(reportRepository.loadExpendReportForYear(date))
    }
}