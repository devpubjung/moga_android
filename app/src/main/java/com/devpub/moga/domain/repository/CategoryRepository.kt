package com.devpub.moga.domain.repository

import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.model.Target

interface CategoryRepository {
    suspend fun loadCategoryList(target: Target): List<Category>

    suspend fun loadParentCategoryList(target: Target): List<Category>

    suspend fun addCategory(target: Target, name: String, parentItem: Category?): List<Category>

    suspend fun updateCategory(target: Target, name: String, parentItem: Category?, item: Category): List<Category>

    suspend fun deleteCategory(target: Target, item: Category): List<Category>
}