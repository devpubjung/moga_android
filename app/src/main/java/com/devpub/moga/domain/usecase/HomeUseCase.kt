package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.state.DataState
import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import java.lang.NullPointerException
import javax.inject.Inject

class HomeUseCase @Inject constructor(
    private val reportRepository: ReportRepository,
) {

    suspend fun loadBudgetReport() = flow {
        reportRepository.loadBudgetReport()?.let {
            emit(DataState.Success(it))
        } ?: emit(DataState.Error(NullPointerException("가계부 생성이 안되었습니다.\n재실행하거나 앱을 재설치 하시길 추천합니다.")))
    }

    suspend fun loadBudgetDetailReport(isMonthly: Boolean) = flow {
        emit(reportRepository.loadBudgetDetailReport(isMonthly))
    }

    suspend fun loadBudgetDetailReportPaging(isMonthly: Boolean) =
        reportRepository.loadBudgetDetailReportPaging(isMonthly)
}