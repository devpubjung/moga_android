package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class EtcUseCase @Inject constructor(
    private val commonRepository: CommonRepository,
    private val userRepository: UserRepository,
    private val moimRepository: MoimRepository,
) {

    suspend fun loadUserInfo() = flow {
        userRepository.getUser()?.let {
            emit(it)
        }
    }

    fun ping() = commonRepository.ping()

    suspend fun syncMoimData(type : Int = 0) = flow {
        emit(moimRepository.syncMoimData(type))
    }

}