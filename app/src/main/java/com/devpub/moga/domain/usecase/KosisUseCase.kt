package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import java.util.*
import javax.inject.Inject

class KosisUseCase @Inject constructor(
    private val kosisRepository: KosisRepository,
) {

    suspend fun loadKosisByAge(date: Date) = flow {
        emit(kosisRepository.getByAgeData(date))
    }

    suspend fun loadKosisByIncome(date: Date) = flow {
        emit(kosisRepository.getByIncomeData(date))
    }
}