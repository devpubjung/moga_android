package com.devpub.moga.domain.repository

import com.devpub.moga.data.model.external.KakaoAddress

interface KakaoRepository {
    suspend fun searchKeyword(keyword:String): KakaoAddress
}