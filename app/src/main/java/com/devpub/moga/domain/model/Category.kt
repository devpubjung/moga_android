package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem
import com.devpub.moga.DEFAULT_ID

data class Category(
    val moimId: Long,
    val target: Target = Target.EXPEND,
    val id: Long = 0,
    val parentId: Long = DEFAULT_ID,
    val kosisId: Long = DEFAULT_ID,
    val name: String = "전체",
    val tag: String = name.replace("/", ","),
    val ownerId: Long? = null,
    val index: Int = 0,
) : ListItem {
    val canDelete = id != parentId

//    override fun equals(o: Any?): Boolean {
//        if (this === o) return true
//        if (o == null || javaClass != o.javaClass) return false
//
//        val that = o as Category
//        if (moimId != that.moimId) return false
//        if (id != that.id) return false
//        if (parentId != that.parentId) return false
//        if (kosisId != that.kosisId) return false
//        if (ownerId != that.ownerId) return false
//        if (name != that.name) return false
//        return true
//    }
//
//    override fun hashCode(): Int {
//        var result = if (moimId != 0L) moimId else 0
//        result = 31 * result + if (id != 0L) id else 0
//        result = 31 * result + if (parentId != 0L) parentId else 0
//        result = 31 * result + if (kosisId != 0L) kosisId else 0
//        result = 31 * result + (ownerId ?: 0)
//        result = 31 * result + name.toCharArray().contentHashCode()
//        result = 31 * result + index
//        return result.toInt()
//    }

    override fun toString() = name
}