package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.data.model.dto.MoimInfo
import java.io.Serializable

data class Moim(
    val id: Long = 1, //userId+No,
    val ownerId: Long = DEFAULT_ID, //userId
    val userList: List<User> = emptyList(),
    val categoryList: List<Category> = emptyList(),
    val budgetList: List<Budget> = emptyList(),
) : ListItem

data class UpdatedMoimData(
    val moimInfo: MoimInfo? = null,
    val moim: Moim,
    val accountItemList: List<AccountItem>,
    val removedAccountItemList: List<AccountItem>,
) : Serializable