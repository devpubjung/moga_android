package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SuggestUseCase @Inject constructor(
    private val commonRepository: CommonRepository,
) {

    fun ping() = commonRepository.ping()

    suspend fun sendSuggest(content: String)= flow {
        emit(commonRepository.sendSuggest(content))
    }
}