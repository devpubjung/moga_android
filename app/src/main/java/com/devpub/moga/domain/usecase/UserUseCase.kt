package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UserUseCase @Inject constructor(
    private val moimRepository: MoimRepository,
) {

    suspend fun loadUsers() = flow {
        emit(moimRepository.loadUsers())
    }
}