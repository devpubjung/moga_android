package com.devpub.moga.domain.model.state

sealed class LoginState {
    object Loading: LoginState()
    object Login: LoginState()
    object SingUp: LoginState()
    object Fail: LoginState()
    object SocialLogin: LoginState()
    data class Error(val error: Throwable?): LoginState()
}