package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.model.AccountItemNote
import com.devpub.moga.domain.model.Target
import com.devpub.moga.domain.model.state.DataState
import com.devpub.moga.domain.repository.*
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.flow.flow
import java.util.*
import javax.inject.Inject

class NoteUseCase @Inject constructor(
    private val commonRepository: CommonRepository,
    private val accountItemRepository: AccountItemRepository,
) {

    suspend fun loadMonthNote(date: Date) = flow {
        emit(DataState.Success(getNoteList(accountItemRepository.loadMonthlyAccountList(date))))
    }

    suspend fun loadDayNote(date: Date) = flow {
        emit(DataState.Success(getNoteList(accountItemRepository.loadDayAccountList(date))))
    }

    suspend fun removeNote(accountItem: AccountItem) = flow {
        accountItemRepository.removeItem(accountItem)
        emit(DataState.Success(getNoteList(accountItemRepository.loadMonthlyAccountList(accountItem.date))))
    }

    suspend fun getCategoryInfo(accountItem: AccountItem)  = commonRepository.getCategoryInfo(accountItem)

    private fun getNoteList(accountItemList: List<AccountItem>): List<AccountItemNote> {
        var preDate: Date? = null
        var incomeSum: Long = 0
        var expendSum: Long = 0
        var transferSum: Long = 0
        var transferExpendSum: Long = 0
        val noteList = mutableListOf<AccountItemNote>()

        accountItemList.forEach {
            val item = AccountItemNote(it)
            if (!DateUtil.sameDate(preDate, it.date)) {
                item.showHeader = true
                preDate = it.date
            }

            noteList.add(item)
        }

        for (i in noteList.size - 1 downTo 0) {
            noteList[i].let {
                when (it.accountItem.target) {
                    Target.INCOME -> {
                        incomeSum += it.accountItem.amount
                    }
                    Target.TRANSFER -> {
                        transferSum += it.accountItem.amount
                        transferExpendSum += if (it.accountItem.expend) it.accountItem.amount else 0
                    }
                    else -> {
                        expendSum += it.accountItem.amount
                    }
                }

                if (it.showHeader) {
                    it.incomeMoney = incomeSum
                    it.expendMoney = expendSum
                    it.transferMoney = transferSum
                    it.transferExpendMoney = transferExpendSum
                    val total = incomeSum - expendSum - transferExpendSum
                    it.isPlus = total >= 0
                    it.totalMoney = total
                    incomeSum = 0
                    expendSum = 0
                    transferSum = 0
                    transferExpendSum = 0
                }
            }
        }
        return noteList
    }
}