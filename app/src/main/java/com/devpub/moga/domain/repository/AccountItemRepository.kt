package com.devpub.moga.domain.repository

import com.devpub.moga.domain.model.AccountItem
import com.devpub.moga.domain.model.User
import java.util.*

interface AccountItemRepository {
    suspend fun loadAccountItem(itemId: Long?, date: Date?) : AccountItem

    suspend fun saveAccountItem(accountItem: AccountItem)

    suspend fun saveAccountItemList(accountItemList: List<AccountItem>)

    suspend fun updateAccountItemForUser(user: User)

    suspend fun loadMonthlyAccountList(date: Date): List<AccountItem>

    suspend fun loadDayAccountList(date: Date): List<AccountItem>

    suspend fun removeItem(accountItem: AccountItem)
}