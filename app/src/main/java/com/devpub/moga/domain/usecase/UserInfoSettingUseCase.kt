package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.Gender
import com.devpub.moga.domain.model.state.DataState
import com.devpub.moga.domain.repository.AccountItemRepository
import com.devpub.moga.domain.repository.BudgetRepository
import com.devpub.moga.domain.repository.MoimRepository
import com.devpub.moga.domain.repository.UserRepository
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.flow.flow
import java.lang.NullPointerException
import javax.inject.Inject

class UserInfoSettingUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val moimRepository: MoimRepository,
    private val budgetRepository: BudgetRepository,
    private val accountItemRepository: AccountItemRepository,
) {

    suspend fun loadUserInfo() = flow {
        userRepository.getUser()?.let {
            emit(DataState.Success(it))
        } ?: emit(DataState.Error(NullPointerException("User 정보를 가져올 수 없습니다.")))
    }

    suspend fun saveUserInfo(
        email: String,
        name: String,
        birthYear: Int,
        gender: Gender,
        totalBudget: Int?,
    ) = flow {
         val savedUser = userRepository.getUser()?.let { user ->
            user.email = email
            user.nickName = name
            user.birthYear = birthYear
            var ageRange = (DateUtil.thisYear() - birthYear) / 10
            if (ageRange <= 1) ageRange = 1
            user.ageRange = ageRange
            user.gender = gender
            userRepository.saveUser(user)
            accountItemRepository.updateAccountItemForUser(user)
            user
        }

        if (totalBudget != null) {
            moimRepository.getMoim()?.let { moim ->
                moim.budgetList[0].budget = totalBudget
                moimRepository.updateMoim(moim)
                budgetRepository.saveBudget(moim.budgetList[0])
            }
        }
        emit(DataState.Success(savedUser))
    }

    suspend fun deleteUser() = flow {
        if (userRepository.deleteUser()) {
            emit(DataState.Success(Unit))
        } else {
            emit(DataState.Error())
        }
    }
}