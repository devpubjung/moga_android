package com.devpub.moga.domain.model.state

sealed class DataState {
    data class Success<T>(val data: T): DataState()
    data class Error(val error: Throwable? = null): DataState()
}