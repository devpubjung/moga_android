package com.devpub.moga.domain.repository

import com.devpub.moga.domain.model.Provider
import com.devpub.moga.domain.model.User

interface UserRepository {

    suspend fun getUser() : User?

    suspend fun getUserUid(uid: Long, provider: Provider) : User?

    suspend fun saveUser(user: User, me : Boolean = true) : User

    suspend fun updateUser(user: User)

    suspend fun deleteUser() : Boolean
}