package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem

class Assets(
    val moimId: Long = 0,
    val id: Long = 0,
    val name: String = "추가",
    var index: Int = 1
) : ListItem