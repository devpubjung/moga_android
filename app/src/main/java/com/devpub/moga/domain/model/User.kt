package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.R
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User(
    val id: Long = DEFAULT_ID,
    var uid: Long = DEFAULT_ID,
    val provider: Provider = Provider.KAKAO,
    var nickName: String = "",
    var email: String = "",
    var profileImgUrl: String = "",
    var thumbNailImgUrl: String = "",
    var ageRange: Int = 1,
    var birthYear: Int = 1987,
    var gender: Gender = Gender.MALE,
    val defaultProfile: Int = R.drawable.ic_groups,
) : ListItem {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val that = o as User
        if (uid != that.uid) return false
        if (provider != that.provider) return false
        if (id != that.id) return false
        return true
    }

    override fun hashCode(): Int {
        var result = if (uid != 0L) uid else 0
        result = 31 * result + provider.hashCode()
        result = 31 * result + if (id != 0L) id else 0
        result = 31 * result + nickName.toCharArray().contentHashCode()
        result = 31 * result + email.toCharArray().contentHashCode()
        result = 31 * result + profileImgUrl.toCharArray().contentHashCode()
        result = 31 * result + thumbNailImgUrl.toCharArray().contentHashCode()
        result = 31 * result + if (ageRange != 0) ageRange else 0
        result = 31 * result + if (birthYear != 0) birthYear else 0
        result = 31 * result + gender.hashCode()
        return result.toInt()
    }
}

enum class Provider(val value: String) : Serializable {
    @SerializedName("NONE")
    NONE("NONE"),

    @SerializedName("KAKAO")
    KAKAO("KAKAO"),

    @SerializedName("NAVER")
    NAVER("NAVER"),

    @SerializedName("GOOGLE")
    GOOGLE("GOOGLE")
}

enum class Gender(val value: String) : Serializable{
    @SerializedName("MALE")
    MALE("MALE"),

    @SerializedName("FEMALE")
    FEMALE("FEMALE")
}