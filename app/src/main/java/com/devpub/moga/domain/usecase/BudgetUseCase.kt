package com.devpub.moga.domain.usecase

import com.devpub.moga.domain.model.Budget
import com.devpub.moga.domain.model.Category
import com.devpub.moga.domain.repository.*
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class BudgetUseCase @Inject constructor(
    private val budgetRepository: BudgetRepository,
) {

    suspend fun loadBudgetList() = flow {
        emit(budgetRepository.loadBudgetList())
    }

    suspend fun addBudget(list: List<Budget>, category: Category)= flow {
        emit(budgetRepository.addBudget(list, category))
    }

    suspend fun updateBudgetList(list: List<Budget>)= flow {
        emit(budgetRepository.updateBudgetList(list))
    }
}