package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem
import com.devpub.moga.DEFAULT_ID
import com.devpub.moga.DEFAULT_NUMBER
import com.devpub.moga.R
import com.devpub.moga.util.DateUtil
import java.util.*

data class AccountItem(
    val moimId: Long,
    val id: Long = "${moimId}$DEFAULT_NUMBER".toLong(),
    var target: Target = Target.EXPEND,
    var date: Date = DateUtil.today.time,
    var user: User? = null,
    var amount: Long = 0,
    var assets: Assets? = null,
    var category: Category? = null,
    var content: String? = null,
    var memo: String? = null,
    var expend: Boolean = true,
    var share: Boolean = true,
    var report: Boolean = true,
    var ownerId: Long = DEFAULT_ID,
    var type: NoteType = NoteType.NONE,
    var assetType: AssetType = AssetType.CASH,
    val isWriteMe : Boolean  = true
) : ListItem

enum class Target(val color: Int) {
    INCOME(R.color.incomeColor),
    EXPEND(R.color.expendColor),
    TRANSFER(R.color.transferColor)
}

enum class NoteType {
    NONE,
    NOTI,
    SMS
}

enum class AssetType {
    CASH,
    CHECK,
    CREDIT
}

class AccountItemNote(
    val accountItem: AccountItem,
) : ListItem {
    var showHeader: Boolean = false
    var incomeMoney: Long = 0
    var expendMoney: Long = 0
    var transferMoney: Long = 0
    var transferExpendMoney: Long = 0
    var totalMoney: Long = 0
    var isPlus: Boolean = false
}
