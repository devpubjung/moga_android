package com.devpub.moga.domain.model

import com.devpub.common.model.ListItem

data class CompareItem(
    val category: Category,
    val target: Target = Target.EXPEND,
    var isHeader: Boolean = false,
    val prevAmount: Long = 0,
    val nextAmount: Long = 0,
    val sum: Long = 0,
    var isUnderLine: Boolean = false
) : ListItem