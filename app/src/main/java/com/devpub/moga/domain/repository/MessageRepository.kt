package com.devpub.moga.domain.repository

import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.data.model.common.ApiDataResponse
import com.devpub.moga.data.model.entity.AccountItemEntity
import com.devpub.moga.data.model.external.KakaoAddress

interface MessageRepository {
    suspend fun saveAccountItem(accountItemEntity: AccountItemEntity)

    suspend fun searchKeyword(keyword:String): KakaoAddress

    suspend fun parseSms(smsRequest: SmsRequest) : ApiDataResponse<PayMessage>

    suspend fun parseNoti(notiRequest: NotiRequest) : ApiDataResponse<PayMessage>
}