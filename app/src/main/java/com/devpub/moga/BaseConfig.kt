package com.devpub.moga

private const val URL_DEBUG = "192.168.31.16"
private const val URL_RELEASE = "devpub.co.kr"
private const val PORT = "3030"
const val BASE_URL = "http://$URL_RELEASE:$PORT"
const val DEBUG_URL = "http://$URL_DEBUG:$PORT"

const val API = "/api"
const val VERSION_1 = "/v1"
const val AUTH = "/auth"
const val DEFAULT_ID = -1L
const val DEFAULT_NUMBER = 1
const val DEFAULT_MONTHLY_BUDGET = 300
const val MONEY_SLICE = 10000

const val NETWORK_TAG = "OkHttp_network"