package com.devpub.moga.manager

import android.app.Activity
import com.google.android.play.core.review.ReviewManagerFactory

class ReviewManager(private val activity: Activity) {

    private val manager = ReviewManagerFactory.create(activity)

    fun showReviewPopup(callback : ((Boolean) -> Unit)? = null) {
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { request ->
            if (request.isSuccessful) {
                val reviewInfo = request.result
                val flow = manager.launchReviewFlow(activity, reviewInfo)
                flow.addOnCompleteListener { result ->
                    callback?.invoke(result.isComplete)
                }
            }
        }
    }
}