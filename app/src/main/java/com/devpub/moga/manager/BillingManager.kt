package com.devpub.moga.manager

import android.app.Activity
import android.content.Context
import android.util.Log
import com.android.billingclient.api.*
import com.devpub.common.util.TAG
import com.devpub.moga.BuildConfig
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.util.extensions.showToast
import javax.inject.Inject

class BillingManager @Inject constructor(val context: Context) {

    private lateinit var billingClient: BillingClient
    private val appPreference: AppPreference
    private val purchasableList = listOf("remove_ads", "donation")
    private var skuDetails: List<SkuDetails> = listOf()
    private var purchaseListener: PurchaseListener? = null
    init {
        initBillingClient()
        appPreference = AppPreferenceImpl(context)
    }

    private fun initBillingClient() {
        billingClient = BillingClient.newBuilder(context)
            .enablePendingPurchases()
            .setListener { billingResult, purchases ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !purchases.isNullOrEmpty()) {
                    purchases.forEach { purchase ->
                        val isRemoveAdsItem = purchase.skus.firstOrNull { it == "remove_ads" }
                        if (isRemoveAdsItem != null) {
                            purchaseRemoveAds(purchase)
                        } else {
                            purchaseDonation(purchase)
                        }
                    }
                } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                    "상품 주문이 취소되었습니다.".showToast(context)
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, billingResult.debugMessage)
                        "구매요청이 실패했습니다.(${billingResult.responseCode})".showToast(context)
                    }
                }
            }
            .build()
    }

    fun startConnection() {
        billingClient.startConnection(
            object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        querySkuDetails()
                    }
                }

                override fun onBillingServiceDisconnected() {
                    Log.e(TAG, "Service Disconnected")
                    // Try to restart the connection on the next request to
                    // Google Play by calling the startConnection() method.
                }
            })
    }

    fun isRemoveAdsItem(): Boolean {
        var isRemoveAdsItem = false
        billingClient.queryPurchasesAsync(BillingClient.SkuType.INAPP) { result, purchases ->
            if (purchases.isNullOrEmpty()) {
                isRemoveAdsItem = false
            } else {
                purchases.forEach { acknowledgePurchase ->
                    purchasableList.forEach { itemId ->
                        val isPurchaseId = acknowledgePurchase.skus.firstOrNull { it == itemId }
                        isRemoveAdsItem = isPurchaseId != null
                    }
                }
            }
        }

        return isRemoveAdsItem
    }

    fun queryPurchaseHistoryAsync(): Boolean {
        var isRemoveAdsItem = false
        billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP) { result, purchaseHistoryList ->
            if (result.responseCode == BillingClient.BillingResponseCode.OK) {
                if (!purchaseHistoryList.isNullOrEmpty()) {
                    appPreference.setPurchasedRemoveAds(true)
                    isRemoveAdsItem = true
                }
            }
        }

        return isRemoveAdsItem
    }

    fun purchaseItem(itemIndex: Int, purchaseListener: () -> Unit) {
        this.purchaseListener = object : PurchaseListener {
            override fun onSuccessBilling() {
                purchaseListener()
            }
        }
        if (skuDetails.isNullOrEmpty() || skuDetails.size <= itemIndex) {
            "시스템상의 문제로 아이템을 구매하실 수 없습니다.".showToast(context)
            return
        }

        val flowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetails[itemIndex]).build()
        val responseCode = billingClient.launchBillingFlow(context as Activity, flowParams).responseCode
        if (responseCode == BillingClient.BillingResponseCode.OK) {
            //인앱 결제 바텀시트가 노출됨.
        } else {
            "구매요청이 실패했습니다.($responseCode)".showToast(context)
        }
    }

    private fun querySkuDetails() {
        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(purchasableList).setType(BillingClient.SkuType.INAPP)
        billingClient.querySkuDetailsAsync(params.build()) { result, skuDetails ->
            if (result.responseCode == BillingClient.BillingResponseCode.OK) {
                this.skuDetails = skuDetails as ArrayList<SkuDetails>
            } else {
                Log.e(TAG, result.debugMessage)
                "아이템 구매가 실패했습니다.(${result.responseCode})".showToast(context)
            }
        }
    }

    //소비성 아이템
    private fun purchaseDonation(purchase: Purchase) {
        val consumeParams =
            ConsumeParams.newBuilder().setPurchaseToken(purchase.purchaseToken).build()
        billingClient.consumeAsync(consumeParams) { billingResult, _ ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                appPreference.setPurchasedRemoveAds(true)
                purchaseListener?.onSuccessBilling()
            } else {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, billingResult.debugMessage)
                    "아이템 구매가 실패했습니다.(${billingResult.responseCode})".showToast(context)
                }
            }
        }
    }

    //1회성 아이템
    private fun purchaseRemoveAds(purchase: Purchase) {
        if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged) {
                val params =
                    AcknowledgePurchaseParams.newBuilder().setPurchaseToken(purchase.purchaseToken)
                        .build()
                billingClient.acknowledgePurchase(params) { billingResult ->
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        appPreference.setPurchasedRemoveAds(true)
                        purchaseListener?.onSuccessBilling()
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, billingResult.debugMessage)
                            "아이템 구매가 실패했습니다.(${billingResult.responseCode})".showToast(context)
                        }
                    }
                }
            }
        }
    }


    interface PurchaseListener {
        fun onSuccessBilling()
    }
}