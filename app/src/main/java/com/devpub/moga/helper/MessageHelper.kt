package com.devpub.moga.helper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.devpub.common.util.TAG
import com.devpub.moga.BuildConfig
import com.devpub.moga.R
import com.devpub.moga.data.database.AppDatabase
import com.devpub.moga.data.extension.launchBackground
import com.devpub.moga.data.model.*
import com.devpub.moga.domain.model.Target
import com.devpub.moga.data.model.entity.AccountItemEntity
import com.devpub.moga.data.model.entity.AssetsEntity
import com.devpub.moga.data.model.entity.CategoryEntity
import com.devpub.moga.data.preference.AppPreference
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.domain.repository.MessageRepository
import com.devpub.moga.ui.activity.StartActivity
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MogaUtil
import com.devpub.moga.util.MoneyUtil.commaWon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import java.util.regex.Pattern
import kotlin.Exception

internal class MessageHelper(
    private val context: Context,
    private val messageRepository: MessageRepository,
) {

    private val appPreference: AppPreference by lazy { AppPreferenceImpl(context) }
    private var appDatabase: AppDatabase? = AppDatabase.getInstance(context)

    fun parseSmsContentMessage(text: String, date: Date = DateUtil.todayDate): PayMessage? {
        try {
            val numberAndLetter = "[^\\[\\]\\(\\)\\-]"

            val cardMatcher = CARD_PATTERN.matcher(text)

            val isCard = cardMatcher.find()
            val isBank = isBank(text)
            if (isCard || isBank) {
                val payMessage = PayMessage(date)
                val content = getExceptedTotalMoneyText(text)
                payMessage.content = content.trim()
                payMessage.isBank = isBank

                if (isCard) {
                    var cardText: String
                    val cardStatusText: String
                    val cardRejectMatcher = Pattern.compile("거절").matcher(content)
                    if (cardRejectMatcher.find()) {
                        return null
                    }
                    if (cardMatcher.groupCount() > 1) {
                        cardText = cardMatcher.group(cardMatcher.groupCount() - 1)
                        val subCardMatcher = CARD_PATTERN.matcher(cardText)
                        while (subCardMatcher.find()) {
                            if (subCardMatcher.groupCount() > 1) {
                                cardText = subCardMatcher.group(subCardMatcher.groupCount() - 1)
                            }
                        }
                        cardStatusText = cardMatcher.group(cardMatcher.groupCount())
                    } else {
                        val cardMatchingText = cardMatcher.group()
                        val cardDetailMatcher = Pattern.compile("승인|취소").matcher(cardMatchingText)
                        if (cardDetailMatcher.find()) {
                            cardText = cardMatchingText.substring(0, cardDetailMatcher.start())
                            cardStatusText = cardDetailMatcher.group()
                        } else {
                            cardText = cardMatchingText
                            cardStatusText = "승인"
                        }
                    }

                    payMessage.assetsName =
                        getRegexString(cardText.replace("Web발신", ""), numberAndLetter).trim()
                    if (cardStatusText.contains("취소")) {
                        payMessage.target = Target.INCOME
                    } else {
                        payMessage.target = Target.EXPEND
                    }
                } else {
                    val bankNameMatcher =
                        Pattern.compile("(?:.*은행|.*뱅크|.*증권|(.*기업|농협|우리|신한|NH|씨티|카카오|케이|국민|KB|하나|부산|수협|전북|제주|광주|경남))")
                            .matcher(content)

                    if (bankNameMatcher.find()) {
                        val bankNameText = bankNameMatcher.group()
                        payMessage.assetsName = getRegexString(bankNameText.replace("Web발신", ""),
                            numberAndLetter).trim()
                    }

                    payMessage.target = getTransaction(content)
                }

                payMessage.payMoney = getMoney(content)
                payMessage.storeName = getStoreName(isCard, payMessage.assetsName, content)

                return payMessage
            }

        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        return null
    }

    fun isCard(text: String): Boolean {
        val cardMatcher = CARD_PATTERN.matcher(text)
        return cardMatcher.find()
    }

    fun isBank(text: String): Boolean {
        val bankMatcher = BANK_PATTERN.matcher(text)
        return bankMatcher.find()
    }

    fun getExceptedTotalMoneyText(text: String): String {
        val contentText = text.replace("[Web발신]", "")
        val totalMoneyMatcher = TOTAL_MONEY_PATTERN.matcher(contentText)
        if (totalMoneyMatcher.find()) {
            return contentText.replace(totalMoneyMatcher.group(), "")
        }
        return contentText
    }

    fun getTransaction(content: String): Target? {
        val transactionMatcher = TRANSACTION_PATTERN.matcher(content)
        if (transactionMatcher.find()) {
            val transactionText = transactionMatcher.group()
            return when {
                transactionText.contains("지급") -> {
                    Target.TRANSFER
                }
                transactionText.contains("출금") -> {
                    Target.TRANSFER
                }
                else -> {
                    Target.INCOME
                }
            }
        }

        return null
    }

    fun getMoney(content: String): Long {
        val moneyMatcher = MONEY_PATTERN.matcher(content)
        if (moneyMatcher.find()) {
            return try {
                getRegexString(moneyMatcher.group(), "\\d").toLong()
            } catch (e: Exception) {
                0
            }
        }
        return 0
    }

    fun getStoreName(
        isCard: Boolean,
        assetsName: String,
        content: String,
        additionalRegex: String = "",
    ): String {
        var storeName = ""
        var startMerge = false
        fun getFitStore(regex: String): String? {
            val fitStoreMatcher = Pattern.compile(regex).matcher(content)
            if (fitStoreMatcher.find()) {
                return fitStoreMatcher.takeIf { it.groupCount() > 0 }?.group(1)?.trim()
            }
            return null
        }

        try {
            getFitStore(".*가맹점명:(.+)")?.let {
                return it
            }
            getFitStore(".*입금하신분 : (.+)")?.let {
                return it
            }
        } catch (e: Exception) {
            storeName = ""
        }

        val storeMatcher = Pattern.compile("\\s*\\S*\\s*").matcher(content)
        var storeWordCount = 1
        while (storeMatcher.find()) {
            val storeTempText = storeMatcher.group()
            if (storeTempText.trim().isNotEmpty()) {
                if (isCard) {
                    val totalTextMatcher =
                        Pattern.compile("누적|잔여|잔액|승인|([\\d|,]+원)").matcher(storeTempText)
                    if (!totalTextMatcher.find() && startMerge && storeWordCount >= 0) {
                        storeName += storeTempText
                        storeWordCount--
                    }
                    if (!startMerge) {
                        val startTextMatcher = TIME_PATTERN.matcher(storeTempText)
                        startMerge = startTextMatcher.find()
                    }
                } else {
                    val ignoreMatcher =
                        Pattern.compile("누적|잔여|잔액|입금|출금|지급|승인|금액|계좌번호|:|(.*Web.*)|([\\d|,]+원)|(\\d{2}[/.:]\\d{2})|(.*(?=[*]).*)$additionalRegex")
                            .matcher(storeTempText)
                    if (!ignoreMatcher.find() && !storeTempText.contains(assetsName)) {
                        storeName += storeTempText
                    }
                }
            }
        }
        return storeName.trim()
    }

    fun getDateTime(content: String): String {
        var dateTime = DateUtil.getYearText()
        val dateMatcher = DAY_PATTERN.matcher(content)
        val timeMatcher = TIME_PATTERN.matcher(content)
        if (dateMatcher.find()) {
            dateTime += "/${dateMatcher.group()}"
        }

        if (timeMatcher.find()) {
            dateTime += " ${timeMatcher.group()}"
        }
        return dateTime
    }

    fun getRegexString(str: String, regex: String): String {
        val sb = StringBuffer()
        if (str.isNotEmpty()) {
            val pattern = Pattern.compile(regex)
            val matcher = pattern.matcher(str)
            while (matcher.find()) {
                sb.append(matcher.group())
            }
        }
        return sb.toString()
    }

    suspend fun insertData(
        payMessage: PayMessage,
        savedPayMessage: PayMessage? = null,
    ): PayMessage? {
        if (payMessage.target == null || payMessage.payMoney <= 0 || payMessage.assetsName.isEmpty()) {
            return null
        }

        try {
            payMessage.time = DateUtil.yyyy_MM_dd_HH_mm.parse(getDateTime(payMessage.content))
        } catch (e: Exception) {
        }

        savedPayMessage?.let {
            if (MogaUtil.isSameTarget(payMessage.target!!, it.target)
                && payMessage.payMoney == it.payMoney
                && payMessage.time.time.minutes() == it.time.time.minutes()
            ) {
                return null
            }
        }

        if (payMessage.assetsName.length > 10) {
            payMessage.assetsName = payMessage.assetsName.substring(0, 9)
        }

        var success = false
        withContext(Dispatchers.IO) {
            appDatabase?.let { appDatabase ->
                val assetsList = appDatabase.assetsDao().getAssets()
                var assets =
                    assetsList.find { assets -> assets.name == payMessage.assetsName }
                if (assets == null) {
                    assets =
                        AssetsEntity(moimId = appPreference.moimId, name = payMessage.assetsName)
                    appDatabase.assetsDao().insert(assets)
                }

                if (payMessage.category == null) {
                    payMessage.category = getCategory(payMessage.storeName, payMessage.target)
                }

                if (payMessage.category != null) {
                    val me = appDatabase.userDao().getUser(appPreference.userId)
                    val accountItem = AccountItemEntity(moimId = appPreference.moimId,
                        "${appPreference.moimId}${appPreference.moimUserIndex}${appPreference.accountItemIndex}".toLong()).apply {
                        this.target = payMessage.target!!
                        this.date = payMessage.time
                        this.user = me
                        this.amount = payMessage.payMoney
                        this.assets = assets
                        this.category = payMessage.category
                        this.content = payMessage.storeName
                        this.memo = payMessage.content
                        this.ownerId = appPreference.userId
                        this.share = appPreference.notiShare
                        this.type = payMessage.type
                    }

                    success = true
                    appDatabase.accountItemDao().insert(accountItem)
                    appPreference.saveAccountItemIndex(appPreference.accountItemIndex + 1)

                    if (appPreference.notiIcon) {
                        showNotification(context, payMessage)
                    }

                    if (accountItem.share) {
                        Dispatchers.IO.launchBackground {
                            messageRepository.saveAccountItem(accountItem)
                        }
                    }
                }
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "입력완료 \n" +
                        "카드: ${payMessage.assetsName}\n" +
                        "상태: ${payMessage.target}\n" +
                        "금액: ${payMessage.payMoney}\n" +
                        "상호: ${payMessage.storeName}\n" +
                        "시간: ${DateUtil.yyyy_MM_dd_HH_mm.format(payMessage.time)}\n" +
                        "내용: ${payMessage.content}\n")
            }
        }
        return if (success) payMessage else null
    }

    private fun Long.minutes() = this / 1000 / 60

    private suspend fun getCategory(storeName: String, target: Target?) =
        appDatabase?.let { appDatabase ->
            appDatabase.moimDao().getMoim(appPreference.moimId)?.let { moim ->
                val etcCategoryName = "기타"

                val categoryName = try {
                    val result = messageRepository.searchKeyword(storeName)
                    result.documents.firstOrNull()?.categoryName ?: etcCategoryName
                } catch (e: Exception) {
                    etcCategoryName
                }

                var etcCategory: CategoryEntity? = null
                moim.categoryList.filter { MogaUtil.isSameTarget(it.target, target) }
                    .find { category ->
                        if (category.name == etcCategoryName) {
                            etcCategory = category
                        }

                        !category.tag.split("[/,]".toRegex()).findLast { subCategory ->
                            categoryName.contains(subCategory)
                        }.isNullOrEmpty()
                    } ?: etcCategory
            }
        }

    private fun showNotification(context: Context, payMessage: PayMessage) {
        createNotificationChannel(context, NotificationManagerCompat.IMPORTANCE_DEFAULT, false,
            context.getString(R.string.app_name), "App notification channel")

        val channelId = "${context.packageName}-${context.getString(R.string.app_name)}"
        val title = when (payMessage.target) {
            Target.INCOME -> "수입"
            Target.TRANSFER -> "이체"
            else -> "지출"
        }
        val content =
            "${payMessage.assetsName} ${payMessage.payMoney.commaWon()} ${payMessage.storeName}"

        val intent = Intent(context, StartActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            putExtra("tab", "note")
        }

        val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_MUTABLE)
        } else {
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val builder = NotificationCompat.Builder(context, channelId)
        builder.setSmallIcon(R.drawable.ic_stat_moga)
        builder.setContentTitle(title)
        builder.setContentText(content)
        builder.priority = NotificationCompat.PRIORITY_DEFAULT
        builder.setAutoCancel(true)
        builder.setContentIntent(pendingIntent)

        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(1, builder.build())
    }

    private fun createNotificationChannel(
        context: Context, importance: Int, showBadge: Boolean,
        name: String, description: String,
    ) {
        val channelId = "${context.packageName}-$name"
        val channel = NotificationChannel(channelId, name, importance)
        channel.description = description
        channel.setShowBadge(showBadge)

        val notificationManager = context.getSystemService(NotificationManager::class.java)
        notificationManager.createNotificationChannel(channel)
    }

    fun insertNotParsingNotiInfo(payMessage: PayMessage) {
        payMessage.assetsName = "현금"
        payMessage.target = Target.EXPEND

        GlobalScope.launch(Dispatchers.IO) {
            appDatabase?.let { appDatabase ->
                val assetsList = appDatabase.assetsDao().getAssets()
                var assets = assetsList.find { assets -> assets.name == payMessage.assetsName }
                if (assets == null) {
                    assets =
                        AssetsEntity(moimId = appPreference.moimId, name = payMessage.assetsName)
                    appDatabase.assetsDao().insert(assets)
                }

                payMessage.category =
                    getCategory(payMessage.storeName, payMessage.target) ?: return@launch

                val me = appDatabase.userDao().getUser(appPreference.userId)
                val accountItem = AccountItemEntity(moimId = appPreference.moimId,
                    "${appPreference.moimId}${appPreference.moimUserIndex}${appPreference.accountItemIndex}".toLong()).apply {
                    this.target = payMessage.target!!
                    this.date = DateUtil.todayDate
                    this.user = me
                    this.amount = 1
                    this.assets = assets
                    this.category = payMessage.category
                    this.content = ""
                    this.memo = payMessage.content
                }

                appDatabase.accountItemDao().insert(accountItem)

                appPreference.saveAccountItemIndex(appPreference.accountItemIndex + 1)
            }
        }
    }

    companion object {
        private val CARD_PATTERN = Pattern.compile("(.+)(승인|취소|체크|신용)")
        private val BANK_PATTERN = Pattern.compile("출금|지급|입금|입출금")
        private val TRANSACTION_PATTERN = Pattern.compile("지급|입금|출금")
        private val MONEY_PATTERN = Pattern.compile("([\\d,]+원)")
        private val TOTAL_MONEY_PATTERN =
            Pattern.compile("(?:누적|잔여|잔액|총|출금가능금액(?: :)?)\\s?(?:[\\d,]+원?)")
        private val DAY_PATTERN = Pattern.compile("\\d{2}[/.]\\d{2}\\s")
        private val TIME_PATTERN = Pattern.compile("\\d{2}:\\d{2}")
    }
}