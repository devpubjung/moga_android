package com.devpub.moga.helper

import android.app.Application
import android.content.Context
import com.devpub.moga.R
import com.kakao.sdk.auth.model.OAuthToken
import com.kakao.sdk.common.KakaoSdk
import com.kakao.sdk.user.UserApiClient
import com.kakao.sdk.user.model.AccessTokenInfo
import com.kakao.sdk.user.model.User

class KakaoHelper {

    fun init(application: Application) {
        // Kakao SDK 초기화
        KakaoSdk.init(application, application.getString(R.string.kakao_app_id))
    }

    fun login(context: Context, block: (OAuthToken?, Throwable?) -> Unit ) {
        // 카카오톡이 설치되어 있으면 카카오톡으로 로그인, 아니면 카카오계정으로 로그인
        if (UserApiClient.instance.isKakaoTalkLoginAvailable(context)) {
            UserApiClient.instance.loginWithKakaoTalk(context, callback = block)
        } else {
            UserApiClient.instance.loginWithKakaoAccount(context, callback = block)
        }
    }

    fun logout(block: (Throwable?) -> Unit) {
        UserApiClient.instance.logout(callback = block)
    }

    fun getTokenInfo(block: (AccessTokenInfo?, Throwable?) -> Unit) {
        UserApiClient.instance.accessTokenInfo(block)
    }

    fun getUserInfo(block: (User?, Throwable?) -> Unit) {
        UserApiClient.instance.me(callback = block)
    }

}