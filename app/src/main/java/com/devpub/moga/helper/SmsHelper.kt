package com.devpub.moga.helper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.SmsMessage
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.data.model.SmsRequest
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.data.repository.MessageRepositoryImpl
import com.devpub.moga.domain.model.NoteType
import com.devpub.moga.domain.repository.MessageRepository
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.PermissionUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class SmsHelper : BroadcastReceiver() {

    private val messageRepository: MessageRepository by lazy { MessageRepositoryImpl() }
    private var savedPayMessage: PayMessage? = null

    override fun onReceive(context: Context, intent: Intent) {
        if (PermissionUtil.isNotiPermissionAllowed(context)) {
            return
        }
        intent.extras?.let { bundle ->
            val messages = parseSmsMessage(bundle)

            if (messages.isNotEmpty()) {
                messages[0]?.let {
                    val sender = it.originatingAddress
                    val content = it.messageBody.toString()
                    val date = Date(it.timestampMillis)

                    val appPreference = AppPreferenceImpl(context)
                    val messageHelper = MessageHelper(context, messageRepository)
                    GlobalScope.launch(Dispatchers.IO) {
                        try {
                            val resultSmsParsing =
                                messageRepository.parseSms(SmsRequest(sender!!,
                                    content,
                                    DateUtil.getServerDateString(date),
                                    appPreference.moimId,
                                    appPreference.accountItemIndex))
                            if (resultSmsParsing.data != null) {
                                resultSmsParsing.data.type = NoteType.SMS
                                messageHelper.insertData(resultSmsParsing.data,
                                    savedPayMessage)?.also { result ->
                                    savedPayMessage = result
                                }
                            } else {
                                messageHelper.parseSmsContentMessage(content)?.let { payMessage ->
                                    payMessage.type = NoteType.SMS
                                    messageHelper.insertData(payMessage,
                                        savedPayMessage)?.also { result ->
                                        savedPayMessage = result
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            messageHelper.parseSmsContentMessage(content)?.let { payMessage ->
                                payMessage.type = NoteType.SMS
                                messageHelper.insertData(payMessage,
                                    savedPayMessage)?.also { result ->
                                    savedPayMessage = result
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun parseSmsMessage(bundle: Bundle): Array<SmsMessage?> {
        // PDU: Protocol Data Units
        val objs = bundle["pdus"] as Array<*>
        val messages: Array<SmsMessage?> = arrayOfNulls(objs.size)
        for (i in objs.indices) {
            val format: String? = bundle.getString("format")
            messages[i] = SmsMessage.createFromPdu(objs[i] as ByteArray, format)
        }
        return messages
    }

}