package com.devpub.moga.helper

import android.app.Notification
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import com.devpub.common.util.TAG
import com.devpub.moga.BuildConfig
import com.devpub.moga.domain.model.NoteType
import com.devpub.moga.data.model.NotiRequest
import com.devpub.moga.data.model.PayMessage
import com.devpub.moga.domain.model.Target
import com.devpub.moga.data.preference.AppPreferenceImpl
import com.devpub.moga.data.repository.MessageRepositoryImpl
import com.devpub.moga.domain.repository.MessageRepository
import com.devpub.moga.util.DateUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import java.util.regex.Pattern


class NotificationHelper : NotificationListenerService() {

    private val messageRepository: MessageRepository by lazy { MessageRepositoryImpl() }
    private var savedPayMessage: PayMessage? = null

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        super.onNotificationPosted(sbn)
        try {
            if (ignore(sbn.packageName)) return
            val appPreference = AppPreferenceImpl(applicationContext)

            if (!appPreference.notiAutoInput) return

            val extras = sbn.notification.extras
            val title = extras.getString(Notification.EXTRA_TITLE)
            val text = extras.getCharSequence(Notification.EXTRA_TEXT)?.toString()
            val subText = extras.getCharSequence(Notification.EXTRA_SUB_TEXT)?.toString()

            if (title != null && text != null) {
                if (text.length > 180) {
                    return
                }
                CoroutineScope(Dispatchers.IO).launch {
                    val messageHelper = MessageHelper(applicationContext, messageRepository)
                    try {
                        val isPayMessage =
                            messageHelper.getMoney(title) > 0 || messageHelper.getMoney(text) > 0
                        if (isPayMessage) {
                            val resultParsing =
                                messageRepository.parseNoti(NotiRequest(
                                    sbn.packageName,
                                    title,
                                    text,
                                    subText,
                                    DateUtil.getServerDateString(Date(sbn.postTime)),
                                    appPreference.moimId,
                                    appPreference.accountItemIndex))
                            if (resultParsing.data != null) {
                                resultParsing.data.type = NoteType.NOTI
                                messageHelper.insertData(resultParsing.data, savedPayMessage)?.also {
                                    savedPayMessage = it
                                }
                            } else {
                                parseNotification(messageHelper,
                                    sbn.packageName,
                                    title,
                                    text,
                                    Date(sbn.postTime))?.let { payMessage ->
                                    payMessage.type = NoteType.NOTI
                                    messageHelper.insertData(payMessage, savedPayMessage)?.also {
                                        savedPayMessage = it
                                    }
                                }
                            }
                        }
                    } catch (e: Exception) {
                        parseNotification(messageHelper,
                            sbn.packageName,
                            title,
                            text,
                            Date(sbn.postTime))?.let { payMessage ->
                            payMessage.type = NoteType.NOTI
                            messageHelper.insertData(payMessage, savedPayMessage)?.also {
                                savedPayMessage = it
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    private fun ignore(packageName: String): Boolean {
        return listOf(applicationContext.packageName).contains(packageName)
    }

    private fun parseNotification(
        messageHelper: MessageHelper,
        packageName: String,
        title: String,
        text: String,
        date: Date,
    ): PayMessage? {
        val isCard = messageHelper.isCard(title) || messageHelper.isCard(text)
        val isBank = messageHelper.isBank(title) || messageHelper.isBank(text)
        if (isCard || isBank) {
            val payMessage = PayMessage(date)
            val content = messageHelper.getExceptedTotalMoneyText(text)
            payMessage.content = content.trim()
            payMessage.payMoney = messageHelper.getMoney(content)
            payMessage.target = messageHelper.getTransaction(content)
            payMessage.isBank = messageHelper.isBank(title) || messageHelper.isBank(content)

            when {
                packageName.contains("com.shinhan") -> {
                    /**
                    packageName: com.shinhan.sbanking
                    title: 입금
                    text : 1원 입출금(6524) 06.27 15:04
                    정효찬 잔액 1,075,380원
                    subText: 입출금 알림
                     */
                    payMessage.assetsName = "신한은행"
                    payMessage.target = if (title.contains("입금")) {
                        Target.INCOME
                    } else {
                        Target.TRANSFER
                    }
                }
                packageName.contains("com.kakaobank") -> {
                    /**
                    packageName: com.kakaobank.channel
                    title: 입출금내역 안내
                    text : 정*찬(4516) 06/27 15:04 출금 1원 정효찬 잔액 963,061원
                    subText: null
                     */
                    payMessage.assetsName = "카카오뱅크"
                }
                packageName.contains("com.kbankwith") -> {
                    /**
                    packageName: com.kbankwith.smartbank
                    title: 케이뱅크
                    text : 출금 1원
                    정효찬 | 듀얼K입출금통장(0088)
                    잔액 765,756원
                    subText: null
                     */
                    payMessage.assetsName = "케이뱅크"
                    val storeMatcher = Pattern.compile("\\S+\\s\\|").matcher(content)
                    if (storeMatcher.find()) {
                        val storeName = storeMatcher.group()
                        payMessage.storeName = storeName.substring(0, storeName.length - 1).trim()
                    }
                }
                packageName.contains("viva.republica") -> {
                    /**
                    packageName: viva.republica.toss
                    title: 1원 출금
                    text : 내 토스증권 계좌 → 정효찬님
                    subText: null
                     */
                    payMessage.assetsName = "토스"
                    payMessage.payMoney = messageHelper.getMoney(title)
                    payMessage.target = messageHelper.getTransaction(title)
                }
                packageName.contains("com.IBK") -> {
                    /**
                    packageName: com.IBK.SmartPush.app
                    title: 입출금내역 알림
                    text : [입금] 1원 정효찬
                    021-******-01-015
                    06/27 17:21
                    subText: null
                     */
                    payMessage.assetsName = "기업은행"
                }
                packageName.contains("com.kbstar") -> {
                    /**
                    packageName: com.kbstar.starpush
                    title: KB스타알림
                    text : 06/27 17:21 758602-**-***648 기업정효찬 스마트폰출금 1 잔액781,716
                    subText: null
                     */
                    payMessage.assetsName = "KB국민은행"
                    val moneyMatcher = Pattern.compile("(\\s[\\d,]+\\s)").matcher(content)
                    if (moneyMatcher.find()) {
                        payMessage.payMoney =
                            messageHelper.getRegexString(moneyMatcher.group(), "\\d").toLong()
                    }

                    payMessage.storeName = messageHelper.getStoreName(false,
                        payMessage.assetsName,
                        content,
                        "|([\\d,]+)")
                }
                else -> {
                    messageHelper.parseSmsContentMessage(text, date)?.apply {
                        payMessage.assetsName = assetsName
                        payMessage.content = content
                        payMessage.isBank = isBank
                        payMessage.payMoney = payMoney
                        payMessage.storeName = storeName
                        payMessage.target = target
                    }
                }
            }
            if (payMessage.assetsName.isNotEmpty() && payMessage.storeName.isEmpty()) {
                payMessage.storeName =
                    messageHelper.getStoreName(isCard, payMessage.assetsName, content)
            }

            if (BuildConfig.DEBUG) {
                if (payMessage.target == null || payMessage.payMoney <= 0 || payMessage.assetsName.isEmpty()) {
                    if (!packageName.contains("com.kakao")) {
                        payMessage.content = "Noti Info ->\n" +
                                "packageName: $packageName\n" +
                                "title: $title\n" +
                                "text : $text"
                        messageHelper.insertNotParsingNotiInfo(payMessage)
                    }
                }
            }

            return payMessage
        }
        return null
    }
}