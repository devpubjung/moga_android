package com.devpub.moga

import android.app.Application
import android.util.Base64
import android.util.Log
import com.devpub.moga.helper.KakaoHelper
import com.kakao.sdk.common.util.Utility
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class RootApplication : Application() {

    @Inject lateinit var kakaoHelper: KakaoHelper

    override fun onCreate() {
        super.onCreate()
        kakaoHelper.init(this)
        if (BuildConfig.DEBUG) {
            Log.d("hyo", "keyHash:${Utility.getKeyHash(this)}")
            getGoogleKeyHash()
        }
    }

    private fun getGoogleKeyHash() {
        val sha1 = byteArrayOf(0xD8.toByte(),
            0x67.toByte(),
            0xCE.toByte(),
            0x0F.toByte(),
            0x18.toByte(),
            0xC1.toByte(),
            0xBF.toByte(),
            0x3C.toByte(),
            0x35.toByte(),
            0x2E.toByte(),
            0x56.toByte(),
            0xAD.toByte(),
            0x54.toByte(),
            0x92.toByte(),
            0x50.toByte(),
            0xBC.toByte(),
            0x07.toByte(),
            0xE7.toByte(),
            0x8C.toByte(),
            0xF2.toByte())

        Log.d("hyo", "GoogleKeyHash:" + Base64.encodeToString(sha1, Base64.NO_WRAP))
    }
}