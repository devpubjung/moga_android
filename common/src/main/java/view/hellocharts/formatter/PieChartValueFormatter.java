package view.hellocharts.formatter;

import view.hellocharts.model.SliceValue;

public interface PieChartValueFormatter {

    public int formatChartValue(char[] formattedValue, SliceValue value);

    public int formatChartOnlyValue(char[] formattedValue, SliceValue value);

    public int formatChartMaxValue();
}
