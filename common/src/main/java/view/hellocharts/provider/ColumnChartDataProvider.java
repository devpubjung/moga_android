package view.hellocharts.provider;

import view.hellocharts.model.ColumnChartData;

public interface ColumnChartDataProvider {

    public ColumnChartData getColumnChartData();

    public void setColumnChartData(ColumnChartData data);

}
