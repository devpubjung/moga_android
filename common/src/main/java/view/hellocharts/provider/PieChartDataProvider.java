package view.hellocharts.provider;

import view.hellocharts.model.PieChartData;

public interface PieChartDataProvider {

    public PieChartData getPieChartData();

    public void setPieChartData(PieChartData data);

}
