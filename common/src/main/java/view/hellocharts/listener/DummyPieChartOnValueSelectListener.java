package view.hellocharts.listener;


import view.hellocharts.model.SliceValue;

public class DummyPieChartOnValueSelectListener implements PieChartOnValueSelectListener {

    @Override
    public void onValueSelected(int arcIndex, SliceValue value) {

    }

    @Override
    public void onValueDeselected() {

    }
}
