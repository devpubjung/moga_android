package view.hellocharts.model;

public enum ValueShape {
    CIRCLE, SQUARE, DIAMOND
}
