package com.devpub.common.mvvm

import androidx.annotation.MainThread
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider
import com.devpub.common.binidng.BindingActivity
import com.devpub.common.widget.loading.CatLoadingView

abstract class BaseActivity<VB : ViewDataBinding> : BindingActivity<VB>() {

    private val loadingView by lazy { CatLoadingView() }

    fun showLoading(cancelable: Boolean = false) {
        loadingView.setClickCancelable(cancelable)
        loadingView.show(supportFragmentManager, "Activity")
    }

    fun hideLoading() {
        if (loadingView.showsDialog) {
            loadingView.dismiss()
        }
    }

    protected fun setLoading(show: Boolean) {
        if (show) {
            showLoading()
        } else {
            hideLoading()
        }
    }

    @MainThread
    protected inline fun <reified VM : BaseViewModel> viewModels(
        noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null,
    ): Lazy<VM> {
        val factoryPromise = factoryProducer ?: {
            defaultViewModelProviderFactory
        }

        return ViewModelLazy(VM::class, { viewModelStore }, factoryPromise)
            .also {
//                it.value.isLoading.observe { show ->
//                    if (show) {
//                        showLoading()
//                    } else {
//                        hideLoading()
//                    }
//                }
            }
    }
}