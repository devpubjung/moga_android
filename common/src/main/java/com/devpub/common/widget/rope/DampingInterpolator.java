package com.devpub.common.widget.rope;

import android.view.animation.Interpolator;

class DampingInterpolator implements Interpolator {
    private final float mCycles;

    public DampingInterpolator() {
        this(1.0F);
    }

    public DampingInterpolator(float cycles) {
        this.mCycles = cycles;
    }

    public float getInterpolation(float input) {
        return (float)(Math.sin((double)(this.mCycles * 2.0F) * 3.141592653589793D * (double)input) * (double)((input - 1.0F) * (input - 1.0F)));
    }
}
