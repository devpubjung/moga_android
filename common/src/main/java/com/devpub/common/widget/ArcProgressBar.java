package com.devpub.common.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.devpub.common.R;

import java.text.NumberFormat;
import java.util.Locale;

public class ArcProgressBar extends View {

    private final int defaultTextSize = dipToPx(13);
    private final String defaultColor = "#676767";
    private final String defaultBackColor = "#111111";
    private float defaultContentSize = dipToPx(60);

    private int mWidth;
    private int mHeight;
    private int diameter = 500;
    private float centerX;
    private float centerY;

    private Paint allArcPaint;
    private Paint progressPaint;
    private Paint degreePaint;
    private Paint contentPaint;
    private Paint titlePaint;
    private Paint unitPaint;
    private Paint goalPaint;
    private Paint usePaint;

    private RectF bgRect;

    private ValueAnimator progressAnimator;
    private PaintFlagsDrawFilter mDrawFilter;
    private SweepGradient sweepGradient;
    private Matrix rotateMatrix;

    private float startAngle = 135;
    private float sweepAngle = 270;
    private float currentAngle = 0;
    private float lastAngle;
    private int[] colors = new int[]{Color.GREEN, Color.YELLOW, Color.RED, Color.RED};
    private long maxValues = 60;
    private long curValues = 0;
    private float bgArcWidth = dipToPx(2);
    private float progressWidth = dipToPx(10);
    private int aniSpeed = 1000;
    private float longdegree = defaultTextSize;
    private float shortdegree = dipToPx(5);
    private final int DEGREE_PROGRESS_DISTANCE = dipToPx(8);

    private String longDegreeColor = defaultBackColor;
    private String shortDegreeColor = defaultBackColor;
    private int bgArcColor = Color.parseColor(defaultBackColor);

    private String titleString;
    private float titleSize = defaultTextSize;
    private int titleColor = Color.parseColor(defaultColor);

    private float contentSize = defaultContentSize;
    private int contentColor = Color.BLACK;

    private String unitString;
    private float unitSize = dipToPx(15);
    private int unitColor = Color.parseColor(defaultColor);

    private String goalString = "";
    private float goalSize = dipToPx(15);
    private int goalColor = Color.parseColor(defaultColor);

    private String useString = "";
    private float useSize = goalSize;
    private int useColor = goalColor;

    private boolean showDial;
    private boolean showContent;

    private float k;

    public ArcProgressBar(Context context) {
        super(context, null);
        initView();
    }

    public ArcProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        initConfig(context, attrs);
        initView();
    }

    public ArcProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initConfig(context, attrs);
        initView();
    }

    private void initConfig(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ColorArcProgressBar);
        int color1 = a.getColor(R.styleable.ColorArcProgressBar_front_color1, Color.GREEN);
        int color2 = a.getColor(R.styleable.ColorArcProgressBar_front_color2, color1);
        int color3 = a.getColor(R.styleable.ColorArcProgressBar_front_color3, color1);
        colors = new int[]{color1, color2, color3, color3};

        sweepAngle = a.getInteger(R.styleable.ColorArcProgressBar_total_engle, 270);
        bgArcColor = a.getColor(R.styleable.ColorArcProgressBar_back_color, bgArcColor);
        bgArcWidth = a.getDimension(R.styleable.ColorArcProgressBar_back_width, dipToPx(2));
        progressWidth = a.getDimension(R.styleable.ColorArcProgressBar_front_width, dipToPx(10));
        showContent = a.getBoolean(R.styleable.ColorArcProgressBar_show_content, false);
        showDial = a.getBoolean(R.styleable.ColorArcProgressBar_show_dial, false);
        contentSize = a.getDimension(R.styleable.ColorArcProgressBar_content_size, contentSize);
        contentColor = a.getColor(R.styleable.ColorArcProgressBar_content_color, contentColor);
        titleString = a.getString(R.styleable.ColorArcProgressBar_title);
        titleSize = a.getDimension(R.styleable.ColorArcProgressBar_title_size, defaultTextSize);
        titleColor = a.getColor(R.styleable.ColorArcProgressBar_title_color, titleColor);
        unitString = a.getString(R.styleable.ColorArcProgressBar_unit);
        unitSize = a.getDimension(R.styleable.ColorArcProgressBar_unit_size, unitSize);
        unitColor = a.getColor(R.styleable.ColorArcProgressBar_unit_color, unitColor);
        goalString = a.getString(R.styleable.ColorArcProgressBar_description);
        goalSize = a.getDimension(R.styleable.ColorArcProgressBar_description_size, goalSize);
        goalColor = a.getColor(R.styleable.ColorArcProgressBar_description_color, goalColor);
        useSize = a.getDimension(R.styleable.ColorArcProgressBar_description_size, useSize);
        useColor = a.getColor(R.styleable.ColorArcProgressBar_description_color, useColor);

        curValues = a.getInt(R.styleable.ColorArcProgressBar_current_value, 0);
        maxValues = a.getInt(R.styleable.ColorArcProgressBar_max_value, 60);
        setCurrentValues(curValues);
        setMaxValues(maxValues);
        a.recycle();

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = (int) (2 * longdegree + progressWidth + diameter + 2 * DEGREE_PROGRESS_DISTANCE);
        int height = (int) (2 * longdegree + progressWidth + diameter + 2 * DEGREE_PROGRESS_DISTANCE);
        setMeasuredDimension(width, height);
    }

    private void initView() {

        diameter = 3 * getScreenWidth() / 5;
        bgRect = new RectF();
        bgRect.top = longdegree + progressWidth / 2 + DEGREE_PROGRESS_DISTANCE;
        bgRect.left = longdegree + progressWidth / 2 + DEGREE_PROGRESS_DISTANCE;
        bgRect.right = diameter + (longdegree + progressWidth / 2 + DEGREE_PROGRESS_DISTANCE);
        bgRect.bottom = diameter + (longdegree + progressWidth / 2 + DEGREE_PROGRESS_DISTANCE);

        centerX = (2 * longdegree + progressWidth + diameter + 2 * DEGREE_PROGRESS_DISTANCE) / 2;
        centerY = (2 * longdegree + progressWidth + diameter + 2 * DEGREE_PROGRESS_DISTANCE) / 2;

        degreePaint = new Paint();
        degreePaint.setColor(Color.parseColor(longDegreeColor));

        allArcPaint = new Paint();
        allArcPaint.setAntiAlias(true);
        allArcPaint.setStyle(Paint.Style.STROKE);
        allArcPaint.setStrokeWidth(bgArcWidth);
        allArcPaint.setColor(bgArcColor);
        allArcPaint.setStrokeCap(Paint.Cap.ROUND);

        progressPaint = new Paint();
        progressPaint.setAntiAlias(true);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setStrokeCap(Paint.Cap.ROUND);
        progressPaint.setStrokeWidth(progressWidth);
        progressPaint.setColor(Color.GREEN);

        contentPaint = new Paint();
        contentPaint.setTextSize(contentSize);
        contentPaint.setColor(contentColor);
        contentPaint.setTextAlign(Paint.Align.CENTER);

        titlePaint = new Paint();
        titlePaint.setTextSize(titleSize);
        titlePaint.setColor(titleColor);
        titlePaint.setTextAlign(Paint.Align.CENTER);

        unitPaint = new Paint();
        unitPaint.setTextSize(unitSize);
        unitPaint.setColor(unitColor);
        unitPaint.setTextAlign(Paint.Align.CENTER);

        goalPaint = new Paint();
        goalPaint.setTextSize(goalSize);
        goalPaint.setColor(goalColor);
        goalPaint.setTextAlign(Paint.Align.CENTER);

        usePaint = new Paint();
        usePaint.setTextSize(useSize);
        usePaint.setColor(useColor);
        usePaint.setTextAlign(Paint.Align.CENTER);

        mDrawFilter = new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        sweepGradient = new SweepGradient(centerX, centerY, colors, null);
        rotateMatrix = new Matrix();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.setDrawFilter(mDrawFilter);

        if (showDial) {
            for (int i = 0; i < 40; i++) {
                if (i > 15 && i < 25) {
                    canvas.rotate(9, centerX, centerY);
                    continue;
                }
                float startY = centerY - ((float) diameter / 2) - progressWidth / 2 - DEGREE_PROGRESS_DISTANCE;
                if (i % 5 == 0) {
                    degreePaint.setStrokeWidth(dipToPx(2));
                    degreePaint.setColor(Color.parseColor(longDegreeColor));
                    canvas.drawLine(centerX, startY,
                            centerX, startY - longdegree, degreePaint);
                } else {
                    degreePaint.setStrokeWidth(dipToPx(1.4f));
                    degreePaint.setColor(Color.parseColor(shortDegreeColor));
                    canvas.drawLine(centerX, startY - (longdegree - shortdegree) / 2,
                            centerX, startY - (longdegree - shortdegree) / 2 - shortdegree, degreePaint);
                }

                canvas.rotate(9, centerX, centerY);
            }
        }

        canvas.drawArc(bgRect, startAngle, sweepAngle, false, allArcPaint);

        rotateMatrix.setRotate(130, centerX, centerY);
        sweepGradient.setLocalMatrix(rotateMatrix);
        progressPaint.setShader(sweepGradient);

        canvas.drawArc(bgRect, startAngle, currentAngle, false, progressPaint);

        if (showContent) {
            canvas.drawText(NumberFormat.getInstance(Locale.KOREA).format(curValues), centerX, centerY + contentSize / 3, contentPaint);
        }
        if (!titleString.isEmpty()) {
            canvas.drawText(titleString, centerX, centerY - 2 * defaultContentSize / 3, titlePaint);
        }
        if (!unitString.isEmpty()) {
            canvas.drawText(unitString, centerX, centerY + 2 * defaultContentSize / 3, unitPaint);
        }
        if (goalString != null && !goalString.isEmpty()) {
            canvas.drawText("주간 예산 : " + goalString, centerX, centerY + 4.8F * defaultContentSize / 3, goalPaint);
        }
        if (useString != null && !useString.isEmpty()) {
            canvas.drawText(useString + " 지출", centerX, centerY + 4.8F * defaultContentSize / 3 + (goalSize * 1.5f), goalPaint);
        }

        invalidate();

    }

    public void setGoal(String goal) {
        this.goalString = goal;
    }

    public void setUse(String use) {
        this.useString = use;
    }

    public void setMaxValues(long maxValues) {
        this.maxValues = maxValues;
        k = sweepAngle / maxValues;
    }

    public void setCurrentValues(long currentValues) {
        if (currentValues > maxValues) {
            currentValues = maxValues;
        }
        if (currentValues < 0) {
            currentValues = 0;
        }
        this.curValues = currentValues;
        lastAngle = currentAngle;
        setAnimation(lastAngle, currentValues * k, aniSpeed);
    }

    public void setBgArcWidth(int bgArcWidth) {
        this.bgArcWidth = bgArcWidth;
    }

    public void setProgressWidth(int progressWidth) {
        this.progressWidth = progressWidth;
    }

    public void setContentSize(int contentSize) {
        this.contentSize = contentSize;
    }

    public void setHintSize(int hintSize) {
        this.unitSize = hintSize;
    }

    public void setUnitSize(String hintString) {
        this.unitString = hintString;
        invalidate();
    }

    public void setDiameter(int diameter) {
        this.diameter = dipToPx(diameter);
    }

    public void setTitle(String title) {
        this.titleString = title;
    }

    private void setIsNeedDial(boolean isNeedDial) {
        this.showDial = isNeedDial;
    }

    private void setAnimation(float last, float current, int length) {
        progressAnimator = ValueAnimator.ofFloat(last, current);
        progressAnimator.setDuration(length);
        progressAnimator.setTarget(currentAngle);
        progressAnimator.addUpdateListener(animation -> {
            currentAngle = (float) animation.getAnimatedValue();
            curValues = (int)Math.round(currentAngle / k);
        });
        progressAnimator.start();
    }

    private int dipToPx(float dip) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f * (dip >= 0 ? 1 : -1));
    }

    private int getScreenWidth() {
        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }
}