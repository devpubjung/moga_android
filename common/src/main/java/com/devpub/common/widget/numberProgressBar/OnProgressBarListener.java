package com.devpub.common.widget.numberProgressBar;

public interface OnProgressBarListener {

    void onProgressChange(int current, int max);
}