package com.devpub.common.widget.rope;

public interface ProgressFormatter {
    String getFormattedText(int var1, int var2);
}
