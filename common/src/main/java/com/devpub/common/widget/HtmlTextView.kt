package com.devpub.common.widget

import android.annotation.SuppressLint
import android.content.Context
import android.text.Html
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.devpub.common.R

@SuppressLint("NewApi")
class HtmlTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.HtmlTextView)

            typedArray.getString(R.styleable.HtmlTextView_html)?.apply {
                text = Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
            }

            typedArray.recycle()
        }
    }
}