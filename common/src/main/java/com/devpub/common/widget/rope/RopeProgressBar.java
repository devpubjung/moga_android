package com.devpub.common.widget.rope;


import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path.Direction;
import android.os.Looper;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

import com.devpub.common.R;

public class RopeProgressBar extends View {
    private final Paint mBubblePaint;
    private final Paint mLinesPaint;
    private final Paint mTextPaint;
    private final float m1Dip;
    private final float m1Sp;
    private int mProgress;
    private int mMax;
    private int mPrimaryColor;
    private int mSecondaryColor;
    private int mTextColor;
    private float mSlack;
    private boolean mDynamicLayout;
    private ProgressFormatter mFormatter;
    private final Rect mBounds;
    private final Path mBubble;
    private final Path mTriangle;
    private static final Interpolator INTERPOLATOR = new DampingInterpolator(5.0F);
    private ValueAnimator mAnimator;
    private float mBounceX;
    private int mStartProgress;
    private boolean mDeferred;
    private boolean mSlackSetByUser;
    private final Runnable mRequestLayoutRunnable;

    public RopeProgressBar(Context context) {
        this(context, (AttributeSet)null);
    }

    public RopeProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressLint("WrongConstant")
    public RopeProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mBubblePaint = new Paint(1);
        this.mLinesPaint = new Paint(1);
        this.mTextPaint = new Paint(1);
        this.mBounds = new Rect();
        this.mBubble = new Path();
        this.mTriangle = new Path();
        this.mRequestLayoutRunnable = () -> RopeProgressBar.this.requestLayout();
        this.m1Dip = this.getResources().getDisplayMetrics().density;
        this.m1Sp = this.getResources().getDisplayMetrics().scaledDensity;
        int max = 0;
        int progress = 0;
        float width = this.dips(8.0F);
        float slack = this.dips(32.0F);
        boolean dynamicLayout = false;
        int primaryColor = -16738680;
        int secondaryColor = -2434342;
        int textColor = -16777216;
        if (VERSION.SDK_INT >= 21) {
            TypedValue out = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorControlActivated, out, true);
            primaryColor = out.data;
            context.getTheme().resolveAttribute(R.attr.colorControlHighlight, out, true);
            secondaryColor = out.data;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RopeProgressBar, defStyleAttr, 0);
        if (a != null) {
            max = a.getInt(R.styleable.RopeProgressBar_ropeMax, max);
            progress = a.getInt(R.styleable.RopeProgressBar_ropeProgress, progress);
            primaryColor = a.getColor(R.styleable.RopeProgressBar_ropePrimaryColor, primaryColor);
            secondaryColor = a.getColor(R.styleable.RopeProgressBar_ropeSecondaryColor, secondaryColor);
            textColor = a.getColor(R.styleable.RopeProgressBar_ropeTextColor, textColor);
            slack = a.getDimension(R.styleable.RopeProgressBar_ropeSlack, slack);
            width = a.getDimension(R.styleable.RopeProgressBar_ropeStrokeWidth, width);
            dynamicLayout = a.getBoolean(R.styleable.RopeProgressBar_ropeDynamicLayout, false);
            this.mSlackSetByUser = a.hasValue(R.styleable.RopeProgressBar_ropeSlack);
            a.recycle();
        }

        this.mPrimaryColor = primaryColor;
        this.mSecondaryColor = secondaryColor;
        this.mTextColor = textColor;
        this.mSlack = slack;
        this.mDynamicLayout = dynamicLayout;
        this.mLinesPaint.setStrokeWidth(width);
        this.mLinesPaint.setStyle(Style.STROKE);
        this.mLinesPaint.setStrokeCap(Cap.ROUND);
        this.mBubblePaint.setColor(mPrimaryColor);
        this.mBubblePaint.setStyle(Style.FILL);
        this.mBubblePaint.setPathEffect(new CornerPathEffect(this.dips(2.0F)));
        this.mTextPaint.setColor(mTextColor);
        this.mTextPaint.setTextSize(this.sp(14));
        this.mTextPaint.setTextAlign(Align.CENTER);
        this.mTextPaint.setTypeface(Typeface.create("sans-serif", 0));
        this.setMax(max);
        this.setProgress(progress);
        this.setLayerType(1, (Paint)null);
    }

    private void dynamicRequestLayout() {
        if (this.mDynamicLayout) {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                this.mRequestLayoutRunnable.run();
            } else {
                this.post(this.mRequestLayoutRunnable);
            }
        }

    }

    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!this.mSlackSetByUser) {
            this.mSlack = (float)MeasureSpec.getSize(widthMeasureSpec) * 0.1F;
        }

        String progress = this.getBubbleText();
        this.mTextPaint.getTextBounds(progress, 0, progress.length(), this.mBounds);
        int bubbleHeight = (int)Math.ceil((double)this.getBubbleVerticalDisplacement());
        float slack = this.mDynamicLayout ? this.getCurrentSlackHeight() : this.getSlack();
        float strokeWidth = this.getStrokeWidth();
        int dh = (int)Math.ceil((double)((float)(this.getPaddingTop() + this.getPaddingBottom()) + strokeWidth + slack));
        this.setMeasuredDimension(getDefaultSize(this.getSuggestedMinimumWidth(), widthMeasureSpec), resolveSizeAndState(dh + bubbleHeight, heightMeasureSpec, 0));
        this.mTriangle.reset();
        this.mTriangle.moveTo(0.0F, 0.0F);
        this.mTriangle.lineTo(this.getTriangleWidth(), 0.0F);
        this.mTriangle.lineTo(this.getTriangleWidth() / 2.0F, this.getTriangleHeight());
        this.mTriangle.lineTo(0.0F, 0.0F);
    }

    protected synchronized void onDraw(Canvas canvas) {
        float radius = this.getStrokeWidth() / 2.0F;
        float bubbleDisplacement = this.getBubbleVerticalDisplacement();
        float top = (float)this.getPaddingTop() + radius + bubbleDisplacement;
        float left = (float)this.getPaddingLeft() + radius;
        float end = (float)(this.getWidth() - this.getPaddingRight()) - radius;
        float max = (float)this.getMax();
        float offset = max == 0.0F ? 0.0F : (float)this.getProgress() / max;
        float slackHeight = this.getCurrentSlackHeight();
        float progressEnd = this.clamp(this.lerp(left, end, offset) + this.mBounceX * this.perp(offset), left, end);
        this.mLinesPaint.setColor(this.mSecondaryColor);
        canvas.drawLine(progressEnd, top + slackHeight, end, top, this.mLinesPaint);
        this.mLinesPaint.setColor(this.mPrimaryColor);
        if (progressEnd == left) {
            this.mLinesPaint.setStyle(Style.FILL);
            canvas.drawCircle(left, top, radius, this.mLinesPaint);
            this.mLinesPaint.setStyle(Style.STROKE);
        } else {
            canvas.drawLine(left, top, progressEnd, top + slackHeight, this.mLinesPaint);
        }

        String progress = this.getBubbleText();
        this.mTextPaint.getTextBounds(progress, 0, progress.length(), this.mBounds);
        float bubbleWidth = this.getBubbleWidth();
        float bubbleHeight = this.getBubbleHeight();

        this.mBubblePaint.setColor(mPrimaryColor);
        float bubbleRound = dips(4);
        float bubbleHalfRound = bubbleRound / 2;
        @SuppressLint("DrawAllocation")
        float[] round = new float[8];
        round[0] = bubbleRound;
        round[1] = bubbleRound;
        round[2] = bubbleRound;
        round[3] = bubbleRound;
        round[4] = bubbleRound;
        round[5] = bubbleRound;
        round[6] = bubbleRound;
        round[7] = bubbleRound;
        float bubbleTop = Math.max(slackHeight, 0.0F);
        float bubbleLeft = this.clamp(progressEnd - bubbleWidth / 2.0F, 0.0F, (float)this.getWidth() - bubbleWidth);
        float triangleValue = progressEnd - this.getTriangleWidth() / 2.0F - bubbleLeft;
        float triangleMax  = (float)this.getWidth() - this.getTriangleWidth();
        if (getProgressPercent() >= 100) {
            round[4] = bubbleHalfRound;
            round[5] = bubbleHalfRound;
        } else if (getProgressPercent() <= 0) {
            round[6] = bubbleHalfRound;
            round[7] = bubbleHalfRound;
        }
        this.mBubble.reset();
        this. mBubble.addRoundRect(0.0F, 0.0F, bubbleWidth, bubbleHeight, round, Direction.CW);
        int saveCount = canvas.save();
        canvas.translate(bubbleLeft, bubbleTop);
        canvas.drawPath(this.mBubble, this.mBubblePaint);
        float triangleLeft = this.clamp(triangleValue , 0.0F, triangleMax);
        this.mTriangle.offset(triangleLeft, bubbleHeight);
        canvas.drawPath(this.mTriangle, this.mBubblePaint);
        this.mTriangle.offset(-triangleLeft, -bubbleHeight);
        float textX = bubbleWidth / 2.0F;
        float textY = bubbleHeight - this.dips(8.0F);
        canvas.drawText(progress, textX, textY, this.mTextPaint);
        canvas.restoreToCount(saveCount);
    }

    private float getCurrentSlackHeight() {
        float max = (float)this.getMax();
        float offset = max == 0.0F ? 0.0F : (float)this.getProgress() / max;
        return this.getSlack() * this.perp(offset);
    }

    private float getBubbleVerticalDisplacement() {
        return this.getBubbleMargin() + this.getBubbleHeight() + this.getTriangleHeight();
    }

    public float getBubbleMargin() {
        return this.dips(4.0F);
    }

    public float getBubbleWidth() {
        return (float)this.mBounds.width() + this.dips(16.0F);
    }

    public float getBubbleHeight() {
        return (float)this.mBounds.height() + this.dips(16.0F);
    }

    public float getTriangleWidth() {
        if (getProgressPercent() <= 0){
            return this.dips(7.0F);
        } else if (getProgressPercent() >= 100) {
            return this.dips(7.0F);
        } else {
            return this.dips(10.0F);
        }
    }

    public float getTriangleHeight() {
        return this.dips(7.0F);
    }

    public String getBubbleText() {
        if (this.mFormatter != null) {
            return this.mFormatter.getFormattedText(this.getProgress(), this.getMax());
        } else {
            int progress = getProgressPercent();
            return progress + "%";
        }
    }

    private int getProgressPercent() {
        return (int)((float)(100 * this.getProgress()) / (float)this.getMax());
    }

    public void defer() {
        if (!this.mDeferred) {
            this.mDeferred = true;
            this.mStartProgress = this.getProgress();
        }

    }

    public void endDefer() {
        if (this.mDeferred) {
            this.mDeferred = false;
            this.bounceAnimation(this.mStartProgress);
        }

    }

    public synchronized void setProgress(int progress) {
        progress = (int)this.clamp((float)progress, 0.0F, (float)this.getMax());
        if (progress != this.mProgress) {
//            if (!this.mDeferred) {
//                this.bounceAnimation(progress);
//            }

            this.dynamicRequestLayout();
            this.mProgress = progress;
            this.postInvalidate();
        }
    }

    public void animateProgress(int progress) {
        final int startProgress = this.getProgress();
        int endProgress = (int)this.clamp((float)progress, 0.0F, (float)this.getMax());
        int diff = Math.abs(this.getProgress() - endProgress);
        long duration = Math.max(500L, (long)(2000.0F * ((float)diff / (float)this.getMax())));
        ValueAnimator animator = ValueAnimator.ofInt(new int[]{this.getProgress(), endProgress});
        animator.setDuration(duration);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.addUpdateListener(animation -> RopeProgressBar.this.setProgress((Integer)animation.getAnimatedValue()));
        animator.start();
    }

    private void bounceAnimation(int startProgress) {
        int diff = Math.abs(startProgress - this.mProgress);
        float diffPercent = Math.min(1.0F, (float)(4 * diff) / (float)this.getMax());
        if (this.mAnimator != null) {
            this.mAnimator.cancel();
        }

        this.mAnimator = ValueAnimator.ofFloat(new float[]{0.0F, diffPercent * this.getTriangleWidth()});
        this.mAnimator.setInterpolator(INTERPOLATOR);
        this.mAnimator.setDuration(1000L);
        this.mAnimator.addUpdateListener(animation -> {
            RopeProgressBar.this.mBounceX = (Float)animation.getAnimatedValue();
            RopeProgressBar.this.invalidate();
        });
        this.mAnimator.start();
    }

    public int getProgress() {
        return this.mProgress;
    }

    public void setMax(int max) {
        max = Math.max(0, max);
        if (max != this.mMax) {
            this.dynamicRequestLayout();
            this.mMax = max;
            if (this.mProgress > max) {
                this.mProgress = max;
            }

            this.postInvalidate();
        }

    }

    public int getMax() {
        return this.mMax;
    }

    public void setDynamicLayout(boolean isDynamic) {
        if (this.mDynamicLayout != isDynamic) {
            this.mDynamicLayout = isDynamic;
            this.requestLayout();
            this.invalidate();
        }

    }

    public void setPrimaryColor(int color) {
        this.mPrimaryColor = color;
        this.invalidate();
    }

    public int getPrimaryColor() {
        return this.mPrimaryColor;
    }

    public void setSecondaryColor(int color) {
        this.mSecondaryColor = color;
        this.invalidate();
    }

    public int getSecondaryColor() {
        return this.mSecondaryColor;
    }

    public void setSlack(float slack) {
        this.mSlack = slack;
        this.requestLayout();
        this.invalidate();
    }

    public float getSlack() {
        return this.mSlack;
    }

    public void setStrokeWidth(float width) {
        this.mLinesPaint.setStrokeWidth(width);
        this.requestLayout();
        this.invalidate();
    }

    public float getStrokeWidth() {
        return this.mLinesPaint.getStrokeWidth();
    }

    public void setTypeface(Typeface typeface) {
        this.mTextPaint.setTypeface(typeface);
        this.requestLayout();
        this.invalidate();
    }

    public void setTextPaint(Paint paint) {
        this.mTextPaint.set(paint);
        this.requestLayout();
        this.invalidate();
    }

    public Paint getTextPaint() {
        return new Paint(this.mTextPaint);
    }

    public void setProgressFormatter(ProgressFormatter formatter) {
        this.mFormatter = formatter;
        this.requestLayout();
        this.invalidate();
    }

    private float clamp(float value, float min, float max) {
        return Math.max(min, Math.min(max, value));
    }

    private float perp(float t) {
        return (float)(-Math.pow((double)(2.0F * t - 1.0F), 2.0D) + 1.0D);
    }

    private float lerp(float v0, float v1, float t) {
        return t == 1.0F ? v1 : v0 + t * (v1 - v0);
    }

    private float dips(float dips) {
        return dips * this.m1Dip;
    }

    private float sp(int sp) {
        return (float)sp * this.m1Sp;
    }
}
