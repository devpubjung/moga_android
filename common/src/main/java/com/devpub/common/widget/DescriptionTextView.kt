package com.devpub.common.widget

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import androidx.core.content.ContextCompat
import com.devpub.common.R
import com.devpub.common.binidng.BindingComponent
import com.devpub.common.databinding.WidgetDescriptionBinding

class DescriptionTextView @JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null) :
    BindingComponent<WidgetDescriptionBinding>(context, attrs, R.layout.widget_description) {

    init {
        attrs?.let {
            context.obtainStyledAttributes(it, R.styleable.DescriptionTextView).apply {
                getString(R.styleable.DescriptionTextView_android_text)?.apply {
                    binding.contentTextView.text = this
                }

                getDimensionPixelSize(R.styleable.DescriptionTextView_android_textSize, 0).apply {
                    if (this > 0) {
                        binding.contentTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.toFloat())
                    }
                }

                getColor(
                    R.styleable.DescriptionTextView_android_textColor,
                    ContextCompat.getColor(context, R.color.textGray)
                ).apply {
                    binding.dot.setTextColor(this)
                    binding.contentTextView.setTextColor(this)
                }
            }.recycle()
        }
    }

    fun setText(text: CharSequence?) {
        binding.contentTextView.text = text
    }

}