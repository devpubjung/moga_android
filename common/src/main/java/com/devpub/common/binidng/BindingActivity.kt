package com.devpub.common.binidng

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import com.devpub.common.util.extension.getBinding

abstract class BindingActivity<VB : ViewDataBinding> : AppCompatActivity() {

    protected lateinit var binding: VB
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        beforeBindView()
        if (::binding.isInitialized.not()) {
            binding = getBinding()
            setContentView(binding.root)
        }
        binding.lifecycleOwner = this
    }

    protected open fun beforeBindView() {
        // Nothing
    }

    protected inline fun bind(action: VB.() -> Unit) {
        binding.run(action)
    }

    protected fun <T> LiveData<T>.observe(block : (T) -> Unit) {
        observe(this@BindingActivity) {
            block(it)
        }
    }
}