package com.devpub.common.list

import androidx.recyclerview.widget.DiffUtil
import com.devpub.common.model.ListItem

class DiffCallback<T : ListItem> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
        oldItem::class.java == newItem::class.java && oldItem.getKey() == newItem.getKey()
}