package com.devpub.common.list

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.devpub.common.list.viewholder.FooterStateViewHolder
import com.devpub.common.util.extension.toBinding

class FooterStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<FooterStateViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): FooterStateViewHolder {
        return FooterStateViewHolder(parent.toBinding(), retry)
    }

    override fun onBindViewHolder(holder: FooterStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }
}