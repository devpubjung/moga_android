package com.devpub.common.list.viewholder

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.devpub.common.list.RecyclerHandler
import com.devpub.common.model.ListItem

abstract class BindingViewHolder<VB : ViewDataBinding>(
    private val binding: VB,
    private val handler: RecyclerHandler? = null
) : RecyclerView.ViewHolder(binding.root) {

    protected var item: ListItem? = null

    open fun bind(item: ListItem) {
        this.item = item
        binding.setVariable(BR.item, this.item)
        binding.setVariable(BR.handler, handler)
    }
}

