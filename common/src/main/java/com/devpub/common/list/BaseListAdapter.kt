package com.devpub.common.list

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.devpub.common.list.viewholder.BindingViewHolder
import com.devpub.common.model.ListItem

abstract class BaseListAdapter(
    private val handler: RecyclerHandler?
) : ListAdapter<ListItem, BindingViewHolder<*>>(DiffCallback()) {

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return getViewType(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<*> {
        return getCreateViewHolder(parent, viewType, handler)
    }

    override fun onBindViewHolder(holder: BindingViewHolder<*>, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holder.bind(item)
        }
    }

    abstract fun getViewType(item: ListItem) : Int

    abstract fun getCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
        handler: RecyclerHandler?
    ): BindingViewHolder<*>

}