package com.devpub.common.list.viewholder

import com.devpub.common.databinding.ItemEmptyBinding

class ItemEmptyViewHolder(binding: ItemEmptyBinding) : BindingViewHolder<ItemEmptyBinding>(binding)