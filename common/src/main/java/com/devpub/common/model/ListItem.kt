package com.devpub.common.model

import java.io.Serializable

interface ListItem: Serializable {

    fun getKey() = hashCode()
}
