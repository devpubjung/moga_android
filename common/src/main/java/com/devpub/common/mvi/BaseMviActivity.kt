package com.devpub.common.mvi

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.devpub.common.R
import com.devpub.common.util.StatusBarUtil
import com.devpub.common.widget.loading.CatLoadingView
import com.devpub.common.util.TAG
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

abstract class BaseMviActivity<Intent, State, ViewModel : BaseMviViewModel<Intent, State>>
    (private val viewModelClass: KClass<ViewModel>) : AppCompatActivity(), MviView {

    private val loadingView = CatLoadingView()
    protected lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        StatusBarUtil.setStatusBarColorForLollipop(window, Color.WHITE)
        super.onCreate(savedInstanceState)
        loadingView.setBackgroundColor(ContextCompat.getColor(this, R.color.primary))
        viewModel = ViewModelProvider(this,
            object : ViewModelProvider.Factory {
                override fun <T : androidx.lifecycle.ViewModel> create(modelClass: Class<T>): T {
                    return initViewModel() as T
                }
            })[viewModelClass.java]
        initContentViews()
        setUpViews()
        observeViewModel()
    }

    override fun showLoading(cancelable: Boolean) {
        try {
            loadingView.setClickCancelable(cancelable)
            loadingView.show(supportFragmentManager, TAG)
        } catch (e: Exception) {
        }
    }

    override fun hideLoading() {
        try {
            loadingView.dismiss()
        } catch (e: Exception) {
        }
    }

    override fun onPause() {
        super.onPause()
        hideLoading()
    }

    protected fun sendIntent(intent: Intent) {
        lifecycleScope.launchWhenCreated {
            viewModel.intent.send(intent)
        }
    }

    protected fun sendIntentBlocking(intent: Intent) {
        viewModel.intent.trySendBlocking(intent)
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.state.collect {
                handleState(it)
            }
        }
    }

    abstract fun initViewModel(): ViewModel
    abstract fun initContentViews()
    abstract fun setUpViews()
    abstract fun handleState(state: State)
}
