package com.devpub.common.mvi

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.devpub.common.util.StatusBarUtil
import kotlin.reflect.KClass

abstract class BaseMviFragment<Intent, State, ViewModel : BaseMviViewModel<Intent, State>>
    (private val viewModelClass: KClass<ViewModel>) : Fragment(), MviView {

    protected lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        StatusBarUtil.setStatusBarColorForLollipop(requireActivity().window,Color.WHITE)
        super.onCreate(savedInstanceState)
        viewModel = initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        observeViewModel()
    }

    fun setStatusBarColor(colorRes: Int) {
        activity?.run {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(requireContext(), colorRes)
        }
    }

    override fun showLoading(cancelable: Boolean) {
        if (activity is MviView) {
            (activity as MviView).showLoading(cancelable)
        }
    }

    override fun hideLoading() {
        if (activity is MviView) {
            (activity as MviView).hideLoading()
        }
    }

    override fun onPause() {
        super.onPause()
        hideLoading()
    }

    protected fun sendIntent(intent: Intent) {
        lifecycleScope.launchWhenCreated {
            viewModel.intent.send(intent)
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.state.collect {
                handleState(it)
            }
        }
    }

    abstract fun initViewModel(): ViewModel
    abstract fun setUpViews()
    abstract fun handleState(state: State)
}
