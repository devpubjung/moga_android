package com.devpub.common.mvi

abstract class BaseState {
    object Idle
    object Loading
    data class Error(val throwable: Throwable)
}

