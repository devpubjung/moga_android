package com.devpub.common.mvi

interface MviView {
    fun showLoading(cancelable: Boolean = false)

    fun hideLoading()
}