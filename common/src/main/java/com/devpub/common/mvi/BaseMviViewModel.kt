package com.devpub.common.mvi

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow

abstract class BaseMviViewModel<Intent, State> : ViewModel(), MviViewModel {

    val intent = Channel<Intent>(Channel.UNLIMITED)
    val state: StateFlow<State> get() = _state

    protected val _state = MutableStateFlow(initState())

    private val asyncJob = SupervisorJob()
    protected val asyncScope = CoroutineScope(asyncJob + Dispatchers.IO)
    protected val asyncErrorHandler = CoroutineExceptionHandler { context, exception ->
        Log.e("okhttp_AsyncScope", exception.toString())
        context.cancel()
    }

    init {
        handleIntent()
    }

    private fun handleIntent() {
        viewModelScope.launch {
            intent.consumeAsFlow().collect {
                handleIntent(it)
            }
        }
    }

    abstract fun initState(): State
    abstract suspend fun handleIntent(intent: Intent)

}
