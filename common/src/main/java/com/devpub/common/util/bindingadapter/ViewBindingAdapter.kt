package com.devpub.common.util.bindingadapter

import android.annotation.SuppressLint
import android.text.Html
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.devpub.common.R
import com.devpub.common.list.CustomDecoration
import com.devpub.common.util.ViewUtil.dpToPx
import com.devpub.common.util.ViewUtil.dpToPxFloat
import com.devpub.common.widget.HtmlTextView


@BindingAdapter("android:layout_marginTop")
fun setLayoutMarginTop(view: View, margin: Boolean) {
    if (view.layoutParams is ViewGroup.MarginLayoutParams) {
        (view.layoutParams as ViewGroup.MarginLayoutParams).topMargin =
            if (margin) view.dpToPx(20F) else 0
    }
}

@BindingAdapter("visible")
fun setVisible(view: View, visible: Boolean) {
    view.isVisible = visible
}


@BindingAdapter("checked")
fun bindChecked(view: View, checked: Boolean) {
    view.isSelected = checked
}

@SuppressLint("NewApi")
@BindingAdapter("html")
fun setItemContent(htmlTextView: HtmlTextView, text: String) {
    htmlTextView.text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
}

@BindingAdapter(
    value = ["divider", "divider_height", "divider_padding", "divider_color"],
    requireAll = false
)
fun RecyclerView.setDivider(
    divider: Boolean?,
    dividerHeight: Float?,
    dividerPadding: Float?,
    @ColorInt dividerColor: Int?
) {
    if (divider != false) {
        addItemDecoration(
            CustomDecoration(
                height = dividerHeight ?: dpToPxFloat(1f),
                padding = dividerPadding ?: 0f,
                color = dividerColor ?: ContextCompat.getColor(context, R.color.divider)
            )
        )
    }
}


