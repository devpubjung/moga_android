package com.devpub.common.util

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi

object StatusBarUtil {
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun setStatusBarColorForLollipop(window: Window, @ColorInt colorValue: Int) {
        addSystemBarFlag(window)
        val newColor = getStatusBarColor(colorValue)
        clearLightStatusBar(window)
        window.statusBarColor = newColor
    }

    private fun addSystemBarFlag(window: Window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun getStatusBarColor(@ColorInt statusBarColor: Int): Int {
        val newColor: Int
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            newColor = statusBarColor
        } else {
            val hsv = FloatArray(3)
            Color.colorToHSV(statusBarColor, hsv)
            hsv[2] *= 0.8f
            newColor = Color.HSVToColor(hsv)
        }
        return newColor
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun clearLightStatusBar(window: Window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decorView = window.decorView
            var flags = decorView.systemUiVisibility
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            decorView.systemUiVisibility = flags
        }
    }
}